# AC to DC 12V 3A Wall Adapter Power Supply For Arduino-Meduino

<img src="images/ac-to-dc-12v-3a-wall-adapter-power-supply-for-arduino_微信截图_20160126174621-300x270.png" alt="微信截图_20160126174621" width="300" style="padding:5px 5px 12px 0px;">

### Overview

AC to DC 12V 3A Wall Adapter Power Supply For Arduino/Meduino is a power supply device of small portable electronic devices and electrical appliances. Its input is the city power, and its output is 12V DC. Its self  contained 5.5×2.1 mm  DC plug can be plugged into the Orion Makeblock development board to provide power for motor driver.

This adapter can provide large current to meet large scale
construction and application with a large number of motors.

### Technical specifications

- Input voltage: 100-240V AC
- Frequency of input voltage: 50/60 HZ
- Output voltage: 12V DC
- Output current: 3A
- Peak current: 10A
- Output power: 36W
- Maximum ripple noise: 120 mVp-p
- Operating temperature: 0\~40℃
- Storage temperature: -20\~60℃
- Length of output line: 1.2 m
- Size of DC plug: 5.5 x 2.1 x 10 mm (OD x ID x L)
- Dimension: 84 x 47 x 37 mm (L x W x H)

### Technical specifications

- Full range AC input voltage
- Good anti-interference performance and high reliability
- Small DC ripple and high efficiency
- Compact and efficient
- Good insulation performance and high dielectric strength
- Short circuit protection and over-current protection

 
