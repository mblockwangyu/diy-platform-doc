<img src="project_images/megapi-pro-esc-driver_电调转接板.jpg" alt="MegaPi Pro ESC Driver" width="1080" />

###  Overview

This module is used to connect the electronic
speed controller (ESC) and can be easily installed on the MegaPi
Pro.

###  Features

-   Supports the operating voltage of 9\~12
    V
-   12 V power supply, operating current up to 3
    A (peak current up to 5 A)
-   Provides colorful male and female sockets to
    prevent mis-insertion
-   The module is small in size and easy to be
    replaced

### Technical Specifications

-   Minimum operating voltage: 9 V
-   Maximum operating voltage: 12 V
-   Typical voltage: 11.1 V
-   Rated current: 3 A
-   Peak current: 5 A
-   Module size: 30 mm\*15 mm
    (L\*W)

### Programming Guidance

MegaPi Pro Shield for RJ25:

This program is to make the brushless motor
rotate at a uniform speed.

mBlock programming：

<img src="project_images/megapi-pro-esc-driver_电调转接板.jpg" width="1504" />

**Arduino programming：**

<img src="project_images/megapi-pro-esc-driver_电调转接板-arduino.jpg" width="764" />

### Relevant module information

MegaPi Pro：<https://makeblock.com/project/megapi-pro>

[← MegaPi Pro](https://makeblock.com/project/megapi-pro) [MegaPi Pro
Encoder/DC Motor Driver
→](https://makeblock.com/project/megapi-pro-encoder-dc-motor-driver)

###  Overview

This module is used to connect the electronic
speed controller (ESC) and can be easily installed on the MegaPi
Pro.

###  Features

-   Supports the operating voltage of 9\~12
    V
-   12 V power supply, operating current up to 3
    A (peak current up to 5 A)
-   Provides colorful male and female sockets to
    prevent mis-insertion
-   The module is small in size and easy to be
    replaced

### Technical Specifications

-   Minimum operating voltage: 9 V
-   Maximum operating voltage: 12 V
-   Typical voltage: 11.1 V
-   Rated current: 3 A
-   Peak current: 5 A
-   Module size: 30 mm\*15 mm
    (L\*W)

### Programming Guidance

MegaPi Pro Shield for RJ25:

This program is to make the brushless motor
rotate at a uniform speed.

mBlock programming：

<img src="project_images/megapi-pro-esc-driver_电调转接板.jpg" width="1504" />

**Arduino programming：**

<img src="project_images/megapi-pro-esc-driver_电调转接板-arduino.jpg" width="764" />

### Relevant module information

MegaPi Pro：<https://makeblock.com/project/megapi-pro>
