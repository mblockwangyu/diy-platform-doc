# MegaPi Shield for RJ25

<img src="images/megapi-shield-for-rj25.png" width="200">

### Overview

This module is designed for MegaPi main control boards. It can convert pins of MegaPi main control boards to Makeblock RJ25 ports for connecting Makeblock electronic modules.

### Technical specifications

* Operation voltage: 5V
* Dimensions: 25mm x 63mm (Width x Height)

### Features

* Four ports
* One power indicator
* 6P6C RJ25 ports, facilitating the wiring
* Arduino library, facilitating programming

### Connection

<img src="images/megapi-shield-for-rj25-1.png" width="300">