# MegaPi Pro Shield for RJ25

<img src="images/megapi-pro-shield-for-rj25-1.png" width="200">

### Overview

This module is designed for MegaPi Pro to convert the pins of MegaPi Pro into Makeblock RJ25 ports that can be used to connect Makeblock electronic modules.

### Technical specifications

* Operating voltage: 5 V
* Dimensions: 66 mm x 42 mm (width × height)

### Features

* Eight ports
* One power indicator
* 6P6C RJ25 ports, facilitating the wiring
* Arduino library, facilitating programming

**Note:** Ports 6, 11, and 12 are not compatible with the IR receiver (Me Infrared Receiver Decode).


### Connection

<img src="images/megapi-pro-shield-for-rj25-2.png" width="400">