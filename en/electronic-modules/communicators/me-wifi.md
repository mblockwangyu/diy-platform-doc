# Me WiFi Module

<img src="images/me-wifi_微信截图_20160129114325.png" alt="微信截图_20160129114325" width="223" style="padding:5px 5px 12px 0px;">

### Overview

The main component of Me WiFi Module is ESP8266 module. As a low power consumption UART-WiFi pass-through module using serial communication, ESP8266 can be set to connect with WiFi to construct a WiFi remote control cart, a remote control lamp, etc. Its blue/gray ID means that RJ25 cable should be used to connect it to the port with blue or gray ID
on Makeblock Orion.

### Technical specifications

- Operating voltage: 3.3V DC  
- Input voltage: 5V DC  
- Wireless supported: 802.11 b/g/n standard  
- Frequency range: 2.412 GHz\~2.484 GHz  
- Operating current: 50 mA  
- Peak current: 200 mA℃  
- Chip type: ESP8266  
- Default baud rate: 9600  
- Module size: 51 x 24 x 18 mm (L x W x H)

### Functional characteristics

- Operating mode: STA (workstation) + AP (access point)  
- Built-in TCP/IP protocol stack  
- Support the WPA WPA2/WPA2–PSK encryption  
- Support switch-over of Work and PROG modes, and support WiFi programming  
- White area of module is the reference area to contact metal beams  
- Anti-reverse protection – connecting the power supply inversely will not damage IC  
- Adopt RJ25 port for easy connection  
- Provide pins to support most Arduino Baseboards

### Pin definition

The port of Me WiFi Module has four pins, and their functions are as follows:

<img src="images/me-wifi_微信截图_20160129114521.png" alt="微信截图_20160129114521" width="592" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25**

Since the port of Me WiFi Module has blue/gray ID, you need to connect the port with blue or gray ID on Makeblock Orion when using RJ25 port.

Taking Makeblock Orion as example, you can connect to ports No. 3, 4, 5, and 6 as follows:

<img src="images/me-wifi_微信截图_20160129114619.png" alt="微信截图_20160129114619" width="270" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire** 

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its TX and RX pins should be connected to RX and TX pins on the UNO board respectively as follows:

<img src="images/me-wifi_微信截图_20160129114705.png" alt="微信截图_20160129114705" width="299" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming**  

If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control the Me WiFi Module. This program serves to receive data by Me WiFi Module through Arduino programming.

<img src="images/me-wifi_微信截图_20160129114840.png" alt="微信截图_20160129114840" width="425" style="padding:5px 5px 12px 0px;">

<img src="images/me-wifi_微信截图_20160129114904.png" alt="微信截图_20160129114904" width="598" style="padding:5px 5px 12px 0px;">

### Principle analysis

This module supports three operating modes: STA/AP/STA+AP.  
- STA mode: The module is connected to the Internet through a router.
The mobile phone or computer implements remote control of devices by
Internet.  
- AP mode: The module is taken as the access point to implement direct
communication between the mobile phone or computer and the module, that
is, the LAN wireless control.  
- STA+AP mode: the coexistence of two modes, that is, you can perform seamless switching via the Internet control conveniently.

When the power is supplied to the module, its WiFi signal in
configuration mode is “ESP (+ chip ID)” without password. Input the address of WiFi extension board: 192.168.4.1 in the browser to open the configuration page, and then you can configure this module. When the module is connected, its red power indicator lights on. After about one
second its blue Link indicator is flashing, which means it is started normally but not connected. When the device is connected successfully and performs the first data transmission, the indicator lights on. When the module receives data, the blue receiving indicator is flashing. The
toggle switch is used to select a working mode from Work and PROG. Work is the normal working state (usually in this state), and PROG is a programming mode. It should be restarted when mode is switched.

Note: Since the usage of this module is complex, refer to the website for details.

### Schematic

<img src="images/me-wifi_Me-wifi.png" alt="Me wifi" width="1082" style="padding:5px 5px 12px 0px;">
