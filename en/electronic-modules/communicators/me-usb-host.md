# Me USB Host

<img src="images/me-usb-host_微信截图_20160129113429.png" alt="微信截图_20160129113429" width="329" style="padding:5px 5px 12px 0px;">

### Description

Me USB Host is an adapter for USB devices. You can use it to connect a Makeblock mainboard to a USB device, such as a USB joystick, a mouse, or a thumb drive. So you can control your robot with your own joystick or something else.

Now, we can add new controller method for mBot using wireless joystick: mBot controlled by Wireless Joystick using Me USB Host.

### Features

- Main chip: CH375B
- Reserved pads for advanced DIY
- Hot plug NOT supported
- 16mm interval M4 mounting holes, compatible with Makeblock beams
- 2.54mm pin holes for connecting with Dupont wire
- Easy wiring with 6P6C RJ25 interface
- Arduino library for easy programming

### Specification

- Supported device: HID device whose descriptor is shorter than 64
  bytes
- Rated Voltage: 5V
- Size: 24 x 48 x 16 mm (length x width x height)

### Schematic

<img src="images/me-usb-host_Me-USB-Host.png" alt="Me USB Host" width="1284" style="padding:5px 5px 12px 0px;">
