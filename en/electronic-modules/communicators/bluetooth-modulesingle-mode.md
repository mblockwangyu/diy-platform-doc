# Bluetooth Module(Single Mode)

<img src="images/bluetooth-modulesingle-mode_Bluetooth_single2.png" alt="微信截图_20160203161005" width="301" />

### Description

The Bluetooth module is mainly used for short-range wireless data
transmission. It can easily connect to Bluetooth devices including
computers and smart phones. Besides, the module allows wireless data
transmission between two modules. So, users will no longer be hindered
by messy wires and spatial constraints. Using the Bluetooth module, you
can wirelessly control Makeblock robots (mBot, Ranger, Ultimate and
more) with your smart phone or computer (only from mBot and Ranger and
Bluetooth 4.0 required).

### Features

-   Makes it easier for users to learn graphical programming languages
    with software like Makeblock and mBlock;
-   Supports Bluetooth 4.0 and has low power consumption, helping to
    extend the battery life;
-   Great performances of the wireless transceiver system: Stable, High
    speed, Max range of 20-30 meters(free field).

### Specifications

-   Version: BT4.0;
-   Dimension: 31 x 20.5 x 3.5mm (Length x Width x Height)
