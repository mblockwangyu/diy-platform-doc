# Adapters

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td><a href="adapters/me-rj25-adapter.html" target="_blank"><img src="adapters/images/me-rj25-adapter_Me-RJ25-Adapter.jpg" width="150px;"></a><br>
<p>Me RJ25 Adapter</p></td>

<td><a href="adapters/me-shield-for-raspberry-pi.html" target="_blank"><img src="adapters/images/me-shield-for-raspberry-pi_Me-Shield-for-Raspberry-Pi.jpg" width="150px;"></a><br>
<p>Me Shield for Raspberry Pi</p></td>

<td><a href="adapters/me-uno-shield.html" target="_blank"><img src="adapters/images/me-uno-shield_Me-UNO-Shield.jpg" width="150px;"> </a><br>
<p>Me UNO Shield</p></td>
</tr>
<tr>
<td><a href="adapters/megapi-pro-shield-for-rj25.html" target="_blank"><img src="../../zh/electronic-modules/adapters/images/megapi-pro-shield-for-rj25.png" width="150px;"> </a><br>
<p>MegaPi Pro Shield for RJ25</p></td>
<td><a href="adapters/megapi-shield-for-rj25.html" target="_blank"><img src="../../en/electronic-modules/adapters/images/megapi-shield-for-rj25-link.png" width="150px;"> </a><br>
<p>MegaPi Shield for RJ25</p></td>
</table>
