# DC Frame Type Solenoid HCNE1-0530

<img src="images/dc-frame-type-solenoid-hcne1-0530_微信截图_20160128164127.png" alt="微信截图_20160128164127" width="215" style="padding:5px 5px 12px 0px;">

### Description

Open Frame Solenoids are a basic, opened-coil type of
linear motion solenoids. Due to their simple design structure, they are less costly, and are offered in a wide variety of sizes and types to best meet your application requirements.

### Technical specifications

1.  Voltage:12VDC
2.  Resistance:6.6 ohms
3.  Stroke:6mm
4.  Force:50g
5.  ED:25%

### Application range

1. Game: Tiger eating angles,  Coin chooser  
2. Saler machine, coil changer, ticket saler  
3. Office equipment: listing machine computer, fax duty printer, Photo printer, type writer, caser drawing machine drinking  
4. Transport devices: auto door lock, safe belt lock, car electromagnetic valve  
5. Domestic appliance: tadio electronic organ, auto weave machine choir  
6. Other package machine mechanical hand farming device pressure equipment fir prevention,burglary alarm save water valve

### Characteristics

- Easy fix and continuous loading
- Stable temperature rising can length usage lift and sure good
character  
- E-ring and rubber pad make valve no loice  
- No rub length usage lift  
- Easy and stable structure  
- Normal type 

\***Operation condition** 

- Operation temperature-5 to 40,the valve will not solidify
- Operation humidity not solidify within the relative humidity of 45% to 85%  
- Store temperature not solidify at the temperature of -40℃ to 75 ℃  
- Storage humidity not solidify during the relative humidity of 0% to 95%  

**\*Character note**
  
- Temperature rising under 65(class a) lift: more than 1 million under loading  

**\*Test temperature**  
- Environment temperature 23±2 ℃  
- Relative humidity 50±10%  
- Air pressure 1013MPa  
- Insulation resistance need 100MΩ insulating resistance when 500 VDC insulator testing  
- Insulation strength the strength should be 600 VAC/1 min
