# Me 4 Button

<img src="images/me-4-button_微信截图_20160128173416.png" alt="微信截图_20160128173416" width="234" style="padding:5px 5px 12px 0px;">

### Overview

The Me 4-Button module contains 4 superior instantaneous push buttons featuring comfortable feel and long life. With simple structure and good feedback, it can be used in the control moving direction of the cart and interactive video games. Its black ID means that it has an analog port and should be connected to the port with black ID on Makeblock Orion.

### Technical specifications

- Operating voltage: 5V DC  
- Number of buttons: 4  
- Control mode: one-way analog port  
- Module size: 51 x 24 x 18 mm (L x W x H)

### Functional characteristics

- White area of module is the reference area to contact metal beams  
- Me 4-Button comprises a state indicator light and a power indicator light  
- Anti-reverse protection – connecting the power supply inversely will not damage IC  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Provide pin-type of port to support most development boards including Arduino series

### Pin definition

The port of Me 4-Button has three pins, and their functions are as follows:

<img src="images/me-4-button_微信截图_20160128173826.png" alt="微信截图_20160128173826" width="791" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25**  

Since the port of Me 4-Button has black ID, you need to connect the port with black ID on Makeblock Orion when using RJ25 port. 

Taking Makeblock Orion as example, you can connect it to ports No. 6, 7, and 8 as follows:

<img src="images/me-4-button_微信截图_20160129101531.png" alt="微信截图_20160129101531" width="275" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire**  

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its OUT pin should be connected to analog pin as follows:

<img src="images/me-4-button_微信截图_20160129101626.png" alt="微信截图_20160129101626" width="267" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming** 

If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control the Me 4-Button.

This program serves to read the key value of pressed button and output it to the serial port through Arduino programming.

<img src="images/me-4-button_微信截图_20160129101733.png" alt="微信截图_20160129101733" width="492" style="padding:5px 5px 12px 0px;">

<img src="images/me-4-button_微信截图_20160129101817.png" alt="微信截图_20160129101817" width="581" style="padding:5px 5px 12px 0px;">

<img src="images/me-4-button_微信截图_20160129101851.png" alt="微信截图_20160129101851" width="520" style="padding:5px 5px 12px 0px;">

### Principle analysis

The Me 4-Button module contains four-leg buttons which are protected by metal spring strip. In the switch with four-leg buttons, when a button is pressed, the circuit is switched on; when it is released, the circuit is switched off. The applied force is that we use our hands to press or
release the button. The four buttons in the switch share an
analog output port. When different button is pressed, the output analog value is different consequently. Thus, it can be identified which button is pressed down.

### Schematic

<img src="images/me-4-button_Me-4-Button-V1.0_schematic.png" alt="Me-4 Button V1.0_schematic" width="2259" style="padding:5px 5px 12px 0px;">
