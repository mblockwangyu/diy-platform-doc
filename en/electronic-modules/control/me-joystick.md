# Me Joystick

<img src="images/me-joystick_微信截图_20160128172254.png" alt="微信截图_20160128172254" width="252" style="padding:5px 5px 12px 0px;">

### Overview

Including a cross joystick, Me Joystick Module is used to control the moving direction of cart and the interactive video game. Its black ID means that it has an analog port and should be connected to the port with black ID on Makeblock Orion.

### Technical specifications

- Operating voltage: 5V DC  
- Signal node: 2-shaft analog output  
- Cross joystick: comprising two potentiometers and a gimbal 
- Module size: 51 x 24 x 32 mm (L x W x H)

### Functional characteristics

- White area of module is the reference area to contact metal beams  
- The gimbal separates displacement of joystick into horizontal (X) and vertical(Y) components  
- Collect analog signal of potentiometer voltage to identify the position of joystick  
- Anti-reverse protection – connecting the power supply inversely will not damage IC  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Provide pin type ports to support most Arduino Baseboards

### Pin definition

The port of Me Joystick Module has four pins, and their functions are as follows:

<img src="images/me-joystick_微信截图_20160128172418.png" alt="微信截图_20160128172418" width="720" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25** 

Since the port of Me Joystick Module has black ID, you need to connect the port with black ID on Makeblock Orion when using RJ25 port. 

Taking Makeblock Orion as example, you can connect to ports No. 6, 7, and 8 as follows:

<img src="images/me-joystick_微信截图_20160128172516.png" alt="微信截图_20160128172516" width="331" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire**  

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its X and Y pins should be connected to analog pin as follows:

<img src="images/me-joystick_微信截图_20160128172549.png" alt="微信截图_20160128172549" width="336" style="padding:5px 5px 12px 0px;">

### Guide to programming 

● **Arduino programming** 

If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control the Me Joystick Module. 

This program serves to read the X- and Y-axis position and
output to the serial port monitor in the cycle of 10 ms through Arduino programming.

<img src="images/me-joystick_微信截图_20160128172722.png" alt="微信截图_20160128172722" width="512" style="padding:5px 5px 12px 0px;">

**Function List of Me Joystick Module**

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;width:50%;">Function name</th>
<th style="border: 1px solid black;width:50%">Function</th>
</tr>

<tr>
<td style="border: 1px solid black;">MeJoystick(uint8_t port)</td>
<td style="border: 1px solid black;">select a port</td>
</tr>

<tr>
<td style="border: 1px solid black;">int readX()</td>
<td style="border: 1px solid black;">Read the X-axis analog output(-490~490)</td>
</tr>

<tr>
<td style="border: 1px solid black;">int readY()</td>
<td style="border: 1px solid black;">Read the Y-axis analog output(-490~490)</td>
</tr>
</table>

<br>