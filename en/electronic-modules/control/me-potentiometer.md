# Me Potentiometer

<img src="images/me-potentiometer_微信截图_20160128165613.png" alt="微信截图_20160128165613" width="223" />

### Overview

Featuring an adjustable range with the highest resistance of 10 KΩ, the
Me Potentiometer module is a resistor with three terminals, and its
resistance can be adjusted by rotating the knob. The module can be used
to adjust rotational speed of motor, and brightness of LED lamp. Its
black ID means that it has an analog signal port and needs to
be connected to the port with black ID on Makeblock Orion.

### Technical specifications

● Operating voltage: 5V DC  
● Maximum current: 30 mA  
● Rated power: 0.1 W  
● Rotation angle: 280°  
● Total resistance: 10 KΩ  
● Signal type: analog signal (0\~980)  
● Module size: 51 x 24 x 22 mm (L x W x H)

### Functional characteristics

● A blue LED is provided on the module, and its brightness change
reflects the change of analog output value  
● White area of module is the reference area to contact metal beams  
● Support Arduino IDE programming, and provide a runtime library to
simplify the programming  
● Support mBlock GUI programming, and applicable to users of all ages  
● Adopt RJ25 port for easy connection  
● Provide pins to support most Arduino Baseboards

### Pin definition

The port of Me Potentiometer has three pins, and their functions are as
follows:

<img src="images/me-potentiometer_微信截图_20160128165901.png" alt="微信截图_20160128165901" width="899" />

### Wiring mode

● Connecting with RJ25  
Since the port of Me Potentiometer has black ID, youneed to connect the
port with black ID on Makeblock Orion when using RJ25 port. Taking
Makeblock Orion  
as example, you can connect to ports No. 6, 7, and 8 as follows:

<img src="images/me-potentiometer_微信截图_20160128170029.png" alt="微信截图_20160128170029" width="339" />

● Connecting with Dupont wire  
When the Dupont wire is used to connect the module to the Arduino UNO
Baseboard, its AOP pin should be connected to analog pin as follows:

<img src="images/me-potentiometer_微信截图_20160128170200.png" alt="微信截图_20160128170200" width="404" />

### Guide to programming

● Arduino programming  
If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control and read
the output value of Me
Potentiometer.

<img src="images/me-potentiometer_微信截图_20160128170249.png" alt="微信截图_20160128170249" width="403" />

<img src="images/me-potentiometer_微信截图_20160128170335.png" alt="微信截图_20160128170335" width="805" />

The function of the code segment is: to read the parameter of Me
Potentiometer and output the result to the serial monitor in Arduino IDE
in the cycle of 100 ms. Upload the code segment to the Makeblock Orion
and click the Arduino serial monitor, and then you will see the running
result as follows:

<img src="images/me-potentiometer_微信截图_20160128170858.png" alt="微信截图_20160128170858" width="460" />

The value range of potentiometer is 0\~980. It decreases when you rotate
the knob counterclockwise and increases when you rotate it clockwise.  
● **mBlock programming**  
Me Potentiometer supports the mBlock programming environment and its
instructions are introduced as
follows:

<img src="images/me-potentiometer_微信截图_20160128170944.png" alt="微信截图_20160128170944" width="790" />

This is an example on how to use mBlock programming to control the Me
Potentiometer module. This program serves to make the panda speaking out
the analog output value of Me Potentiometer and moving corresponding
abscissa. Its range is 0\~980. The running result is as
follows:

<img src="images/me-potentiometer_微信截图_20160128171021.png" alt="微信截图_20160128171021" width="790" />

### Principle analysis

Main component of the Me Potentiometer module is a resistor with three
terminals and its resistance can be adjusted according to a kind of
variation. The resistor usually comprises a resistor and a moving
electric brush. Based on the voltage-dividing principle of series
resistors, when the electric brush moves along the resistor body, a
resistance value with certain relation to the displacement of electric
brush is obtained at the output end, and consequently a
different voltage is output from the analog port. This module can be
used with other module to build some interesting projects, for example,
used with DC motor to build a speed-controllable toy car, or used with
LED to build a dimmable lamp.

### Schematic

<img src="images/me-potentiometer_Petention.png" alt="Petention" width="1143" />
