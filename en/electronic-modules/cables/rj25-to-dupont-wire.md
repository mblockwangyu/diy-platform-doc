# RJ25 to Dupont Wire

<img src="images/rj25-to-dupont-wire_微信截图_20160126173803-300x253.png" alt="微信截图_20160126173803" width="300" style="padding:5px 5px 12px 0px;">

### Overview

RJ25 to Dupont Wire is to connect Makeblock electronic modules to other modules with Doupont interface. With this wire, you can use Makeblock Orion to control a greater range of sensors and electronic modules, and also use development boards from other manufacturers to control electronic modules from Makeblock. RJ25 to Dupont Wire extract all 6pin,
hence users need to confirm the correct pin connection before powering on.

### Technical specifications

- Ports: Standard RJ25 interface/Dupont interface x 6

### Functional characteristics

<img src="images/rj25-to-dupont-wire_微信截图_20160126151842-1-300x97.png" alt="微信截图_20160126151842" width="300" style="padding:5px 5px 12px 0px;">
