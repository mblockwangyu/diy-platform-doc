# Main Control Boards

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%"><a href="main-control-boards/makeblock-orion.html" target="_blank"><img src="main-control-boards/images/makeblock-orion_makeblock_orion.jpg" width="200px;"></a><br>
<p>Makeblock Orion</p></td>

<td width="33%"><a href="main-control-boards/mcore.html" target="_blank"><img src="main-control-boards/images/mcore_mCore.jpg" width="200px;"></a><br>
<p>mCore – Main Control Board for mBot</p></td>

<td width="33%"><a href="main-control-boards/megapi.html" target="_blank"><img src="main-control-boards/images/megapi_MegaPi.jpg" width="200px;"></a><br>
<p>MegaPi</p></td>
</tr>

<tr>
<td><a href="main-control-boards/me-auriga.html" target="_blank"><img src="main-control-boards\images\me-auriga-1.jpg" width="200px;"></a><br>
<p>Me Auriga</p></td>

<td><a href="main-control-boards/megapi-pro.html" target="_blank"><img src="main-control-boards\images\megapi-pro.png" width="200px;"></a><br>
<p>MegaPi Pro</p></td>

<td><a href="main-control-boards/novapi.html" target="_blank"><img src="main-control-boards\images\novapi.png" width="200px;"></a><br>
<p>NovaPi</p></td>

</table>
</table>
