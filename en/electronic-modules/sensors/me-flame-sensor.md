# Me Flame Sensor

<img src="images/me-flame-sensor_微信截图_20160129135225.png" alt="微信截图_20160129135225" width="241" style="padding:5px 5px 12px 0px;">

### Overview

Me Flame Sensor can be used to detect fire source or light source with wavelength in the range of 760 nm to 1100 nm. Its detection angle is up to 60 degrees, and detection accuracy can be adjusted. When the flame is detected, its blue indicator will light on. It can be applied to the
safety monitoring projects such as fire-fighting robot and flame alarm. The port of this module has black ID, and RJ25 cable can used to connect to the port with black ID on Makeblock Orion.

### Technical specifications

- Operating voltage: 5V DC  
- Spectrum band detected: 840-1200nm  
- Detection angle: 60°  
- Feedback time: 15μs  
- Control mode: digital and analog port  
- Module dimension: 51 x 24 x 18mm (L x W x H)

### Functional characteristics

- The detection distance is 1m when the height of flame is 5cm  
- The onboard potentiometer can be adjusted for its sensitivity  
- Anti-reverse protection – connecting the power supply inversely will not damage IC  
- White area of module is the reference area to contact metal beams  
- Adopt RJ25 port for easy connection  
- Provide output ports for digital and analog signal  
- Provide pin-type port to support most development boards including Arduino series

### Pin definition

The port of Me Flame Sensor has three pins, and their functions are as follows:

<img src="images/me-flame-sensor_微信截图_20160129135537.png" alt="微信截图_20160129135537" width="785" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25**  

Since the port of Me Flame Sensor has black ID, you need to connect the port with black ID on Makeblock Orion when using RJ25 port. 

Taking Makeblock Orion as example, you can connect to ports No. 6, 7, and 8 as follows:

<img src="images/me-flame-sensor_微信截图_20160129135616.png" alt="微信截图_20160129135616" width="360" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire**  

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its DO pin should be connected to digital port, and its AO pin should be connected to analog port as follows:

<img src="images/me-flame-sensor_微信截图_20160129135715.png" alt="微信截图_20160129135715" width="316" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming**  

If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control the Me Flame Sensor. 

This is a routine to identify if there is flame by this module
through Arduino programming.

<img src="images/me-flame-sensor_微信截图_20160129135754.png" alt="微信截图_20160129135754" width="456" style="padding:5px 5px 12px 0px;">

<img src="images/me-flame-sensor_微信截图_20160129135828.png" alt="微信截图_20160129135828" width="1096" style="padding:5px 5px 12px 0px;">

Read the detection result from Me Flame Sensor, and output the result to the serial monitor in Arduino IDE. Upload the code segment to Makeblock Orion and click the Arduino serial monitor, and you will see the running result as follows:

<img src="images/me-flame-sensor_微信截图_20160129135900.png" alt="微信截图_20160129135900" width="827" style="padding:5px 5px 12px 0px;">

### Principle analysis

The flame is composed of various combustion products, intermediates, high temperature gases, hydrocarbons, and high temperature solid particles (mainly the inorganic materials). The thermal radiation of the flame has gas emission with discrete spectrum and solid radiation with continuous spectrum. The radiation intensity and wavelength distribution
of flame varies from burning objects. Generally the near-infrared light of flame has great radiation intensity. Me Flame Sensor is made on the basis of this feature.

Me Flame Sensor can detect the infrared light with wavelength from 700 nm to 1200 nm. When the infrared wavelength is near 940 nm, it reaches the highest sensitivity. The probe of Me Flame Sensor converts the intensity of surrounding infrared light into the change of current, and then implements analog-to-digital conversion (ADC) to identify whether
there is fire nearby.

### Schematic

<img src="images/me-flame-sensor_Me-Flame.png" alt="Me Flame" width="1242" style="padding:5px 5px 12px 0px;">
