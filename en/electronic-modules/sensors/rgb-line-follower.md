# Me RGB Line Follower

<img src="images/rgb-line-follower_20183271632.png" alt="微信截图_20160129150749" width="400" style="padding:5px 5px 12px 0px;">

### General

ME RGB line follower is designed for line-following. It has 4 RGB supplement lights and 4 light-sensitive receivers. The module applies to line following for either light-colored tracks on dark backgrounds, or dark tracks on light-colored backgrounds. All kinds of RGB line following modules are applicable, as long as the grey scale of the chromatic aberration between the background and the track is higher than the threshold (the higher the chromatic aberration, the better the line following performance). The module is characterized by fast detection, field learning function through keys and excellent adaptability. A blue-white labeled interface is equipped, indicating that it is a dual-digital, I2C interface that needs to be connected to the mainboard with a blue-white
marking interface.

### Technical Specification

- Operating voltage: 5V DC  
- Operating temperature: 0 ºC\~70 ºC  
- Detection height: it is suggested that the sensor is 5mm~15mm from the track, depending on material of the track and illumination.  
- Signal mode: I2C communication (corresponding to the blue and white labels)  
- Dimensions: 48 x 72 x 26.5 mm (W x H x D)

### Functional Characteristics

- Arduino IDE programmable, with runtime library to simplify programming;  
- Four LEDs for feedback of line-following and learning of new surroundings;  
- RJ25 interface for convenient connection;  
- Suitable for metal parts of Makeblock DIY platform: the white area on the module is the reference area in contact with metal beams;  
- Modularized installation, compatible with LEGO;  
- Equipped with interfaces compatible with most Arduino control boards;  
- Field learning function: identifying and recording background of the field and color of the route;  
- Supporting eeprom storage: data learned will be stored in eeprom, preventing data loss in case of power failure;  
- Colors of RGB supplement lamps switchable: currently 3 colors switchable (read, green and blue), by pressing the switching key for 2 seconds;  
- Sensitivity of line-following is adjustable;  
- Reverse polarity protection, protecting IC in case of reverse polarity;  
- Up to 4 line following sensors available on one main board.

### Principle

Me RGB line follower provides 4 pairs of RGB transmitters and light-sensitive receivers, as shown in the following figure: 

<img src="images/rgb-line-follower_1111.png" width="722" style="padding:5px 5px 12px 0px;">  

When RGB passes through different color backgrounds, the light-sensitive receivers converts the received light information into electrical signals, and outputs specific values from the analog ports after passing through the amplifier. The software then uses the analog values of the four light-sensitive receivers as a fusion algorithm to finally calculate the deviation of the position of the output sensor module from the track. The user can directly use this offset to control the rotation speed of the left and right two motors.

### Definitions of Pins

Me RGB line follower provides a 6-pin interface. The following table describes the function of each pin.

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">S/N</th>
<th style="border: 1px solid black;">Pin</th>
<th style="border: 1px solid black;">Function</th>
</tr>

<tr>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">SDA</td>
<td style="border: 1px solid black;">I2C data interface</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">SCL</td>
<td style="border: 1px solid black;">I2C clock interface</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">Power line</td>
</tr>

<tr>
<td style="border: 1px solid black;">4</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">Ground line</td>
</tr>

<tr>
<td style="border: 1px solid black;">5</td>
<td style="border: 1px solid black;">S2</td>
<td style="border: 1px solid black;">I2C address assignment port</td>
</tr>

<tr>
<td style="border: 1px solid black;">6</td>
<td style="border: 1px solid black;">S1 </td>
<td style="border: 1px solid black;">2C address assignment port</td>
</tr>

</table>

<br>


### Connection

● **RJ25 Connection**
  
Me RGB line follower supports interfaces labeled white or blue. Therefore, you can connect it to an RJ25 interface with the blue or white label. 

Taking Makeblock MegaPi as an example, you can connect to interfaces 5, 6, 7, and 8, as shown in the following figure:

<img src="images/rgb-line-follower_22222-400x198.jpg" alt="微信截图_20160129151012" width="650" style="padding:5px 5px 12px 0px;">

● **DuPont Connection**

When using a DuPont cable to connect RGB line follower to the Arduino Uno mainboard, the A0 pin of the module needs to be connected to the ANALOG port as shown below:  

<img src="images/rgb-line-follower_324333333.jpg" alt="微信截图_20160129151050" width="906" style="padding:5px 5px 12px 0px;">

### Learning Methods

In case of any change to the field, the surroundings or location where the RGB sensor is installed, it is suggested to start learning again.

The data learned will be stored in eeprom, avoiding data loss in case of power failure. The learning process includes 4 steps:

1. Adjusting colors of supplement lamps: select suitable colors of the supplement lamps by long pressing the switching key according to colors
of the field background and the line.  
2. Learning color of the background: install the RGB sensor in position, face the 4 RGB lamps right against the background, and click the key to make the 4 LEDs flash slowly. Learning will finish in 2~3 seconds, upon which the LEDs will stop flashing.  
3. Learning color of the track: install the RGB sensor in position, make the 4 RGB lights right against the track, and double-click the key to make the 4 LEDs flashing slowly. Learning will finish in 2~3 seconds, upon which the LEDs will stop flashing.  
4. Test and confirmation: keep the module in position, and adjust locations of the 4 sensors, to make sure they can detect colors of the background and the line properly. When detecting the background, LED of corresponding sensor will be on; and when detecting the line, the corresponding LED will be off.

### Programming

● **Arduino Programming**

When conducting Arduino programming, `Makeblock-Library-master` shall be called to control the RGB line follower.

Two enhanced 180 encoder motors are used for line-following control. Source files "MeRGBLineFollower" and "MeEnhanceEncoderOnBoard" must be included in the project root directory as follows:

[Program Download Link](http://download.makeblock.com/MeRGBLineFollower180motorAuriga%2820171212%29.rar)  

<img src="images/rgb-line-follower_44444.png" alt="微信截图_20160129151123" width="419" style="padding:5px 5px 12px 0px;">  

"MeRGBLineFollower180motorAuriga.ino" is an example of line following, based on the Auriga control board, with 180-encoded motor as chassis. The following code is used:  

<img src="images/rgb-line-follower_5123123.png" alt="微信截图_20160129151123" width="607" style="padding:5px 5px 12px 0px;">

List of Main Functions and Features of Me RGB Line Follower

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th>Function</th>
<th>Features</th>
</tr>

<tr>
<td style="border: 1px solid black;">void setKp(float value)</td>
<td style="border: 1px solid black;">Setting line following sensitivity, which is used for adjusting line following response speed. The larger the value is,more sensitive it turns.</td>
</tr>

<tr>
<td style="border: 1px solid black;">uint8_t getStudyTypes(void)</td>
<td style="border: 1px solid black;">Obtaining learning status of the RGB sensor: 0-not learning, 1-learning background color, 2-learning track color</td>
</tr>

<tr>
<td style="border: 1px solid black;">int16_t getPositionOffset(void)</td>
<td style="border: 1px solid black;">Obtaining offset of the sensor from the track. The output is taken as the product of the line following sensitivity Kp and the analog value resulted from fusion algorithm of the 4 sensors, ranging from -512 to 512. If it is larger than 0, it means that the line deflects to RGB4. The larger the value, the higher the offset. If it is smaller than 0, it means that the line deflects to RGB1. The greater the negative value, the higher the offset.

<img src="images/rgb-line-follower_12.png"  width="607" style="padding:5px 5px 12px 0px;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">uint8_t getPositionState(void)</td>
<td style="border: 1px solid black;">
<img src="images/rgb-line-follower_13.png"  width="607" style="padding:5px 5px 12px 0px;">
As shown in the figure, bit0~bit3 represent RGB4, RGB3, RGB2 and RGB1 from left to right respectively.<br>
"1" means that the background is detected (corresponding LED is on), while "0" means that the line is detected (corresponding LED is off).<br>
"0000" means that all the 4 sensors detect the line (all the LEDs are off), with 0 returned;<br>
"0111" means that the 4 sensors from left to right detect "line", "background", "background" and "background" respectively (the LEDs are off, on, on and on), with 7 returned;<br>
"0011" means that the 4 sensors from left to right detect "line", "line", "background" and "background" respectively (the LEDs are off, off, on and on), with 3 returned.</td>
</tr>

<tr>
<td style="border: 1px solid black;">uint8_t getADCValueRGB1(void)

uint8_t getADCValueRGB2(void)

uint8_t getADCValueRGB3(void)

uint8_t getADCValueRGB4(void)</td>
<td style="border: 1px solid black;">Obtaining the ADC value of each probe, with a value ranging from 0 to 255 returned.</td>
</tr>

<tr>
<td style="border: 1px solid black;">int8_t setRGBColour(uint8_t colour)</td>
<td style="border: 1px solid black;">Setting the color parameter for the RGB supplement light. The value "1" indicates red, "2" indicates green, and "3" indicates blue.</td>
</tr>

</table>

<br>

● **mBlock programming** 

Me RGB Line Follower is mBlock programmable, but
"RGBLineFollower" or "RGBLineFollowerAdvanced" must be installed. Besides, if you use the 180 encoder motor as the chassis, the "EnhanceEncoderOnBoard " enhanced encoder motor is also needed.

**RGBLineFollower**

RGBLineFollower provides the basic functions for implementing line following.

To install this plug-in:  
Open the mBlock software→ Extension → Extension Manager → Find plug-in packages "RGBLineFollower’ and "EnhanceEncoderOnBoard" → Download the plug-in packages. The following blocks are displayed:  

<img src="images/20180329103315.jpg" alt="微信截图_20160129151218" width="448" style="padding:5px 5px 12px 0px;"> 

Description of the blocks:    
- Interface: Port1–Port12  
- Address: add1–add4 (0–3). Any port can be bound to any address, but one port of all related blocks in one program can be bound to only one address, and each address corresponds to only one RGB line following module.

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;width:40%">Statement block</th>
<th style="border: 1px solid black;width:60%">Description</th>
</tr>


<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_s1201.jpg" alt="微信截图_20160129151218" width="428" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Setting line following sensitivity, which is used for adjusting line following response speed. The larger the value is, the more sensitive it turns.</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_s2201.jpg" alt="微信截图_20160129151218" width="428" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Obtaining offset of the sensor from the track. The output is taken as the product of the line following sensitivity Kp and the analog value resulted from fusion algorithm of the 4 sensors, ranging from -512 to 512. If it is larger than 0, it means that the line deflects to RGB4. The larger the value, the higher the offset. If it is smaller than 0, it means that the line deflects to RGB1. The greater the negative value, the higher the offset.
<img src="images/rgb-line-follower_12.png"  width="607" style="padding:5px 5px 12px 0px;">
</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_s3201.jpg" alt="微信截图_20160129151218" width="428" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">
<img src="images/rgb-line-follower_13.png"  width="607" style="padding:5px 5px 12px 0px;">
s shown in the figure, bit0~bit3 represent RGB4, RGB3, RGB2 and RGB1 from left to right respectively.<br>
"1" means that the background is detected (corresponding LED is on), while "0" means that the line is detected (corresponding LED is off).<br>
"0000" means that all the 4 sensors detect the line (all the LEDs are off), with 0 returned;<br>
"0111" means that the 4 sensors from left to right detect "line", "background", "background" and "background" respectively (the LEDs are off, on, on and on), with 7 returned;<br>
"0011" means that the 4 sensors from left to right detect "line", "line", "background" and "background" respectively (the LEDs are off, off, on and on), with 3 returned.</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_s4201.jpg" alt="微信截图_20160129151218" width="426" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Obtaining learning status of the RGB sensor. Output: 0-not learning, 1-learning background color, 2-learning track color.</td>
</tr>
</table>


The following is an example of line following based on the Auriga control board, with the 180 encoder motor as the chassis:  

<img src="images/rgb-line-follower_rgblinefollower.png" alt="微信截图_20160129151218" width="598" style="padding:5px 5px 12px 0px;">  

The following is an example of line following based on mCore control board, with the TT motor as the chassis:  

<img src="images/rgb-line-follower_mbot-rgbline-en.png" alt="微信截图_20160129151218" width="618" style="padding:5px 5px 12px 0px;">

**RGBLineFollowerAdvanced**

RGBLineFollowerAdvanced provides the more underlying blocks, which also include the original returned values of the probes. It facilitates more flexible education and application scenarios. 

To install this plug-in:

Open the mBlock software→ Extensions → Manage Extensions → Find plug-in packages "RGBLineFollower’ and "EnhanceEncoderOnBoard" → Download the plug-in packages. The following blocks are displayed:

<img src="images/rgb-line-follower_14.png" width="448" style="padding:5px 5px 12px 0px;">

**Description of the blocks:** 
- Interface: Port1–Port12  
- Sensor number: 1–4. Any port can be bound to any sensor, but one number can be bound to only one RGB line following module.

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;width:40%">Statement block</th>
<th style="border: 1px solid black;width:60%">Description</th>
</tr>


<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_15.png" width="428" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Initializing the RGB line follower, binding it to the port it has been connected to. To use blocks related to the RGB line follower, you need to invoke this block first.</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_16.png" width="500" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Setting the color of the RGB supplement light. Currently, you can set the color to red, green, or blue.
</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_17.png" width="500" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Setting the line following sensitivity, which is used for adjusting line following response speed. The value ranges from 0 to 1. The larger the value is, the more sensitive it turns.
</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_18.png" width="426" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Obtaining the analog value (ADC value) collected by each probe of the line follower. The value ranges from 0 to 255. The closer to black the color detected by the probe is, the closer to zero the value is; the closer to white the color is, the closer to 255 the value is.</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_19.png" width="428" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Obtaining offset of the sensor from the track. The output is taken as the product of the line following sensitivity Kp and the analog value resulted from fusion algorithm of the 4 sensors, ranging from –512 to 512. If it is larger than 0, it means that the line deflects to RGB4. The larger the value, the higher the offset. If it is smaller than 0, it means that the line deflects to RGB1. The greater the negative value, the higher the offset.
<img src="images/rgb-line-follower_12.png"  width="607" style="padding:5px 5px 12px 0px;">
</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_20.png" width="428" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">
<img src="images/rgb-line-follower_13.png"  width="607" style="padding:5px 5px 12px 0px;">
As shown in the figure, bit0~bit3 represent RGB4, RGB3, RGB2 and RGB1 from left to right respectively.<br>
"1" means that the background is detected (corresponding LED is on), while "0" means that the line is detected (corresponding LED is off).<br>
"0000" means that all the 4 sensors detect the line (all the LEDs are off).<br>
"0111" means that the 4 sensors from left to right detect "line", "background", "background" and "background" respectively (the LEDs are off, on, on and on).<br>
"0011" means that the 4 sensors from left to right detect "line", "line", "background" and "background" respectively (the LEDs are off, off, on and on).<br>If the state detected by the probe is the same as the one you set, the value "True" is returned; and if the state detected is not the same as the one you set, "False" is returned.</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_21.png" width="428" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Obtaining the state of a probe, determining whether it detects a background or track. If it detects the background, the value "True" is returned; and if it detects the track, "False" is returned.

<img src="images/rgb-line-follower_13.png"  width="607" style="padding:5px 5px 12px 0px;">
</td>
</tr>
</table>

The following is an example (using the default strategy) of line following based on the Auriga control board, with the 180 encoder motor as the chassis. If the robot comes to a crossroad during line following, it stops moving for two seconds, runs forward for half a second, and then continues the line following.

<img src="images/rgb-line-follower_22.png"  width="607" style="padding:5px 5px 12px 0px;">

The following is an example of line following based on the Auriga control board, with the 180 encoder motor as the chassis. In this example, the user obtains the ADC value of each probe and compiles the line following algorithm. It uses the dark-track-on-light-background mode.

<img src="images/rgb-line-follower_23.png"  width="700" style="padding:5px 5px 12px 0px;">

● **mBlock 5 programming** 

The following uses Auriga as an example to describe how to add the RGB Line Follower extension. 

1\. Add the device **mBot Ranger**.

<img src="images/rgb-line-follower_24.png"  width="700" style="padding:5px 5px 12px 0px;">

2\. Add the RGB Line Follower extension.

<img src="images/rgb-line-follower_25.png"  width="700" style="padding:5px 5px 12px 0px;">

Then, you can see the blocks in the Blocks area.

<img src="images/rgb-line-follower_26.png"  width="400" style="padding:5px 5px 12px 0px;">

Description of the blocks

Port: Port 1–Port 12

Sensor number: 1–4. Any port can be bound to one and only one RGB line follower.

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;width:40%">Statement block</th>
<th style="border: 1px solid black;width:60%">Description</th>
</tr>


<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_27.png" width="428" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Initializing the RGB line follower, binding it to the port it has been connected to. To use blocks related to the RGB line follower, you need to drag this block to the Scripts area first.</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_28.png" width="500" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Setting the color of the RGB supplement light. Currently, you can set the color to red, green, or blue.
</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_29.png" width="500" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Setting the line following sensitivity, which is used for adjusting line following response speed. The value ranges from 0 to 1. The larger the value is, the more sensitive it turns.
</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_30.png" width="426" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Obtaining the analog value (ADC value) collected by each probe of the line follower. The value ranges from 0 to 255. The closer to black the color detected by the probe is, the closer to zero the value is; the closer to white the color is, the closer to 255 the value is.</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_31.png" width="428" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Obtaining offset of the sensor from the track. The output is taken as the product of the line following sensitivity Kp and the analog value resulted from fusion algorithm of the 4 sensors, ranging from –512 to 512. If it is larger than 0, it means that the line deflects to RGB4. The larger the value, the higher the offset. If it is smaller than 0, it means that the line deflects to RGB1. The greater the negative value, the higher the offset.
<img src="images/rgb-line-follower_12.png"  width="607" style="padding:5px 5px 12px 0px;">
</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_32.png" width="428" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">
<img src="images/rgb-line-follower_13.png"  width="607" style="padding:5px 5px 12px 0px;">
As shown in the figure, bit0~bit3 represent RGB4, RGB3, RGB2 and RGB1 from left to right respectively.<br>
"1" means that the background is detected (corresponding LED is on), while "0" means that the line is detected (corresponding LED is off).<br>
"0000" means that all the 4 sensors detect the line (all the LEDs are off).<br>
"0111" means that the 4 sensors from left to right detect "line", "background", "background" and "background" respectively (the LEDs are off, on, on and on).<br>
"0011" means that the 4 sensors from left to right detect "line", "line", "background" and "background" respectively (the LEDs are off, off, on and on).<br>If the state detected by the probe is the same as the one you set, the value "True" is returned; and if the state detected is not the same as the one you set, "False" is returned.</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_33.png" width="428" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Obtaining the state of a probe, determining whether it detects a background or track. If it detects the background, the value "True" is returned; and if it detects the track, "False" is returned.

<img src="images/rgb-line-follower_13.png"  width="607" style="padding:5px 5px 12px 0px;">
</td>
</tr>
</table>

**Example program**

<img src="images/rgb-line-follower_34.png"  width="607" style="padding:5px 5px 12px 0px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="../sensors/4RGBauriga.mblock" download style="color:white;font-weight:bold;">Download the Example Program</a></span></p>