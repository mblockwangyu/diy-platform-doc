# Me Audio Player

<img src="images/me-audio-player-1.jpg" width="300px;" style="padding: 5px 5px 12px 0px;">

### 1. Overview

Me Audio Player module, compatible with the entire series of MakeBlock control boards, is able to play music and records with a built-in voice decoding chip. This module’s connector is marked in white, meaning that it is controlled by I2C signals, and must be connected to main board’s white-marked port. Insert the TF memory card to feel the joy of music and it is very convenient to use.

### 2. Technological Specifications:

- Working voltage: 5V DC  
- Microphone sensitivity (1Khz): 50-54dB  
- Microphone impedance: 2.2 kΩ  
- Microphone signal to noise ratio: 58 db  
- Speaker rated power: 1W  
- Speaker rated impedance: 8±15%Ω  
- Communication method: I2C  
- Largest electric current: 500mA  
- Module size: 56 x 41 x 28 mm (LxWxH)

### 3. Characteristics

- Onboard blue LED solid on means music playback mode, flashing means recording mode;  
- High sensitivity to sound;  
- The module’s metal hole area is the reference area in contact with the metal beam;  
- With reverse polarity protection, reverse power supply will not damage the IC;  
- Supports mBlock graphical programming, suitable for all ages users;  
- Convenient connection by using the RJ25 port;  
- Modular installation, compatible with LEGO series;  
- Module supports Micro USB to copy audio files directly without the need for a card reader;  
- Module directly supports MP3, WMA, and WAV files.

### 4. Connection Method

● **RJ25 connection**

Since the Me Audio Player module’s connector port is marked in white, when using the RJ25 port, it needs to be connected to the port marked in white on the main control board as well. Taking MakeBlock Orion as an example, you can connect it to ports 3, 4, 6, 7 and 8, as shown.

<img src="images/me-audio-player_20184171.jpg" alt="微信截图_20160129151012" width="350" style="padding: 5px 5px 12px 0px;">

### 5. Audio File Importing

To play an MP3 file, you need to import the file into Me Audio Player first. You can import a file in either of the following two ways:

* Take the memory card out of Me Audio Player, use a card reader to connect it to a computer, and import files to it. 

   The following figure shows the position of the memory card. You can press to eject it.

   <img src="images/me-audio-player_memory_card.png" width="150" style="padding: 5px 5px 12px 0px;">

* Use a Micro USB cable to connect Me Audio Player to a computer and import audio files into it.

### 6. Programming Guide

● **Arduino programming** 

If you are using Arduino programming, you must call the
``Makeblock-Library-master`` to control the audio playback module.  
This program controls Me Audio Player module through buttons through Arduino programming, achieving the playback, pausing, start recording, and stop recording.  

<img src="images/me-audio-player_20184172.png" alt="微信截图_20160129151123" width="569" style="padding: 5px 5px 12px 0px;">

**Me Audio Player module-function list**

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<td style="border: 1px solid black;width:40%;">Function</td>
<td style="border: 1px solid black;width:30%;">Instruction</td>
<td style="border: 1px solid black;width:30%;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">MeAudioPlayer(uint8_t port)</td>
<td style="border: 1px solid black;">Choose a port</td>
<td style="border: 1px solid black;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">playMusicFileIndex(uint16_t music_index)</td>
<td style="border: 1px solid black;">Specify audio file index playback</td>
<td style="border: 1px solid black;">Numbers：1.2.3……</td>
</tr>

<tr>
<td style="border: 1px solid black;">pauseMusic()</td>
<td style="border: 1px solid black;">Pause playback</td>
<td style="border: 1px solid black;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">stopMusic()</td>
<td style="border: 1px solid black;">stop playback</td>
<td style="border: 1px solid black;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">playNextMusic()</td>
<td style="border: 1px solid black;">Next</td>
<td style="border: 1px solid black;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">playPrevMusic()</td>
<td style="border: 1px solid black;">Previous</td>
<td style="border: 1px solid black;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">setMusicVolume(uint8_t vol)</td>
<td style="border: 1px solid black;">Set the volume</td>
<td style="border: 1px solid black;">Range0~100</td>
</tr>

<tr>
<td style="border: 1px solid black;">setMusicPlayMode(uint8_t mode)</td>
<td style="border: 1px solid black;">Set playback mode</td>
<td style="border: 1px solid black;">0.Single play<br>
1.Single cycle<br>
2.Playlist loop<br>
3.Random play</td>
</tr>

<tr>
<td style="border: 1px solid black;">startRecordingFileName(char *str)</td>
<td style="border: 1px solid black;">Select a file name to begin</td>
<td style="border: 1px solid black;"></td>
</tr>

<tr>
<td style="border: 1px solid black;">stopRecording()</td>
<td style="border: 1px solid black;">Stop recording</td>
<td style="border: 1px solid black;"></td>
</tr>
</table>

<br>

● **mBlock programming**

Me Audio Player supports mBlock programming. To use mBlock to program Me Audio Player, you need to add the extension as follows:

1\. Open the extension manager.

<img src="images/me-audio-player_1.png" width="400" style="padding: 5px 5px 12px 0px;">

2\. Download the **MeAudioPlayer** extension on the **Manage Extensions** page.

<img src="images/me-audio-player_2.png" width="300" style="padding: 5px 5px 12px 0px;">

After the extension is downloaded, mBlock displays the corresponding blocks.

<img src="images/me-audio-player_3.png" width="400" style="padding: 5px 5px 12px 0px;">

The following table describes the blocks.

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;width:50%">Block</th>
<th style="border: 1px solid black;width:50%">Description</th>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling1.png" alt="微信截图_20160129151218" width="368" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Initializes the audio player, specifying its port<br><strong>Note: </strong>You must add this block before using any other audio play block.</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling2.png" alt="微信截图_20160129151218" width="178" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;">Plays the audio file with the specified index number</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling3.png" alt="微信截图_20160129151218" width="190" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;">Plays the audio file with the specified name</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling4.png" alt="微信截图_20160129151218" width="210" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;">Sets the play mode: single play, single cycle, playlist loop, random play</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling5.png" alt="微信截图_20160129151218" width="150" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;">Plays the previous audio file</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling6.png" alt="微信截图_20160129151218" width="120" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Plays the next audio file</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling7.png" alt="微信截图_20160129151218" width="130" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;">Pauses or continues the playing</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling8.png" alt="微信截图_20160129151218" width="95" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;">Stops playing</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling9.png" alt="微信截图_20160129151218" width="150" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;">Sets the volume</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling10.png" alt="微信截图_20160129151218" width="95" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;">Increases the volume</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling11.png" alt="微信截图_20160129151218" width="106" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;"> Decreases the volume</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling12.png" alt="微信截图_20160129151218" width="310" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;">Starts audio recording and saves to a file named <strong>T001</strong></td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ezling13.png" alt="微信截图_20160129151218" width="220" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Stops recording</td>
</tr>
</table>

<br>

The following is an example of how to use mBlock to control the Me Audio Player module：

This program can control Me Audio Player through the [Me 4 button module](http://learn.makeblock.com/en/me-4-button/). To achieve playback, pause, start recording, and stopping recording audio files, the following are the results:

<img src="images/me-audio-player_20184201111.png" alt="微信截图_20160129151218" width="1108" style="padding: 5px 5px 12px 0px;">

● **mBlock 5 programming**

Me Audio Player supports mBlock 5 programming. To use mBlock 5 to program Me Audio Player, you need to add the extension first. The following takes mBot as an example to describe how to add the extension:

1\. Click **+ extension**.

<img src="images/me-audio-player_4.png" width="400" style="padding: 5px 5px 12px 0px;">

2\. Click **+ Add** under the **Audio Player** extension on the **Extension center** page that appears.

<img src="images/me-audio-player_5.png" width="600" style="padding: 5px 5px 12px 0px;">

After the extension is added, mBlock 5 displays the corresponding blocks.

<img src="images/me-audio-player_6.png" width="500" style="padding: 5px 5px 12px 0px;">

The following table describes the blocks.

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;width:50%">Block</th>
<th style="border: 1px solid black;width:50%">Description</th>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_7.png" width="200" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Initializes the audio player, specifying its port<br><strong>Note: </strong>You must add this block before using any other audio play block.</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_8.png" width="200" style="padding: 5px 5px 12px 0px;">   </td>
<td style="border: 1px solid black;">Plays the audio file with the specified index number</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_8-1.png" width="200" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;">Plays the audio file with the specified audio name</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_9.png" width="200" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;">Sets the play mode: single play, single cycle, playlist loop, random play</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_10.png" width="200" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;">Plays the previous audio file</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_11.png" width="200" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;">Plays the next audio file</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_12.png" width="200" style="padding: 5px 5px 12px 0px;">   </td>
<td style="border: 1px solid black;">Pauses or continues the playing</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_13.png" width="200" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;">Stops playing</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_14.png" width="200" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Sets the volume</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_15.png" width="200" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Increases the volume</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_16.png" width="200" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;">Decreases the volume</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_17.png" width="120" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">Starts audio recording and saves to a file named <strong>T001</strong></td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_18.png" width="200" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;">Stops recording</td>
</tr>
</table>

**Program example**

<img src="images/me-audio-player_19.png" width="200" style="padding: 5px 5px 12px 0px;">

### 7. Audio file format instructions:

- Please add an appropriate delay after After the volume setting blocks, and wait for it to take effect;  
- Using external memory TF card to store audio files, support MP3, WAV, WMA high quality audio format files;  
- Using FAT and FAT32 file systems;  
- The audio file naming format supports English naming (case-insensitive). English letters and numbers are mixed and the length of the naming is recommended to be less than 8 characters. For example: Hello.MP3, T002.MP3, and R000001.MP3. (It is not recommended to use pure number naming);  
- Sorting of audio files in TF card: It is recommended to sort by file name;  
- This module does not support Chinese naming of audio files;  
- Do not use special character names such as: v1.0″, o\_o0, … ( not support).
