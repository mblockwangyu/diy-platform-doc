# Me Compass

![](images/me-compass_Me-Compass.jpg)

<img src="images/me-compass_微信截图_20160129141724.png" alt="微信截图_20160129141724" width="233" style="padding:5px 5px 12px 0px;">

### Overview

The Me Compass module can detect the intensity of surrounding magnetic field, and make the moving device or equipment rotate to specified direction. For example, when it is mounted on a cart, the cart can be
controlled to rotate to or walk along specified direction. The module has a key and indicator for calibration. When the surrounding mechanical structures or the position/direction of the module are changed, you can download the program provided by Makeblock and operate the key to calibrate it, so that it can accurately measure the angle in new environment. This is one of the advantages of this module compared to similar products from other manufacturers. Its white ID means that it’s in I2C communication mode and should be connected to the port with white ID on Makeblock Orion.

### Technical specifications

- Operating voltage: 5V DC  
- Resolution: 5 mil gauss  
- Dynamic range of magnetic field: ±1\~±8 gauss  
- Precision of electronic compass: 1°\~2° (Theoretical value)  
- Signal mode: I²C communication  
- Module size: 51 x 24 x 18 mm (L x W x H)

### Functional characteristics

- White area of module is the reference area to contact metal beams  
- Provide two operating modes: measure mode → blue light on, calibrate mode → blue light flashing  
- It needs to be calibrated after it is first used or its surrounding metal structure changes.  
- Built-in electronic compass calibration procedure  
- Anti-reverse protection – connecting the power supply inversely will not damage IC  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Provide pin-type port to support most development boards including
Arduino series

### Pin definition

The port of Me Compass has 6 pins, and their functions are as follows:

<img src="images/me-compass_微信截图_20160129141909.png" alt="微信截图_20160129141909" width="719" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25**  

Since the port of Me Compass has white ID, you need to connect the port with white ID on Makeblock Orion when using RJ25 port. Taking Makeblock Orion as example, you can connect it to ports No. 3, 4, 6, 7, and 8 as follows:

<img src="images/me-compass_微信截图_20160129141947.png" alt="微信截图_20160129141947" width="336" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire**  

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its SCL and SDA pins should be connected to I2C port, that is, the port A5 and A4 respectively as follows:

<img src="images/me-compass_微信截图_20160129142017.png" alt="微信截图_20160129142017" width="362" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming** 

If you use Arduino to write a program, the library
``Makeblock-Library-master`` should be invoked to control the Me Compass. 

This is a routine to read the angle value detected by Me Compass and output the result to serial monitor through Arduino programming. Upload the code segment to Makeblock Orion, click Arduino serial monitor, and
you can see the running result.

<img src="images/me-compass_微信截图_20160129142055.png" alt="微信截图_20160129142055" width="457" style="padding:5px 5px 12px 0px;">

<img src="images/me-compass_微信截图_20160129142120.png" alt="微信截图_20160129142120" width="745" style="padding:5px 5px 12px 0px;">

### Precaution

Me Compass module is rather sensitive to the changes of ambient magnetic field. Therefore, any changes occur to the surrounding mechanical structure or the assembly way of Me Compass may result in the changes of
the ambient magnetic field, which then lead to deviation of the measurement result from the Me Compass module. In this case, users need to calibrate the module to get correct angular value in current circumstance. 

**Process of calibrating the module:**  

1\) Connect Me Compass to Makeblock Orion correctly, power on, and then download any one of the compass library we provide.

2\) Press the key on the module until the blue light of the module start flashing, then let go of the key. 

3\) During the flashing of the blue light, spin the module (including the mechanical structure connected with it) along the horizontal plane for over 360°
steadily.

<img src="images/me-compass_me-compass-560x296.png" alt="me compass" width="560" style="padding:5px 5px 12px 0px;">  

4\) After the spinning, press the key on the module to exit the calibration. Now the blue light on the module will be solid on.

**Remarks:** 

1\) Two modes of Me Compass:   
Measurement mode → blue light solid on;  
Calibration mode → blue light flashes;

2\) If you have calibrated the module in current circumstance, it won’t be necessary to calibrate the module after power off and power on again

3\) Do not press the button on the module if you do not intend to calibrate the module, otherwise it will invalid your previous calibration 

4\) During the calibration, make sure to spin the module (including the mechanical structure connected with it) along the horizontal plane for Over 360°. Otherwise the calibration will fail to work.

### Principle analysis

Traditional compass uses a magnetized needle to induce the magnetic field of the earth. The magnetic force between the magnetic field and the needle spins the needle until the needle aligns itself with the horizontal component of the Earth’s magnetic field (North and South). This also applies to electronic compass, except the needle is replaced by a magneto-resistance sensor. The magneto-resistance sensor will convert the geomagnetic info it receives into digital signal and output it for usage. 

A 3-axis digital compass composes of 3-axis magnetic-resistance sensor, 2-axis tilt sensor, and MCU. The 3-axis magnetic-resistance sensor is applied to measure the magnetic field; the tilt sensor is used to compensate when the magnetometer is not in horizontal state; MCU is in charge of analyzing the signal output from the magnetometer and the tilt
sensor, outputting the data, and compensating the soft and hard iron.

Three mutually perpendicular magnetic resistance sensors detect the intensity of the geomagnetic field on their individual axial direction.
The analog output will then be amplified and transferred to MCU.

### Schematic

<img src="images/me-compass_Me-compass.png" alt="Me compass" width="1440" style="padding:5px 5px 12px 0px;">
