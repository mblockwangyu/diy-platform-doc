# Me Line Follower


![](images/me-line-follower_Me-Line-Follower.jpg)

### Overview

Me Line Follower is designed for line-following robots. It includes two sensors, each one consisting of one infrared emitting LED and one infrared sensing phototransistor. Me Line Follower enables a robot to follow a black line in a white area or a white line in a black area. Even with simple circuits, it can detect lines quickly. The interface of Me Line Follower is marked with the color of blue, which indicates that it is a dual-digital interface that can be connected only to an interface also marked with blue on a main control board. 

### Technical specifications

- Operating voltage: 5V DC  
- Detection range: 1\~2cm  
- Detection angle: within 120°  
- Control mode: dual-digital interface
- Module dimension: 51 x 24 x 21mm (L x W x H)

### Functional characteristics

- White area of module is the reference area to contact metal beams  
- Provide two LED indicators for feedback during line-following  
- Anti-reverse protection – connecting the power supply inversely will not damage IC  
- Easy to be influenced by natural light, and limited by greatly changed ambient light  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Provide pin-type port to support most development boards including Arduino series

### Pin definition

The port of Me Line Follower has four pins, and their functions are as follows:

<img src="images/me-line-follower_微信截图_20160129150938.png" alt="微信截图_20160129150938" width="906" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25**   

Since the port of Me Line Follower has blue ID, you need to connect the port with blue ID on Makeblock Orion when using RJ25 port. 

Taking Makeblock Orion as example, you can connect to ports No. 3, 4, 5, and 6 as follows:

<img src="images/me-line-follower_微信截图_20160129151012.png" alt="微信截图_20160129151012" width="421" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire**  

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its S1 and S2 pins should be connected to digital ports as follows:

<img src="images/me-line-follower_微信截图_20160129151050.png" alt="微信截图_20160129151050" width="426" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming**  

If you use Arduino to write a program, the library `Makeblock-Library-master` should be invoked to control the Me Line Follower.

<img src="images/me-line-follower_微信截图_20160129151123.png" alt="微信截图_20160129151123" width="519" style="padding:5px 5px 12px 0px;">

<img src="images/me-line-follower_微信截图_20160129151147.png" alt="微信截图_20160129151147" width="1089" style="padding:5px 5px 12px 0px;">

The function of the code segment is: to read the results detected by the sensor of Me Line Follower continuously and output the results to the serial monitor in Arduino IDE every 200 ms. Upload the code segment to Makeblock Orion and click the Arduino serial monitor, and you will see the running results as follows:

<img src="images/me-line-follower_微信截图_20160129151218.png" alt="微信截图_20160129151218" width="468" style="padding:5px 5px 12px 0px;">

● **mBlock programming**  

Me Line Follower supports the mBlock programming environment and its instructions are introduced as follows:

<img src="images/me-line-follower_微信截图_20160129151249.png" alt="微信截图_20160129151249" width="625" style="padding:5px 5px 12px 0px;">

### Principle analysis

Me Line Follower is an accessory of robot developed on the principle of reflective photoelectric sensor. The infrared light features different reflection intensity when irradiating on different color of object surface. In the process of running, the cart irradiates infrared light
on the floor continuously. When the infrared light reaches white paper floor, diffuse reflection occurs and the reflected light is received by a receiver mounted on the cart. If the infrared light reaches black line, it is absorbed and can not be received by the receiver of cart. By
identifying whether the reflected infrared light is received, we can determine the position of black line and the running line of cart. (The output is 0 when irradiating to a black line, and the output is 1 when irradiating to a white line).

### Schematic

<img src="images/me-line-follower_1-80f60e3f9c.jpg" alt="1-80f60e3f9c" width="1122" style="padding:5px 5px 12px 0px;">
