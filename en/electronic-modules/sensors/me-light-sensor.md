# Me Light Sensor


![](images/me-light-sensor_Me-Light-Sensor.jpg)

<img src="images/me-light-sensor_微信截图_20160129151455.png" alt="微信截图_20160129151455" width="245" style="padding:5px 5px 12px 0px;">

### Overview

Developed on the basis of photoelectric effect principle in
semiconductor, Me Light Sensor can be used to detect the intensity of ambient light. Users can use it to make some light interactive projects, such as an intelligent dimming lamplet, a laser communication system,
and so on. Its black ID means that it has an analog signal port and needs to be connected to the port with black ID on Makeblock Orion.

### Technical specifications

- Operating voltage: 5V DC  
- Control mode: single analog port control  
- Value of analog output: exposure to sunlight (&gt;500),  
evening (0\~100), indoor lighting condition (100\~500)  
- Photosensitive wavelength range: 400nm\~1100nm  
- Module dimension: 51 x 24 x 22mm (L x W x H)

### Functional characteristics

- Only sensitive to visible light, and need no additional filter  
- White area of module is the reference area to contact metal beams  
- Anti-reverse protection – connecting the power supply inversely will
not damage IC  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Provide pin-type port to support most development boards including
Arduino series

### Pin definition

The port of Me Light Sensor has three pins, and their functions are as follows:

<img src="images/me-light-sensor_微信截图_20160129151644.png" alt="微信截图_20160129151644" width="799" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25**  

Since the port of Me Light Sensor has black ID, you need to connect the port with black ID on Makeblock Orion when using RJ25 port. 

Taking Makeblock Orion as example, you can connect to ports No. 6, 7, and 8 as follows:

<img src="images/me-light-sensor_微信截图_20160129151717.png" alt="微信截图_20160129151717" width="383" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire** 

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its AO pin should be connected to analog port as follows:

<img src="images/me-light-sensor_微信截图_20160129151746.png" alt="微信截图_20160129151746" width="390" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming**  

If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control the Me
Light Sensor. This program instructs the Me Light Sensor to read the current light intensity through Arduino programming. 

<img src="images/me-light-sensor_微信截图_20160129152025.png" alt="微信截图_20160129152025" width="516" style="padding:5px 5px 12px 0px;">

<img src="images/me-light-sensor_微信截图_20160129152048.png" alt="微信截图_20160129152048" width="800" style="padding:5px 5px 12px 0px;">

The function of the code segment is: to read the detected results of light intensity and output them to the serial monitor in Arduino IDE, and you will see the running result as follows:

<img src="images/me-light-sensor_微信截图_20160129152118.png" alt="微信截图_20160129152118" width="511" style="padding:5px 5px 12px 0px;">

● **mBlock programming**  

Me Light Sensor supports the mBlock programming environment and its instructions are introduced as follows:

<img src="images/me-light-sensor_微信截图_20160129152149.png" alt="微信截图_20160129152149" width="444" style="padding:5px 5px 12px 0px;">

This is an example on how to use mBlock to control the Me Light Sensor. The panda will speak out the value of light intensity read from the Me Light Sensor. The higher the light intensity is, the bigger the value is. 

The running results are as follows:

<img src="images/me-light-sensor_微信截图_20160129152226.png" alt="微信截图_20160129152226" width="659" style="padding:5px 5px 12px 0px;">

### Principle analysis

This module (Me Light Sensor) is a light sensor developed on the basis of photoelectric effect principle of semiconductor. Its main component is a photoelectric transistor whose resistance decreases with the increase of light intensity. By cascading it with another resister and
outputting the value of divider resistance, the changing light signal can be converted into changing electrical signal and then output from analog port. This sensitive module can be used to make light interactive projects, such as an intelligent dimming lamplet, to ensure the ambient
light intensity is comfortable for human body.

### Schematic

<img src="images/me-light-sensor_lightsensor.png" alt="Me light sensor" width="1065" style="padding:5px 5px 12px 0px;">
