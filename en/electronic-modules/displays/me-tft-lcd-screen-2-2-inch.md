# Me TFT LCD Screen – 2.4 Inch

<img src="images/me-tft-lcd-screen-2-2-inch_微信截图_20160129104542.png" alt="微信截图_20160129104542" width="213" style="padding:5px 5px 12px 0px;">

### Overview

The main component of Me TFT LCD Screen module is a LCD display communicating with Makeblock Orion through serial port to show characters and graphics of different size and colors. The module is integrated with MCU and memory chip, and the Chinese characters, letters, and figures stored in the memory chip can be invoked easily through the serial port. Its blue/gray ID means that it has a double-digital signal port and needs to be connected to the port with
blue or gray ID on Makeblock Orion.

### Technical specifications

- Operating voltage: 5V DC  
- Whether support touch: No  
- Screen size: 2.4″  
- Screen resolution: 240×320  
- Control mode: Serial port communication  
- Default baud rate: 9600  
- Module size: 78 x 48 x 18 mm (L x W x H)

### Functional characteristics

- Flash memory capacity: 2M  
- Occupy two IO ports only (serial port communication)  
- Support automatic extracting Chinese characters of 24, 32, 48, 64-dot matrix  
- Support plotting point, line, circle, rectangle, filled box, etc.  
- Support displaying true color JPG graphics  
- White area of module is the reference area to contact metal beams  
- Support displaying in four directions  
- Adopt RJ25 port for easy connection  
- Support displaying a variety of colors  
- Provide pin-type of port to support most development boards including Arduino series

### Pin definition

The port of Me TFT LCD Screen has four pins, and their functions are as follows:

<img src="images/me-tft-lcd-screen-2-2-inch_微信截图_20160129104806.png" alt="微信截图_20160129104806" width="792" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25** 

Since the port of Me TFT LCD Screen has blue/gray ID, you need to connect the port with blue or gray ID on Makeblock Orion when using RJ25 port. Taking Makeblock Orion as example, you can connect to ports No. 5 as
follows:

<img src="images/me-tft-lcd-screen-2-2-inch_微信截图_20160129104918.png" alt="微信截图_20160129104918" width="366" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire** 

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its TX and RX pins should be connected to TX and RX ports respectively as follows:

<img src="images/me-tft-lcd-screen-2-2-inch_微信截图_20160129104954.png" alt="微信截图_20160129104954" width="399" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming** 

If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control the Me TFT LCD Screen. This program serves to display different graphics and characters through Arduino programming.

<img src="images/me-tft-lcd-screen-2-2-inch_微信截图_20160129105214.png" alt="微信截图_20160129105214" width="480" style="padding:5px 5px 12px 0px;">

<img src="images/me-tft-lcd-screen-2-2-inch_微信截图_20160129105238.png" alt="微信截图_20160129105238" width="806" style="padding:5px 5px 12px 0px;">

### Principle analysis

This module (Me TFT LCD Screen – 2.4 Inch) contains a voltage converter, an STM32 chip, and a serial flash memory of 2M. In contrast with other displays, it needs only serial port for communication, so it is easy to operate and connect.

A special serial port assistant is provided to help you set the baud rate of transmission, and store the processed pictures you want to display into the flash memory so as to implement the display or switch of boot pictures in your own project. To download pictures, you need other serial port for conversion. In addition, it also supports superposition of background pictures and the letters, and GUI display. Its applications include the calendar, voltage meter, ampere meter, etc.
