# Me 7-Segment Serial Display – Red

<img src="images/me-7-segment-display_微信截图_20160129110823.png" alt="微信截图_20160129110823" width="181" style="padding:5px 5px 12px 0px;">

### Overview

Me 7-Segment Display-Red module adopts a 4-digit common-anode digital tube for displaying the numbers and a few special characters. The module can be used in the robot project to display the data such as the speed, time, score, temperature, distance, etc. It uses specific chip with LED
drive and control, so that the digital tube can be controlled easier. Makeblock also provides Arduino library for easy programming to allow users easily controlling the digital tube. Its blue ID means that it has a double-digital signal port and needs to be connected to the port with
blue ID on Makeblock Orion.

### Technical specifications

- Operating voltage: 5V DC  
- Number of digits: 4  
- Control mode: Double-digital control  
- Driver chip: TM1637  
- Color of digital tube: Red  
- Module size: 51 x 24 x 23.4 mm (L x W x H)

### Functional characteristics

- White area of module is the reference area to contact metal beams  
- Highlight Me 7-Segment Display-Red, allow users to see the content even in the day  
- Anti-reverse protection – connecting the power supply inversely will not damage IC  
- Provide eight levels of adjustable brightness for the digital tube  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Double serial bus port (DIO, CLK)  
- Provide pin-type port to support most development boards including Arduino series

### Pin definition

The port of Me 7-Segment Display-Red has four pins, and their functions are as follows:

<img src="images/me-7-segment-display_微信截图_20160129111113.png" alt="微信截图_20160129111113" width="795" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25** 

Since the port of Me 7-Segment Display-Red has blue ID, you need to connect the port with blue ID on Makeblock Orion when using RJ25 port.

Taking Makeblock Orion as example, you can connect to ports No. 3, 4, 5, and 6 as follows:

<img src="images/me-7-segment-display_微信截图_20160129111212.png" alt="微信截图_20160129111212" width="391" style="padding:5px 5px 12px 0px;">

● **Connecting with Dupont wire**  

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its DIO and CLK pins should be connected to digital ports as follows:

<img src="images/me-7-segment-display_微信截图_20160129111242.png" alt="微信截图_20160129111242" width="365" style="padding:5px 5px 12px 0px;">

### Guide to programming

● **Arduino programming**  

If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control the Me 7- Segment Display-Red module. This program can make the Me 7-Segment Display-Red displaying 15 digits of number and moving the characters 1, 2, 3, 4, 5, 6, 7, 8, 9, A, b, C, d, E, and F from right to left through Arduino programming.

<img src="images/me-7-segment-display_微信截图_20160129111343.png" alt="微信截图_20160129111343" width="548" style="padding:5px 5px 12px 0px;">

### Schematic

<img src="images/me-7-segment-display_Me-7-segment.png" alt="Me 7-segment" width="1489" style="padding:5px 5px 12px 0px;">
