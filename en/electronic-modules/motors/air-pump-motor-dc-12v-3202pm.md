# Air Pump Motor DC 12V-3202PM

<img src="images/air-pump-motor-dc-12v-3202pm_微信截图_20160128110215-279x300.png" alt="微信截图_20160128110215" width="279" style="padding:5px 5px 12px 0px;">

### Description

Makeblock air pump motor – DC 12V/3202PM is widely used for aquarium tank oxygen circulate, DIY projects.

### Specification

- Rated voltage: DC 12V
- Load: Air
- Current(With load): Less than 400mA
- Flow : 1.8LPM
- Size : D27 x 52mm
- Maximum pressure : More than 600mmHg
- Noise: &lt;60dB

### Size Charts(mm)

<img src="images/air-pump-motor-dc-12v-3202pm_微信截图_20160128110614-300x113.png" alt="微信截图_20160128110614" width="478" style="padding:5px 5px 12px 0px;">

### Demo

<img src="images/air-pump-motor-dc-12v-3202pm_微信截图_20160128110711-300x140.png" alt="微信截图_20160128110711" width="474" style="padding:5px 5px 12px 0px;">
