# DC Encoder Motor – 25 6V-185RPM

<img src="images/dc-encoder-motor-25-6v-185rpm_微信截图_20160128115903-300x243.png" alt="微信截图_20160128115903" width="300" style="padding:5px 5px 12px 0px;">

### Description

Me Encoder Motor Driver can control up to 2 DC Encoder Motors. Precise speed and position of 2 motors can be set and get separately. The motor driver IC is TB6612, same as Me Dual DC Motor Driver. We provide a software for tuning P-I-D parameters of the driver, so that it could work well in different conditions. The reduction-gear ratio of the motor is about 46. You can also edit the reduction-gear ratio in the software.

> Note: It requires some PID tuning experience, and a USB-to-UART module.

### Features

- Precise speed and position controlling.
- Real-time speed and position feedback.
- TB6612PNG motor driver IC with high efficient MOSFET-based H-bridges.
- Easy programming with Arduino library.
- 6V to 12V motor supply range.
- 1A maximum continous current per motor.
- Over-current protection.
- Easy wiring with RJ25 interface.
- 2.54mm pins for connecting with dubond lines.

### Size Chart (mm) 

<img src="images/dc-encoder-motor-25-6v-185rpm_微信截图_20160128160612.png" alt="微信截图_20160128160612" width="732" style="padding:5px 5px 12px 0px;">

### Demo

<img src="images/dc-encoder-motor-25-6v-185rpm_微信截图_20160128160724.png" alt="微信截图_20160128160724" width="703" style="padding:5px 5px 12px 0px;">  

<img src="images/dc-encoder-motor-25-6v-185rpm_微信截图_20160128120227-300x199.png" alt="微信截图_20160128120227" width="455" style="padding:5px 5px 12px 0px;">

> For API document, please visit: [API Documentation of Me Encoder Motor](http://learn.makeblock.com/api-documentation-of-me-encoder-motor/)
