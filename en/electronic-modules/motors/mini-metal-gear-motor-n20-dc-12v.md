# Mini Metal Gear Motor – N20 DC 12V

<img src="images/mini-metal-gear-motor-n20-dc-12v_微信截图_20160128121748-300x256.png" alt="微信截图_20160128121748" width="300" style="padding:5px 5px 12px 0px;">

### Description

Makeblock mini metal gear motor has a mini body, delicate gears and easy connector, and is widely used in many DIY projects. Makeblock has three different revolving speed for your choice, which is 100RPM, 210RPM and 390RPM.

### Specification

- Rated Voltage: DC 12V
- Revolving Speed: 100RPM/210RPM/390RPM (Depends on your needs)
- Total Size : 46 x 12mm
- Operating Temperature Range: -20℃\~+60℃

### Size Charts(mm)

<img src="images/mini-metal-gear-motor-n20-dc-12v_微信截图_20160128122108-300x100.png" alt="微信截图_20160128122108" width="543" style="padding:5px 5px 12px 0px;">
