# 180 Optical Encoder Motor

<img src="../../../en/electronic-modules/motors/images/180-optical-encoder-motor-1.png" width="300">

### Overview
The 180 optical encoder motor is equipped with an optical encoder that enables high-accuracy control. With two M4 threaded holes on each of the three sides, it can be easily connected to Makeblock mechanical parts  and thus can be used flexibly in combination with various other parts. The customized materials it uses reduce noise and promise large output torque.

### Technical specifications
* Gear reduction ratio: 39.6
* Rated voltage: 7.4V
* No-load current: 240 mA
* Load current: ≤750 mA
* No-load rotating speed: 350 RPM±5%
* Original rotating speed：14,000 RPM±5%
* Start torque: 5 kg·cm
* Rated torque: 800 g·cm
* Output shaft length: 9 mm
* Power：3.7W
* Encoder accuracy: 360

### Connection and installation

<img src="../../../en/electronic-modules/motors/images/180-optical-encoder-motor-2.png" width="300">

<img src="../../../en/electronic-modules/motors/images/180-optical-encoder-motor-3.png" width="300">

### Programming example

<img src="../../../en/electronic-modules/motors/images/180-optical-encoder-motor-4.png" width="300">

