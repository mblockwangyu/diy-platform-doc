# Water Pump Motor – DC 12V-370-04PM

<img src="images/water-pump-motor-dc-12v-370-04pm_微信截图_20160128104754-300x292.png" alt="微信截图_20160128104754" width="300" style="padding:5px 5px 12px 0px;">

### Description

Makeblock water pump motor – DC 12V/370-04PM  has a 12V motor and a
tough thermoplastic body, it is widely used for water priming pump,
automotive pump, experiment pump, bonsai rockery, DIY projects and so
on.

### Specification

- Rated Voltage: DC 12V
- Load: Water
- Water absorption: 1L-1.2L/min
- Current(With load): Less than 320mA
- Flow : 2.0LPM
- Total Size : D27 x 75mm
- Water Hole Diameter: 6.5mm
- Maximum pressure : More than 360mmHg
- Noise:Less than &lt;60dB

### Size Charts(mm)

<img src="images/water-pump-motor-dc-12v-370-04pm_微信截图_20160128105928-300x100.png" alt="微信截图_20160128105928" width="480" style="padding:5px 5px 12px 0px;">
