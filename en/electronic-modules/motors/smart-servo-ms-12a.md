<br />![](https://cdn.nlark.com/yuque/0/2020/jpeg/752133/1592486410687-40fabde6-2bbe-4677-a353-873eb98fb6c7.jpeg#crop=0&crop=0&crop=1&crop=1&height=200&id=y9Yip&originHeight=3097&originWidth=4644&originalType=binary&ratio=1&rotation=0&showTitle=false&size=0&status=done&style=none&title=&width=300)
<a name="RdQFL"></a>
## Description
Smart Servo MS-12A is a new-generation smart servo developed by Makeblock. It surpasses traditional servos mainly in easy control, large rotation angle, continuous rotation, and compatibility with various accessories. Smart Servo MS-12A can be used to assemble multi-joint robots such as humanoid robots, multi-legged spider robots, and mechanical arms. Its continuous rotation allows you to build structures such as controllable rotating platforms and wheel drivers.<br />
<br />In combination with compatible software, Smart Servo MS-12A can record actions and perform complex movements without programming. Besides, it works well with Makeblock programming software, such as mBlock 3, mBlock 5, and the Makeblock app. It also supports Arduino programming.
<a name="vrbUI"></a>
## Specifications
<table  style="border:1px solid black;">
<tr>
<th style="border:1px solid black;">Parameter</th><th style="border:1px solid black;">Description</th>
</tr>
<tr>
<td style="border:1px solid black;">SKU</td><td style="border:1px solid black;">95080</td>
</tr>
<tr>
<td style="border:1px solid black;">Model</td><td style="border:1px solid black;">MS-12A</td>
</tr>
<tr>
<td style="border:1px solid black;">Motor</td><td style="border:1px solid black;">Iron core motor</td>
</tr>
<tr>
<td style="border:1px solid black;">Gear</td><td style="border:1px solid black;">All-metal gear (gear ratio 1:305)</td>
</tr>
<tr>
<td style="border:1px solid black;">Bearing</td><td style="border:1px solid black;">Ball bearing</td>
</tr>
<tr>
<td style="border:1px solid black;">Exterior material</td><td style="border:1px solid black;">PA</td>
</tr>
<tr>
<td style="border:1px solid black;">Installation hole</td><td style="border:1px solid black;">M4 Brass rivet nut
<br>Depth: 6mm</br>
Spacing: 16mm</td>
</tr>
<tr>
<td style="border:1px solid black;">Servo hub</td><td style="border:1px solid black;">Providing 4 M4 threaded holes, which can be used to fit the servo on a servo bracket or another part</td>
</tr>
<tr>
<td style="border:1px solid black;">Mounting bracket</td><td style="border:1px solid black;">Aluminium alloy<br>Providing holes of diameter 4mm and spacing 16mm</br>
Can be fit on Makeblock and Lego parts with screws</td>
</tr>
<tr>
<td style="border:1px solid black;">Size</td><td style="border:1px solid black;">48 x 24 x 41 (mm)</td>
</tr>
<tr>
<td style="border:1px solid black;">Weight</td><td style="border:1px solid black;">72g</td>
</tr>
<tr>
<td style="border:1px solid black;">Communication	</td><td style="border:1px solid black;">Full-duplex serial port UART</td>
</tr>
<tr>
<td style="border:1px solid black;">Speed</td><td style="border:1px solid black;">0.18s/60°, 7.4v</td>
</tr>
<tr>
<td style="border:1px solid black;">Servo resolution</td><td style="border:1px solid black;">4096</td>
</tr>
<tr>
<td style="border:1px solid black;">Torque</td><td style="border:1px solid black;">12kgf.cm</td>
</tr>
<tr>
<td style="border:1px solid black;">Angle</td><td style="border:1px solid black;">0–360°, continuous rotation</td>
</tr>
<tr>
<td style="border:1px solid black;">Indicator</td><td style="border:1px solid black;">RGB LED (programmable in light color and brightness)</td>
</tr>
<tr>
<td style="border:1px solid black;">Connector</td><td style="border:1px solid black;">IN (Red); OUT (White)</td>
</tr>
<tr>
<td style="border:1px solid black;">Operating voltage</td><td style="border:1px solid black;">DC 6V–12.6V</td>
</tr>
<tr>
<td style="border:1px solid black;">Position feedback</td><td style="border:1px solid black;">Magnetic encoding</td>
</tr>
<tr>
<td style="border:1px solid black;">Current at locked rotor</td><td style="border:1px solid black;">2A</td>
</tr>
<tr>
<td style="border:1px solid black;">Operating temperature</td><td style="border:1px solid black;">0℃–80℃</td>
</tr>
<tr>
<td style="border:1px solid black;">Protection</td><td style="border:1px solid black;">Against overcurrent, overvoltage, overtemperature, and undervoltage</td>
</tr>
<tr>
<td style="border:1px solid black;">Dead bandwidth</td><td style="border:1px solid black;">No deadband</td>
</tr>
<tr>
<td style="border:1px solid black;">Feedback</td><td style="border:1px solid black;">Position, speed, temperature, current, voltage</td>
</tr>
<tr>
<td style="border:1px solid black;">Service life</td><td style="border:1px solid black;">90,000 times</td>
</tr>
<tr>
<td style="border:1px solid black;">Compatible mainboard</td><td style="border:1px solid black;">Me Auriga, MegaPi Pro, and NovaPi</td>
</tr>
</table>


<a name="HRmMH"></a>
## Features

- Compatible with various hardware accessories
- Continuous rotation
- Supporting actions recording
- Smart protection: protection is automatically triggered when overtemperature or overcurrent occurs
- Supporting Arduino programming, for which an Arduino library is provided
- Supporting mBlock 3 and mBlock 5, which are intended for users of all ages
- Supporting Python programming on Raspberry Pi
<a name="XwWMo"></a>
## Structures and dimensions
![1.jpg](https://cdn.nlark.com/yuque/0/2021/jpeg/25506042/1638789315438-02ce43f1-639f-4e59-8529-406f45784933.jpeg#clientId=ud0f55298-7275-4&crop=0&crop=0&crop=1&crop=1&from=ui&id=u194f4718&margin=%5Bobject%20Object%5D&name=1.jpg&originHeight=271&originWidth=500&originalType=binary&ratio=1&rotation=0&showTitle=false&size=67456&status=done&style=stroke&taskId=u30febe28-c131-4abd-b139-d8cfc902f94&title=)

<a name="jIH8h"></a>
## Connection
![image.png](https://cdn.nlark.com/yuque/0/2021/png/757912/1639035717215-64d2ede7-fcb2-4105-8618-3f90d847a1f1.png#clientId=u56fc6e20-9117-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=543&id=ub9d3a35e&margin=%5Bobject%20Object%5D&name=image.png&originHeight=543&originWidth=500&originalType=binary&ratio=1&rotation=0&showTitle=false&size=182899&status=done&style=none&taskId=uccfba348-1252-444d-8c8b-2bec4fd5c05&title=&width=500)
<a name="5aBPO"></a>
## Examples
<br />![image.png](https://cdn.nlark.com/yuque/0/2021/png/757912/1639036288889-51dbdae5-eaa8-40a8-99fb-83c70d3ea949.png#clientId=u56fc6e20-9117-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=164&id=ua436fadd&margin=%5Bobject%20Object%5D&name=image.png&originHeight=164&originWidth=500&originalType=binary&ratio=1&rotation=0&showTitle=false&size=79780&status=done&style=none&taskId=u4264ed16-5022-4ad3-8fba-8ac92a77e16&title=&width=500)<br />![image.png](https://cdn.nlark.com/yuque/0/2021/png/757912/1639036455195-a5b79517-b1fc-4caf-8251-21ab7acce022.png#clientId=u56fc6e20-9117-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=317&id=uce39f36c&margin=%5Bobject%20Object%5D&name=image.png&originHeight=317&originWidth=500&originalType=binary&ratio=1&rotation=0&showTitle=false&size=128780&status=done&style=none&taskId=u2cfa7271-eea5-4249-ab3b-384a54110fe&title=&width=500)
<a name="bW4Mu"></a>
## Servo zeroing
<a name="swkut"></a>
### Description
Servo zeroing literally refers to setting a servo to its zero position. Smart Servo MS-12A is delivered with a default zero position. Unlike traditional servos, Smart Servo supports 360-degree rotation, so you can set its zero position on your own.
<a name="CVUA2"></a>
### Steps
The following describes how to set the zero position on Smart Servo MS-12A by using mBlock 3 when it's connected to mBot Ranger. The way of setting the zero position on mBlock 3 is similar to that on mBlock 5.
[Set the zero position on mBlock 3](images/mblock3.mp4)

### Note:

1. Disconnect Smart Servo from the power supply and manually set it to the target zero position and state; alternatively, set it to the target state through software.
1. Connect Smart Servo to the main control board first and update its firmware.
1. Connect the main control board to an external power supply and then turn on its power button to supply power to the servo (the indicator of the servo is normally on in blue when is is powered on). Servo zeroing can't be done if the servo is not powered on.
1. Set the servo to its zero position on the software.
<a name="FwERP"></a>
## Programming
You can program the servo in the block-based, C, C++, or Python language. Block-based programming is easy to learn and can be mastered by most of the users. Whether you are a greenhand or an experienced master of programming, you can soon have a good command of basic knowledge on block-based programming.<br />
<br />Different from block-based programming, traditional C, C++, and Python programming may be a choice for those whom have learned C or Python or other programming languages.<br />
<br />The following describes how to program Smart Servo MS-12A when it is connected to MegaPi Pro.
<a name="Rd2Fc"></a>
### Block-based programming—mBlock
<a name="MTK7c"></a>
#### Configuration

1. Download the version of mBlock 5 applicable to the system running on your PC; read [mBlock 5 Online Help](https://www.yuque.com/makeblock-help-center-en/mblock-5) to undertsand the UIs and basic operation of mBlock 5.
2. On the **Devices** tab, connect MegaPi Pro to mBlock 5 and then find the blocks for Smart Servo MS-12A in the **Power** category.
<a name="zGdlH"></a>
#### Blocks
<table style="border:1px solid black;">
<tr><th style="border:1px solid black;">Block</th><th style="border:1px solid black;">Function</th></tr>
<tr><td style="border:1px solid black;"><img src="../../../en/electronic-modules/motors/images/1.png"></td><td style="border:1px solid black;">**Parameter**: sets the sequence number of the servo, ranging from 1 to 6<br />**Function**: sets the servo to its zero position. </td></tr>
<tr>
<td style="border:1px solid black;"><img src="../../../en/electronic-modules/motors/images/2.png"></td><td style="border:1px solid black;">**Parameter 1**:** **sets the sequence number of the servo, ranging from 1 to 6<br />**Parameter 2**: sets the power, ranging from –100 to +100<br />**Function**：sets the power of the servo. </td>
</tr>
<tr>
<td style="border:1px solid black;"><img src="../../../en/electronic-modules/motors/images/3.png"></td><td style="border:1px solid black;">**Parameter 1**: sets the sequence number of the servo, ranging from 1 to 6<br />**Parameter 2**: sets the servo to a degree (an Integer).<br />**Parameter 3**: sets the speed (0–50 r/min).<br />**Function**: sets the servo to rotate to the specified degree at the specified speed.</td>
</tr>
<tr>
<td style="border:1px solid black;"><img src="../../../en/electronic-modules/motors/images/4.png"></td><td style="border:1px solid black;"> **Parameter 1**: sets the sequence number of the servo, ranging from 1 to 6<br />**Parameter 2**: sets the degrees the servo is to rotate (an Integer).<br />**Parameter 3**: sets the speed (0–50 r/min).<br />**Function**：sets the servo to rotate the specified degrees at the specified speed.</td>
</tr>
<tr>
<td style="border:1px solid black;"><img src="../../../en/electronic-modules/motors/images/5.png"></td><td style="border:1px solid black;"> **Parameter**: sets the sequence number of the servo, ranging from 1 to 6<br />**Function**: obtains the current, voltage, speed, degree, or temperature</td>
</tr>
</table>


<a name="dSSbC"></a>
#### Example program
When the following program is executed, the first Smart Servo MS-12A connected to MegaPi Pro rotates to 30° within 1 second, then rotates to 120° within another second and repeat this whole process.<br />![image.png](https://cdn.nlark.com/yuque/0/2021/png/25506042/1638870961032-128d245c-b2d3-4a87-84d7-fbab5cab6588.png#clientId=u2e663e79-ff95-4&crop=0&crop=0&crop=1&crop=1&from=paste&height=234&id=u4206c708&margin=%5Bobject%20Object%5D&name=image.png&originHeight=234&originWidth=441&originalType=binary&ratio=1&rotation=0&showTitle=false&size=20834&status=done&style=stroke&taskId=uc1e88446-abe6-4e71-8cdc-14ff4cdb6cc&title=&width=441)
<a name="XCAjT"></a>
### C/C++programming—Arduino IDE
<a name="6UUOz"></a>
#### Configuration
Makeblock-Library-master is required to control the motor in programming Smart Servo MS-12A. Refer to the following steps to configure the Arduino IDE.
##### IDE configuration

To program Orion with C/C++, you need Arduino IDE (click to learn about Arduino). Perform the following steps to configure the Arduino IDE:<br>
1）Download and install Arduino IDE.<br>
2）Download and install the CH340 driver required by Orion.<br>
● Windows <br>
● MacOS<br>
3）Download Makeblock library from GitHub, see the GitHub page on how to download and use the library files.<br>
4）Connect Orion to the computer with a USB cable, select the corresponding board (Orion is developed based on Arduino Uno) and the corresponding COM port, then select and upload the sample program in MakeBlockDriver and see how it is executed.<br>

#### Function description
<table style="border:1px solid black;">
<tr><th style="border:1px solid black;">Function</th><th style="border:1px solid black;">Description</th></tr>
<tr>
<td style="border:1px solid black;">mysmartservo.begin(115200)<br>
mysmartservo.assignDevIdRequest()</td><td style="border:1px solid black;">Sets the baud rate and initializes the smart servo.</td>
</tr>
<tr>
<td style="border:1px solid black;">mysmartservo.setRGBLed(index,R,G,B)</td><td style="border:1px solid black;">Sets the color of the LED.
<br>Parameter: servo number，R，G，B</br>
R，G，B: 0–255</td>
</tr>
<tr>
<td style="border:1px solid black;">mysmartservo.setInitAngle(index)</td><td style="border:1px solid black;">Resets the servo.<br>
Parameter: servo number</td>
</tr>
<tr>
<td style="border:1px solid black;">mysmartservo.moveTo(index,position,speed)</td><td style="border:1px solid black;">Sets the servo to rotate to the specified degree at the specified speed.<br>
Parameter: servo number<br>
Degree: an integer<br>
Speed: –50–+50</td>
</tr>
<tr>
<td style="border:1px solid black;">mysmartservo.move(index,position,speed)</td><td style="border:1px solid black;">Sets the servo to rotate the specified degrees at the specified speed.<br>
Parameter: servo number<br>
Degree: an integer<br>
Speed: –50–+50</td>
</tr>
<tr>
<td style="border:1px solid black;">mysmartservo.setPwmMove(index,speed)</td><td style="border:1px solid black;">Sets the servo to rotate at a specified speed modulated by PWM.<br>
Parameter: servo number<br>
Speed: –255–+255</td>
</tr>
<tr>
<td style="border:1px solid black;">mysmartservo.getSpeedRequest(index)<br>
mysmartservo.getTempRequest(index)<br>
mysmartservo.getCurrentRequest(index)<br>
mysmartservo.getCurrentRequest(index)<br>
mysmartservo.getAngleRequest(index)</td><td style="border:1px solid black;">Obtains the speed, temperature, current, voltage, or degree<br>
Parameter: servo number</td>
</tr>
</table>

<a name="OB7r9"></a>
### Python programming
<a name="MdGqU"></a>
#### Configuration
To program the servo, perform the following steps to configure a Python environment:

1. Connect MegaPi Pro to Raspberry Pi; connect Me RJ25 Adapter to a RJ25 Port on MegaPi Pro; and connect Smart Servo MS-12A to Me RJ25 Adapter.
1. Install the latest Makeblock library _pip3 install makeblock --upgrade_ for Rasepberry Pi.
1. Create a new Python file with the suffix ".py".
1. Compile the Python file.
1. Execute the Python file, for example, _python123.py_.
<a name="43VWZ"></a>
#### Function description
<table style="border:1px solid black;">
<tr><th style="border:1px solid black;">Function</th><th style="border:1px solid black;">Description</th></tr>
<tr>
<td style="border:1px solid black;">SmartServo(port)</td><td style="border:1px solid black;">Creates the object—Smart Servo<br>
Port: port, MegaPiPro.PORT5 (default)</td>
</tr>
<tr>
<td style="border:1px solid black;">run(index,pwm)</td><td style="border:1px solid black;">Sets the power<br>
Index: servo number; pwm: PWM Duty Cycle (–100–+100)</td>
</tr>
<tr>
<td style="border:1px solid black;">move_to(index,position,speed)</td><td style="border:1px solid black;">Sets the servo to rotate to the specified degree at the specified speed<br>
Index: servo number; position: the target degree; speed: rotating speed (1–50 r/min)</td>
</tr>
<tr>
<td style="border:1px solid black;">move(index,position,speed)</td><td style="border:1px solid black;">Sets the servo to rotate the specified degrees at the specified speed.<br>
Index: servo number; position: number of degrees to rotate; speed: rotating speed (1–50 r/min)</td>
</tr>
<tr>
<td style="border:1px solid black;">set_zero(index)</td><td style="border:1px solid black;">Sets the current position as the zero position.<br>
index: servo number</td>
</tr>
<tr>
<td style="border:1px solid black;">set_led(index,red,green,blue)</td><td style="border:1px solid black;">Index: servo number; red: red; green: green; blue: blue</td>
</tr>
</table>

<a name="z0FEs"></a>
#### Example 1
When the following program is executed, servo 1 on MegaPi Pro rotates at the speed of 10 r/min for 2 seconds and holds 1 second, and then rotates at the speed of –10 r/min for 2 seconds and holds 1 second. The process is repeated.
```python
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
smartservo = board.SmartServo()
while True:
    smartservo.run(1,10)
    sleep(2)
    smartservo.run(1,0)
    sleep(1)
    smartservo.run(1,-10)
    sleep(2)
    smartservo.run(1,0)
    sleep(1)
```
<a name="NynfA"></a>
#### Example 2
When the following program is executed, servo 1 on MegaPi Pro rotates by 720 degrees at the speed of 50 r/min and then stops. The process is repeated.
```python
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
smartservo = board.SmartServo()
while True:
    smartservo.move_to(1,720,50)
    sleep(4)
    smartservo.move_to(1,0,50)
    sleep(4)
```
<a name="34bU3"></a>
## Purchase
[Contact us](https://education.makeblock.com/contact-us/) if you want to take it home. 
