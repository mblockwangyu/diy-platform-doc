# DC Motor-37 12V


![](images/dc-motor-37-12v_DC-Motor-37-12V.jpg)

<img src="images/dc-motor-37-12v_微信截图_20160128155423.png" alt="微信截图_20160128155423" width="288" style="padding:5px 5px 12px 0px;">

### Description

Dc motors are the most commonly used motors in Makeblock Platform. With Makeblock DC Motor-37 Brackets, they may be easy to connect to Makeblock structural components.

### Specification

The types of Makeblock 37mm DC Motors are as follow:

<img src="images/dc-motor-37-12v_微信截图_20160128160057.png" alt="微信截图_20160128160057" width="794" style="padding:5px 5px 12px 0px;">

### Size Chart (mm)

<img src="images/dc-motor-37-12v_微信截图_20160128155640.png" alt="微信截图_20160128155640" width="748" style="padding:5px 5px 12px 0px;">

### Demo

<img src="images/dc-motor-37-12v_微信截图_20160128155833.png" alt="微信截图_20160128155833" width="729" style="padding:5px 5px 12px 0px;">

<img src="images/dc-motor-37-12v_微信截图_20160128155948.png" alt="微信截图_20160128155948" width="430" style="padding:5px 5px 12px 0px;">

 
