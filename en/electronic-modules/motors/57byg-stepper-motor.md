# 57BYG Stepper Motor

<img src="images/57byg-stepper-motor_微信截图_20160128152218.png" alt="微信截图_20160128152218" width="370" style="padding:5px 5px 12px 0px;">

### Description

Makeblock 57BYG Stepper Motor has higher torque and faster response speed than 42 stepper motor.  With Makeblock 57BYG Stepper Motor Bracket,  they may be easy to connect to Makeblock structural components.

### Features

- high output torque
- low noise
- low power consumption

### Specification

- Weight: 700 g
- Motor length： 56mm
- Output shaft length： 8mm D shaft
- Pin count: 4
- Step Angle (degrees) :1.8°
- Holding Torque : 1.2N.m
- Phase current : 2.8A

### Size Charts(mm)

<img src="images/57byg-stepper-motor_微信截图_20160128152447.png" alt="微信截图_20160128152447" width="575" style="padding:5px 5px 12px 0px;">

### Demo

<img src="images/57byg-stepper-motor_微信截图_20160128152516.png" alt="微信截图_20160128152516" width="580" style="padding:5px 5px 12px 0px;">

<img src="images/57byg-stepper-motor_微信截图_20160128152547.png" alt="微信截图_20160128152547" width="559" style="padding:5px 5px 12px 0px;">
