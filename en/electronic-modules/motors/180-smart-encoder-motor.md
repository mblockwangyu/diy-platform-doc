# 180 Smart Encoder Motor

<img src="../../../en/electronic-modules/motors/images/180-smart-encoder-motor-1.png" width="150">

### Overview
The 180 smart encoder motor is an entry-level mBuild encoder motor. It works with NovaPi to serve as the chassis motor for MakeX Challenge.

### Technical specifications
* Gear reduction ratio: 39.43
* Rated voltage: 12V
* No-load current: 350 mA
* Communication port and protocol: serial communication
* No-load rotating speed: 580±10% RPM

### Features

* Full-metal gear set, rugged and durable
* Built-in encoder, ensuring accurate control over robot motion
* Mounting holes for bracket-free mounting on Makeblock metal structural parts
* Mounting holes on multiple sides, ensuring flexible mounting
* High torque and high power
* Smart power interface


### Product appearance

<img src="../../../en/electronic-modules/motors/images/180-smart-encoder-motor-2.png" width="300">

①: Smart motor interface<br>
②: Structural part interface

### Installation description

The 180 smart encoder motor is compatible with Makeblock Maker's Platform mechanical parts. With M4 threaded holes, it can be easily mounted.

### Software
The following table describes the blocks for the 180 smart encoder motor.

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:top}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:top}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">Name</th>
    <th class="tg-7g6k">Description</th>
    <th class="tg-7g6k">Remarks</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../en/electronic-modules/motors/images/180-smart-encoder-motor-3.png" width="200"></td>
    <td class="tg-c6of">Sets the power output of the motor</td>
    <td class="tg-c6of">Power range: –100–+100%<br>A positive value indicates forward running,<br> and a negative one indicates reverse running. <br>When value you set exceeds the limits,<br> the motor runs at the upper or lower limit, <br>and the nearest limit value is used.</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../en/electronic-modules/motors/images/180-smart-encoder-motor-4.png" width="200"></td>
    <td class="tg-c6of">Rotates the motor the specified<br> degrees relative to the current<br> position at the specified speed</td>
    <td class="tg-c6of">A positive degree value indicates forward running,<br> and a negative one indicates reverse running.<br> The speed setting range is 0 to 300. <br>When a value you set exceeds the limits,<br> the motor runs at the highest speed.</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../en/electronic-modules/motors/images/180-smart-encoder-motor-5.png" width="200"></td>
    <td class="tg-c6of">Rotates the motor the specified<br> degrees relative to the default zero-point<br> position at the specified speed</td>
    <td class="tg-c6of">A positive degree value indicates forward running,<br> and a negative one indicates reverse running.<br> The speed setting range is 0 to 300.<br> When a value you set exceeds the limit, <br>the motor runs at the highest speed.</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../en/electronic-modules/motors/images/180-smart-encoder-motor-6.png" width="200"></td>
    <td class="tg-ycr8">Rotates the motor at the specified speed</td>
    <td class="tg-ycr8">A positive speed value indicates forward running,<br> and a negative one indicates reverse running.<br> Setting range: –300–+300<br> When a value you set exceeds the limits,<br> the motor runs at the highest speed.</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../en/electronic-modules/motors/images/180-smart-encoder-motor-7.png" width="200"></td>
    <td class="tg-ycr8">Obtains the current rotating speed<br> of the motor</td>
    <td class="tg-i81m">/</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../en/electronic-modules/motors/images/180-smart-encoder-motor-8.png" width="200"></td>
    <td class="tg-ycr8">Obtains the current angle of the motor<br> relative to the zero-point position</td>
    <td class="tg-i81m">/</td>
  </tr>
</table>

### Programming example

<img src="../../../en/electronic-modules/motors/images/180-smart-encoder-motor-9.png" width="300">