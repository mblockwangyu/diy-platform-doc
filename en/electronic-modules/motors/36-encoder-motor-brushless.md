# 36 Brushless Encoder Motor

<img src="../../../en/electronic-modules/motors/images/36-encoder-motor-brushless-1.png" width="300">

### Overview
The 36 brushless encoder motor can work in combination with the NovaPi main control board to provide powerful motion capabilities for robots. 

### Specifications
* Dimensions：115.3 mm × φ36 mm 
* Operating voltage: 10V–15V
* No-load current: 750 mA
* Rated load torque: 5 kg·cm
* No-load rotating speed: 300±10%RPM
* Communication port and protocol:serial communication

### Features
* Full-metal gear set, rugged and durable
* Built-in encoder, ensuring accurate control over robot motion
* Mounting holes for bracket-free mounting
* High torque and high power
* Smart power interface

### Product appearance

<img src="../../../en/electronic-modules/motors/images/36-encoder-motor-brushless-2.png" width="300">

①: Structural part interface<br>
②: Smart motor interface

### Installation and application precautions
* The 36 brushless encoder motor is compatible with Makeblock mechanical parts, providing four M4 threaded holes for installation.
* Use screws of the proper length to prevent damage to the motor gearbox.

### Software
The following table describes the blocks for the 36 brushless encoder motor.

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">Name</th>
    <th class="tg-7g6k">Description</th>
    <th class="tg-7g6k">Remarks</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../en/electronic-modules/motors/images/36-encoder-motor-brushless-3.png" width="200"></td>
    <td class="tg-c6of">Sets the power output of <br>the encoder motor</td>
    <td class="tg-c6of">Power range: –100–+100%<br>A positive value indicates forward running,<br> and a negative one indicates reverse running. <br>When value you set exceeds the limits,<br> the motor runs at the upper <br>or lower limit, and the nearest <br>limit value is used.</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../en/electronic-modules/motors/images/36-encoder-motor-brushless-4.png" width="200"></td>
    <td class="tg-c6of">Rotates the motor the specified<br> degrees relative to the current<br> position at the specified speed</td>
    <td class="tg-c6of">A positive degree value indicates forward running,<br> and a negative one indicates reverse running.<br> The speed setting range is 0 to 300.<br> When a value you set exceeds the limits, <br>the motor runs at the highest speed.</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../en/electronic-modules/motors/images/36-encoder-motor-brushless-5.png" width="200"></td>
    <td class="tg-c6of">Rotates the motor the specified<br> degrees relative to the default<br> zero-point position at the specified speed</td>
    <td class="tg-c6of">A positive degree value indicates forward running,<br> and a negative one indicates reverse running.<br> The speed setting range is 0 to 300.<br> When a value you set exceeds the limit,<br> the motor runs at the highest speed.</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../en/electronic-modules/motors/images/36-encoder-motor-brushless-6.png" width="200"></td>
    <td class="tg-ycr8">Rotates the motor at the specified speed</td>
    <td class="tg-ycr8">A positive speed value indicates forward running,<br> and a negative one indicates reverse running.<br> Setting range: –300–+300<br> When a value you set exceeds the limits, <br>the motor runs at the highest speed.</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../en/electronic-modules/motors/images/36-encoder-motor-brushless-7.png" width="200"></td>
    <td class="tg-ycr8">Obtains the current rotating speed of the motor</td>
    <td class="tg-i81m">/</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../en/electronic-modules/motors/images/36-encoder-motor-brushless-8.png" width="200"></td>
    <td class="tg-ycr8">Obtains the current angle of the motor<br> relative to the zero-point position</td>
    <td class="tg-i81m">/</td>
  </tr>
</table>


#### Most basic way of using the blocks

<img src="../../../en/electronic-modules/motors/images/36-encoder-motor-brushless-9.png" width="300">