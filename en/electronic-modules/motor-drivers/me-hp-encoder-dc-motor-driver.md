# Me High-Power Encoder/DC Motor Driver

<img src="images/me-hp-encoder-dc-motor-driver.png" width="352" style="padding:5px 5px 12px 0px;">

### Overview

Me High-Power Encoder/DC Motor Driver can drive two 36 DC/encoder motors or DC motors and implement precise control over the speed and position of encoder motors. With the software provides at the Makeblock official website, you can modify the proportional–integral–derivative (PID) parameters to optimize the operation of motors in various environments. The connector of this module is marked red and white, which indicates that the input voltage can be 6V to 12V, or 5V. When using this module, connect it to a port marked red or white on the main control board.


### Features

- Two DIP switches, facilitating operation and control
- Main control chip can be manually reset
- Built-in resettable 8A fuse, providing short circuit protection
- XT30PW-M terminals, supporting large current and providing the function for preventing reverse connection
- High-power MOS tube, providing a maximum driving capability of 20A
- Six M4 screw installing holes, facilitating the installation
- RJ25 ports and pins, supporting safe and flexible connection
- Supporting ports marked red or white when connected to a main control board
- Implementing precise control over position, speed, and direction
- Capable of driving two 36 encoder motors or high-power DC motors simultaneously

### Technical specifications

- Number of motor channels: 2
- Operation voltage: 6V–12V DC
- Continuous output current: 8A
- Controller voltage: 5V
- Main control chip: ATmega328P
- Operation temperature: –40 to +85℃
- Control mode: I²C, digital I/O, analog input
- Device address setting mode: manual setting through DIP switches
- Dimensions: 100 mm x 47 mm x 18 mm (width × height × depth)

### Operation instruction

#### 1. DIP switch status
You need to toggle the DIP switch to the corresponding status when using this module.
 
<img src="images/me-hp-encoder-dc-motor-driver-1.png" width="352" style="padding:5px 5px 12px 0px;">

#### 2. Blocks on mBlock

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k" width="50%;">Name</th>
    <th class="tg-7g6k" width="50%;">Description</th>
  </tr>
  <tr>
    <td class="tg-3xi5" width="50%;"><img src="images/me-hp-encoder-dc-motor-driver-2.png" width="300"></td>
    <td class="tg-c6of" width="50%;">Used to control an encoder or DC motor. <br>You need to toggle the DIP switch to the corresponding status when using this module. <br>
    Power range: 0–100</td>
  </tr>
  <tr>
    <td class="tg-3xi5" width="50%;"><img src="images/me-hp-encoder-dc-motor-driver-3.png" width="300"></td>
    <td class="tg-c6of" width="50%;">Used to control an encoder or DC motor. <br>You need to toggle the DIP switch to the corresponding status when using this module. <br>
    Speed range: 0–240</td>
  </tr>
  <tr>
    <td class="tg-3xi5" width="50%;"><img src="images/me-hp-encoder-dc-motor-driver-4.png" width="300"></td>
    <td class="tg-c6of" width="50%;">Used to control an encoder motor. <br>You need to toggle the DIP switch to the corresponding status when using this module. <br>
    Speed range: 0–240</td>
  </tr>
</table>
<br>

##### Example program

<img src="images/me-hp-encoder-dc-motor-driver-5.png" width="500">

#### 3. Blocks on mBlock 5

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k" width="50%;">Name</th>
    <th class="tg-7g6k" width="50%;">Description</th>
  </tr>
  <tr>
    <td class="tg-3xi5" width="50%;"><img src="images/me-hp-encoder-dc-motor-driver-6.png" width="300"></td>
    <td class="tg-c6of" width="50%;">Used to control an encoder or DC motor. <br>You need to toggle the DIP switch to the corresponding status when using this module. <br>
    Power range: 0–100</td>
  </tr>
  <tr>
    <td class="tg-3xi5" width="50%;"><img src="images/me-hp-encoder-dc-motor-driver-7.png" width="300"></td>
    <td class="tg-c6of" width="50%;">Used to control an encoder or DC motor. <br>You need to toggle the DIP switch to the corresponding status when using this module. <br>
    Speed range: 0–240</td>
  </tr>
  <tr>
    <td class="tg-3xi5" width="50%;"><img src="images/me-hp-encoder-dc-motor-driver-8.png" width="300"></td>
    <td class="tg-c6of" width="50%;">Used to control an encoder motor. <br>You need to toggle the DIP switch to the corresponding status when using this module. <br>
    Speed range: 0–240</td>
  </tr>
  <tr>
    <td class="tg-3xi5" width="50%;"><img src="images/me-hp-encoder-dc-motor-driver-9.png" width="300"></td>
    <td class="tg-c6of" width="50%;">Used to report the real-time rotating speed of the motor</td>
  </tr>
  <tr>
    <td class="tg-3xi5" width="50%;"><img src="images/me-hp-encoder-dc-motor-driver-10.png" width="300"></td>
    <td class="tg-c6of" width="50%;">Used to report the real-time angle of the motor</td>
  </tr>
</table>
<br>

##### Example program

See the preceding mBlock example program.