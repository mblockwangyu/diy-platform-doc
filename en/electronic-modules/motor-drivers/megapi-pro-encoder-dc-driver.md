# MegaPi Pro Encoder/DC Motor Driver

<img src="images/megapi-pro-encoder-dc-driver-1.png" width="200">

### Overview

MegaPi Pro Encoder/DC Motor Driver can drive two DC motors or one encoder motor. It adopts the 2 × 8 pin connection mode and thus can be easily mounted on MegaPi Pro.

### Technical specifications

* Motor driver: MP80495
* Number of motor channels: 2
* Minimum operating voltage: 6V
* Maximum operating voltage: 12V
* Logical voltage: 5V
* Rated operating current of each channel: 3A
* Peak operating current: 5.5A
* Dimensions: 30 mm x 15 mm (width × height)


### Features

* Supports motors with the operating voltage range of 6V to 12V
* Provides a maximum operating current of 3A when the power supply is 12V 
* Equipped with overvoltage, overcurrent, and overtemperature protection, which ensures safe application
* Able to drive an encoder drive (using the white interface) or two DC drivers (using the green interface)
* Equipped with a colored socket and plug that prevent plugging mistakes
* Provides the transient voltage suppressor (TVS) protection on the encoder motor dirver
* Small in volume and thus easy to replace


### Connection

<img src="images/megapi-pro-encoder-dc-driver-2.png" width="400">