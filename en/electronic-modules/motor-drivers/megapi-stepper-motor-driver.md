# MegaPi Stepper Motor Driver

<img src="../../../en/electronic-modules/motor-drivers/images/megapi-stepper-motor-driver-1.png" width="347" style="padding:5px 5px 12px 0px;">

### Description

This module is designed to drive stepper motors. It uses the DRV8825 chip and has a
maximum driving current of 2.5 A. DRV8825 is a complete micro-stepping motor
driver, equipped with a built-in converter for easy operation. It is designed to drive bipolar stepper
motors that work in full, 1/2, 1/4, 1/8/ 1/16, or 1/32 step modes. An on-board
potentiometer is provided, which allows you to change the current of a motor easily.

### Features

- Compatible with 4-wire bipolar stepper motors
- Potentiometer, used to control the current of a stepper motor
- On-board heat sink, facilitating heat dissipation
- Capable of driving one-step stepper motors (green interface)
• Color-coded male and female pins, ensuring correct connection
• Small size, facilitating connection

### Specifications
- Motor driver: DRV8825
- Output current：1.75 A (with proper heat sinking at 24 V and 25°C）
- Max current: 2.5 A (pay attention to heat dissipation)
- Driver voltage: 8.2 V–45 V
  **Note:** The maximum power supply voltage of MegaPi is 12 V.
- Logic voltage: 5 V
- Dimensions: 1.5 cm x 2.0 cm