# Me 130 DC Motor

<img src="images/me-130-dc-motor_微信截图_20160203154056.png" alt="微信截图_20160203154056" width="352" style="padding:5px 5px 12px 0px;">

### Description

Me 130 DC Motor Pack is a funny package that comes with a 130g DC motor
with circuit board for easy-using and protecting the motor, four nylon
studs for support, and a mini fan. You can put it all together to build
a electric fan, kids would love it! And by adding some other Makeblock
components, you can build funny projects such as cooling machine, smart
fan, bluetooth control, bubble machine.

### Features

-   Build funny projects such as cooling machine, smart fan, bluetooth control, bubble machine
-   Support Scratch, Ardublock, Arduino programming and App(IOS&Android) provided
-   wide voltage range from 0V to 12V
-   two indicators for reflecting the direction of the motor
-   a built-in resettable fuses protects the motor from over current or any shortage by any reason

### 130 DC Motor Specification

-   Rated voltage:12V DC
-   voltage range: 0-12V
-   No Load Speed: 16000±10%rpm
-   No Load Current: 90±10% mA
-   working temperture: -10℃\~50℃
-   working humidity: 5%\~85%RH

### Useful Links

-   Cooling Machine: <http://openlab.makeblock.com/topic/585e0a5b26fc9fe837ad85b6>
-   Bluetooth control: [http://openlab.makeblock.com/topic/56e023bcfe1510290c6e2bc7](http://openlab.makeblock.com/topic/56e28e176ac42c1b332d51f5)
-   Bubble Machine: [http://openlab.makeblock.com/topic/56e02437fe1510290c6e2bd1](http://openlab.makeblock.com/topic/56e28b4f6ac42c1b332d50c4)
