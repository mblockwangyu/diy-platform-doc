# MegaPi Encoder/DC Driver V1

<img src="images/megapi-encoder-dc-driver-v1-1.png" width="200">

### Overview

MegaPi Encoder/DC Motor Driver V 1 can drive two DC motors or one encoder motor. It adopts the 2 × 8 pin connection mode and thus can be easily mounted on MegaPi.

### Technical specifications

* Motor driver: MP80495
* Number of motor channels: 2
* Minimum operating voltage: 6V
* Maximum operating voltage: 12V
* Logical voltage: 5V
* Rated operating current of each channel: 3A
* Peak operating current: 5.5A
* Dimensions: 30 mm x 15 mm (width × height)


### Features

* Supports motors with the operating voltage range of 6V to 12V
* Provides a maximum operating current of 3A (a maximum peak operating current of 5.5A) when the power supply is 12V
* Equipped with overvoltage, overcurrent, and overtemperature protection, which ensures safe application
* High rotating speed at start
* Provides the offline function
* Photoelectric isolated signal input/output
* Provides the overvoltage, undervoltage, overcurrent, and phase-to-phase short-circuit protection


### Connection

<img src="images/megapi-encoder-dc-driver-v1-2.png" width="400">