# Me Encoder Motor Driver

<img src="images/me-encoder-motor-driver_encoderimage2.png" alt="微信截图_20160203150524" width="361" style="padding:5px 5px 12px 0px;">

### Overview

Supporting two-channel DC encoder motor, the Me DC Encoder Motor Driver module includes a MCU and a motor driver chip. The MCU has built-in PID algorithm, and implements accurate control to the speed and direction of motor.  

The PID parameters of motor can be controlled through the software provided on the official website of Makeblock, so that it can reach the best working condition in different environment. This module is designed to control the motor easily, quickly and accurately. 

If you want to better use this module, for example, the self-balancing cart. You need to adjust the PID and some parameters, and this requires the user to have relevant basis. The module can also be applied to the learning of PID algorithm, and opens the SPI and serial port for the convenience of second development, so it is equivalent to a development board of small motor. Its red ID means that its input voltage is 6\~12V and should be connected to the port with red ID on Makeblock Orion.

### Technical specifications

- Motor channels: 2  
- Input voltage: 6V-12V DC  
- Single channel maximum persistent current: 1.2A  
- Single channel peak current: 3.2A  
- MCU operating voltage: 5V DC  
- Communication port: I²C  
- Outlet port: SPI – serial port  
- Motor ports: M+, M-, GND, 5V, ENC1, ENC2  
- Operating temperature: -40\~85℃  
- Module size: 67.5 x 32 x 18 mm (L x W x H)

### Functional characteristics

- White area of module is the reference area to contact metal beams  
- Control position, speed and direction of motor precisely  
- Provide real time position and speed feedback  
- Equipped with effective MOSFET H-bridge-based motor driver module IC  
- Over-current protection and anti-reverse protection  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Double-channel motor feedback forwarding/reversing indicator with MCU Reset key  
- Built with pin-type port to support most development boards including Arduino series

### Pin definition

The port of Me DC Encoder Motor Driver has 12 pins, and their functions are as follows:

<img src="images/me-encoder-motor-driver_微信截图_20160203150905.png" alt="微信截图_20160203150905" width="720" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25**  

Since the port of Me DC Encoder Motor Driver has red ID, you need to connect the port with red ID on Makeblock Orion when using RJ25 port.

Taking Makeblock Orion as example, you can connect it to ports No. 1 and 2 as follows:

<img src="images/me-encoder-motor-driver_微信截图_20160203150949.png" alt="微信截图_20160203150949" width="358" style="padding:5px 5px 12px 0px;">

> Figure 1 Connecting Me DC Encoder Motor Driver to Makeblock Orion

● **Connecting with Dupont wire**  

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its SCL and SDA pins should be connected to I2C port, that is, A5 and A4 port respectively as follows:

<img src="images/me-encoder-motor-driver_微信截图_20160203151003.png" alt="微信截图_20160203151003" width="343" style="padding:5px 5px 12px 0px;">

> Figure 2 Connecting Me DC Encoder Motor Driver to Arduino UNO

### Programming guide

● **Arduino programming**

If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control the Me DC Encoder motor module.  

This program serves to make the motor rotating clockwise in three speeds through Arduino programming.

<img src="images/me-encoder-motor-driver_微信截图_20160203151124.png" alt="微信截图_20160203151124" width="456" style="padding:5px 5px 12px 0px;">

<img src="images/me-encoder-motor-driver_微信截图_20160203151209.png" alt="微信截图_20160203151209" width="804" style="padding:5px 5px 12px 0px;">

> For API document, please visit: [API Documentation of Me Encoder Motor](http://learn.makeblock.com/api-documentation-of-me-encoder-motor/)

### Principle analysis

The Me DC Encoder Motor Driver needs to work with encoder motor. The major difference between encoder motor and DC motor is the motor rotation feedback, which is achieved by encoder. Currently, there are 2 types of encoder: magnetic encoder and optical encoder. Being fixed on motor shaft, the magnet ring or coding disc will rotate with the motor.
Meanwhile, the detection device will detect the changes occur to the magnetic pole or the optical grating, convert the changes to pulse signal, and eventually deliver the signal to the encoder motor driver.

The rotation number of the motor can be obtained by calculating the number of the pulse signal, speed of the motor by the number of the pulse signal per unit time.

### Schematic

<img src="images/me-encoder-motor-driver_encode.png" alt="encode" width="1270" style="padding:5px 5px 12px 0px;">
