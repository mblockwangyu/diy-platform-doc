# Me Dual DC Motor Driver

<img src="images/me-dual-motor-driver_微信截图_20160203153331.png" alt="微信截图_20160203153331" width="352" style="padding:5px 5px 12px 0px;">

### Overview

The Me Dual Motor Driver module can drive two DC motors by the onboard RJ25 port with power supply of 6V-12V, and it also has the PWM speed regulation function. The IC used in the module is an efficient, low heat dissipated MOSFET with over-current protection function. Its red ID means that it should be connected to the port with red ID on Makeblock Orion.

### Technical specifications

- Operating voltage: 6-12V DC  
- Single channel sustained output current: 1.2A  
- Single channel peak output current: 3.2A  
- Number of motor channels: 2  
- Type of drive motor: DC motor  
- Control mode: 2 I/O (direction and PWM speed regulation)  
- Module size: 56 x 32 x 18 mm (L x W x H)

### Functional characteristics

- White area of module is the reference area to contact metal beams  
- Equipped with effective MOSFET H-bridge-based motor driver module IC  
- Maximum 1.2A sustained current for each motor (peak value 3.2A)  
- Over-current protection (OCP)  
- Support Arduino IDE programming and provide runtime library to
simplify programming  
- Support mBlock GUI programming, and applicable to users of all ages  
- Adopt RJ25 port for easy connection  
- Provide pin-type port to support most Arduino Baseboards

### Pin definition

The port of Me Dual Motor Driver has four pins, and their functions are as follows:

<img src="images/me-dual-motor-driver_微信截图_20160203153453.png" alt="微信截图_20160203153453" width="723" style="padding:5px 5px 12px 0px;">

### Wiring mode

● **Connecting with RJ25**  

Since the port of Me Dual Motor Driver has red ID, you need to connect the port with red ID on Makeblock Orion when using RJ25 port. Taking Makeblock Orion as example, you can connect it to ports No. 1 and 2 as
follows:

<img src="images/me-dual-motor-driver_微信截图_20160203153627.png" alt="微信截图_20160203153627" width="357" style="padding:5px 5px 12px 0px;">

> Figure 1: Connecting Me DC Motor Driver to Makeblock Orion

● **Connecting with Dupont wire**  

When the Dupont wire is used to connect the module to the Arduino UNO Baseboard, its PWM and DIR pins should be connected to digital ports as follows:

<img src="images/me-dual-motor-driver_微信截图_20160203153657.png" alt="微信截图_20160203153657" width="352" style="padding:5px 5px 12px 0px;">

> Figure 2: Connecting Me DC Motor Driver to Arduino Uno

> Note: When Dupont wire is used, pin header should be welded on the module

### Guide to programming

● **Arduino programming**  

If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control the Me Dual Motor Driver. 

This program serves to make two DC motors rotating simultaneously in the same or inverse direction through
Arduino programming.

<img src="images/me-dual-motor-driver_微信截图_20160203153738.png" alt="微信截图_20160203153738" width="568" style="padding:5px 5px 12px 0px;">

<img src="images/me-dual-motor-driver_微信截图_20160203153758.png" alt="微信截图_20160203153758" width="742" style="padding:5px 5px 12px 0px;">

● **mBlock programming**  

Me DC Motor Driver supports the mBlock programming environment and its instructions are introduced as
follows:

<img src="images/me-dual-motor-driver_微信截图_20160203153827.png" alt="微信截图_20160203153827" width="721" style="padding:5px 5px 12px 0px;">

The following is the effect to make two DC motors rotating in the same or inverse direction respectively:

<img src="images/me-dual-motor-driver_微信截图_20160203153849.png" alt="微信截图_20160203153849" width="723" style="padding:5px 5px 12px 0px;">

### Principle analysis

Drive principle of DC motor

The DC motor is added with drive circuit in order to better control the rotational direction and speed of motor. As a common circuit designed to control the DC motor, the H-bridge drive circuit is mainly to implement forward and reverse drive of DC motor.

Its shape is similar to the letter H, the location of four switches (MOSFET) are called the bridge arm, and the DC motor serving as load is like a bridge across the top, so it is called the H-bridge. To drive a lot of integrated motors, the core of drive chip is the
H-bridge. The opening and closing states of 4 switches can be combined into 4 working states for the motor: forwarding, reversing, braking and coasting.

### Schematic

<img src="images/me-dual-motor-driver_Me-Dual-Motor-Driver-1.png" alt="Me Dual Motor Driver" width="1499" style="padding:5px 5px 12px 0px;">
