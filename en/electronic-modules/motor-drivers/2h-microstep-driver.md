# Me 2H Microstep Driver

<img src="images/2h-microstep-driver_微信截图_20160203145533.png" alt="微信截图_20160203145533" width="336" style="padding:5px 5px 12px 0px;">

### Overview

Me 2H Microstep Driver is a kind of 2-phase hybrid stepper motor driver, and adopts the power supply of DC 12\~36V. It is suitable for driving the 2-phase hybrid stepper motor with voltage of 24\~36V and current less than 2.0A. This driver uses the full digital current loop to implement microstepping control, so the motor features small torque ripple, low speed and smooth running, low vibration and noise. When running at high speed, its output torque is relatively high, and its positioning accuracy is high. It can be used in engraving machine, CNC machine tools, packaging machinery, transmission equipment, and other high resolution devices.

### Technical specifications

- Input voltage: 12\~36V DC  
- Input current: less than 2 A  
- Output current: 0.4\~2.83 A  
- Power consumption: 40 W  
- Internal fuse: 6 A  
- Humidity: dew and water drop are not allowed  
- Dimension: 96 x 56 x 35mm (L x W x H)

### Functional characteristics

- Average current control, 2-phase sinusoidal current drive output  
- 8 grades of microstepping and semi-flow function  
- 8 grades of output phase current settings  
- Large torque at high speed  
- High star-up rotary speed  
- Offline function  
- Photoelectric isolation signal input/output  
- Over-voltage, under-voltage, over-current, and inter-phase short
circuit protection function

### Pin definition

- PUL: Input a pulse signal  
- +5V: Common anode for signal input  
- DIR: Input a direction signal  
- ENBL: Input an offline enable signal  
- A+: A+ stepper motor winding  
- A-: A- stepper motor winding  
- B+:B+ tepper motor winding  
- B-: B- stepper motor winding  
- DC+: anode of power supply for stepper motor  
- DC-: cathode of power supply for stepper motor

### Wiring mode

● **Connecting with Dupont wire**

<img src="images/2h-microstep-driver_微信截图_20160203145908.png" alt="微信截图_20160203145908" width="406" style="padding:5px 5px 12px 0px;">

> Figure 1 Connecting Me 2H Microstep Driver to Arduino UNO  

> Note: This module needs external wire to connect to the electronic module in RJ25 system

### Guide to programming

● **Arduino programming**   

If you use Arduino to write a program, the library
`Makeblock-Library-master` should be invoked to control the Me
2H Microstep Driver. This program serves to make the motor rotating on demand through Arduino programming.

<img src="images/2h-microstep-driver_微信截图_20160203150021.png" alt="微信截图_20160203150021" width="479" style="padding:5px 5px 12px 0px;">

<img src="images/2h-microstep-driver_微信截图_20160203150045.png" alt="微信截图_20160203150045" width="739" style="padding:5px 5px 12px 0px;">

● **mBlock programming**  

Me 2H Microstep Driver supports the mBlock programming environment and its instructions are introduced as follows:

<img src="images/2h-microstep-driver_微信截图_20160203150120.png" alt="微信截图_20160203150120" width="722" style="padding:5px 5px 12px 0px;">

This is an example on how to use mBlock to control the Me 2H Microstep Driver module. mBlock can make the Me 2H Microstep Driver rotating from lower speed to higher speed again and again.

<img src="images/2h-microstep-driver_微信截图_20160203150151.png" alt="微信截图_20160203150151" width="718" style="padding:5px 5px 12px 0px;">

### Principle analysis

Set the number of steps per round for the motor  
The driver can set the number of steps per round as 200, 400, 800, 1600, 3200, 6400, 12800, 25600 steps for the motor. Users can set the number of steps for the driver (Pulse/rev) by using the SW5, SW6, and SW7 of DIP switch on the front panel of driver. See following table:

<img src="images/2h-microstep-driver_微信截图_20160203150244.png" alt="微信截图_20160203150244" width="721" style="padding:5px 5px 12px 0px;">

### Selecting the control mode

The SW4 of DIP switch can be set as follows:
  
- OFF – semi-flow function  
- ON – no semi-flow function 

The semi-flow function refers to when no-stepping pulse exists over 500ms, the output current of driver automatically reduces to 70% of its rated output current to prevent the motor overheat.
