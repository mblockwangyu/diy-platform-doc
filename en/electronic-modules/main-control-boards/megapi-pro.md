# MegaPi Pro

<img src="../../../en/electronic-modules/main-control-boards/images/megapi-pro-1.png" width="300">

### Overview

MegaPi Pro is an ATmega2560-based micro control board. It is fully compatible with Arduino programming. It provides powerful programming functions and a maximum output power of 120 W. With four port jacks, one 4-way DC motor expansion interface, one RJ25 expansion board interface, and one smart servo interface, MegaPi Pro is highly expansible. Its strong expansion ability enables it to meet the requirements of education, competition, entertainment, etc. MegaPi Pro can be easily installed on Raspberry Pi, and connected through serial ports. With the corresponding programs, Raspberry Pi can be used to control electronic modules, such as motors and sensors.

### Technical specifications

* Microcontroller: ATMEGA2560-16AU
* Input voltage: DC 6–12 V
* Operating voltage: DC 5 V
* Serial port: 3
* I²C interface: 1
* SPI interface: 1
* Analog input port: 16
* DC Current per I/O Pin: 20 mA
* Flash memory: 256 KB
* SRAM: 8 KB
* EEPROM: 4 KB
* Clock speed: 16 MHz
* Dimensions: 87 mm x 63 mm (width × height)

### Features

* Four motor driver interfaces for adding encoder motor driver and stepper motor driver, and thus drving DC motors, encoder motors, and stepper motors
* One wireless communication interface for adding modules, such as Bluetooth module or 2.4G module
* One smart servo interface that enables the expansion of four DC motors
* One RJ25 expansion board interface that enables the expansion of eight RJ25 interfaces
* Three M4 installation holes that are compatible with Raspberry Pi
* Overcurrent protection
* Fully compatible with Arduino
* Uses RJ25 interfaces for communication
* Supports Arduino programming, equipped with the professional Makeblock library functions to facilitate programming
* Supports graphical programming on mBlock and can be used by users of all ages

### Interface function

* Red pin: firmware burning
* Red socket: power output or motor output
* Yellow pin, black pin, black socket: input/output 
* White pin: smart management system interface
* Green interface: motor interface
* Yellow interface: four-way motor drive power interface
* White interface: smart servo interface

<img src="../../../en/electronic-modules/main-control-boards/images/megapi-pro-2.png" width="500">

