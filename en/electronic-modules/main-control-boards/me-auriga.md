# Me Auriga

![](images/me-auriga-1.jpg)

Me Auriga’s mainboard is the updated version of Orion and is equipped with multiple onboard sensors for temperature, sound intensity, a gyroscope, a buzzer driver; features a one-key power switch, wireless Bluetooth control and firmware upgrade capability, the original two red ports have been updated to four ports with the same functions; PORT5 is isolated only with serial communication function, so it cannot be used to update a program but only for communication. It is also compatible with USB serial port. PORT6 to PORT10 are compatible with dual-digital, simulation, 12C bus, unibus, and simulate serial port. Me Auriga has an encoder motor port, smart servo port and LED Ring Light Panel port (with power switch). The size of PCB is also enlarged.

### Technical Parameters

Output Voltage: 5V DC & 6-12V DC

Nominal Voltage: 6 ~ 12V DC

SCM: ATmega 2560

Dimensions (Length x Width x Height): 100 x 65 x 20mm

 

### Features

+ Supports DC motors, stepper motors, servo controllers, smart servos, encoder motors, etc
+ Can drive two encoder motors and support over-current protection for 4A (instant).
+ Supports Bluetooth and Bluetooth wireless upgrade firmware.
+ One-key power switch to control the whole circuit.
+ PORT5 – PORT10 support continuous 5V DC and 4A output (max 3A)
+ PORT1 – PORT4 support continuous 3.5A output (max 5A)
+ Onboard gyroscope, sound sensor, passive buzzer and temperature sensor.
+ PORT5 – PORT10 have short-circuit protection and over-current protection for 3A.
+ PORT1 – PORT4 have short-circuit protection and over-current protection for 3.5A.
+ USB port with antistatic protection.
+ Compatible with ARDUINO IDE.
+ Compatible with RJ25 network port.
+ Provides a dedicated library compatible with Arduino, a user-friendly interface which is powerful and easy to understand.
+ Supports mBlock.
 

### Me Auriga Electronic Components Diagram

![](images/Auriga电子器件布局-en.png)
 

### Introduction to Ports
<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:60px;">

<tr>
<td>Color</td>
<td>Function</td>
<tr>

<tr>
<td>
<img src="images/红.png" width="50px" style="padding:0px 0px 0px 0px;">
</td>
<td>
Output voltage 6-12V DC
</td>
<tr>

<tr>
<td>
<img src="images/黄.png" width="50px" style="padding:0px 0px 0px 0px;">
</td>
</td>
<td>
Single digital interface
</td>
<tr>

<tr>
<td>
<img src="images/蓝.png" width="50px" style="padding:0px 0px 0px 0px;">
</td>
</td>
<td>
Dual digital interface
</td>
<tr>

<tr>
<td>
<img src="images/灰.png" width="50px" style="padding:0px 0px 0px 0px;">
</td>
</td>
<td>
Hardware serial port
</td>
<tr>

<tr>
<td>
<img src="images/黑.png" width="50px" style="padding:0px 0px 0px 0px;">
</td>
</td>
<td>
Single and dual analog interface
</td>
<tr>

<tr>
<td>
<img src="images/白.png" width="50px" style="padding:0px 0px 0px 0px;">
</td>
</td>
<td>
I²C Port
</td>
<tr>

</table>

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">

<tr>
<td>Port NO.</td>
<td>Port Color</td>
<td>Compatible Module Types</td>
<td>Compatible Modules</td>
<tr>

<tr>
<td>
1&2&3&4
</td>
<td>
<img src="images/1234-2.png" width="200px" style="padding:0px 0px 0px 0px;">
</td>
</td>
<td>
Motor-driven modules
(6-12V DC)
</td>
<td>
Dual Motor Driver
<br>
Stepper Driver
</td>
<tr>

<tr>
<td>
5
</td>
<td>
<img src="images/5-2.png" width="50px" style="padding:0px 0px 0px 0px;">
</td>
</td>
<td>
Hardware serial port
</td>
<td>
Bluetooth Module<br>
Raspberry Pi GPIO
</td>
<tr>

<tr>
<td>
6&7&8&9&10
</td>
<td>
<img src="images/678910-2.png" width="250px" style="padding:0px 0px 0px 0px;">
</td>
</td>
<td>
Single digital interface<br>
Dual digital interface<br>
I²C Port<br>
Single and dual analog interface
</td>
<td>
Ultrasonic Sensor<br>
RGB Module<br>
Limit Switch Module<br>
Nixie Tube<br>
IR Receiver<br>
Shutter Release Module<br>
Line-follower Sensor<br>
Gyro Sensor<br>
Potentiometer<br>
Sound Sensor<br>
Remote Control Module<br>
Humidity Sensor<br>
Flame Sensor
</td>
<tr>

</table>
 
### Pin Diagram

![](images/Me-Auriga-引脚图－英文.jpg)

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">

<th style="width: 300px;">Modules<th style="width: 300px;" >No.
<tr>
    <tr>
        <td>Sound Sensor<td>A1</td>
    <tr>

<tr>
<td>Buzzer<td>D45</td>
<tr>

<tr>
<td rowspan="6">M1</td>
<td>D11(PWM)</td>
<tr>
<tr>
<td>D48<tr>
<tr>
<td>D49</tr>
<tr>

<tr>
<td rowspan="6">M2</td>
<td>D10(PWM)</td>
<tr>
<tr>
<td>D46<tr>
<tr>
<td>D47</tr>


</table>

 

### Introduction to Onboard Sensors
Sound Sensor


![](images/声音传感器-en.png)

The sound sensor on Me Auriga is designed to detect the intensity of sound in the surrounding environment. The sound sensor is based on the microphone. Its main component is an LM2904 low-power amplifier. It can be used in interactive projects, such as a voice-activated switch, or a robot that follows dance movements.

Gyroscope Sensor

![](images/陀螺仪-en.png)

Me Auriga has an onboard gyroscope sensor.  It is an ideal motion detection and posture detection module for robot, including a 3-axis accelerometer, 3-axis angular velocity sensor and a motion processor. It can be applied to a self-balancing robot or other mobile devices.

 

NTC thermistor

![](images/温度传感器-en.png)

Me Auriga has an onboard thermistor that can be used to detect ambient temperature changes.

 

### Programming Guide
1.Arduino IDE programming

Arduino is an open source electronic prototyping platform with flexible and easy-to-use hardware and software. The software section contains the Arduino development environment (IDE) and core libraries. The IDE is written in Java and is based on the Processing development environment.

Me Auriga is compatible with the Arduino Mega 2560, so you can use the Arduino IDE to develop your program. If you use the Makeblock electronic module, we recommend that you install the Makeblock library.

For more details please click here: http://learn.makeblock.com/en/learning-arduino-programming-ranger/

 2.mBlock programming 
  
Me Auriga supports mBlock programming. mBlock is a programming software most suitable for beginners, which inherited the simple, easy to use characteristics of Scratch, and is integrated with the powerful scalability of Arduino. Control support and program uploads are available online. Simply drag over the mBlock code – it’s as easy as playing with drag-and-drop blocks. At the same time, it will help beginners transit smoothly to genuine programming language.

For more details, please click here: http://learn.makeblock.com/en/getting-started-programming-with-mblock-2/

### Note
+ Me Auriga uses 6-12V DC voltage.
+ Cautious of short-circuit.
+ Prevent contamination from water, acidic/alkaline liquids or solids.
+ Do not discard arbitrarily.
 

### Schematic diagram

To download schematic diagram, please visit:

[Download](http://download.makeblock.com/Me%20Auriga%20电路图V1.1%20-%2020160221.pdf)

### Case size diagram

[Case – 1 Download](http://download.makeblock.com/ranger/Me%20Auriga%20Case%20-%201.PDF)

[Case – 2 Download ](http://download.makeblock.com/ranger/Me%20Auriga%20Case%20-2%20.PDF)