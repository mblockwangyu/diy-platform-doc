# NovaPi

<img src="../../../en/electronic-modules/main-control-boards/images/novapi-1.png" width="300">

### Overview

NovaPi is the new generation of main control board used in the MakeX Robotics Competition, featuring a high-performance M7 processor ATSAMS70N20A-AN and an STM32F030CCT6 coprocessor. It provides five sensor interfaces, and six smart motor interfaces. Sensor interfaces use serial communication and provide 5 V output. Smart motor interfaces use serial communication and provide 12 V output. NovaPi comes with a 6-axis sensor to measure both the acceleration and angular speed of rotation in x, y, and z axes. NovaPi can also work with a power extension board to provide more interfaces and resources.

### Specifications

* Dimensions: 85 mm × 56 mm × 13  mm
* Operating voltage: 6–13 V (if a motor is used, the minimum input voltage needs to meet the operating voltage requirement of the motor)
* Operating current: 60 mA
* Communication port and protocol: serial port/mBuild protocol

### Features

* Supports Makeblock metal parts and Lego pins
* Applicable to mBuild-system main control boards, such as HaloCode
* Provides five mBuild interfaces
* Provides six smart motor interfaces
* On-board 6-axis motion sensor
* Supports power extension boards
* Small in volume and flexible in installation, which allows it to be easily used in various projects
* Equipped with the high-performance M7 processor

### Product appearance

<img src="../../../en/electronic-modules/main-control-boards/images/novapi-2.png" width="300">

<img src="../../../en/electronic-modules/main-control-boards/images/novapi-3.png" width="500">

<img src="../../../en/electronic-modules/main-control-boards/images/novapi-4.png" width="300">

* Micro USB interface for program download
* Power switch: 1
* 12 V power input interface: 1
* Sensor interface: 5
* Smart motor interface: 6
* 12 V power output interface: 2
* State indicator: 3
* Power extension board interface: 1
* 6-axis motion sensor: 1
* M7 processor ATSAMS70N20A-AN: 1
* STM32F030CCT6 processor: 1

### Installation and application precautions

* Pay attention to prevent the main control board from short-circuiting when installing it. When assembling the main control board, you can add plastic spacers to the bottom of the screw holes to ensure that the main board and the bracket have a sufficient safety distance to prevent the main board from being crushed and short-circuited. Pay attention to the wiring when connecting the main control board to external devices, such as motors and sensors. 

* The spacing of the holes on the NovaPi main control board may not match the spacing of structural component mounting holes. Do not force the screws in. Otherwise, the main control board may be damaged.

* Do not install the main control board when power is applied. Check the installation before powering it on. Do not expose any conductive objects such as copper wires, tin slag, screws on the main control board.

### Built-in functions

The NovaPi main control board is equipped with a 6-axis motion sensor that can detect its postures, such as the curren speed, acceleration, angular speed, and tilt angle.

### Software

The following table describes the blocks for the NovaPi main control board.

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">Name</th>
    <th class="tg-7g6k">Description</th>
    <th class="tg-7g6k">Input/Output</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../en/electronic-modules/main-control-boards/images/novapi-5.png" width="60"></td>
    <td class="tg-c6of">Function: obtains the count value of the system timer<br>Unit: ms<br>Return type: float</td>
    <td class="tg-i81m">Input</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../en/electronic-modules/main-control-boards/images/novapi-6.png" width="60"></td>
    <td class="tg-c6of">Function: resets the system timer to zero<br>Unit: none<br>Return type: none</td>
    <td class="tg-i81m">Output</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../en/electronic-modules/main-control-boards/images/novapi-7.png" width="200"></td>
    <td class="tg-c6of">Function: obtains the acceleration of the on-board<br> gyroscope in the x, y, and z axes<br>Unit: m/s²<br>Return type: float</td>
    <td class="tg-i81m">Input</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../en/electronic-modules/main-control-boards/images/novapi-8.png" width="200"></td>
    <td class="tg-ycr8">Function: obtains the angular speed of the on-board<br> gyroscope in the x, y, and z axes<br>Unit: degree per second<br>Return type: float</td>
    <td class="tg-i81m">Input</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../en/electronic-modules/main-control-boards/images/novapi-9.png" width="200"></td>
    <td class="tg-ycr8">Function: obtains the angles of the on-board<br> gyroscope in the x, y, and z axes<br>Unit: degree<br>return type: float</td>
    <td class="tg-i81m">Input</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../en/electronic-modules/main-control-boards/images/novapi-10.png" width="200"></td>
    <td class="tg-ycr8">Function: sets the shaking threshold<br>Unit: none<br>Return type: none</td>
    <td class="tg-i81m">Output</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../en/electronic-modules/main-control-boards/images/novapi-11.png" width="200"></td>
    <td class="tg-ycr8">Function: detects whether the main control board is shaken<br>Unit: none<br>Return type: BOOL; True-shaken; False-not shaken</td>
    <td class="tg-i81m">Input</td>
  </tr>
</table>


