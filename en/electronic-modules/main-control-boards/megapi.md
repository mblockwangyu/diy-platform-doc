# MegaPi


![](images/megapi_MegaPi.jpg)


### 1. Overview

MegaPi is a main control board specially designed for makers and also an ideal option for being applied to education field and all kinds of matches. It is based on Arduino MEGA 2560 and supports programming with
Arduino IDE perfectly. MegaPi can be divided into 6 function area, allowing you to connect with various plug-in modules to drive motors and sensor and to realize wireless communication. MegaPi has strong
motor-driving ability which is capable of driving 10 servos or 8 DC motors simultaneously. It is the ideal option for various robotic projects, such as smart robot car and 3D printer.

### 2. Features

- Four motor driver interfaces for adding
encoder motor driver and stepper motor driver, and thus to drive DC
motors, encoder motors and stepper motors
- One wireless communication interface
for adding Bluetooth module or 2.4G module
- Ten servo interfaces which enable the
board to drive up to 10 servos at the same time
- Two high-power MOS driver interface
which is able to drive devices with a maximum current of 10A. Maximum output of normal I/O ports is DC 5V 3A
- One Raspberry Pi switch interface (requires manual soldering) to realize 5V to 3.3V serial communication
- Three M4 mounting holes which allow the board to be connected with Raspberry Pi
- Slide switch for controlling the
power supply
- B-type USB interface for downloading
programs and communications. It uses the CH340G USB to serial chip which can realize communication between the computer and MegaPi easily and stably
- High-power DC input interface with an over-current protection of 2A and anti-reverse measurement
- One reset key, one power indicator
(red) and one I/O indicator (blue)

### 3. Specification

- Microcontroller:
ATMEGA2560-16AU
- Input Voltage: DC 6V-12V
- Operating Voltage: DC 5V
- I/O Pins: 43
- Serial Ports: 3
- I2C Interface: 1
- SPI Interface: 1
- Analog Input Pins: 15
- DC Current per I/O Pin: 20mA
- Flash Memory: 256KB
- SRAM: 8KB
- EEPROM: 4KB
- Clock Speed: 16 MHz
- Dimension: 85\*63mm

### 4. Introducing Interfaces of MegaPi and Other Plug-in Modules

<img src="images/megapi_10050_10.png" alt="10050_10" width="760" style="padding:5px 5px 12px 0px;">

The various colors on MegaPi represents specialized functions:  
- Red pin–power output/motor output  
- Yellow pin–I/O pin  
- Blue pin–wireless communication interface  
- Black Pin–power GND  
- Green Interface–power output/motor output

### 5. Introduction to ports

MegaPi for RJ 25 provides four RJ25 ports identified by labels of five different colors. Their colors and functions are as follows:

<img src="images/megapi_MegaPi-Port.png" alt="MegaPi Port" width="1200" style="padding:5px 5px 12px 0px;">

- Wireless communication interface – for adding Bluetooth module or 2.4G module (require corresponding communication module)
- Port 1 – motor driver interface for driving DC motor, stepping motor and encoder motor (require corresponding driving module)
- Port2 –motor driver interface for driving DC motor, stepping motor and encoder motor (require corresponding driving module) 
- Port3 – motor driver interface for
driving DC motor, stepping motor and encoder motor (require
corresponding driving module)
- Port4 – motor driver interface for
driving DC motor, stepping motor and encoder motor (require
corresponding driving module)
- Port5 – RJ25 communication module interface for adding Bluetooth or WIFI which requires RJ25 (require keyset for MegaPi’s transferring to RJ25)
- Port6 – RJ25 dual-digital/analog interface for adding sensors or input/output RJ25 modules (require keyset for MegaPi’s transferring to RJ25)
- Port7– RJ25 dual-digital/analog interface for adding sensors or input/output RJ25 modules (require keyset for MegaPi’s transferring to RJ25)
- Port8– RJ25 dual-digital/analog interface for adding sensors or input/output RJ25 modules (require keyset for MegaPi’s transferring to RJ25)

### 6.Guide to programming

1\. [Graphical Programming-mBlock](http://learn.makeblock.com/getting-started-programming-with-mblock/)

2\. [Arduino Programming](http://learn.makeblock.com/ultimate2-arduino-programming/)

[Arduino](http://www.arduino.cc/) is an open-source electronics platform based on easy-to-use hardware and software. It’s intended for anyone making interactive projects. The Arduino development environment makes it easy
to write code and upload it to the i/o board. You can use Arduino language (C\\C++) to interface with Arduino hardware. We provide a complete Arduino using environment.

MegaPi is compatible with Arduino Mega 2560, so you can develop program with Arduino IDE. We suggest you install Makeblock program library if using Makeblock’s electronic modules.

3\. [Python Programming](http://learn.makeblock.com/python-for-megapi/)

4\. [Node JS Programming](https://github.com/Makeblock-official/NodeForMegaPi)

<img src="images/megapi_MegaPi_17.jpg" alt="英文产品单页_Ranger_V0.5.1" width="760" style="padding:5px 5px 12px 0px;">


### 7.Schematic Circuit Diagram

<img src="images/megapi_MegaPiSchematic1.jpg" alt="MegaPiSchematic1" width="820" style="padding:5px 5px 12px 0px;">

<img src="images/megapi_MegaPiSchematic-2.jpg" alt="MegaPiSchematic 2" width="820" style="padding:5px 5px 12px 0px;">

### 8. FAQ of MegaPi

8.1 Where to get the driver package if the computer fails to install it automatically?

A: Manually install the driver

Windows :
[Download](https://raw.githubusercontent.com/Makeblock-official/Makeblock-USB-Driver/master/Makeblock_Driver_Installer.zip)

Mac OSX :
[Download](http://blog.sengotta.net/wp-content/uploads/2015/11/CH34x_Install.zip) 


8.2 How much current can the large current driver interface (MOS driver interface) output?

A: Two interfaces can output current up to DC12V10A.

8.3 Why the stepping motor get hot?

A: Considering the usage scenario of stepping motor, the driver current was adjusted to the larger one, so the motor may get hot. Please contact the cooling fin and the driver module well, or the master chip may get burn.

8.4 What’s the function of the black adjustable potentiometer on the stepping motor?

A: This potentiometer is for adjusting the current of stepping motor driver. The default position is on the middle, but you can turn it up or down. When potentiometer turned
up, the chip will get hotter. So remember to dissipate heat with larger cooling pin or in good cooling conditions.

8.5 Why the master chip reset when MegaPi drives several motors at the same time? How to deal with this problem?

A: When the motor starts to run or rotate clockwise/anticlockwise, the power consumption will be enormous. In this situation, the current from the power source will be insufficient, which leads to the result of low voltage and the reset of master chip. You should use stronger power
source or adjust the program to avoid the motor rotating  clockwise/anticlockwise frequently.

8.6 How to weld 2\*10 pin header while
connecting MegaPi with Raspberry Pi?

A: MegaPi is not weld to the Raspberry Pi, so you have to weld by yourself. Do avoid short circuit when welding the pins.

8.7 Raspberry Pi resets when connected to Raspberry Pi and drive high power device. How to deal with this problem?

When supplying power to  Raspberry Pi  through MegaPi and controlling motors at the same time, the voltage will be unstable and Raspberry Pi will reset. Just supply power to Raspberry Pi directly.

8.8 How to get after-sales service if there are some quality problem with MegaPi?

A:support@makeblock.com

8.9 Is there any accessories that can work with MegaPi? Where should I
buy them?

A: Encoder motor drivers, stepping motor drivers, Bluetooth, 2.4G, DC motors, encoder motors, stepping motors, Shield, sensors of RJ25, steering engines, structural parts, etc.


### 9. Notice

- Apply MegaPi voltage DC 6-12V
- Prevent short circuits
- Prevent the contamination of
water, acidity/ alkalinity liquid, or solid debris, etc.
- Keep away from children and pets 
- Do not throw away

