<img src="../../../zh/mechanical-parts/timing-pulleys/images/belt-connector.jpg" style="width:300;padding:5px 5px 15px 0px;">

# Belt Connector

### Description

Belt Connector provides a simple way to clamp open-end timing belts. Mating with some clamping parts, you can use a open-end timing belts more flexible for example using in the belt system of the 3D printer.

### Features

- Made from 6061 aluminum extrusion, high precision and strength
- Compatible with Makeblock components


 
