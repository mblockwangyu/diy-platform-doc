# Bearings

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="bearings/flange-bearing-8x16x5mm.html" target="_blank"><img src="../../zh/mechanical-parts/bearings/images/flange-bearing-8x16x5.jpg" width="150px;"></a><br>
<p>Flange Bearing 8x16x5mm</p></td>

<td width="25%;"><a href="bearings/flange-bearing-4x8x3mm.html" target="_blank"><img src="../../zh/mechanical-parts/bearings/images/flange-bearing-4x8x3mm.jpg" width="150px;"></a><br>
<p>Flange Bearing 4x8x3mm</p></td>

<td width="25%;"><a href="bearings/female-rod-end-bearing.html" target="_blank"><img src="../../zh/mechanical-parts/bearings/images/female-rod-end-bearing.jpg" width="150px;"></a><br>
<p>Female Rod End Bearing</p></td>

<td width="25%;"><a href="bearings/male-rod-end-bearing.html" target="_blank"><img src="../../zh/mechanical-parts/bearings/images/male-rod-end-bearing.jpg" width="150px;"></a><br>
<p>Male Rod End Bearing</p></td>
</tr>

<tr>
<td><a href="bearings/plain-ball-bearing-8-16-5mm.html" target="_blank"><img src="../../zh/mechanical-parts/bearings/images/plain-ball-bearing-8-16-5mm.jpg" width="150px;"></a><br>
<p>Plain Ball Bearing 8*16*5mm</p></td>
<td><a href="bearings/plain-ball-bearing-4-8-3mm.html" target="_blank"><img src="../../zh/mechanical-parts/bearings/images/plain-ball-bearing-4-8-3mm.jpg" width="150px;"></a><br>
<p>Plain Ball Bearing 4*8*3mm</p></td>
<td><a href="bearings/plane-bearing-turntable-d34x24mm.html" target="_blank"><img src="../../zh/mechanical-parts/bearings/images/plane-bearing-turntable-d34x24mm.jpg" width="150px;"></a><br>
<p>Plane Bearing Turntable D34*24mm</p></td>
<td><a href="bearings/linear-motion-block-bracket.html" target="_blank"><img src="../../zh/mechanical-parts/bearings/images/linear-motion-block-bracket.jpg" width="150px;"></a><br>
<p>Linear Motion Block Bracket</p></td>
</tr>

<tr>
<td><a href="bearings/roller-motion-block-bracket.html" target="_blank"><img src="../../zh/mechanical-parts/bearings/images/roller-motion-block.jpg" width="150px;"></a><br>
<p>Roller Motion Block Bracket</p></td>
</tr>
</table>