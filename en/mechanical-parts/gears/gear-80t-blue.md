<img src="../../../zh/mechanical-parts/gears/images/gear-80t.jpg" style="width:400px;padding:5px 5px 15px 0px;">

# Gear 80T

### Description

Gear 80T is designed for transmitting motion from a motor to a wheel or shaft. Usually it is used with shaft connector or flange bearing 4\*8\*3.

### Features

-  Aluminum extrusion(high strength) , anodized surface(long time to shelve with barely rusting)
- 80 teeth
- With 8mm diameter hub compatible with shaft connector or bearings.

### Note

This product contains functional sharp points on components, please be careful to use it.

### SizeChart(mm)

<img src="../../../zh/mechanical-parts/gears/images/gear-80t-blue_sizecharts.jpg" alt="sizecharts.jpg" width="760" />
