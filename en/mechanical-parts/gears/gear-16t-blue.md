<img src="../../../zh/mechanical-parts/gears/images/gear-16t.jpg" style="width:400px;padding:5px 5px 15px 0px;">

# Gear 16T

### Description

Gear 16T is designed for transmitting motion from a motor to another gear or
shaft.  You can use a headless screw M3x5 to attach it.
  
### Features

- Aluminum extrusion(high strength), anodized surface(long time to shelve with barely rusting)
- 16 teeth
- With 4mm diameter hub compatible with 4mm diameter shaft 
- A headless screw M3x5 is included

  
### Note
  
This product contains functional sharp points on components, please be careful to use it.

### SizeChart(mm)

<img src="../../../zh/mechanical-parts/gears/images/gear-16t-blue_sizechart.png" alt="sizechart.png" width="760" />

 
