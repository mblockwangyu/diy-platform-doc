<img src="../../../zh/mechanical-parts/fasteners-gaskets/images/rivet-r4100.jpg" style="width:400px;padding:5px 5px 15px 0px;">

# Plastic Rivet R4100

### Features

- Material: plastic

### Size Charts

<img src="../../../zh/mechanical-parts/fasteners-gaskets/images/rivet-r4100-1.png" style="width:500px;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/fasteners-gaskets/images/rivet-r4100-2.jpg" style="width:500px;padding:5px 5px 15px 0px;">