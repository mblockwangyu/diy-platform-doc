<img src="../../../zh/mechanical-parts/fasteners-gaskets/images/brass-stud-m4×16.jpg" style="width:400px;padding:5px 5px 15px 0px;">

# Brass Stud M4x16

### Features

- Material: brass

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/fasteners-gaskets/images/brass-stud-m4×16-1.png" style="width:800px;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/fasteners-gaskets/images/brass-stud-m4×16-2.png" style="width:800px;padding:5px 5px 15px 0px;">