# Socket Cap Screw

### Description

Button Head Socket Cap Screw is a new kind of Socket Cap Screw, it has smaller head that can solve most problem of interfere with other components. The tool of this Button Head Socket Cap Screw  is HEX Screwdriver 2.5mm.

### Features

- Made of stainless steel.
- Compatible with structures which have 4mm holes.
- Smaller head that can solve most problem of interfere with other components.

## Socket Cap Screw 4x8-Button Head

<img src="../../../zh/mechanical-parts/fasteners-gaskets/images/socket-cap-screw-m4x8.jpg" style="width:360px;padding:5px 5px 15px 0px;">

## Socket Cap Screw 4x14-Button Head

<img src="../../../zh/mechanical-parts/fasteners-gaskets/images/socket-cap-screw-m4x14.jpg" style="width:360px;padding:5px 5px 15px 0px;">

## Socket Cap Screw 4x16-Button Head

<img src="../../../zh/mechanical-parts/fasteners-gaskets/images/socket-cap-screw-m4x16.jpg" style="width:360px;padding:5px 5px 15px 0px;">

## Socket Cap Screw 4x22-Button Head

<img src="../../../zh/mechanical-parts/fasteners-gaskets/images/socket-cap-screw-m4x22.jpg" style="width:360px;padding:5px 5px 15px 0px;">

## Socket Cap Screw 4x30-Button Head

<img src="../../../zh/mechanical-parts/fasteners-gaskets/images/socket-cap-screw-m4x30.jpg" style="width:360px;padding:5px 5px 15px 0px;">

## Socket Cap Screw 4x35-Button Head

<img src="../../../zh/mechanical-parts/fasteners-gaskets/images/socket-cap-screw-m4x35.jpg" style="width:360px;padding:5px 5px 15px 0px;">

## Socket Cap Screw 4x40-Button Head

<img src="../../../zh/mechanical-parts/fasteners-gaskets/images/socket-cap-screw-m4x40.jpg" style="width:360px;padding:5px 5px 15px 0px;">


