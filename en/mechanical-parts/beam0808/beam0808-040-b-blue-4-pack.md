# Beam0808-040-B


![](images/beam0808-040-b-blue-4-pack_Beam0808-040-B.jpg)

**Description:**

Makeblock
Beam0808 is One of the most frequently used part in Makeblock platform,
it compatible with most makeblock motion and Structure
components. 

** **

**Features:**

-   Made
    from 6061 aluminum extrusion, anodized surface. Excellent strength
    and twist resistance.
-   With
    holes on 8mm increments, can be drilled for 4mm hardware.
-   Threaded
    slot enables easy and flexible connection.
-   Cross-sectional
    area 8x8mm, length 40mm.
-   Sold
    in Packs of 4.

 

**Size
Charts(mm):**

<img src="images/beam0808-040-b-blue-4-pack_60510-s1.jpg" alt="60510-s1.jpg" width="760" /> ![](http://)

 

**Demo:**

<img src="images/beam0808-040-b-blue-4-pack_linear-guide-assembly-1-3.jpg" alt="linear-guide-assembly-1-3.jpg" width="760" />
