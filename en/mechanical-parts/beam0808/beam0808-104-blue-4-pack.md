# Beam0808-104


![](images/beam0808-104-blue-4-pack_Beam0808-104.jpg)

**Description:**

 

Makeblock Beam0808 is one of the most
frequently used part in Makeblock platform, it is compatible with most
makeblock motion and structure components. 

** **

**Features:**

-   Made from 6061 aluminum extrusion,
    anodized surface. Excellent strength and twist resistance.

<!-- -->

-   With holes on 16mm increments, can
    be drilled for 4mm hardware.

<!-- -->

-   Threaded slot enables easy and
    flexible connection.

<!-- -->

-   Cross-sectional area 8x8mm, length
    104mm.

<!-- -->

-   Sold in Packs of 4.

 

**Size Charts(mm):**

 <img src="images/00808-.jpg" alt="00808-.jpg" width="760" />

 

**Demo:**

<img src="images/2770251.jpg" alt="2770251.jpg" width="760" />
