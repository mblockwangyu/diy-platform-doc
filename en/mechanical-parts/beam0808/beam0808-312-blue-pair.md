# Beam0808-312


![](images/beam0808-312-blue-pair_Beam0808-312.jpg)

**Description:**

Makeblock
Beam0808 is One of the most frequently used part in Makeblock platform,
it compatible with most makeblock motion and Structure
components. 

** **

**Features:**

-   Made
    from 6061 aluminum extrusion, anodized surface. Excellent strength
    and twist resistance.
-   With
    holes on 16mm increments, can be drilled for 4mm hardware.
-   Threaded
    slot enables easy and flexible connection.
-   Cross-sectional
    area 8x8mm, length 312mm.
-   Sold
    in Pair.

 

**Size
Charts(mm):**

 ![](http://)<img src="images/beam0808-312-blue-pair_60576-s1.jpg" alt="60576-s1.jpg" width="760" />

 

**Demo:**
