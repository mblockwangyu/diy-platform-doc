<img src="../../../zh/mechanical-parts/plates/images/plate-45.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Plate 45°


### Description

Plate 45°is usually designed to
connect mechanical parts with 45 or 135 degree Angle.

 

### Features

-    Designed
    specially with 135 degrees angle
-   With
    M4 mounting holes on 8mm increments
-   Made
    from aluminum extrusion (high strength), 3mm thick, anodized
    surface(long time to put without barely rusting)

 

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/plates/images/plate-45-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/plates/images/plate-45-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/plate-45-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/plate-45-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/plate-45-5.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/plate-45-6.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/plate-45-7.jpg" style="width:300;padding:5px 5px 15px 0px;">

 
