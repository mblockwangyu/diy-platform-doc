<img src="../../../zh/mechanical-parts/plates/images/disc-d72.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Disc D72

### Description

Disc
D72 is perfect for building symmetrical mechanical structure such as 120
degree three-wheeled car, three axis helicopter, four axis helicopter.
Besides, With a 72mm diameter, you can also  use it as base
plate.

 

### Features

-   8mm
    diameter center hole compatible with 8mm shafts or bearings and can
    be attached with DC Motor-25 and Stepper motor due to M3 mounting
    holes
-   with
    M4 mounting holes and 4mm mounting slots compatible with Makeblock
    components.
-   Made
    from aluminum extrusion (high strength), 3mm thick, anodized
    surface(long time to shelve without barely rusting)

 

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/plates/images/disc-d72-1.png" style="width:700;padding:5px 5px 15px 0px;">
 

### Demo

<img src="../../../zh/mechanical-parts/plates/images/disc-d72-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/disc-d72-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/disc-d72-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/disc-d72-5.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/disc-d72-6.jpg" style="width:300;padding:5px 5px 15px 0px;">