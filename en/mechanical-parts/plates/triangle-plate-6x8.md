<img src="../../../zh/mechanical-parts/plates/images/triangle-plate-6x8.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Triangle Plate 6\*8

### Features

-   Made
    from 6061 aluminum, 3mm thick, anodized surface.
-   Can
    be used to build triangle structure. 
-   Sold
    in Pack of 2


### Size Charts(mm)

<img src="../../../zh/mechanical-parts/plates/images/triangle-plate-6x8-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/plates/images/triangle-plate-6x8-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/triangle-plate-6x8-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/triangle-plate-6x8-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/plates/images/triangle-plate-6x8-5.jpg" style="width:300;padding:5px 5px 15px 0px;">



 
