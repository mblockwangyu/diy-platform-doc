<img src="../../../zh/mechanical-parts/plates/images/linear-motion-block-bracket-a.jpg" style="width:400;padding:5px 5px 15px 0px;">


# Linear Motion Block Bracket A

### Description   

Makeblock Linear Motion Block
Bracket A  works as a necessary part of a linear motion guide
robot. It comes with smooth connectivity and flexibility which is
suitable for multiple linear motion structures.

 

### Features

- Computer numerical
control (CNC)，strong aluminum extrusion parts with
anodized surface                   

- Easy-to-use stepper
motor
driver                                                                            

- Stationary
motion                                                                                                   


- High accuracy with
small friction and low noise.    


- High temperature
resistance and high
load-capacity                                                       

 - Compatible with
Makeblock components  

