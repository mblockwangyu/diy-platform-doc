<img src="../../../zh/mechanical-parts/plates/images/plate-3x6.jpg" style="width:400;padding:5px 5px 15px 0px;">


# Plate 3\*6
 
### Features
-   Made
    from 6061 aluminum, 2mm thick, anodized surface.
-   With
    holes on 8mm increments,  can be drilled for 4mm hardware. 
-   Sold
    in Packs of 4.

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/plates/images/plate-3x6-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/plates/images/plate-3x6-2.jpg" style="width:700;padding:5px 5px 15px 0px;">

