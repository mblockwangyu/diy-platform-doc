<img src="../../../zh/mechanical-parts/plates/images/plate-135.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Plate 135°


### Description

Plate 135°is usually designed to
connect mechanical parts with 45 or 135 degree Angle.

 

### Features

-  Designed specially with 135 degrees angle