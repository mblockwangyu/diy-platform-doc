<img src="../../../zh/mechanical-parts/shafts/images/linear-motion-shaft-d4-80mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Linear Motion Shaft D4

### Features

-   Made of stainless steel.
-   Can drill into 4mm holes. 
-   Compatible
    with thread drive beam.
-   Length:80mm, 128mm, 240mm, 288mm
          
## Linear Motion Shaft D4x80mm

<img src="../../../zh/mechanical-parts/shafts/images/linear-motion-shaft-d4-80mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/shafts/images/linear-motion-shaft-d4-80mm-1.jpg" style="width:600;padding:5px 5px 15px 0px;">

## Linear Motion Shaft D4x128mm

<img src="../../../zh/mechanical-parts/shafts/images/linear-motion-shaft-d4-128mm.jpg" style="padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/shafts/images/linear-motion-shaft-d4-128mm-1.jpg" style="width:600;padding:5px 5px 15px 0px;">

## Linear Motion Shaft D4x240mm

<img src="../../../zh/mechanical-parts/shafts/images/linear-motion-shaft-d4-240mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/shafts/images/linear-motion-shaft-d4-240mm-1.jpg" style="width:600;padding:5px 5px 15px 0px;">

## Linear Motion Shaft D4x288mm

<img src="../../../zh/mechanical-parts/shafts/images/linear-motion-shaft-d4-288mm.jpg" style="width:400;padding:5px 5px 15px 0px;">