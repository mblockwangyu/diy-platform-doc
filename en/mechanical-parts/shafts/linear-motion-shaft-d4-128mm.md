# Linear Motion Shaft D8x128mm


![](images/Linear-Motion-Shaft-D8×128mm.jpg)

**Description:**

This
linear motion shaft of 4 mm in diameter and 128mm in length is suitable
for using as shaft, supporting part or using with slide units in linear
motion applications. It is made from high-carbon steel and chrome plated
for corrosion resistance, case hardened for wear resistance and
precision ground for consistent ball bushing radial clearance.

  
**Features:**

-   Round
    steel shaft for using as shaft, supporting part or using with slide
    units in linear motion applications
-   Chrome
    plated for corrosion resistance
-   Case
    hardened for wear resistance

 

**SizeCharts:**

<img src="images/linear-motion-shaft-d8-128mm_shaft-d8x128mm-sj.jpg" alt="shaft-d8x128mm-sj.jpg" width="760" />

 

**Demo:**

<img src="images/linear-motion-shaft-d8-128mm_shaft-d8x96mm-380x380.jpg" alt="shaft-d8x96mm-380x380.jpg" width="380" /><img src="images/linear-motion-shaft-d8-128mm_shaft-d8x96mm-demo01.jpg" alt="shaft-d8x96mm-demo01.jpg" width="380" />

<img src="images/linear-motion-shaft-d8-128mm_shaft-d8x96mm-demo02.jpg" alt="shaft-d8x96mm-demo02.jpg" width="380" /><img src="images/linear-motion-shaft-d8-128mm_shaft-d8x128mm-deme03.jpg" alt="shaft-d8x128mm-deme03.jpg" width="380" />

 

 
