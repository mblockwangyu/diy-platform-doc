<img src="../../../zh/mechanical-parts/shafts/images/d-shaft-8x96mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# D Shaft 8x96mm

### Description

This
linear motion shaft of 8 mm in diameter and 96mm in length is suitable
for using as shaft or supporting part. It is made from high-carbon steel
and chrome plated for corrosion resistance, case hardened for wear
resistance.

  
### Features

- Steel shaft for using as shaft or supporting part
-   Chrome
    plated for corrosion resistance
-   Case
    hardened for wear resistance

 

### SizeCharts

<img src="../../../zh/mechanical-parts/shafts/images/d-shaft-8x96mm-1.png" style="width:700;padding:5px 5px 15px 0px;">

 

 
