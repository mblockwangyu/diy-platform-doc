<img src="../../../zh/mechanical-parts/shafts/images/shaft-connector-4mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Shaft Connector 4mm

### Features

-   Compatible
    with most 4mm shaft
-   Easy
    to connect wheels on motors with 4mm shaft
-   4
    Screw M4x14 and 2 Headless Screw M3x5 included.
-   Sold
    in Pair.

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/shafts/images/shaft-connector-4mm-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/shafts/images/shaft-connector-4mm-2.jpg" style="width:700;padding:5px 5px 15px 0px;">
