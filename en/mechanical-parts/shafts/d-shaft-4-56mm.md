<img src="../../../zh/mechanical-parts/shafts/images/d-shaft-4x56mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# D Shaft 4x56mm


### Size Charts(mm)

<img src="../../../zh/mechanical-parts/shafts/images/d-shaft-4x56mm-1.png" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/shafts/images/d-shaft-4x56mm-2.jpg" style="width:700;padding:5px 5px 15px 0px;">