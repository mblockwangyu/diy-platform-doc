<img src="../../../zh/mechanical-parts/shafts/images/universal-joint-4x4mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Universal Joint 4\*4mm

### Description

This Cross-Coupling Universal
Joint is made by two fork-type connectors and a cross shaft. The
structure is simple but the power transmission is large. The angle of
deflection is up to max  45°.

 

### Features

-   Material:
     stainless steel 
-   Max
    deflection angle: 45°
-   Size:
    D9\*Φ4\*Φ4\*M3\*L22mm
-   Sold
    in single pack.

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/shafts/images/universal-joint-4x4mm-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/shafts/images/universal-joint-4x4mm-2.jpg" style="width:700;padding:5px 5px 15px 0px;">
 
<img src="../../../zh/mechanical-parts/shafts/images/universal-joint-4x4mm-3.jpg" style="width:700;padding:5px 5px 15px 0px;">

 
