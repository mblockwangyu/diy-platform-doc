# Shaft Clamping Hub 8mm

![](images/shaft-clamping-hub-8mm_Shaft-Clamping-Hub-8mm.jpg)

**Description:**

This Shaft Claming Hub 8mm is suitable for attaching some other
mechanisms to 8mm diameter shafts. It has a 8mm bore, four tapped holes,
one M4\*22 screw included for tightening.

**PartList:**

Shaft
Claming Hub 8mm x 1   


M4\*22 x
1 

 

**SizeChart:**

<img src="images/shaft-clamping-hub-8mm_shaft-claming-hub-sj1.jpg" alt="shaft-claming-hub-sj1.jpg" width="760" />

 

**Demo:**

<img src="images/shaft-clamping-hub-8mm_shaft-claming-hub-8mm-demo-s.jpg" alt="shaft-claming-hub-8mm-demo-s.jpg" width="380" /><img src="images/shaft-clamping-hub-8mm_shaft-claming-hub-8mm-demo-e.jpg" alt="shaft-claming-hub-8mm-demo-e.jpg" width="380" />

<img src="images/shaft-clamping-hub-8mm_shaft-claming-hub-8mm-demo-g.jpg" alt="shaft-claming-hub-8mm-demo-g.jpg" width="380" /><img src="images/shaft-clamping-hub-8mm_shaft-claming-hub-8mm-demo-f.jpg" alt="shaft-claming-hub-8mm-demo-f.jpg" width="380" />

 

 

 
