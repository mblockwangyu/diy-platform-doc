<img src="../../../zh/mechanical-parts/bearings/images/flange-bearing-4x8x3mm.jpg" style="width:400;padding:5px 5px 15px 0px;">


# Flange Bearing 4x8x3mm


### Features

- Perfect for adding to Timing Pulley 66T , Timing Pulley 90T, and Bracket
P3. 
- Makes the rotating part virtually friction-free.
- Diameters: 4mm (inside), 8mm (outside)
- Sold in pack of 10.

 

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/bearings/images/flange-bearing-4x8x3mm-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/bearings/images/flange-bearing-4x8x3mm-2.jpg" style="width:700;padding:5px 5px 15px 0px;">