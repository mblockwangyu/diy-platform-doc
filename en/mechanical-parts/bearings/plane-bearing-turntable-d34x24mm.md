<img src="../../../zh/mechanical-parts/bearings/images/plane-bearing-turntable-d34x24mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Plane Bearing Turntable D34\*24mm


### Description

Makeblock plane bearing turntable can
be used as motion joint or rotatable turntable in the  high load and
high precision designs. 

It has plane bearing for motion
stability and smoothing. It also can be used in Makeblock
mDrawbot.

### Features

-   The max diameter is 34mm, the height is
    24mm.
-   M4
    holes in turntable can be compatible with most parts and structures
    in Makeblock.

 

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/bearings/images/plane-bearing-turntable-d34x24mm-1.png" style="width:700;padding:5px 5px 15px 0px;">

### Demo

 
<img src="../../../zh/mechanical-parts/bearings/images/plane-bearing-turntable-d34x24mm-2.jpg" style="width:700;padding:5px 5px 15px 0px;">
 

 
