<img src="../../../zh/mechanical-parts/bearings/images/flange-bearing-8x16x5.jpg" style="width:400;padding:5px 5px 15px 0px;">


# Flange Bearing 8x16x5mm

8x16x5
flange bearing can operate with 8mm of shaft. It can be mounted to the
part of 16mm hole, e.g. U-shaped rack C.

 

### Features

-   Used
    with with 8mm shaft.

### Specifications

-   Type:
    Bearing
-   Material:
    steel 
-   Thickness:
    5 mm 
-   Outer
    diameter: 16 mm
-   Inner
    diameter: 8 mm 
-   Flange
    diameter: 18 mm.
