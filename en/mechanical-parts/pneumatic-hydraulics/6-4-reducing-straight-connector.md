# φ6 – φ4 Reducing Straight Connector

![](../../../zh/mechanical-parts/pneumatic-hydraulics/images/φ6---φ4-Reducing-Straight-Connector.jpg)

### Description

One end is used with φ6 air pipe and
the other end with φ4 air pipe. The channel is of straight-through type and reduces the φ6 air pipe to φ4 air pipe.

### Features

- Reduce the φ4 air pipe to φ6 air pipe.

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/6-4-reducing-straight-connector-4-pack_59012.png" alt="59012.png" width="600" />

### Connection Illustration

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/6-4-reducing-straight-connector-4-pack_1322.jpg" alt="1322.jpg" width="700" />

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/6-4-reducing-straight-connector-4-pack_59012-connector.jpg" alt="59012-connector.jpg" width="700" />
