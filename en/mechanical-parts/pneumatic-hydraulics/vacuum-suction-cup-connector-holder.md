# Vacuum Suction Cup Connector Holder

![](../../../zh/mechanical-parts/pneumatic-hydraulics/images/vacuum-suction-cup-connector-holder_Vacuum-Suction-Cup-Connector-Holder.jpg)

### Description

Makeblock vacuum suction cup connector
holder is a connector of air tube and vacuum suction
cup. It is a great spare fittings, which
is widely used in industry device and DIY projects.

### Features

- Material: Metal
- Total Size: 33×22mm
- M4 screw head for better compatibility with Makeblock parts and structures
- Air Tube Mouth Outside Dia(Each): 5.2mm
- Suction Cup Connected Dia: 8.2mm

### Prat List

- 1 × Vacuum suction cup connector
holder
- 1 × Screw Plug M5

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/vacuum-suction-cup-connector-holder_59004-size.png" alt="59004-size.png" width="565" />

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/vacuum-suction-cup-connector-holder_59004-size-2.png" alt="59004-size-2.png" width="332" />

### Demo

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/vacuum-suction-cup-connector-holder_59000-demo.png" alt="59000-demo.png" width="686" />

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/vacuum-suction-cup-connector-holder_50000-50001-59000-59001-59002-59003-59004-off.jpg" width="868" />

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/vacuum-suction-cup-connector-holder_50000-50001-59000-59001-59002-59003-59004-on.jpg" width="868" />
