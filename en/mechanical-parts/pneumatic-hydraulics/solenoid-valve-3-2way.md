# Solenoid Valve (3/2way)

![](../../../zh/mechanical-parts/pneumatic-hydraulics/images/solenoid-valve-3-2way_Solenoid-Valve-3-2way.jpg)

### Description

Makeblock 3/2way solenoid valve is
usually used with a single acting pneumatic actuator. Two-position means two controllable positions and three-way means that there are three air channels, one of which is connected with the air supply in general, one of the other two of which is connected with the air inlet of the actuator and the other connected with the air outlet of the actuator.Energize the coil and then the air circuit is connected. Deenergize the coil and then the air circuit is disconnected.

 
### Features

- DC12V single electrically controlled solenoid valve switch.
- Can control the single acting pneumatic actuator directly.
- Can be involved in the controlling of complex air circuits.

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/solenoid-valve-3-2way_95061-1.png" alt="95061-1.png" width="785" />

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/solenoid-valve-3-2way_95061-2.png" alt="95061-2.png" width="783" />

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/solenoid-valve-3-2way_95061-3.png" alt="95061-3.png" width="607" />

<img src="../../../zh/mechanical-parts/pneumatic-hydraulics/images/solenoid-valve-3-2way_4.png" alt="4.png" width="580" />
