# Slider 496

![](images/slider496-blue-4-pack_Slider496.jpg)

 

**Features:**

-   Made
    from 6061 aluminum extrusion, anodized surface. Excellent strength
    and twist resistance.
-   Threaded
    slot enables easy and flexible connection.
-   Compatible
    to V-slot Bearing
-   Sold
    in Packs of 4.

 

**Size
Charts(mm):**

<img src="images/slider496-blue-4-pack_slider1.jpg" alt="slider1.jpg" width="714" />

********Demo:********

<img src="images/slider496-blue-4-pack_slider2.jpg" alt="slider2.jpg" width="697" />

 
