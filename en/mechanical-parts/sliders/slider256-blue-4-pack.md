# Slider 256

![](images/slider256-blue-4-pack_Slider256.jpg)

 

**Features:**

 

-   Made
    from 6061 aluminum extrusion, anodized surface. Excellent strength
    and twist resistance.
-   Threaded
    slot enables easy and flexible connection.
-   Compatible
    to V-slot Bearing
-   Sold
    in Packs of 4.

 

**Size
Charts(mm):**

<img src="images/slider256-blue-4-pack_slider1.jpg" alt="slider1.jpg" width="714" />

********Demo:********

<img src="images/slider256-blue-4-pack_slider2.jpg" alt="slider2.jpg" width="697" />
