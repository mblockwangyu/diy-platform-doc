# Shafts

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="shafts/linear-motion-shaft-d4.html" target="_blank"><img src="../../zh/mechanical-parts/shafts/images/linear-motion-shaft-d4-80mm.jpg" width="150px;"></a><br>
<p>Linear Motion Shaft D4</p></td>

<td width="25%;"><a href="shafts/d-shaft-4-56mm.html" target="_blank"><img src="../../zh/mechanical-parts/shafts/images/d-shaft-4x56mm.jpg" width="150px;"></a><br>
<p>D Shaft 4x56mm</p></td>

<td width="25%;"><a href="shafts/d-shaft-4-128mm.html" target="_blank"><img src="../../zh/mechanical-parts/shafts/images/d-shaft-4x128mm.jpg" width="150px;"></a><br>
<p>D Shaft 4x128mm</p></td>

<td width="25%;"><a href="shafts/d-shaft-4-160mm.html" target="_blank"><img src="../../zh/mechanical-parts/shafts/images/d-shaft-4x160mm.jpg" width="150px;"></a><br>
<p>D Shaft 4x160mm</p></td>
</tr>

<tr>
<td><a href="shafts/shaft-d8-96mm.html" target="_blank"><img src="../../zh/mechanical-parts/shafts/images/d-shaft-8x96mm.jpg" width="150px;"></a><br>
<p>D Shaft 8x96mm</p></td>
<td><a href="shafts/flexible-coupling-4x4mm.html" target="_blank"><img src="../../zh/mechanical-parts/shafts/images/flexible-coupling-4x4mm.jpg" width="150px;"></a><br>
<p>Flexible coupling 4*4mm</p></td>
<td><a href="shafts/shaft-collar-4mm.html" target="_blank"><img src="../../zh/mechanical-parts/shafts/images/shaft-collar-4mm.jpg" width="150px;"></a><br>
<p>Shaft Collar 4mm</p></td>
<td><a href="shafts/shaft-collar-8mm.html" target="_blank"><img src="../../zh/mechanical-parts/shafts/images/shaft-collar-8mm.jpg" width="150px;"></a><br>
<p>Shaft Collar 8mm</p></td>
</tr>

<tr>
<td><a href="shafts/shaft-connector-4mm.html" target="_blank"><img src="../../zh/mechanical-parts/shafts/images/shaft-connector-4mm.jpg" width="150px;"></a><br>
<p>Shaft Connector 4mm</p></td>
<td><a href="shafts/t6-l256mm-lead-screw-and-brass-flange-nut-set.html" target="_blank"><img src="../../zh/mechanical-parts/shafts/images/t6-l256mm.jpg" width="150px;"></a><br>
<p>T6 L256mm Lead Screw and Brass Flange Nut Set</p></td>
<td><a href="shafts/threaded-shaft-4x22mm.html" target="_blank"><img src="../../zh/mechanical-parts/shafts/images/threaded-shaft-4x22mm-1.jpg" width="150px;"></a><br>
<p>Threaded Shaft 4*22mm</p></td>
<td><a href="shafts/threaded-shaft-4x39mm.html" target="_blank"><img src="../../zh/mechanical-parts/shafts/images/threaded-shaft-4x39mm.jpg" width="150px;"></a><br>
<p>Threaded Shaft 4*39mm</p></td>
</tr>

<tr>
<td><a href="shafts/universal-joint-4x4mm.html" target="_blank"><img src="../../zh/mechanical-parts/shafts/images/universal-joint-4x4mm.jpg" width="150px;"></a><br>
<p>Universal Joint 4*4mm</p></td>
</tr>

</table>