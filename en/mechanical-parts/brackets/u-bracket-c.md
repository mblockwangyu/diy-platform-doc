<img src="../../../zh/mechanical-parts/brackets/images/u-bracket-c.jpg" style="width:400;padding:5px 5px 15px 0px;">

# U Bracket C


U-Bracket
C can operate with 8mm shaft, 8mm bearing and 16mm bearing for mounting
4mm or 8mm shaft.

 

### Features

-   With
    8mm and 16mm holes, it can be used for installation of 8mm and 16mm
    bearings.
-   With
    alluminum alloy material, it can vertically bear high
    pressure.


### Specifications

-   Material:
    Aluminum alloy 
-   Thickness:
    3 mm 
-   Length:
    30 mm 
-   Width:
    24 mm 
-   Height:
    60 mm

### Size Chart

<img src="../../../zh/mechanical-parts/brackets/images/u-bracket-c-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

 

### Examples

<img src="../../../zh/mechanical-parts/brackets/images/u-bracket-c-2.jpg" style="width:600;padding:5px 5px 15px 0px;">
 
