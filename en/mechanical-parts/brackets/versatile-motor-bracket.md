<img src="../../../zh/mechanical-parts/brackets/images/versatile-motor-bracket.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Versatile Motor Bracket

### Description

Makeblock Versatile Motor Bracket has
the more function as 42BYG Stepper Motor Bracket B. It is compatible to [Makeblock 42BYG geared
stepper motors](http://www.makeblock.cc/42byg-geared-stepper-motor/) and
[Makeblock 36 DC geared
motors](http://www.makeblock.cc/36-dc-geared-motor-12v240rpm/).

 

### Features 

-   Made from 6061 aluminum extrusion,
    anodized surface.
-   With four taped holes for mounting
    your stepper motor and M4 through holes for attachment.
-   4 Screw M3x8 included.
-   Size: 61x27x3mm

 

### SizeChart(mm)

<img src="../../../zh/mechanical-parts/brackets/images/versatile-motor-bracket-1.png" style="width:700;padding:5px 5px 15px 0px;">


### Demo

<img src="../../../zh/mechanical-parts/brackets/images/versatile-motor-bracket-2.png" style="width:700;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/versatile-motor-bracket-3.png" style="width:700;padding:5px 5px 15px 0px;">
