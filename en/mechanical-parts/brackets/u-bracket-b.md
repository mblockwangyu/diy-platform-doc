<img src="../../../zh/mechanical-parts/brackets/images/u-bracket-b.jpg" style="width:400;padding:5px 5px 15px 0px;">

# U Bracket B

### Description

U Bracket B is usually used as the structural support or connection
point for shafts, timing pulley 18T, timing pulley 62T, timing pulley
90T and so on.  

                           
### Features 

- With seven 8mm center holes that can be attached with 8mm shaft or
bearings 

- 44 x 4mm holes which
are compatible with most Makeblock components     

- High-strength
aluminum extrusion with anodized surface and 3mm of thickness designed
for a long time service   

 

### Size
Charts(mm)

<img src="../../../zh/mechanical-parts/brackets/images/u-bracket-b-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/brackets/images/u-bracket-b-2.jpg" style="width:400;padding:5px 5px 15px 0px;">
 
<img src="../../../zh/mechanical-parts/brackets/images/u-bracket-b-3.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/u-bracket-b-4.jpg" style="width:400;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/u-bracket-b-5.png" style="width:400;padding:5px 5px 15px 0px;">
