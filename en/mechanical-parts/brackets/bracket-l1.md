<img src="../../../zh/mechanical-parts/brackets/images/bracket-l1.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Bracket L1

### Description

Bracket L1 is an important
fastener to reinforce right angle corner
joints.


### Features

-   Specially
    designed with 90 degrees angle
-   two
    8mm diameter center holes and 16 M4 mounting holes
-   Aluminum
    extrusion (high strength), 2mm thick, anodized surface(long time to
    put without barely rusting)

 

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/brackets/images/bracket-l1-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### Demo

<img src="../../../zh/mechanical-parts/brackets/images/bracket-l1-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/bracket-l1-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/bracket-l1-4.jpg" style="width:300;padding:5px 5px 15px 0px;">
 
<img src="../../../zh/mechanical-parts/brackets/images/bracket-l1-5.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/bracket-l1-6.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/bracket-l1-7.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/bracket-l1-8.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="../../../zh/mechanical-parts/brackets/images/bracket-l1-9.jpg" style="width:300;padding:5px 5px 15px 0px;">
