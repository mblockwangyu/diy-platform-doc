# Beam0412-124

![](images/beam0412-124-blue-4-pack_Beam0412-124.jpg)

 
### Building Examples:

<img src="images/beam0412-124-blue-4-pack_beam0412-124.gif" width="720" />

### Size Chart (mm):

<img src="images/beam0412-124-blue-4-pack_2015-12-25-14-38-57.jpg" width="720" />

### Specifications

- SKU: 60713
- Material: 6061 Aluminum
- Diameter of holes: 4mm
- Cross-section area: 4 x 12mm
- Length: 124mm
- Package content: 4 x Beam 0412-124
- Dimension: 124 x 12 x 4mm (4.88 x 0.47 x 0.16'')
- Net Weight: 45.6g (1.61oz)
