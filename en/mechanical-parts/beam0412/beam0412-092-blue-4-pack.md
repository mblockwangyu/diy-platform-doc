# Beam0412-092

![](images/beam0412-092-blue-4-pack_Beam0412-092.jpg)

 
### Building Examples:

<img src="images/beam0412-092-blue-4-pack_beam0412-092.gif" width="720" />

### Size Chart (mm):

<img src="images/beam0412-092-blue-4-pack_2015-12-24-17-57-44.jpg" width="720" />

### Specifications

- SKU: 60709
- Material: 6061 Aluminum
- Diameter of holes: 4mm
- Cross-section area: 4 x 12mm
- Length: 92mm
- Package content: 4 x Beam 0412-092
- Dimension: 92 x 12 x 4mm (3.62 x 0.47 x 0.16'')
- Net Weight: 32.8g (1.16oz)
