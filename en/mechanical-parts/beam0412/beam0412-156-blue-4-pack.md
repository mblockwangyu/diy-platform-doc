# Beam0412-156

![](images/beam0412-156-blue-4-pack_Beam0412-156.jpg)


### Building Examples:

<img src="images/beam0412-156-blue-4-pack_beam0412-156.gif" width="720" />

### Size Chart (mm):

<img src="images/beam0412-156-blue-4-pack_2015-12-29-11-51-34.jpg" width="720" />

### Specifications

- SKU: 60717
- Material: 6061 Aluminum
- Diameter of holes: 4mm
- Cross-section area: 4 x 12mm
- Length: 156mm
- Package content: 4 x Beam 0412-156
- Dimension: 156 x 12 x 4mm (6.14 x 0.47 x 0.16'')
- Net Weight: 56.8g (2oz)
