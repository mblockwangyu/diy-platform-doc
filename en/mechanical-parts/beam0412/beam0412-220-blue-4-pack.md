# Beam0412-220

![](images/beam0412-220-blue-4-pack_Beam0412-220.jpg)

 
### Building Examples:

<img src="images/beam0412-220-blue-4-pack_beam0412-220.gif" width="720" />

### Size Chart (mm):

<img src="images/beam0412-220-blue-4-pack_2015-12-29-15-58-05.jpg" width="720" />

### Specifications

- SKU: 60725
- Material: 6061 Aluminum
- Diameter of holes: 4mm
- Cross-section area: 4 x 12mm
- Length: 220mm
- Package content: 4 x Beam 0412-220
- Dimension: 220 x 12 x 4mm (8.66 x 0.47 x 0.16'')
- Net Weight: 82.4g (2.91oz)
