# Beam0412-036


![](images/beam0412-036-blue-4-pack_Beam0412-036.jpg)

 <img src="images/beam0412-036-blue-4-pack_60701-1.jpg" width="350" />

### Building Examples:

<img src="images/beam0412-036-blue-4-pack_beam0412-036.gif" width="720" />

### Size Chart (mm):

<img src="images/beam0412-036-blue-4-pack_60701-sizechat.gif" width="720" />

### Specifications

- SKU: 60701
- Material: 6061 Aluminum
- Diameter of holes: 4mm
- Cross-section area: 4 x 12mm
- Length: 36mm
- Package content: 4 x Beam 0412-036
- Dimension: 36 x 12 x 4mm (1.42 x 0.47 x 0.16'')
- Net Weight: 12g (0.42oz)
