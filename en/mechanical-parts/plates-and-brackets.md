# Plates and Brackets

<table style="text-align:center;">
<tr>
<td><a href="plates-and-brackets/8mm-bearing-bracket-a.html" target="_blank"><img src="plates-and-brackets/images/8mm-bearing-bracket-a_8mm-Bearing-Bracket-A.jpg"></a><br>
<p>8mm Bearing Bracket A</p>
</td>

<td><a href="plates-and-brackets/36mm-motor-bracket.html" target="_blank"><img src="plates-and-brackets/images/36mm-motor-bracket_36mm-Motor-Bracket.jpg"></a><br>
<p>36mm Motor Bracket</p>
</td>

<td><a href="plates-and-brackets/42byg-stepper-motor-bracket-b-blue.html" target="_blank"><img src="plates-and-brackets/images/42byg-stepper-motor-bracket-b-blue_42BYG-Stepper-Motor-Bracket-B-Blue.jpg"></a><br>
<p>42BYG Stepper Motor Bracket B-Blue</p>
</td>

<td><a href="plates-and-brackets/57byg-stepper-motor-bracket-pack-black.html" target="_blank"><img src="plates-and-brackets/images/57byg-stepper-motor-braket-pack-black_57BYG-Stepper-Motor-Braket-Pack-Black.jpg"></a><br>
<p>57BYG Stepper Motor Bracket Pack-Black</p>
</td>
</tr>

<tr>
<td><a href="plates-and-brackets/base-bracket.html" target="_blank"><img src="plates-and-brackets/images/base-bracket_Base-Bracket-B.jpg"></a><br>
<p>Base Bracket B</p>
</td>

<td><a href="plates-and-brackets/bracket-3x3-blue-4-pack.html" target="_blank"><img src="plates-and-brackets/images/bracket-3x3-blue-4-pack_Bracket-3x3-Blue.jpg"></a><br>
<p>Bracket 3*3</p>
</td>

<td><a href="plates-and-brackets/bracket-3x6-blue-4-pack.html" target="_blank"><img src="plates-and-brackets/images/bracket-3x6-blue-4-pack_Bracket-3x6-Blue.jpg"></a><br>
<p>Bracket 3*6</p>
</td>

<td><a href="plates-and-brackets/bracket-l1-blue-pair.html" target="_blank"><img src="plates-and-brackets/images/bracket-l1-blue-pair_Bracket-L1-Blue.jpg"></a><br>
<p>Bracket L1</p>
</td>
</tr>

<tr>
<td><a href="plates-and-brackets/bracket-p1-blue-pair.html" target="_blank"><img src="plates-and-brackets/images/bracket-p1-blue-pair_Bracket-P1-Blue.jpg"></a><br>
<p>Bracket P1</p>
</td>

<td><a href="plates-and-brackets/bracket-p3-blue-pair.html" target="_blank"><img src="plates-and-brackets/images/bracket-p3-blue-pair_Bracket-P3-Blue.jpg"></a><br>
<p>Bracket P3</p>
</td>

<td><a href="plates-and-brackets/bracket-u1-blue-pair.html" target="_blank"><img src="plates-and-brackets/images/bracket-u1-blue-pair_Bracket-U1-Blue.jpg"></a><br>
<p>Bracket U1</p>
</td>

<td><a href="plates-and-brackets/dc-motor-25-bracket-b-blue-pair.html" target="_blank"><img src="plates-and-brackets/images/dc-motor-25-bracket-b-blue-pair_DC-Motor-25-Bracket-B.jpg"></a><br>
<p>DC Motor-25 Bracket</p>
</td>
</tr>

<tr>
<td><a href="plates-and-brackets/dc-motor-37-bracket-a-blue-pair.html" target="_blank"><img src="plates-and-brackets/images/dc-motor-37-bracket-a-blue-pair_DC-Motor-37-Bracket-A.jpg"></a><br>
<p>DC Motor-37 Bracket A</p>
</td>

<td><a href="plates-and-brackets/dc-motor-37-bracket-b-gold.html" target="_blank"><img src="plates-and-brackets/images/dc-motor-37-bracket-b-gold_DC-Motor-37-Bracket-B.jpg"></a><br>
<p>DC Motor-37 Bracket B</p>
</td>

<td><a href="plates-and-brackets/disc-d72-blue-pair.html" target="_blank"><img src="plates-and-brackets/images/disc-d72-blue-pair_Disc-D72.jpg"></a><br>
<p>Disc D72</p>
</td>

<td><a href="plates-and-brackets/linear-motion-block-bracket-a-single-pack.html" target="_blank"><img src="plates-and-brackets/images/linear-motion-block-bracket-a-single-pack_Linear-Motion-Block-Bracket-A.jpg"></a><br>
<p>Linear Motion Block Bracket A</p>
</td>
</tr>

<tr>
<td><a href="plates-and-brackets/meds15-servo-motor-bracket-blue-pair.html" target="_blank"><img src="plates-and-brackets/images/meds15-servo-motor-bracket-blue-pair_MEDS15-Servo-Motor-Bracket.jpg"></a><br>
<p>MECDS-150 Servo Bracket</p>
</td>

<td><a href="plates-and-brackets/plate-3x6-blue-4-pack.html" target="_blank"><img src="plates-and-brackets/images/plate-3x6-blue-4-pack_Plate-3x6-Blue.jpg"></a><br>
<p>Plate 3*6</p>
</td>

<td><a href="plates-and-brackets/plate-7-9-b-blue-pair.html" target="_blank"><img src="plates-and-brackets/images/Plate-7×9-B.jpg"></a><br>
<p>Plate 7*9-B</p>
</td>

<td><a href="plates-and-brackets/plate-45-blue-pair.html" target="_blank"><img src="plates-and-brackets/images/Plate-45°-Blue.jpg"></a><br>
<p>Plate 45°</p>
</td>
</tr>

<tr>
<td><a href="plates-and-brackets/plate-i1-blue-pair.html" target="_blank"><img src="plates-and-brackets/images/plate-i1-blue-pair_Plate-I1-Blue.jpg"></a><br>
<p>Plate I1</p>
</td>

<td><a href="plates-and-brackets/plate-o1-blue-pair.html" target="_blank"><img src="plates-and-brackets/images/plate-o1-blue-pair_Plate-O1.jpg"></a><br>
<p>Plate O1</p>
</td>

<td><a href="plates-and-brackets/servo-bracket-a.html" target="_blank"><img src="plates-and-brackets/images/servo-bracket-a_Servo-Bracket-A.jpg"></a><br>
<p>Servo Bracket A</p>
</td>

<td><a href="plates-and-brackets/shaft-clamping-hub-8mm.html" target="_blank"><img src="plates-and-brackets/images/shaft-clamping-hub-8mm_Shaft-Clamping-Hub-8mm.jpg"></a><br>
<p>Shaft Clamping Hub 8mm</p>
</td>
</tr>

<tr>
<td><a href="plates-and-brackets/triangle-plate-6x8-blue-pair.html" target="_blank"><img src="plates-and-brackets/images/triangle-plate-6x8-blue-pair_Triangle-Plate-6x8.jpg"></a><br>
<p>Triangle Plate 6*8</p>
</td>

<td><a href="plates-and-brackets/u-bracket-b-blue-pair.html" target="_blank"><img src="plates-and-brackets/images/u-bracket-b-blue-pair_U-Bracket-B.jpg"></a><br>
<p>U Bracket B</p>
</td>

<td><a href="plates-and-brackets/u-bracket-c.html" target="_blank"><img src="plates-and-brackets/images/u-bracket-c_U-Bracket-C.jpg"></a><br>
<p>U Bracket C</p>
</td>

<td><a href="plates-and-brackets/versatile-motor-bracket.html" target="_blank"><img src="plates-and-brackets/images/versatile-motor-bracket_Versatile-Motor-Bracket.jpg"></a><br>
<p>Versatile Motor Bracket</p>
</td>
</tr>

</table>