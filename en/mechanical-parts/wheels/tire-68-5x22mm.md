<img src="../../../zh/mechanical-parts/wheels/images/tyre-68-5.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Tyre 68.5\*22mm

### Features

- Made of silicon
- Compatible to Timing Pulley 90T
- Sold in pack of 4

### Demo

<img src="../../../zh/mechanical-parts/wheels/images/tyre-68-5-1.jpg" style="width:600;padding:5px 5px 15px 0px;">

 
