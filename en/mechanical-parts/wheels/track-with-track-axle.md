<img src="../../../zh/mechanical-parts/wheels/images/track-with-track-axle.jpg" style="width:400;padding:5px 5px 15px 0px;">

# Track with Track Axle

### Features:

- Made of silicon.
- Can be spliced into any length.
- Track Axle included.
- Sold in pack of 40.

### Size Charts(mm)

<img src="../../../zh/mechanical-parts/wheels/images/track-with-track-axle-1.jpg" style="width:600;padding:5px 5px 15px 0px;">

### Demo:

<img src="../../../zh/mechanical-parts/wheels/images/track-with-track-axle-2.jpg" style="width:600;padding:5px 5px 15px 0px;">
