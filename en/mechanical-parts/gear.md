# Gears

<table style="text-align:center;">
<tr>
<td><a href="gears/gear-16t-blue.html" target="_blank"><img src="../../zh/mechanical-parts/gears/images/gear-16t.jpg" width="200px;"></a>
<br><p>Gear 16T</p>
</td>

<td><a href="gears/gear48t-blue.html" target="_blank"><img src="../../zh/mechanical-parts/gears/images/gear-48t.jpg" width="200px;"></a>
<br><p>Gear 48T</p>
</td>

<td><a href="gears/gear-80t-blue.html" target="_blank"><img src="../../zh/mechanical-parts/gears/images/gear-80t.jpg" width="200px;"></a>
<br><p>Gear 80T</p>
</td>

</tr>

<tr>
<td><a href="gears/plastic-gear-8t.html" target="_blank"><img src="../../zh/mechanical-parts/gears/images/plastic-gear-8t.jpg" width="200px;"></a>
<br><p>Plastic Gear 8T</p>
</td>

<td><a href="gears/plastic-gear-56t.html" target="_blank"><img src="../../zh/mechanical-parts/gears/images/plastic-gear-56t.png" width="200px;"></a>
<br><p>Plastic Gear 56T</p>
</td>

<td><a href="gears/plastic-gear-72t.html" target="_blank"><img src="../../zh/mechanical-parts/gears/images/plastic-gear-72t.png" width="200px;"></a>
<br><p>Plastic Gear 72T</p>
</td>

</tr>

</table>