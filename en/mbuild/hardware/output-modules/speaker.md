# Speaker

The Speaker module can store and play audio files. You can easily store audio files on the Speaker module and play them by using the blocks in mBlock 5.

<img src="../../../../zh/mbuild/hardware/output-modules/images/speaker.png" style="padding:3px 0px 12px 3px;width:200px;">

## Store customized audio files on the Speaker module

Perform the following steps to store an audio file on the Speaker module:

#### 1\. Connect the Speaker module to a PC

Use a Micro USB cable to connect the Speaker module to your PC. After the connection is complete, the PC displays the disk of the speaker as a removable disk, and you can open the disk to view the files on it.

#### 2\. Store a customized audio file on the disk

Drag and drop the audio file to the disk.

<small>**Note:**<br>Only **audio files stored in the root directory** can be properly played. Do not put an audio file to be played in any folders created by you.<br>The space of the disk is limited. An audio file may fail to be stored because its size is too large. When this happens, compress the file and try storing it again.</small>

#### 3\. Modify the file name
Due to the hardware constraint, the Speaker module can only identify an audio file based on the first 4 characters of its file name (only English letters, numbers or punctuation marks should be included). In order to store your file successfully, you need to give the file a proper name (4 characters made up of English letters, numbers or punctuation marks). 
The examples in the following might help you better understand how to rename your files. On the left are the original names and on the right are the revised names.

- twinkling star.mp3 -------------->twst.mp3
- Mary has a little lamb.mp3 ---------> mhll.mp3
- happy birthday.mp3 ---------> M001.mp3

After you finish modifying the file name, you can use blocks in mBlock 5 to play the audio file. 

#### 4\. Play the audio file

mBlock 5 provides two blocks for playing customized audio files, as shown in the following.

<img src="../../../../en/mbuild/hardware/output-modules/1.png" style="padding:3px 0px 12px 3px;width:200px;">

Example program:

Store the audio file named *twst* on the module and then use the following blocks to play it.

<img src="../../../../en/mbuild/hardware/output-modules/2.png" style="padding:3px 0px 12px 3px;width:200px;">

When you press the space key, the audio file is played.

#### 5\. Restore the files on the Speaker module

Improper operations might cause damage to the module's built-in sound library, as a result of which the module may fail to output those predefined sounds.

When this happens, you can restore the predefined audio files.
Download the following default audio files and use them to replace the original files.

<a href="../../../../en/mbuild/hardware/output-modules/Preset audio files of the speaker.rar" download>Predefined audio files</a>

### Parameters

- Size: 24×36mm
- Storage: 16M
- File type: mp3
- Interface: Micro-USB
- Rated operating current: 400mA