# Dual RGB Color Sensor

The Dual RGB Color Sensor consists of a pair of light-sensitive elements to detect the color of an object. While detecting the color of objects, the Dual RGB Color Sensor can also assist in line-following.

<img src="../../../../zh/mbuild/hardware/sensors/images/dual-color-0.png" style="padding:3px 0px 12px 3px;width:800px;">

<img src="../../../../zh/mbuild/hardware/sensors/images/dual-color-00.png" style="padding:3px 0px 12px 3px;width:800px;">


### Real-Life Examples

- Use color sensor to design an assembly line of parts sorting
<img src="../../../../zh/mbuild/hardware/sensors/images/dual-color-1.png" style="padding:8px 0px 12px 3px;width:400px;">

### Parameters

- Operating distance: 5~15mm from objects
- Operating current: 70mA
