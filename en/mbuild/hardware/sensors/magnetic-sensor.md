# Magnetic Sensor

Magnetic sensor can detect whether there is a magnet around the module.

<img src="../../../../zh/mbuild/hardware/sensors/images/magnetic-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### Real-Life Example

The virtual wall of the sweeping robot uses magnetic field detection to determine whether it can pass.

<img src="../../../../zh/mbuild/hardware/sensors/images/magnetic-sensor-2.png" style="padding:3px 0px 12px 3px;width:400px;">

### Parameters

- Size: 24×20mm
- Range value: <1cm
- Operating current: 15mA