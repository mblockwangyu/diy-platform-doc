# Temperature Sensor

The Temperature Sensor can detect the temperature of water, a body, and other objects.

<img src="../../../../zh/mbuild/hardware/sensors/images/temperature-sensor.png" style="padding:3px 0px 12px 3px;width:450px;">

Before use, please assemble the temperature sensor probe and the block as shown above.

### Real-Life Examples

- Temperature sensor is used on the electronic thermometer<br>
<img src="../../../../zh/mbuild/hardware/sensors/images/temperature-sensor-1.jpg" style="padding:10px 0px 12px 3px;width:300px;">

### Parameters

- Size: 24×20mm
- Range value: -55~125℃
- Precision: ±0.5℃
- Operating current: 14mA