# Ranging Sensor

The Ranging Sensor detects the distance of an obstacle through infrared waves.

<img src="../../../../zh/mbuild/hardware/sensors/images/ranging-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### Real-Life Examples

- The Face ID of iPhone detects human facial structures via infrared waves<br>
<img src="../../../../zh/mbuild/hardware/sensors/images/ranging-1.jpg" style="padding:10px 0px 12px 3px;width:300px;">

### Parameters

- Size: 24×20mm
- Range value: 2~200cm
- Precision: ±5%
- Operating current: 33mA