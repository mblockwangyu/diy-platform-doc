# Flame Sensor

The Flame Sensor detects the flame and its size via infrared lights.

<img src="../../../../zh/mbuild/hardware/sensors/images/flame-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

Sunlight contains a large sum of infrared lights. When placed in direct sunlight, the Flame Sensor will not work properly, due to severe interference from the sunlight. 

### Real-Life Examples

- In some places where fire is strictly prohibited, flame sensor is very helpful<br>
<img src="../../../../zh/mbuild/hardware/sensors/images/flame-sensor-1.png" style="padding:12px 0px 12px 3px;width:250px;">

### Parameters

- Size: 24×20mm
- Flame size: 0~100
- Operating current: 20mA