# DC Motor Driver

The DC (Direct Current) Motor Driver can drive various DC motors, and control both the speed and direction of rotation.

<img src="../../../../zh/mbuild/hardware/motion/images/motor-driver.png" style="padding:3px 0px 12px 3px;width:200px;">

### Real-Life Examples

- The electric cars of Tesla use motor drivers<br>
<img src="../../../../zh/mbuild/hardware/motion/images/motor-driver-2.jpg" style="padding:8px 0px 12px 3px;width:300px;">
- The wind of hairdryer is produced by embedded motor<br>
<img src="../../../../zh/mbuild/hardware/motion/images/motor-driver-1.png" style="padding:8px 0px 12px 3px;width:300px;">

### Parameters

- Size: 24×24mm
- Operating current: &lt; 1A