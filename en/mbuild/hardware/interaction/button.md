# Button

The Button Block can work as event trigger, status switcher, or counter.

<img src="../../../../zh/mbuild/hardware/interaction/images/button.png" style="padding:3px 0px 12px 3px;width:300px;">

### Real-Life Examples

- The power button is used to turn on or turn off the laptop<br>
<img src="../../../../zh/mbuild/hardware/interaction/images/button-1.jpg" style="padding:10px 0px 12px 3px;width:300px;">
- The Home button of iPhone is used to lock or unlock the screen<br>
<img src="../../../../zh/mbuild/hardware/interaction/images/button-2.jpg" style="padding:10px 0px 12px 3px;width:300px;">
- The buttons of the mouse can recorder the time of being clicked<br>
<img src="../../../../zh/mbuild/hardware/interaction/images/button-3.jpg" style="padding:10px 0px 12px 3px;width:300px;">

### Parameters

- Size: 24×20mm
- Life span: 100,000 times
- Operating current: 15mA