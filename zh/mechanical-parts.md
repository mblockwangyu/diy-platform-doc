# 机械零件

### [梁和轨](mechanical-parts/beams-sliders.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="mechanical-parts/beam0824.html" target="_blank"><img src="mechanical-parts/beam0824/images/beam0824.jpg" width="150px;"></a><br>
<p>双孔梁0824</p></td>

<td width="25%;"><a href="mechanical-parts/beam0808.html" target="_blank"><img src="mechanical-parts/beam0808/images/beam0808-072.jpg" width="150px;"></a><br>
<p>单孔梁0808</p></td>

<td width="25%;"><a href="mechanical-parts/beam2424.html" target="_blank"><img src="mechanical-parts/beam2424/images/beam2424.jpg" width="150px;"></a><br>
<p>方形梁2424</p></td>

<td width="25%;"><a href="mechanical-parts/beam0412.html" target="_blank"><img src="mechanical-parts/beam0412/images/beam0412.jpg" width="150px;"></a><br>
<p>单孔连杆0412</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/square-beam.html" target="_blank"><img src="mechanical-parts/square-beam/square-beam.jpg" width="150px;"></a><br>
<p>单孔方梁</p></td>
<td><a href="mechanical-parts/slider-v-slot/slider256.html" target="_blank"><img src="mechanical-parts/slider-v-slot/slider256.jpg" width="150px;"></a><br>
<p>V槽轴承滑轨256mm</p></td>
<td><a href="mechanical-parts/slider-v-slot/slider496.html" target="_blank"><img src="mechanical-parts/slider-v-slot/slider496.jpg" width="150px;"></a><br>
<p>V槽轴承滑轨496mm</p></td>
<td><a href="mechanical-parts/beam.html" target="_blank"><img src="mechanical-parts/Beam/images/beam-1.jpg" width="150px;"></a><br>
<p>精益梁</p></td>
</tr>

</table>

### [轮](mechanical-parts/wheel.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td><a href="mechanical-parts/wheels/mecanum-wheel.html" target="_blank"><img src="mechanical-parts/wheels/images/mecanum-2.png" width="150px;"></a><br>
<p>麦克纳姆轮</p></td>

<td><a href="mechanical-parts/wheels/125mm-pu-wheel.html" target="_blank"><img src="mechanical-parts/wheels/images/125mm-pu-wheel.jpg" width="150px;"></a><br>
<p>125*24mm Pu轮</p></td>

<td><a href="mechanical-parts/wheels/caster-wheel-pair.html" target="_blank"><img src="mechanical-parts/wheels/images/caster-wheel.jpg" width="150px;"></a><br>
<p>万向轮</p></td>

<td><a href="mechanical-parts/wheels/injection-omnidirectional-wheel.html" target="_blank"><img src="mechanical-parts/wheels/images/injection-omnidirectional.png" width="150px;"></a><br>
<p>注塑全向轮</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/wheels/nylon-pulley-with-bearing.html" target="_blank"><img src="mechanical-parts/wheels/images/nylon-pulley.jpg" width="150px;"></a><br>
<p>尼龙轮</p></td>
<td><a href="mechanical-parts/wheels/slick-tyre-64-16mm.html" target="_blank"><img src="mechanical-parts/wheels/images/slick-tyre.jpg" width="150px;"></a><br>
<p>光面轮胎</p></td>
<td><a href="mechanical-parts/wheels/tire-68-5x22mm.html" target="_blank"><img src="mechanical-parts/wheels/images/tyre-68-5.jpg" width="150px;"></a><br>
<p>花纹轮胎</p></td>
<td><a href="mechanical-parts/wheels/track-with-track-axle.html" target="_blank"><img src="mechanical-parts/wheels/images/track-with-track-axle.jpg" width="150px;"></a><br>
<p>履带</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/wheels/track.html" target="_blank"><img src="mechanical-parts/wheels/images/track.png" width="150px;"></a><br>
<p>一体化橡胶履带</p></td>
</tr>

</table>

### [同步带轮](mechanical-parts/timing-pulley.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="mechanical-parts/timing-pulleys/belt-connector-b.html" target="_blank"><img src="mechanical-parts/timing-pulleys/images/belt-connector.jpg" width="150px;"></a><br>
<p>同步带固定片</p></td>

<td width="25%;"><a href="mechanical-parts/timing-pulleys/plastic-timing-pulley-62t-without-steps.html" target="_blank"><img src="mechanical-parts/timing-pulleys/images/plastic-timing-pulley-62t-without-steps.jpg" width="150px;"></a><br>
<p>注塑同步带轮62T无台阶</p></td>

<td width="25%;"><a href="mechanical-parts/timing-pulleys/plastic-timing-pulley-62t.html" target="_blank"><img src="mechanical-parts/timing-pulleys/images/plastic-timing-pulley-62t.jpg" width="150px;"></a><br>
<p>注塑同步带轮62T</p></td>

<td width="25%;"><a href="mechanical-parts/timing-pulleys/plastic-timing-pulley-90t-without-steps.html" target="_blank"><img src="mechanical-parts/timing-pulleys/images/plastic-timing-pulley-90t-without-steps-new.jpg" width="150px;"></a><br>
<p>注塑同步带轮90T无台阶</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/timing-pulleys/plastic-timing-pulley-90t.html" target="_blank"><img src="mechanical-parts/timing-pulleys/images/plastic-timing-pulley-90t.jpg" width="150px;"></a><br>
<p>注塑同步带轮90T</p></td>
<td><a href="mechanical-parts/timing-pulleys/timing-pulley-18t.html" target="_blank"><img src="mechanical-parts/timing-pulleys/images/timing-pulley-18t.jpg" width="150px;"></a><br>
<p>同步带轮18T</p></td>
<td><a href="mechanical-parts/timing-pulleys/timing-pulley-32t.html" target="_blank"><img src="mechanical-parts/timing-pulleys/images/timing-pulley-32t.jpg" width="150px;"></a><br>
<p>同步带轮32T</p></td>
<td><a href="mechanical-parts/timing-pulleys/timing-pulley-62t-blue.html" target="_blank"><img src="mechanical-parts/timing-pulleys/images/timing-pulley-62t.jpg" width="150px;"></a><br>
<p>同步带轮62T</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/timing-pulleys/timing-pulley-90t-blue.html" target="_blank"><img src="mechanical-parts/timing-pulleys/images/timing-pulley-90t.jpg" width="150px;"></a><br>
<p>同步带轮90T</p></td>
<td><a href="mechanical-parts/timing-pulleys/timing-pulley-slice-62t-b-blue.html" target="_blank"><img src="mechanical-parts/timing-pulleys/images/timing-pulley-slice-62t-b.jpg" width="150px;"></a><br>
<p>同步带轮62T挡片 B</p></td>
<td><a href="mechanical-parts/timing-pulleys/timing-pulley-slice-90t-b-blue.html" target="_blank"><img src="mechanical-parts/timing-pulleys/images/timing-pulley-slice-90t-b.jpg" width="150px;"></a><br>
<p>同步带轮90T挡片 B</p></td>
<td><a href="mechanical-parts/timing-pulleys/timing-belt-open-end.html" target="_blank"><img src="mechanical-parts/timing-pulleys/images/timing-belt-5m-open-end.jpg" width="150px;"></a><br>
<p>开口同步带</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/timing-pulleys/timing-belt.html" target="_blank"><img src="mechanical-parts/timing-pulleys/images/timing-belt.jpg" width="150px;"></a><br>
<p>同步带</p></td>
</tr>

</table>

### [支架](mechanical-parts/bracket.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="mechanical-parts/brackets/8mm-bearing-bracket-a.html" target="_blank"><img src="mechanical-parts/brackets/images/8mm-bearing-bracket-a.jpg" width="150px;"></a><br>
<p>8mm轴承支架 A</p></td>

<td width="25%;"><a href="mechanical-parts/brackets/36mm-motor-bracket.html" target="_blank"><img src="mechanical-parts/brackets/images/36mm-motor-bracket.jpg" width="150px;"></a><br>
<p>36mm电机支架</p></td>

<td width="25%;"><a href="mechanical-parts/brackets/42byg-stepper-motor-bracket-b-blue.html" target="_blank"><img src="mechanical-parts/brackets/images/42byg-stepper-motor-bracket-b.jpg" width="150px;"></a><br>
<p>42步进电机支架</p></td>

<td width="25%;"><a href="mechanical-parts/brackets/57byg-stepper-motor-bracket-black.html" target="_blank"><img src="mechanical-parts/brackets/images/57byg-stepper-motor-bracket.jpg" width="150px;"></a><br>
<p>57步进电机支架（黑）</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/brackets/base-bracket.html" target="_blank"><img src="mechanical-parts/brackets/images/base-bracket.jpg" width="150px;"></a><br>
<p>主控板支架 B</p></td>
<td><a href="mechanical-parts/brackets/bracket-3x3.html" target="_blank"><img src="mechanical-parts/brackets/images/bracket-3x3.jpg" width="150px;"></a><br>
<p>支架 3*3</p></td>
<td><a href="mechanical-parts/brackets/bracket-l1.html" target="_blank"><img src="mechanical-parts/brackets/images/bracket-l1.jpg" width="150px;"></a><br>
<p>支架 L1</p></td>
<td><a href="mechanical-parts/brackets/bracket-p1.html" target="_blank"><img src="mechanical-parts/brackets/images/bracket-p1.jpg" width="150px;"></a><br>
<p>支架 P1</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/brackets/bracket-p3.html" target="_blank"><img src="mechanical-parts/brackets/images/bracket-p3.jpg" width="150px;"></a><br>
<p>支架 P3</p></td>
<td><a href="mechanical-parts/brackets/bracket-u1.html" target="_blank"><img src="mechanical-parts/brackets/images/bracket-u1.jpg" width="150px;"></a><br>
<p>支架 U1</p></td>
<td><a href="mechanical-parts/brackets/dc-motor-25-bracket-b.html" target="_blank"><img src="mechanical-parts/brackets/images/dc-motor-25-bracket-b.jpg" width="150px;"></a><br>
<p>25电机支架</p></td>
<td><a href="mechanical-parts/brackets/dc-motor-37-bracket.html" target="_blank"><img src="mechanical-parts/brackets/images/dc-motor-37-bracket-b-gold_DC-Motor-37-Bracket-B.jpg" width="150px;"></a><br>
<p>37电机支架</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/brackets/meds15-servo-motor-bracket.html" target="_blank"><img src="mechanical-parts/brackets/images/mecds15-servo-motor-bracket.jpg" width="150px;"></a><br>
<p>MECDS-150 舵机支架</p></td>
<td><a href="mechanical-parts/brackets/u-bracket-b.html" target="_blank"><img src="mechanical-parts/brackets/images/u-bracket-b.jpg" width="150px;"></a><br>
<p>U形支架 B</p></td>
<td><a href="mechanical-parts/brackets/u-bracket-c.html" target="_blank"><img src="mechanical-parts/brackets/images/u-bracket-c.jpg" width="150px;"></a><br>
<p>U形支架 C</p></td>
<td><a href="mechanical-parts/brackets/versatile-motor-bracket.html" target="_blank"><img src="mechanical-parts/brackets/images/versatile-motor-bracket.jpg" width="150px;"></a><br>
<p>通用电机支架</p></td>
</tr>

</table>

### [轴](mechanical-parts/shaft.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="mechanical-parts/shafts/linear-motion-shaft-d4.html" target="_blank"><img src="mechanical-parts/shafts/images/linear-motion-shaft-d4-80mm.jpg" width="150px;"></a><br>
<p>光轴 D4</p></td>

<td width="25%;"><a href="mechanical-parts/shafts/d-shaft-4-56mm.html" target="_blank"><img src="mechanical-parts/shafts/images/d-shaft-4x56mm.jpg" width="150px;"></a><br>
<p>D形轴 4x56mm</p></td>

<td width="25%;"><a href="mechanical-parts/shafts/d-shaft-4-128mm.html" target="_blank"><img src="mechanical-parts/shafts/images/d-shaft-4x128mm.jpg" width="150px;"></a><br>
<p>D形轴 4x128mm</p></td>

<td width="25%;"><a href="mechanical-parts/shafts/d-shaft-4-160mm.html" target="_blank"><img src="mechanical-parts/shafts/images/d-shaft-4x160mm.jpg" width="150px;"></a><br>
<p>D形轴 4x160mm</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/shafts/shaft-d8-96mm.html" target="_blank"><img src="mechanical-parts/shafts/images/d-shaft-8x96mm.jpg" width="150px;"></a><br>
<p>D形轴 8x96mm</p></td>
<td><a href="mechanical-parts/shafts/flexible-coupling-4x4mm.html" target="_blank"><img src="mechanical-parts/shafts/images/flexible-coupling-4x4mm.jpg" width="150px;"></a><br>
<p>联轴器 4*4mm</p></td>
<td><a href="mechanical-parts/shafts/shaft-collar-4mm.html" target="_blank"><img src="mechanical-parts/shafts/images/shaft-collar-4mm.jpg" width="150px;"></a><br>
<p>轴套 4mm</p></td>
<td><a href="mechanical-parts/shafts/shaft-collar-8mm.html" target="_blank"><img src="mechanical-parts/shafts/images/shaft-collar-8mm.jpg" width="150px;"></a><br>
<p>轴套 8mm</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/shafts/shaft-connector-4mm.html" target="_blank"><img src="mechanical-parts/shafts/images/shaft-connector-4mm.jpg" width="150px;"></a><br>
<p>传动固定盘 4mm</p></td>
<td><a href="mechanical-parts/shafts/t6-l256mm-lead-screw-and-brass-flange-nut-set.html" target="_blank"><img src="mechanical-parts/shafts/images/t6-l256mm.jpg" width="150px;"></a><br>
<p>T6-L256丝杆传动装置</p></td>
<td><a href="mechanical-parts/shafts/threaded-shaft-4x22mm.html" target="_blank"><img src="mechanical-parts/shafts/images/threaded-shaft-4x22mm-1.jpg" width="150px;"></a><br>
<p>螺纹轴 4*22mm</p></td>
<td><a href="mechanical-parts/shafts/threaded-shaft-4x39mm.html" target="_blank"><img src="mechanical-parts/shafts/images/threaded-shaft-4x39mm.jpg" width="150px;"></a><br>
<p>螺纹轴 4*39mm</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/shafts/universal-joint-4x4mm.html" target="_blank"><img src="mechanical-parts/shafts/images/universal-joint-4x4mm.jpg" width="150px;"></a><br>
<p>万向节 4*4mm</p></td>
</tr>

</table>

### [轴承](mechanical-parts/bearing.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="mechanical-parts/bearings/flange-bearing-8x16x5mm.html" target="_blank"><img src="mechanical-parts/bearings/images/flange-bearing-8x16x5.jpg" width="150px;"></a><br>
<p>法兰轴承 8x16x5mm</p></td>

<td width="25%;"><a href="mechanical-parts/bearings/flange-bearing-4x8x3mm.html" target="_blank"><img src="mechanical-parts/bearings/images/flange-bearing-4x8x3mm.jpg" width="150px;"></a><br>
<p>法兰轴承 4x8x3mm</p></td>

<td width="25%;"><a href="mechanical-parts/bearings/female-rod-end-bearing.html" target="_blank"><img src="mechanical-parts/bearings/images/female-rod-end-bearing.jpg" width="150px;"></a><br>
<p>杆端关节轴承-母</p></td>

<td width="25%;"><a href="mechanical-parts/bearings/male-rod-end-bearing.html" target="_blank"><img src="mechanical-parts/bearings/images/male-rod-end-bearing.jpg" width="150px;"></a><br>
<p>杆端关节轴承-公</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/bearings/plain-ball-bearing-8-16-5mm.html" target="_blank"><img src="mechanical-parts/bearings/images/plain-ball-bearing-8-16-5mm.jpg" width="150px;"></a><br>
<p>滚珠轴承 8x16x5mm</p></td>
<td><a href="mechanical-parts/bearings/plain-ball-bearing-4-8-3mm.html" target="_blank"><img src="mechanical-parts/bearings/images/plain-ball-bearing-4-8-3mm.jpg" width="150px;"></a><br>
<p>滚珠轴承 4x8x3mm</p></td>
<td><a href="mechanical-parts/bearings/plane-bearing-turntable-d34x24mm.html" target="_blank"><img src="mechanical-parts/bearings/images/plane-bearing-turntable-d34x24mm.jpg" width="150px;"></a><br>
<p>平面轴承转盘 D34x24mm</p></td>
<td><a href="mechanical-parts/bearings/linear-motion-block-bracket.html" target="_blank"><img src="mechanical-parts/bearings/images/linear-motion-block-bracket.jpg" width="150px;"></a><br>
<p>光轴滑块</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/bearings/roller-motion-block-bracket.html" target="_blank"><img src="mechanical-parts/bearings/images/roller-motion-block.jpg" width="150px;"></a><br>
<p>滚轴滑块</p></td>
</tr>
</table>

### [连接片及连接杆](mechanical-parts/plate.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="mechanical-parts/plates/disc-d72.html" target="_blank"><img src="mechanical-parts/plates/images/disc-d72.jpg" width="150px;"></a><br>
<p>圆盘 D72</p></td>

<td width="25%;"><a href="mechanical-parts/plates/linear-motion-block-bracket.html" target="_blank"><img src="mechanical-parts/plates/images/linear-motion-block-bracket-a.jpg" width="150px;"></a><br>
<p>直线导轨滑块连接器 A</p></td>

<td width="25%;"><a href="mechanical-parts/plates/plate-3x6.html" target="_blank"><img src="mechanical-parts/plates/images/plate-3x6.jpg" width="150px;"></a><br>
<p>连接片 3*6</p></td>

<td width="25%;"><a href="mechanical-parts/plates/plate-7-9-b.html" target="_blank"><img src="mechanical-parts/plates/images/plate-7-9-b.jpg" width="150px;"></a><br>
<p>连接片 7*9-B</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/plates/plate-45.html" target="_blank"><img src="mechanical-parts/plates/images/plate-45.jpg" width="150px;"></a><br>
<p>连接片 45°</p></td>
<td><a href="mechanical-parts/plates/plate-135.html" target="_blank"><img src="mechanical-parts/plates/images/plate-135.jpg" width="150px;"></a><br>
<p>连接片 135°</p></td>
<td><a href="mechanical-parts/plates/plate-i1.html" target="_blank"><img src="mechanical-parts/plates/images/plate-i1.jpg" width="150px;"></a><br>
<p>连接片 I1</p></td>
<td><a href="mechanical-parts/plates/plate-o1.html" target="_blank"><img src="mechanical-parts/plates/images/plate-o1.jpg" width="150px;"></a><br>
<p>连接片 O1</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/plates/shaft-clamping-hub-8mm.html" target="_blank"><img src="mechanical-parts/plates/images/shaft-clamping-8mm.jpg" width="150px;"></a><br>
<p>8mm轴轮连接片</p></td>
<td><a href="mechanical-parts/plates/triangle-plate-6x8.html" target="_blank"><img src="mechanical-parts/plates/images/triangle-plate-6x8.jpg" width="150px;"></a><br>
<p>三角连接片 6*8</p></td>
<td><a href="mechanical-parts/plates/cross-plate.html" target="_blank"><img src="mechanical-parts/plates/images/cross-plate.jpg" width="150px;"></a><br>
<p>十字连接片</p></td>
<td><a href="mechanical-parts/plates/t-plate.html" target="_blank"><img src="mechanical-parts/plates/images/t-plate.jpg" width="150px;"></a><br>
<p>T型连接片</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/plates/plate-0324-184.html" target="_blank"><img src="mechanical-parts/plates/images/plate-0324-184.jpg" width="150px;"></a><br>
<p>连接片0324-184</p></td>
</tr>

</table>

### [链传动](mechanical-parts/sprocket.md)

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="mechanical-parts/sprockets/04c-10t-sprocket.html" target="_blank"><img src="mechanical-parts/sprockets/images/04c-10t-sprocket.jpg" width="150px;"></a><br>
<p>04C2分 10齿链轮</p></td>

<td width="25%;"><a href="mechanical-parts/sprockets/04c-20t-sprocket.html" target="_blank"><img src="mechanical-parts/sprockets/images/04c-20t-sprocket.jpg" width="150px;"></a><br>
<p>04C2分 20齿链轮</p></td>

<td width="25%;"><a href="mechanical-parts/sprockets/04c-30t-sprocket.html" target="_blank"><img src="mechanical-parts/sprockets/images/04c-30t-sprocket.jpg" width="150px;"></a><br>
<p>04C2分 30齿链轮</p></td>

<td width="25%;"><a href="mechanical-parts/sprockets/04c-roller-chain-1-5m.html" target="_blank"><img src="mechanical-parts/sprockets/images/04c-roller-chain-1-5m.jpg" width="150px;"></a><br>
<p>04C2分 1.5m链条</p></td>
</tr>
</table>

### [气动及液压](mechanical-parts/pneumatic-hydraulics.md)

<table cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>

<td width="25%;"><a href="mechanical-parts/pneumatic-hydraulics/double-acting-mini-cylinder-mi10x60ca.html" target="_blank"><img src="mechanical-parts/pneumatic-hydraulics/images/makeblock-double-acting-mini-cylinder-pack-mi10x60ca_Makeblock-Double-Acting-Mini-Cylinder-Pack-MI10X60CA.jpg" style="width:150px;"></a><br>
<p>10x60mm 双作用气缸</p>
</td>

<td width="25%;"><a href="mechanical-parts/pneumatic-hydraulics/single-acting-mini-cylinder-msi8x20ufa.html" target="_blank"><img src="mechanical-parts/pneumatic-hydraulics/images/single-acting-mini-cylinder-pack-msi8x20ufa_Single-Acting-Mini-Cylinder-Pack-MSI8X20UFA.jpg" style="width:150px;"></a><br>
<p>8X20mm 单作用气缸</p>
</td>

<td width="25%;"><a href="mechanical-parts/pneumatic-hydraulics/solenoid-valve-3-2way.html" target="_blank"><img src="mechanical-parts/pneumatic-hydraulics/images/solenoid-valve-3-2way_Solenoid-Valve-3-2way.jpg" style="width:150px;"></a><br>
<p>两位三通电磁阀</p>
</td>



<td width="25%;"><a href="mechanical-parts/pneumatic-hydraulics/solenoid-valve-dc-12v-0520e.html" target="_blank"><img src="mechanical-parts/pneumatic-hydraulics/images/solenoid-valve-dc-12v-0520e_Solenoid-Valve-DC-12V-0520E.jpg" style="width:150px;"></a><br>
<p>两通迷你电磁阀</p>
</td>

</tr>

<tr>
<td><a href="mechanical-parts/pneumatic-hydraulics/vacuum-suction-cup-sp-30.html" target="_blank"><img src="mechanical-parts/pneumatic-hydraulics/images/vacuum-suction-cup-sp-30_Vacuum-Suction-Cup---SP-30.jpg" style="width:150px;"></a><br>
<p>SP-30 吸盘</p>
</td>

<td><a href="mechanical-parts/pneumatic-hydraulics/vacuum-suction-cup-connector-holder.html" target="_blank"><img src="mechanical-parts/pneumatic-hydraulics/images/vacuum-suction-cup-connector-holder_Vacuum-Suction-Cup-Connector-Holder.jpg" style="width:150px;"></a><br>
<p>真空吸盘接头</p>
</td>

<td><a href="mechanical-parts/pneumatic-hydraulics/4-cross-four-way-connector.html" target="_blank"><img src="mechanical-parts/pneumatic-hydraulics/images/φ4-Cross-Four-Way-Connector.jpg" style="width:150px;"></a><br>
<p>φ4 四通十字接头</p>
</td>

<td><a href="mechanical-parts/pneumatic-hydraulics/4-elbow-connector.html" target="_blank"><img src="mechanical-parts/pneumatic-hydraulics/images/φ4-Elbow-Connector.jpg" style="width:150px;"></a><br>
<p>φ4 L形接头</p>
</td>

</tr>


<tr>
<td><a href="mechanical-parts/pneumatic-hydraulics/4-straight-throttle-valve.html" target="_blank"><img src="mechanical-parts/pneumatic-hydraulics/images/φ4-Straight-Throttle-Valve.jpg" style="width:150px;"></a><br>
<p>φ4 直线节流阀</p>
</td>

<td><a href="mechanical-parts/pneumatic-hydraulics/6-4-reducing-straight-connector.html" target="_blank"><img src="mechanical-parts/pneumatic-hydraulics/images/φ6---φ4-Reducing-Straight-Connector.jpg" style="width:150px;"></a><br>
<p>φ6 – φ4 直通接头</p>
</td>

<td><a href="mechanical-parts/pneumatic-hydraulics/8-6-reducing-straight-connector.html" target="_blank"><img src="mechanical-parts/pneumatic-hydraulics/images/φ8---φ6-Reducing-Straight-Connector.jpg" style="width:150px;"></a><br>
<p>φ8 – φ6 直通接头</p>
</td>
</tr>

</table>

### [紧固件及垫片](mechanical-parts/fasteners-gaskets.md)


<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="mechanical-parts/fasteners-gaskets/brass-stud-m4x8-6.html" target="_blank"><img src="mechanical-parts/fasteners-gaskets/images/brass-stud-m4×8-6.jpg" width="150px;"></a><br>
<p>M4x8+6 铜螺柱</p></td>

<td width="25%;"><a href="mechanical-parts/fasteners-gaskets/brass-stud-m4x12-6.html" target="_blank"><img src="mechanical-parts/fasteners-gaskets/images/brass-stud-m4×12-6.jpg" width="150px;"></a><br>
<p>M4x12+6 铜螺柱</p></td>

<td width="25%;"><a href="mechanical-parts/fasteners-gaskets/brass-stud-m4x16.html" target="_blank"><img src="mechanical-parts/fasteners-gaskets/images/brass-stud-m4×16.jpg" width="150px;"></a><br>
<p>M4x16 铜螺柱</p></td>

<td width="25%;"><a href="mechanical-parts/fasteners-gaskets/brass-stud-m4x20.html" target="_blank"><img src="mechanical-parts/fasteners-gaskets/images/brass-stud-m4×20.jpg" width="150px;"></a><br>
<p>M4x20 铜螺柱</p></td>
</tr>

<tr>
<td><a href="mechanical-parts/fasteners-gaskets/brass-stud-m4x32.html" target="_blank"><img src="mechanical-parts/fasteners-gaskets/images/brass-stud-m4×32.jpg" width="150px;"></a><br>
<p>M4x32 铜螺柱</p></td>

<td><a href="mechanical-parts/fasteners-gaskets/nut-4mm.html" target="_blank"><img src="mechanical-parts/fasteners-gaskets/images/nut-4mm.jpg" width="150px;"></a><br>
<p>螺母M4</p></td>

<td><a href="mechanical-parts/fasteners-gaskets/nylon-lock-nut-4mm.html" target="_blank"><img src="mechanical-parts/fasteners-gaskets/images/nylon-lock-nut-4mm.jpg" width="150px;"></a><br>
<p>防松螺母M4</p></td>

<td width="25%;"><a href="mechanical-parts/fasteners-gaskets/rivet-r4060.html" target="_blank"><img src="mechanical-parts/fasteners-gaskets/images/rivet-r4060.jpg" width="150px;"></a><br>
<p>R4060 铆钉</p></td>
</tr>

<tr>
<td width="25%;"><a href="mechanical-parts/fasteners-gaskets/rivet-r4100.html" target="_blank"><img src="mechanical-parts/fasteners-gaskets/images/rivet-r4100.jpg" width="150px;"></a><br>
<p>R4100 铆钉</p></td>

<td width="25%;"><a href="mechanical-parts/fasteners-gaskets/rivet-r4120.html" target="_blank"><img src="mechanical-parts/fasteners-gaskets/images/rivet-r4120.jpg" width="150px;"></a><br>
<p>R4120 铆钉</p></td>

<td><a href="mechanical-parts/fasteners-gaskets/socket-cap-screw.html" target="_blank"><img src="mechanical-parts/fasteners-gaskets/images/socket-cap-screw-m4x8.jpg" width="150px;"></a><br>
<p>半圆头螺丝</p></td>

<td><a href="mechanical-parts/fasteners-gaskets/plastic-spacer.html" target="_blank"><img src="mechanical-parts/fasteners-gaskets/images/spacer-4x7x1.jpg" width="150px;"></a><br>
<p>垫片</p></td>
</tr>
</table>


