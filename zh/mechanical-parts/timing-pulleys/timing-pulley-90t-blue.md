<img src="images/timing-pulley-90t.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 同步带轮62T
 
### 概述

同步带传动通过传动带内表面上等距分布的横向齿和带轮上的相应齿槽的啮合来传递运动。具有转动比准确以及结构紧凑的优点。

### 参数

- 齿数：90
- 厚度：8mm
- 材质：6061铝

### 功能特性

- 与MXL同步带兼容。
- 可用作小车车轮。
- 带8个M4孔

### 使用说明

用于同步带传动

### 尺寸图纸

<img src="images/timing-pulley-90t-1.jpg" style="width:600;padding:5px 5px 15px 0px;">
 
