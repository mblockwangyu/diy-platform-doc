<img src="images/plate-i1.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 连接片I1
 
### 概述

连接片I1是用来搭建直线运动机构的，因为它的长槽是专为摇臂机构设计的。当然它可以作为辅助件来与其他makeblock零件配合搭建。

### 参数

- 材质：60601铝
- 厚度：3mm
- 长度：72mm
- 宽度：24mm

### 尺寸图纸

<img src="images/plate-i1-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例
 
<img src="images/plate-i1-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/plate-i1-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/plate-i1-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/plate-i1-5.gif" style="width:300;padding:5px 5px 15px 0px;">