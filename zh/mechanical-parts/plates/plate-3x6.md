<img src="images/plate-3x6.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 连接片3x6
 

### 概述

连接片3X6可以作为辅助件，配合各种梁来搭建结构框架。

### 参数

- 厚度 2mm
- 孔径 4mm
- 长度48mm
- 材质 6061挤压铝合金

### 功能特性

- 由铝挤压（高强度）制成，表面阳极氧化
- 高强度
- 耐磨
- 高刚度

### 使用说明

表面有相距8mm的M4的安装孔，可用于搭建较大的方形结构

### 尺寸图纸

<img src="images/plate-3x6-1.jpg" style="width:700;padding:5px 5px 15px 0px;">


### 搭建案例
 
<img src="images/plate-3x6-2.jpg" style="width:700;padding:5px 5px 15px 0px;">
