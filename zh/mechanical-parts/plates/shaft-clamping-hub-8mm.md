<img src="images/shaft-clamping-8mm.jpg" style="padding:5px 5px 15px 0px;">

# 8mm轴轮连接片
 
### 概述

8mm轴轮连接片专门设计了两个8mm直径的通孔，与8mm直径的轴配合。 您可以使用它作为连接件，8mm轴的支架，或构建机器人车的底盘。	

### 参数

- 材质：6061铝
- 厚度：3mm
- 长度：90mm
- 宽度：12mm


 

 

 
