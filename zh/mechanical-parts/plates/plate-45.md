<img src="images/plate-45.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 连接片45°

### 概述

连接片45°通常用于连接45°或者135°角的机械零件

### 参数

- 材质：6061铝
- 厚度：3mm

### 尺寸图纸

<img src="images/plate-45-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例
 
<img src="images/plate-45-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/plate-45-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/plate-45-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/plate-45-5.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/plate-45-6.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/plate-45-7.jpg" style="width:300;padding:5px 5px 15px 0px;">
