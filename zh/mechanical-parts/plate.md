# 连接片及连接杆

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="plates/disc-d72.html" target="_blank"><img src="plates/images/disc-d72.jpg" width="150px;"></a><br>
<p>圆盘 D72</p></td>

<td width="25%;"><a href="plates/linear-motion-block-bracket.html" target="_blank"><img src="plates/images/linear-motion-block-bracket-a.jpg" width="150px;"></a><br>
<p>直线导轨滑块连接器 A</p></td>

<td width="25%;"><a href="plates/plate-3x6.html" target="_blank"><img src="plates/images/plate-3x6.jpg" width="150px;"></a><br>
<p>连接片 3*6</p></td>

<td width="25%;"><a href="plates/plate-7-9-b.html" target="_blank"><img src="plates/images/plate-7-9-b.jpg" width="150px;"></a><br>
<p>连接片 7*9-B</p></td>
</tr>

<tr>
<td><a href="plates/plate-45.html" target="_blank"><img src="plates/images/plate-45.jpg" width="150px;"></a><br>
<p>连接片 45°</p></td>
<td><a href="plates/plate-135.html" target="_blank"><img src="plates/images/plate-135.jpg" width="150px;"></a><br>
<p>连接片 135°</p></td>
<td><a href="plates/plate-i1.html" target="_blank"><img src="plates/images/plate-i1.jpg" width="150px;"></a><br>
<p>连接片 I1</p></td>
<td><a href="plates/plate-o1.html" target="_blank"><img src="plates/images/plate-o1.jpg" width="150px;"></a><br>
<p>连接片 O1</p></td>
</tr>

<tr>
<td><a href="plates/shaft-clamping-hub-8mm.html" target="_blank"><img src="plates/images/shaft-clamping-8mm.jpg" width="150px;"></a><br>
<p>8mm轴轮连接片</p></td>
<td><a href="plates/triangle-plate-6x8.html" target="_blank"><img src="plates/images/triangle-plate-6x8.jpg" width="150px;"></a><br>
<p>三角连接片 6*8</p></td>
<td><a href="plates/cross-plate.html" target="_blank"><img src="plates/images/cross-plate.jpg" width="150px;"></a><br>
<p>十字连接片</p></td>
<td><a href="plates/t-plate.html" target="_blank"><img src="plates/images/t-plate.jpg" width="150px;"></a><br>
<p>T型连接片</p></td>
</tr>

<tr>
<td><a href="plates/plate-0324-184.html" target="_blank"><img src="plates/images/plate-0324-184.jpg" width="150px;"></a><br>
<p>连接片0324-184</p></td>
</tr>

</table>