# φ4 直通节流阀

![](images/φ4-Cross-Four-Way-Connector.jpg)

### 概述

φ4直通式节流阀是一种直通式流量控制阀，用于流量的调节和控制，手动旋钮控制调节，可完全锁定关闭。

### 尺寸图纸

<img src="images/5639d68d35809.png" alt="5639d68d35809.png" width="700" />

### 搭建案例

<img src="images/1317.jpg" alt="1317.jpg" width="700" />

<img src="images/4-cross-four-way-connector-4-pack_59013-connector.jpg" alt="59013-connector.jpg" width="700" />
