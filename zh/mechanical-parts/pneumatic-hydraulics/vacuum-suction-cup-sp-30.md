# SP-30吸盘

![](images/vacuum-suction-cup-sp-30_Vacuum-Suction-Cup---SP-30.jpg)

### 概述
SP-30吸盘可以通过气动元件控制吸附在清洁后的玻璃、金属或者其他光滑的表面上。

### 参数

- 材质：硅胶
- 直径：30mm
- 高度：17mm

### 尺寸图纸

<img src="images/vacuum-suction-cup-sp-30_59000-size-1.png" alt="59000-size-1.png" width="599" />

### 搭建案例

<img src="images/vacuum-suction-cup-sp-30_59000-demo.png" alt="59000-demo.png" width="660" />  


<img src="images/vacuum-suction-cup-sp-30_50000-50001-59000-59001-59002-59003-59004-off.jpg" width="868" />

<img src="images/vacuum-suction-cup-sp-30_50000-50001-59000-59001-59002-59003-59004-on.jpg" width="868" />

 
