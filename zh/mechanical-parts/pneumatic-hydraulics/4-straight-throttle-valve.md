# φ4 直通节流阀

![](images/φ4-Straight-Throttle-Valve.jpg)

### 概述

φ4直通式节流阀是一种直通式流量控制阀，用于流量的调节和控制，手动旋钮控制调节，可完全锁定关闭。

### 尺寸图纸

<img src="images/4-straight-throttle-valve-2-pack_4-dimension.png" alt="4-dimension.png" width="800" />

