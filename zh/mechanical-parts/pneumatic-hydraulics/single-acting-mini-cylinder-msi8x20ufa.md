# 8 x 20mm 单作用气缸

![](images/makeblock-double-acting-mini-cylinder-pack-mi10x60ca_Makeblock-Double-Acting-Mini-Cylinder-Pack-MI10X60CA.jpg)


### 概述

气缸是气压传动中将压缩气体的压力能转换为机械能的气动执行元件。8x20mm气缸是一个单作用式的微型气缸，安装方式为面板安装，配有排气消声器，配M5-φ4快速插头。

### 参数

- 缸径：8mm
- 行程：20mm
- 安装螺母：M12X1.25
- 推力：18.6N
- 气压：0.5Mpa

### 尺寸图纸

<img src="images/makeblock-double-acting-mini-cylinder-pack-mi10x60ca_mi10x60casdb-0-1013.png" alt="mi10x60casdb-0-1013.png" width="800" />
