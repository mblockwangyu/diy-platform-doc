# Beam0412-108

![](images/beam0412-108-blue-4-pack_Beam0412-108.jpg)

 
### Building Examples:

<img src="images/beam0412-108-blue-4-pack_beam0412-108.gif" width="720" />

### Size Chart (mm):

<img src="images/beam0412-108-blue-4-pack_2015-12-25-14-25-59.jpg" width="720" />

### Specifications

- SKU: 60711
- Material: 6061 Aluminum
- Diameter of holes: 4mm
- Cross-section area: 4 x 12mm
- Length: 108mm
- Package content: 4 x Beam 0412-108
- Dimension: 108 x 12 x 4mm (4.25 x 0.47 x 0.16'')
- Net Weight: 38.4g (1.35oz)
