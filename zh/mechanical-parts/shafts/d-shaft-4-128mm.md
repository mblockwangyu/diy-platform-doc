<img src="images/d-shaft-4x128mm.jpg" style="width:400;padding:5px 5px 15px 0px;">

# D型轴4x128mm
 
### 概述

D轴4x128毫米通常与作为轴、齿轮,皮带轮、联轴器或轴承配合使用。

### 参数

- 直径：4mm
- 长度：128mm
- 材质：镀铬不锈钢

### 尺寸图纸

<img src="images/d-shaft-4x128mm-1.jpg" style="width:700;padding:5px 5px 15px 0px;">


 

 

 
