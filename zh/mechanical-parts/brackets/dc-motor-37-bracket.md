<img src="images/dc-motor-37-bracket.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 37电机支架
 
### 概述
用来固定37电机，并且和makeblock平台零件兼容。

### 参数

- 厚度：3mm
- 材质：6061铝

### 尺寸图纸

<img src="images/dc-motor-37-bracket-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/dc-motor-37-bracket-2.jpg" style="width:700;padding:5px 5px 15px 0px;">
