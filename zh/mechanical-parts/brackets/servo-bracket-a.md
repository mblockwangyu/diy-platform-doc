# Servo Bracket A

![](images/servo-bracket-a_Servo-Bracket-A.jpg)

**Descriptions:**

Servo
Bracket A allows you to attach the standard sized servo to other
mechanical structures. There are four thread holes for mounting your
servo and other thru holes for attachment.

  
**Features：**

-   Made
    from 6061 aluminum extrusion , 2mm thick, anodized surface.
-   Compatible
    with standard sized servo such as MG995
-   Four
    Screw M4x8 are included, servo is not included.

 

**Size
Charts(mm):**

 <img src="images/servo-bracket-a_sizecharts.png" alt="sizecharts.png" width="760" />

** **

 

 

 

 
