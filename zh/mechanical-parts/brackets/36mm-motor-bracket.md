<img src="images/36mm-motor-bracket.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 36mm电机支架
 
### 概述

36mm电机支架与36mm直流电机兼容。U型比多功能电机支架具有更高的强度。

### 参数

- 长度：30mm
- 宽度：24mm
- 高度：60mm
- 厚度：3mm
- 材质：6061铝

### 尺寸图纸

<img src="images/36mm-motor-bracket-1.jpg" style="width:800;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/36mm-motor-bracket-2.jpg" style="width:600;padding:5px 5px 15px 0px;">


 
