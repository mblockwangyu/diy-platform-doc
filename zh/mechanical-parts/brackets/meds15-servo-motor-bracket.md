<img src="images/mecds15-servo-motor-bracket.jpg" style="width:400;padding:5px 5px 15px 0px;">

# MECDS-150舵机支架
 
### 概述

MEDS15舵机支架通常用作MECDS-150 舵机的结构支撑或连接点。	
### 参数

- 材质：6061铝
- 厚度：2mm

### 尺寸图纸
 
<img src="images/mecds15-servo-motor-bracket-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/mecds15-servo-motor-bracket-2.jpg" style="width:700;padding:5px 5px 15px 0px;">

<img src="images/mecds15-servo-motor-bracket-3.jpg" style="width:700;padding:5px 5px 15px 0px;">