<img src="images/42byg-stepper-motor-bracket-b.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 42步进电机支架
 
### 概述

42步进电机支架用来固定42步进电机。带有四个用于安装步进电机的M3孔和用于搭建的M4通孔。

### 参数

- 厚度：3mm
- 材质：6061铝

### 尺寸图纸
 
<img src="images/42byg-stepper-motor-bracket-b-1.png" style="width:700;padding:5px 5px 15px 0px;">
