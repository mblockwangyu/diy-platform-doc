<img src="images/u-bracket-c.jpg" style="width:400;padding:5px 5px 15px 0px;">

# U型支架C
 
### 概述

U形支架C可与8毫米轴、8毫米轴承和16毫米轴承一起工作，用于安装4mm或8mm轴。

### 参数

- 材质：6061铝
- 厚度：3mm

### 尺寸图纸

<img src="images/u-bracket-c-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/u-bracket-c-2.jpg" style="width:600;padding:5px 5px 15px 0px;">
 
