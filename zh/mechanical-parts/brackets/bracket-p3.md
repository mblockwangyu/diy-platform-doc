<img src="images/bracket-p3.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 支架P3
 
### 概述

支架P3主要是用来配合安装从动轮的，如62T同步带轮。它带有4mm安装孔和两端的螺纹槽, 可以轻松地组装在其他结构上, 兼容大多数Makeblock机械部件和轮子。	

### 参数

- 材质：6061铝
- 长度：33mm
- 宽度：24mm
- 高度：10mm

### 尺寸图纸
 
<img src="images/bracket-p3-1.png" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/bracket-p3-2.png" style="width:700;padding:5px 5px 15px 0px;">


