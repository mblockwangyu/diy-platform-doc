<img src="images/dc-motor-25-bracket-b.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 25电机支架
 
### 概述
25电机支架是用来连接固定25电机的，它跟makeblock平台零件兼容。

### 参数

- 厚度：3mm
- 材质：6061铝

### 尺寸图纸
 
<img src="images/dc-motor-25-bracket-b-1.png" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/dc-motor-25-bracket-b-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/dc-motor-25-bracket-b-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/dc-motor-25-bracket-b-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/dc-motor-25-bracket-b-5.jpg" style="width:300;padding:5px 5px 15px 0px;">