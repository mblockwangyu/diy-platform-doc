# Bracket 3\*6

![](images/bracket-3x6-blue-4-pack_Bracket-3x6-Blue.jpg)

**Features:**

-   Made
    from 6061 aluminum, 2mm thick, anodized surface.
-   With
    holes on 8mm increments,  can be drilled for 4mm hardware.
-   Can
    be used to build bigger rectangular structure. 
-   Sold
    in Packs of 4.

**Size
Charts(mm):**

**<img src="images/bracket-3x6-blue-4-pack_bracket2.png" alt="bracket2.png" width="790" />**

**********Demo:**********

**<img src="images/bracket-3x6-blue-4-pack_bracket3.png" alt="bracket3.png" width="752" />**
