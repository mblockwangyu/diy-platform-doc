# Socket Cap Screw M4x40-Button Head


![](images/socket-cap-screw-m4x40-button-head-25-pack_Socket-Cap-Screw-M4x40-Button-Head.jpg)

**Description：**

Button Head Socket Cap Screw is a new
kind of Socket Cap Screw, it has smaller head that can solve most
problem of interfere with other components. The tool of this Button Head
Socket Cap Screw  is [Cross & 2.5mm HEX
Screwdriver](http://www.makeblock.cc/cross-2-5mm-hex-screwdriver/).

 

**Features:**

-   Made of stainless steel.
-   Compatible with our Makeblock
    structures which have M4mm holes.
-   Smaller head that can solve most
    problem of interfere with other components.
-   Sold in packs of 25.

 

**Size charts(mm):**

**<img src="images/socket-cap-screw-m4x40-button-head-25-pack_70567-s1.jpg" alt="70567-s1.jpg" width="760" />**

**![](http://)**

 

**Demo:**

<img src="images/34.jpg" alt="34.jpg" width="760" />
