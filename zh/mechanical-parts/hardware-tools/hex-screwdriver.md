# Cross &2.5mm HEX Screwdriver


![](images/hex-screwdriver_Cross-&-2.5mm-HEX-Screwdriver.jpg)

**Description:**

Makeblock Cross & 2.5mm HEX Screwdriver
V2.0 is a new two in one screwdriver which replace Makeblock HEX
Screwdriver 2.5mm and Cross Screwdriver 3mm. The Cross & 2.5mm HEX
Screwdriver can be used as the tool of both the Button Head Socket Cap
Screw and the cross screw.    

 

**Features:**

-   Be used to tightening Makeblock
    Button Head Socket Cap Screw and Cross Screw
-   PP+TPR injection moulded handle for
    better hand feeling  
-   Length: 170mm  

 

 

 

 
