# HEX Allen Key


![](images/hex-allen-key-1-5mm-pair_HEX-Allen-Key-1.5mm.jpg)

**Features:**

-   Compatible
    with Headless
    Set Screw M3x5
-   Sold
    in pair.
