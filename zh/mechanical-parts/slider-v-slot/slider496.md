<img src="slider496.jpg" style="width:400;padding:5px 5px 15px 0px;">

# V槽轴承滑轨496mm

### 概述

滑轨496表面平滑，一侧V型，滑轨可兼容V型槽轴承，一侧M4螺纹槽可与M4梁连接。方便安装和拓展应用，是适用性很好的结构件。

### 参数

- 长度：496mm
- 宽度：8mm
- 高度：14mm
- 材质：6061铝

### 尺寸

<img src="slider496-1.jpg" style="width:600;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="slider496-2.jpg" style="width:600;padding:5px 5px 15px 0px;">

