<img src="images/male-rod-end-bearing.jpg" style="width:400;padding:5px 5px 15px 0px;">


# 杆端关节轴承-公
 
### 概述

杆端关节，也称为海姆接头（N. America）或玫瑰接头（英国和其他地方）是机械铰接接头。 这种接头用于控制杆，转向连杆，拉杆或需要精密铰接接头的任何地方的端部。 具有螺栓或其他附接硬件通过的开口的球旋转件被压入带有螺纹轴的圆形壳体中。

### 参数

- 球头内径：4mm
- 材质：铸铁
- 长度：31.5

### 尺寸图纸

<img src="images/male-rod-end-bearing-1.jpg" style="width:700;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/male-rod-end-bearing-2.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/male-rod-end-bearing-3.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/male-rod-end-bearing-4.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/male-rod-end-bearing-5.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/male-rod-end-bearing-6.jpg" style="width:300;padding:5px 5px 15px 0px;">

<img src="images/male-rod-end-bearing-7.jpg" style="width:300;padding:5px 5px 15px 0px;">
 

 
