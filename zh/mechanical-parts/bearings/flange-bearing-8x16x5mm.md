<img src="images/flange-bearing-8x16x5.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 法兰轴承8x16x5
 
### 概述
8x16x5法兰轴承可以和8mm的轴进行配合搭建。 它可以安装在16mm孔中，例如 U形架C。

### 参数

- 内径：8mm
- 外径：16mm
- 宽度：5mm
- 法兰外径：18mm
- 材质：钢

