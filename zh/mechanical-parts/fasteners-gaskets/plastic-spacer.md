# 垫片

### 概述

与金属垫圈相比，具有优异的绝缘、耐蚀、隔热和非磁性能，且重量轻。

### 参数

- 材质：尼龙

## 垫片4x7x1

<img src="images/spacer-4x7x1.jpg" style="width:170px;padding:5px 5px 15px 0px;">

## 垫片4x7x2

<img src="images/spacer-4x7x2.jpg" style="width:170px;padding:5px 5px 15px 0px;">

## 垫片4x7x10

<img src="images/spacer-4x7x10.jpg" style="width:170px;padding:5px 5px 15px 0px;">
