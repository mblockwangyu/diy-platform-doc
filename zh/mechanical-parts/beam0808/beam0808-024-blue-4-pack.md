# Beam0808-024

![](images/beam0808-024-blue-4-pack_Beam0808-024.jpg)

**Description:**

Makeblock
Beam0808 is One of the most frequently used part in Makeblock platform,
it compatible with most makeblock motion and Structure
components. 

** **

**Features:**

-   Made
    from 6061 aluminum extrusion, anodized surface. Excellent strength
    and twist resistance.
-   With
    holes on 16mm increments, can be drilled for 4mm hardware.
-   Threaded
    slot enables easy and flexible connection.
-   Cross-sectional
    area 8x8mm, length 24mm.
-   Sold
    in Packs of 4.

 

**Size
Charts(mm):**

 <img src="images/beam0808-024-blue-4-pack_qq-20150318181131.jpg" alt="qq-20150318181131.jpg" width="760" />

 

**Demo:**
