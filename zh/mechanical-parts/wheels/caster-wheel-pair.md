<img src="images/caster-wheel.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 万向轮
 
### 概述

万向轮轻便小巧，能够实现全方位滚动。M8螺纹接头可与支架P3兼容，配合各种梁使用。

### 参数

- 材质：钢、塑料
- 长度：54mm
- 宽度：25mm
- 高度：36.5mm

### 尺寸

<img src="images/caster-wheel-1.png" style="width:600;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/caster-wheel-2.png" style="width:600;padding:5px 5px 15px 0px;">

