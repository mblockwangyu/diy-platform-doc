<img src="images/track-with-track-axle.jpg" style="width:400;padding:5px 5px 15px 0px;">

# 履带
 
### 概述

履带可以自由拼接成想要的长度，与同步带轮一起使用形成坦克轮。能承重，在艰难的路面行走自如。	

### 参数

- 材质：硅胶

### 尺寸图纸

<img src="images/track-with-track-axle-1.jpg" style="width:600;padding:5px 5px 15px 0px;">

### 搭建案例

<img src="images/track-with-track-axle-2.jpg" style="width:600;padding:5px 5px 15px 0px;">