# 主控类

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td><a href="main-control-boards/makeblock-orion.html" target="_blank"><img src="../../en/electronic-modules/main-control-boards/images/makeblock-orion_makeblock_orion.jpg" width="200px;"></a><br>
<p>Orion 主控板</p></td>

<td><a href="main-control-boards/mcore.html" target="_blank"><img src="../../en/electronic-modules/main-control-boards/images/mcore_mCore.jpg" width="200px;"></a><br>
<p>mCore 主控板</p></td>

<td><a href="main-control-boards/megapi.html" target="_blank"><img src="../../en/electronic-modules/main-control-boards/images/megapi_MegaPi.jpg" width="200px;"></a><br>
<p>MegaPi 主控板</p></td>
</tr>

<tr>
<td><a href="main-control-boards/me-auriga.html" target="_blank"><img src="main-control-boards\images\me-auriga-1.jpg" width="200px;"></a><br>
<p>Me Auriga 主控板</p></td>

<td><a href="main-control-boards/megapi-pro.html" target="_blank"><img src="main-control-boards\images\megapi-pro.png" width="200px;"></a><br>
<p>MegaPi Pro 主控板</p></td>

<td><a href="main-control-boards/novapi.html" target="_blank"><img src="main-control-boards\images\novapi.png" width="200px;"></a><br>
<p>NovaPi 主控板</p></td>

</table>