# 180 智能编码电机

<img src="../../../zh/electronic-modules/motors/images/180-smart-encoder-motor-1.png" width="150">

### 概述
180智能编码电机是mbuild平台下入门级编码电机。主要搭配NovaPi主控作为makeX Challenge 赛项的底盘驱动电机。

### 技术规格
* 减速比：39.43
* 额定电压： 12V
* 空载电流：350mA
* 通讯端口及协议：串口通信
* 空载转速：580±10%RPM

### 功能特性

* 全金属齿轮组，坚固耐用
* 内置编码器，能够精确控制机器人运动
* 自带安装孔，无需电机支架既可稳固连接在makeblock金属结构件上。
* 多个面都有安装孔，位置灵活
* 高扭矩，高功率，提供强劲动力。
* 使用智能动力接口


### 产品外观图

<img src="../../../zh/electronic-modules/motors/images/180-smart-encoder-motor-2.png" width="300">

①：智能电机接口<br>
②：结构件接口

### 安装说明

兼容makeblock蓝色平台机械件，带有M4螺纹孔，可以方便安装。

### 软件
180 智能编码电机的相关积木块如下表所述：

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:top}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:top}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">名称</th>
    <th class="tg-7g6k">说明</th>
    <th class="tg-7g6k">备注</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../zh/electronic-modules/motors/images/180-smart-encoder-motor-3.png" width="200"></td>
    <td class="tg-c6of">设定编码电机动力输出</td>
    <td class="tg-c6of">动力：-100~100% 。<br>正值为正转，<br>负值时为反转。<br>当设定值超出<br>阈值时，电机<br>按照阈值上/下限<br>进行运动。<br>输入超出<br>限值的数值后，<br>自动填与输入<br>最接近的数值。</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../zh/electronic-modules/motors/images/180-smart-encoder-motor-4.png" width="200"></td>
    <td class="tg-c6of">使电机相对于当前位<br>置，以一定速度转动</td>
    <td class="tg-c6of">设定转动角度的<br>正负决定终点相<br>对当前位置方向。<br>速度仅取正值，<br>范围：0 ~ 300<br>。超出阈值的赋值<br>会使电机以最高<br>速运转</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../zh/electronic-modules/motors/images/180-smart-encoder-motor-5.png" width="200"></td>
    <td class="tg-c6of">使电机相对于默认<br>零点，以一定<br>速度转动</td>
    <td class="tg-c6of">设定转动角度的正<br>负决定终点相对当<br>前零点的方向。<br>速度仅取正值，<br>范围：0 ~ 300<br>。超出阈值的赋值<br>会使电机以最<br>高速运转</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../zh/electronic-modules/motors/images/180-smart-encoder-motor-6.png" width="200"></td>
    <td class="tg-ycr8">使电机以固定速度<br>转动</td>
    <td class="tg-ycr8">速度正负决定转<br>动方向，正值为<br>正转，负值时为<br>反转。<br>范围：-300~ 300。<br>超出阈值的赋值会<br>使电机以最高速运转</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../zh/electronic-modules/motors/images/180-smart-encoder-motor-7.png" width="200"></td>
    <td class="tg-ycr8">获取电机的当前转速</td>
    <td class="tg-i81m">/</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../zh/electronic-modules/motors/images/180-smart-encoder-motor-8.png" width="200"></td>
    <td class="tg-ycr8">获取电机当前相对于<br>零点的角度位置</td>
    <td class="tg-i81m">/</td>
  </tr>
</table>

### 编程示例

<img src="../../../zh/electronic-modules/motors/images/180-smart-encoder-motor-9.png" width="200">