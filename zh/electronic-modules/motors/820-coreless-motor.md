# 820 无刷电机

<img src="../../../en/electronic-modules/motors/images/820-coreless-motor_微信截图_20160128145747.png" alt="微信截图_20160128145747" width="315" style="padding:5px 5px 12px 0px;">

### 描述

Makeblock 820无刷电机同时具有正节距和负节距，通常用于四轴飞行器。

### 特点

- 高扭矩;

### 尺寸图纸

<img src="../../../en/electronic-modules/motors/images/820-coreless-motor_微信截图_20160128150106.png" alt="微信截图_20160128150106" width="313" style="padding:5px 5px 12px 0px;">

### 搭建案例

<img src="../../../en/electronic-modules/motors/images/820-coreless-motor_微信截图_20160128150137.png" alt="微信截图_20160128150137" width="661" style="padding:5px 5px 12px 0px;">
