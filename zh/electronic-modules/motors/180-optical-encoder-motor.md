# 180 光电编码电机

<img src="../../../zh/electronic-modules/motors/images/180-optical-encoder-motor/180-optical-encoder-motor-1.png" width="300">

### 概述
180光电编码电机采用光编码器，可以高精度控制。它可以灵活地和各种其他零件组合使用，机身三面各有两个M4螺纹孔可以方便和 makeblock 平台机械件进行连接固定。同时，由于使用定制材料，使得此款电机运行时噪音小，并可以长时间大扭矩输出。此款电机支持多个电机驱动和主控板，如 Orion、MegaPi、MegaPi Pro、Me Auriga 主控板。

### 技术规格
* 减速比：39.6
* 额定电压：7.4V
* 空载电流：240mA
* 负载电流：≤750mA
* 空载转速：350RPM±5%
* 原始转速：14000RPM±5%
* 启动扭矩：5kg·cm
* 额定扭矩：800g·cm
* 输出轴长：9mm
* 功率：3.7W
* 编码器精度：360

### 编程指南

● **Arduino编程**

如果使用 Arduino 编程，需要调用库 `Makeblock-Library-master` 来控制电机。

以下程序运行后编码电机 1 和 2 将以 100 速度运行一秒，停止一秒，重复此过程。

```
#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>

#include <MeMegaPiPro.h>

double angle_rad = PI/180.0;
double angle_deg = 180.0/PI;
MeEncoderOnBoard Encoder_1(SLOT1);
MeEncoderOnBoard Encoder_2(SLOT2);
void isr_process_encoder1(void)
{
    if(digitalRead(Encoder_1.getPortB()) == 0){
        Encoder_1.pulsePosMinus();
    }else{
        Encoder_1.pulsePosPlus();
    }
}
void isr_process_encoder2(void)
{
    if(digitalRead(Encoder_2.getPortB()) == 0){
        Encoder_2.pulsePosMinus();
    }else{
        Encoder_2.pulsePosPlus();
    }
}

void setup(){
    TCCR1A = _BV(WGM10);//PIN12
    TCCR1B = _BV(CS11) | _BV(CS10) | _BV(WGM12);
    TCCR2A = _BV(WGM21) | _BV(WGM20);//PIN8
    TCCR2B = _BV(CS22);
    attachInterrupt(Encoder_1.getIntNum(), isr_process_encoder1, RISING);
    attachInterrupt(Encoder_2.getIntNum(), isr_process_encoder2, RISING);
}

void loop(){
    Encoder_1.setTarPWM(100);
    Encoder_2.setTarPWM(100);
    delay(1);
    Encoder_1.setTarPWM(0);
    Encoder_2.setTarPWM(0);
    delay(1);
    loop();
}

```
<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/180-optical-encoder-motor/180_Encoder_Motor_V1.1.ino" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **mBlock 编程**

**积木块说明**
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/180-optical-encoder-motor/180-optical-encoder-motor-2.png" width="200"></td>
    <td class="tg-c6of">选择接口<br>设置动力（–255~+255）
</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/180-optical-encoder-motor/180-optical-encoder-motor-3.png" width="200"></td>
    <td class="tg-c6of">选择接口<br>设置速度（最大速度由电机型号决定）
</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/180-optical-encoder-motor/180-optical-encoder-motor-4.png" width="200"></td>
    <td class="tg-c6of">选择接口<br>设置旋转角度（非负整数）<br>设置速度（最大速度由电机型号决定）
</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/180-optical-encoder-motor/180-optical-encoder-motor-5.png" width="200"></td>
    <td class="tg-c6of">获取当前电机速度
</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/180-optical-encoder-motor/180-optical-encoder-motor-6.png" width="200"></td>
    <td class="tg-c6of">获取电机当前相对于零点的角度位置
</td>
  </tr>
</table>

以下程序运行后编码电机 1 和 2 将以 100 速度运行一秒，停止一秒，重复此过程。

<img src="images/180-optical-encoder-motor/180-optical-encoder-motor-7.png" width="300"><br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/180-optical-encoder-motor/180 Encoder Motor V1.1.sb2" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **慧编程编程**

**积木块说明（以MegaPi Pro主控为例）**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/180-optical-encoder-motor/180-optical-encoder-motor-8.png" width="200"></td>
    <td class="tg-c6of">选择接口<br>设置动力（–100~+100）
</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/180-optical-encoder-motor/180-optical-encoder-motor-9.png" width="200"></td>
    <td class="tg-c6of">选择接口<br>设置速度（最大速度由电机型号决定）
</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/180-optical-encoder-motor/180-optical-encoder-motor-10.png" width="200"></td>
    <td class="tg-c6of">选择接口<br>设置旋转角度（非负整数）<br>设置速度（最大速度由电机型号决定）
</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/180-optical-encoder-motor/180-optical-encoder-motor-11.png" width="200"></td>
    <td class="tg-c6of">获取当前电机速度
</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/180-optical-encoder-motor/180-optical-encoder-motor-12.png" width="200"></td>
    <td class="tg-c6of">获取电机当前相对于零点的角度位置
</td>
  </tr>
</table>

以下程序运行后编码电机 1 和 2 将以 100 速度运行一秒，停止一秒，重复此过程。

<img src="images/180-optical-encoder-motor/180-optical-encoder-motor-13.png" width="300"><br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/180-optical-encoder-motor/180 Encoder Motor V1.1.mblock" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **Python 编程**

1、主控板 MegaPi Pro 和树莓派连接，180 光电编码电机和直流/编码驱动模块相连接。<br>
2、树莓派安装最新的 Makeblock库 `pip3 install makeblock --upgrade`。<br>
3、新建 python 文件，后缀为 .py。<br>
4、在 python 文件里写入程序。<br>
5、运行 python 文件如 “python123.py”。

**函数功能说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">函数</th>
    <th class="tg-7g6k">功能</th>
  </tr>
  <tr>
    <td class="tg-3xi5">EncoderMotor(port)</td>
    <td class="tg-c6of">创建直流编码电机对象。<br>port: MegaPiPro.PORT1~MegaPiPro.PORT4
</td>
  </tr>
  <tr>
    <td class="tg-3xi5">run(speed)</td>
    <td class="tg-c6of">以指定速度旋转。<br>speed: 转速（范围为–180~+180）
</td>
  </tr>
  <tr>
    <td class="tg-3xi5">move_to(position,speed,callback)</td>
    <td class="tg-c6of">以指定速度旋转到指定位置。<br>position: 目标位置<br>speed: 转速（范围为–180~+180）<br>callback: 达到目标位置时触发回调
</td>
  </tr>
  <tr>
    <td class="tg-3xi5">set_home()</td>
    <td class="tg-c6of">设置当前位置为原点。
</td>
  </tr>
</table>

**程序示例 1：**

以下程序运行后接在 MegaPi Pro 直流/编码电机驱动接口 1 的编码电机以 50 速度旋转 2 秒，停止一秒，以 –50 速度旋转 2 秒，停止一秒，重复此过程。

```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
encoder = board.EncoderMotor(MegaPiPro.PORT1)
while True:
    encoder.run(50)
    sleep(2)
    encoder.run(0)
    sleep(1)
    encoder.run(-50)
    sleep(2)
    encoder.run(0)
    sleep(1)
```

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/180-optical-encoder-motor/EncoderMotor_1.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

**程序示例 2：**

以下程序运行后接在 MegaPi Pro 直流/编码电机驱动接口 1 的编码电机旋转到目标位置

```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
encoder = board.EncoderMotor(MegaPiPro.PORT1)
position = 0
def on_finished(value):
    position = 5000 - position
    encoder.move_to(position,100,on_finished)

on_finished(position)
```

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/180-optical-encoder-motor/EncoderMotor_2.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

### 接线方式

● **电子连接**

使用180编码电机线接在主控板编码电机接口，或者编码电机驱动的编码电机接口上。

<img src="images/180-optical-encoder-motor/180-optical-encoder-motor-14.png" width="300">

● **结构搭建**

180 光电编码电机可用作动力，也可作为小车底盘使用。其轴兼容 Makeblock 创客平台带台阶的注塑同步带轮 62T 和带台阶的注塑同步带轮 90T。

<img src="images/180-optical-encoder-motor/180-optical-encoder-motor-15.png" width="300">