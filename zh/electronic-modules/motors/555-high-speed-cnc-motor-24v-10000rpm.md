# 555 高速 CNC 电机 24V/1000RPM

<img src="../../../en/electronic-modules/motors/images/555-high-speed-cnc-motor-24v-10000rpm_微信截图_20160128115154.png" alt="微信截图_20160128115154" width="281" style="padding:5px 5px 12px 0px;">

### 描述

Makeblock 555高速数控电机24V/10000RPM 由高性能555电机和高精度JTO钻夹头组装而成。它通常用来做一个迷你型数控主轴，制作迷你台钻，迷你台锯等等。

### 技术规格

- 重量：290克
- JTO钻夹头夹紧范围：0.3-4mm
- 额定电压：24V
- 速度：10000rpm

### 尺寸图(mm)

<img src="../../../en/electronic-modules/motors/images/555-high-speed-cnc-motor-24v-10000rpm_微信截图_20160128115210-300x145.png" alt="微信截图_20160128115210" width="559" style="padding:5px 5px 12px 0px;">

### 演示

<img src="../../../en/electronic-modules/motors/images/555-high-speed-cnc-motor-24v-10000rpm_微信截图_20160128115219-300x249.png" alt="微信截图_20160128115219" width="501" style="padding:5px 5px 12px 0px;">

 
