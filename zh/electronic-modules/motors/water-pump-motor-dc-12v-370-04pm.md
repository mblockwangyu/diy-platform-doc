# 水泵马达12V/370-04PM

<img src="../../../en/electronic-modules/motors/images/water-pump-motor-dc-12v-370-04pm_微信截图_20160128104754-300x292.png" alt="微信截图_20160128104754" width="300" style="padding:5px 5px 12px 0px;">

### 描述

Makeblock水泵电机 – 直流 12V /
370-04PM具有12V电机和坚固的热塑体，广泛用于水泵，汽车水泵，实验泵，盆景假山，DIY项目等。

### 规格

- 额定电压：DC 12V
- 负载：水
- 吸水率：1L-1.2L/分
- 电流（负载）：比320毫安小于
- 流速：2.0LPM
- 总尺寸：D27×75
- 水孔直径：6.5毫米
- 最大压力：大于360mmHg
- 噪声：&lt;&nbsp;60分贝

### 尺寸图(mm)

<img src="../../../en/electronic-modules/motors/images/water-pump-motor-dc-12v-370-04pm_微信截图_20160128105928-300x100.png" alt="微信截图_20160128105928" width="480" style="padding:5px 5px 12px 0px;">
