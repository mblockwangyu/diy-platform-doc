# N20减速直流电机 12V

<img src="../../../en/electronic-modules/motors/images/mini-metal-gear-motor-n20-dc-12v_微信截图_20160128121748-300x256.png" alt="微信截图_20160128121748" width="300" style="padding:5px 5px 12px 0px;">

### 描述

Makeblock迷你金属齿轮电机拥有迷你机身，精致的齿轮和简单的连接器，并被广泛应用于众多DIY项目中。有三种不同的转速供您选择，分别是100RPM，210RPM和390RPM。

### 规格

- 额定电压：DC 12V
- 转速：100RPM / 210RPM / 390RPM（取决于您的需求）
- 总大小：46 x 12mm
- 工作温度范围：-20℃〜+ 60℃

### 尺寸图(mm)

<img src="../../../en/electronic-modules/motors/images/mini-metal-gear-motor-n20-dc-12v_微信截图_20160128122108-300x100.png" alt="微信截图_20160128122108" width="543" style="padding:5px 5px 12px 0px;">
