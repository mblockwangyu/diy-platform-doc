# 36 直流编码减速电机 12V

<img src="../../../en/electronic-modules/motors/images/36-dc-geared-motor-12v240rpm_微信截图_20160128122512-300x281.png" alt="微信截图_20160128122512" width="300" style="padding:5px 5px 12px 0px;">

### 描述

36直流减速电机性能更好，扭矩更高。
它可以作为车底盘的动力电机或一些高性能的小型车。

### 技术规格

- 电压：12V
- 无负载RPM：240rpm
- 额定转速：182rpm
- 额定扭矩：4kg<normal>.</normal>cm
- 目前：1.2A

### 使用说明

注意，使用时需和大功率驱动板配合使用，不可直接插入到主控板（编码电机接口会出现动力不足的情况）。

### 尺寸规格

<img src="../../../en/electronic-modules/motors/images/36-dc-geared-motor-12v240rpm_微信截图_20160128122745-300x121.png" alt="微信截图_20160128122745" width="540" style="padding:5px 5px 12px 0px;">

### 演示

<img src="../../../en/electronic-modules/motors/images/36-dc-geared-motor-12v240rpm_微信截图_20160128122822-300x133.png" alt="微信截图_20160128122822" width="528" style="padding:5px 5px 12px 0px;">
