# 36 堆叠无刷电机

<img src="../../../zh/electronic-modules/motors/images/36-encoder-motor-brushless-1.png" width="300">

### 概述
36 堆叠无刷电机配合NovaPi主板使用，为您的机器人提供强力的运动能力。

### 规格
* 模块尺寸：115.3mm × φ36mm 
* 工作电压： 10V ~ 15V
* 空载电流：750mA
* 额定负载力矩：5KG.CM
* 空载转速：300±10%RPM
* 通讯端口及协议：串口通信

### 功能特性
* 全金属齿轮组，坚固耐用
* 内置编码器，能够精确控制机器人运动
* 自带安装孔，无需电机支架即可稳固连接在makeblock金属结构件上。
* 高扭矩，高功率，提供强劲动力。
* 使用智能动力接口。

### 产品图

<img src="../../../zh/electronic-modules/motors/images/36-encoder-motor-brushless-2.png" width="300">

①：结构件接口 <br>
②：智能电机接口

### 安装使用建议
* 36 堆叠无刷电机兼容 Makeblock 平台机械件，电机带有 4 个 M4 螺纹孔方便安装。
* 安装时请选用合适长度的螺钉，以防损坏电机牙箱。

### 软件
36 堆叠无刷电机的相关积木块如下表所述：

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">名称</th>
    <th class="tg-7g6k">说明</th>
    <th class="tg-7g6k">备注</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../zh/electronic-modules/motors/images/36-encoder-motor-brushless-3.png" width="200"></td>
    <td class="tg-c6of">设定编码电机动力输出</td>
    <td class="tg-c6of">动力：-100~100% 。<br>正值为正转，<br>负值时为反转。<br>当设定值超出<br>阈值时，电机<br>按照阈值上/下限<br>进行运动。<br>输入超出<br>限值的数值后，<br>自动填与输入<br>最接近的数值。</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../zh/electronic-modules/motors/images/36-encoder-motor-brushless-4.png" width="200"></td>
    <td class="tg-c6of">使电机相对于当前位<br>置，以一定速度转动</td>
    <td class="tg-c6of">设定转动角度的<br>正负决定终点相<br>对当前位置方向。<br>速度仅取正值，<br>范围：0 ~ 300<br>。超出阈值的赋值<br>会使电机以最高<br>速运转</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../zh/electronic-modules/motors/images/36-encoder-motor-brushless-5.png" width="200"></td>
    <td class="tg-c6of">使电机相对于默认<br>零点，以一定<br>速度转动</td>
    <td class="tg-c6of">设定转动角度的正<br>负决定终点相对当<br>前零点的方向。<br>速度仅取正值，<br>范围：0 ~ 300<br>。超出阈值的赋值<br>会使电机以最<br>高速运转</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../zh/electronic-modules/motors/images/36-encoder-motor-brushless-6.png" width="200"></td>
    <td class="tg-ycr8">使电机以固定速度<br>转动</td>
    <td class="tg-ycr8">速度正负决定转<br>动方向，正值为<br>正转，负值时为<br>反转。<br>范围：-300~ 300。<br>超出阈值的赋值会<br>使电机以最高速运转</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../zh/electronic-modules/motors/images/36-encoder-motor-brushless-7.png" width="200"></td>
    <td class="tg-ycr8">获取电机的当前转速</td>
    <td class="tg-i81m">/</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../zh/electronic-modules/motors/images/36-encoder-motor-brushless-8.png" width="200"></td>
    <td class="tg-ycr8">获取电机当前相对于<br>零点的角度位置</td>
    <td class="tg-i81m">/</td>
  </tr>
</table>


#### 最基本的使用方法：

<img src="../../../zh/electronic-modules/motors/images/36-encoder-motor-brushless-9.png" width="200">