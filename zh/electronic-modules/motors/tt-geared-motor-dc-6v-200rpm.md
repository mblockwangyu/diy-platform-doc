# TT减速马达 6V/200RPM


![](../../../en/electronic-modules/motors/images/tt-geared-motor-dc-6v-200rpm_TT-Geared-Motor-DC-6V-200RPM.jpg)

<img src="../../../en/electronic-modules/motors/images/tt-geared-motor-dc-6v-200rpm_微信截图_20160128120940-300x250.png" alt="微信截图_20160128120940" width="300" style="padding:5px 5px 12px 0px;">

### 描述

Makeblock TT减速电机DC 6V / 200RPM是 Makeblock 平台上带有塑料齿轮的新电源。这款TT齿轮电机完美配合 Makeblock 塑料正时皮带轮62T和塑料正时皮带轮90T，适用于DIY项目的车轮系统。它可以在 Makeblock mBot 中用作电源。

### 特征

- 支持正面和负面转移。
- 流量控制的准确性很高。
- 包括D（1.5-4）mm \* 0.5m硅胶管供使用。
- 食品和医疗行业的环保硅胶管。
- 结构简单，维护成本低。

### 规格

- 额定电压：DC 6V
- 无负载速度：200RPM±10％
- 齿轮比1:48

### 尺寸图(mm)

<img src="../../../en/electronic-modules/motors/images/tt-geared-motor-dc-6v-200rpm_微信截图_20160128121258-300x204.png" alt="微信截图_20160128121258" width="565" style="padding:5px 5px 12px 0px;">

### 演示

<img src="../../../en/electronic-modules/motors/images/tt-geared-motor-dc-6v-200rpm_微信截图_20160128121358-300x210.png" alt="微信截图_20160128121358" width="300" style="padding:5px 5px 12px 0px;">
<img src="../../../en/electronic-modules/motors/images/tt-geared-motor-dc-6v-200rpm_微信截图_20160128121404-300x295.png" alt="微信截图_20160128121404" width="300" style="padding:5px 5px 12px 0px;">

<img src="../../../en/electronic-modules/motors/images/tt-geared-motor-dc-6v-200rpm_微信截图_20160128121409-300x300.png" alt="微信截图_20160128121409" width="300" style="padding:5px 5px 12px 0px;">
<img src="../../../en/electronic-modules/motors/images/tt-geared-motor-dc-6v-200rpm_微信截图_20160128121414-300x290.png" alt="微信截图_20160128121414" width="300" style="padding:5px 5px 12px 0px;">
