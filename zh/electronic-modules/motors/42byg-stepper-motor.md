# 42步进电机

<img src="../../../en/electronic-modules/motors/images/42byg-stepper-motor_微信截图_20160128152924.png" alt="微信截图_20160128152924" width="334" style="padding:5px 5px 12px 0px;">

### 概述

Makeblock 42 步进电机是一种简单但功能强大的步进电机，具有高输出扭矩和响应速度，但噪音低，能耗低。它具有比 42 步进电机更高的扭矩。 它可以用作一些高性能机器的动力电机。

### 技术规格

- 步伐角度（度）：1.8±5%°
- 相电流：1.7A
- 额定电压：DC 12V
- 电线数量：4
- 电机长度：56mm
- 比例： 5.18：1
- 输出轴：D轴5mm
- 额定扭矩：40N.cm Min
- 静力矩：2.2N.cm Max
- 绝缘等级：B

### 功能特性

- 转速均匀，不丢步
- 结构稳定，不卡机
- 转速稳定，噪音低
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程
- 支持mBlock图形化编程，适合全年龄用户
- 支持树莓派python编程

### 编程指南

● **Arduino编程**

**函数功能说明（以MegaPi Pro为例）**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">函数</th>
    <th class="tg-7g6k">功能</th>
  </tr>
  <tr>
    <td class="tg-3xi5">MeStepperOnBoard （uint8_ port）</td>
    <td class="tg-c6of">选择接口<br>（stepper_1(1) ~ stepper_4(4)）
</td>
  </tr>
  <tr>
    <td class="tg-3xi5">move(position)</td>
    <td class="tg-c6of">移动到指定位置（非负整数）
</td>
  </tr>
  <tr>
    <td class="tg-3xi5">setSpeed(speed)</td>
    <td class="tg-c6of">设置速度（最大速度由电机型号决定）
</td>
  </tr>
</table>

以下程序运行后步进电机 1 以 3000/RPM 的速度旋转到指定位置，停止一秒，重复此过程。

```
#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>

#include <MeMegaPiPro.h>
MeStepperOnBoard stepper_1(1);

void setup(){
    TCCR1A = _BV(WGM10);//PIN12
    TCCR1B = _BV(CS11) | _BV(CS10) | _BV(WGM12);
    stepper_1.setMicroStep(16);
    stepper_1.enableOutputs();
}

void loop(){
    stepper_1.move(1000);
    stepper_1.setMaxSpeed(3000);
    stepper_1.setSpeed(3000);
    _delay(1);
    stepper_1.move(0);
    stepper_1.setMaxSpeed(0);
    stepper_1.setSpeed(0);
    _delay(1);
    _loop();
}
```
<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/42byg-stepper-motor/42BYG_Stepper_Motor.ino" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **mBlock 编程**

**积木块说明**
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/42byg-stepper-motor/42byg-stepper-motor-1.png" width="200"></td>
    <td class="tg-c6of">选择接口<br>设置速度（–255~+255）<br>设置指定位置（非负整数）
</td>
  </tr>
</table>

以下程序运行后步进电机 1 以 3000/RPM 的速度旋转到指定位置，停止一秒，重复此过程。

<img src="images/42byg-stepper-motor/42byg-stepper-motor-2.png" width="300"><br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/42byg-stepper-motor/42BYG Stepper Motor.sb2" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **慧编程编程**

**积木块说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/42byg-stepper-motor/42byg-stepper-motor-3.png" width="200"></td>
    <td class="tg-c6of">选择接口<br>设置指定位置（非负整数）<br>设置速度（–255~+255）
</td>
  </tr>
</table>

以下程序运行后步进电机 1 以 3000/RPM 的速度旋转到指定位置，停止一秒，重复此过程。

<img src="images/42byg-stepper-motor/42byg-stepper-motor-4.png" width="300"><br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/42byg-stepper-motor/42BYG Stepper Motor.mblock" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **Python 编程**

1、主控板 MegaPi Pro 和树莓派连接。<br>
2、树莓派安装最新的 Makeblock库 `pip3 install makeblock --upgrade`。<br>
3、新建 python 文件，后缀为 .py。<br>
4、在 python 文件里写入程序。<br>
5、运行 python 文件如 “python123.py”。

**函数功能说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">函数</th>
    <th class="tg-7g6k">功能</th>
  </tr>
  <tr>
    <td class="tg-3xi5">StepperMotor(port)</td>
    <td class="tg-c6of">创建步进电机对象。<br>port: MegaPiPro.PORT1~MegaPiPro.PORT4
</td>
  </tr>
  <tr>
    <td class="tg-3xi5">run(speed)</td>
    <td class="tg-c6of">以指定速度旋转。<br>speed: 转速（由电机型号速度决定）
</td>
  </tr>
  <tr>
    <td class="tg-3xi5">move_to(position,speed,callback)</td>
    <td class="tg-c6of">以指定速度旋转到指定位置。<br>position: 目标位置<br>speed: 转速（由电机型号速度决定）<br>callback: 达到目标位置时触发回调
</td>
  </tr>
</table>

**程序示例 1：**

以下程序运行后 MegaPi Pro 步进电机驱动接口1的步进电机将以 50% 的动力运行 2 秒，停止运动 1 秒，以 -50% 的动力运行 2 秒，停止一秒，循环。

```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
stepper = board.StepperMotor(MegaPiPro.PORT1)
while True:
    stepper.run(50)
    sleep(2)
    stepper.run(0)
    sleep(1)
    stepper.run(-50)
    sleep(2)
    stepper.run(0)
    sleep(1)
```

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/42byg-stepper-motor/StepperMotor_1.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

**程序示例 2：**

以下程序运行后 MegaPi Pro 步进电机驱动接口 1 的步进电机将以 100 的速度旋转到指定位置。

```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
stepper = board.StepperMotor(MegaPiPro.PORT1)
position = 0
def on_finished(value):
    position = 5000 - position
    stepper.move_to(position,100,on_finished)

on_finished(position)
```

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/42byg-stepper-motor/StepperMotor_2.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

### 连接方式

● **电子接线**
电缆颜色与驱动器端口（A +，A-，B +，B-）的对应关系如下所述：
1、步进电机有两个相。所以，你改变相A和相B之间的连接顺序并不重要，即黑色和绿色可以连接到A或B，红色和蓝色可以连接到B或A，会改变电机旋转方向。你需要遵循的规则只有一条，不能把它们混在一起。
2、A+:红
3、A-:黑
4、B+:蓝
5、B-:绿

<img src="images/42byg-stepper-motor/42byg-stepper-motor-5.png" width="300">

● **结构连接**

使用 Makeblock 的 42 步进电机支架可以很方便地固定 42 步进电机，还可以利用 42 步进电机支架将电机连接到其他结构件。

<img src="images/42byg-stepper-motor/42byg-stepper-motor-6.png" width="300">

<img src="images/42byg-stepper-motor/42byg-stepper-motor-7.png" width="200">

### 尺寸图

<img src="images/42byg-stepper-motor/42byg-stepper-motor-8.png" width="500">