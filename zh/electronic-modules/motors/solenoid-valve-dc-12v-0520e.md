# 两通迷你电磁阀

<img src="../../../en/electronic-modules/motors/images/solenoid-valve-dc-12v-0520e_微信截图_20160128154549.png" alt="微信截图_20160128154549" width="366" style="padding:5px 5px 12px 0px;">

### 描述

Makeblock电磁阀DC 12V / 0520E拥有迷你机身，广泛用于工业设备和DIY项目。

### 规格

- 额定电压：DC 12V
- 载入：Air
- 电流（有负载）：小于240mA
- 模式：两个位置，三向
- 总大小：34 x 21mm
- 最大压力：超过300mmHg
- 绝缘等级：A

### 尺寸图(mm)

<img src="../../../en/electronic-modules/motors/images/solenoid-valve-dc-12v-0520e_微信截图_20160128154708.png" alt="微信截图_20160128154708" width="729" style="padding:5px 5px 12px 0px;">
