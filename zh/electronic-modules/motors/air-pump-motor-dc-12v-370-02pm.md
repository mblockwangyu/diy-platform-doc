# 气泵马达 12V/370-02PM

<img src="../../../en/electronic-modules/motors/images/air-pump-motor-dc-12v-370-02pm_微信截图_20160128114507-300x291.png" alt="微信截图_20160128114507" width="300" style="padding:5px 5px 12px 0px;">

### 概述

Makeblock气泵电机 – DC 12V / 370-02PM广泛用于水族箱氧气循环，DIY项目。

### 技术规格

- 额定电压：DC 12V
- 载入：Air
- 电流（有负载）：小于250mA
- 流量：3.0LPM
- 尺寸：D27 x 65毫米
- 最大压力：超过600mmHg
- 噪音：&lt;60dB

### 尺寸图(mm)

<img src="../../../en/electronic-modules/motors/images/air-pump-motor-dc-12v-370-02pm_微信截图_20160128114823-300x103.png" alt="微信截图_20160128114823" width="530" style="padding:5px 5px 12px 0px;">

### 演示

<img src="../../../en/electronic-modules/motors/images/air-pump-motor-dc-12v-370-02pm_微信截图_20160128114931-300x134.png" alt="微信截图_20160128114931"  style="padding:5px 5px 12px 0px;">

<img src="../../../en/electronic-modules/motors/images/air-pump-motor-dc-12v-370-02pm_微信截图_20160128115008-300x259.png" alt="微信截图_20160128115008"  style="padding:5px 5px 12px 0px;">

<img src="../../../en/electronic-modules/motors/images/air-pump-motor-dc-12v-370-02pm_微信截图_20160128115016-300x252.png" alt="微信截图_20160128115016"  style="padding:5px 5px 12px 0px;">

 
