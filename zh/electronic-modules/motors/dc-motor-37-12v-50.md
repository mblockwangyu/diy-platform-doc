# 37直流电机 12V/50RPM

<img src="../../../en/electronic-modules/motors/images/dc-motor-37-12v_微信截图_20160128155423.png" alt="微信截图_20160128155423" width="288" style="padding:5px 5px 12px 0px;">

### 概述

直流电机是 Makeblock 平台中最常用的电机。使用 Makeblock 直流电机-37 支架，易于连接到 Makeblock 平台结构组件。兼容 Makeblock 平台多个主控板，如 MegaPi、MegaPi Pro、Me Auriga、Me Orion、NovaPi。

### 技术规格

- 电压：DC 12V
- 减速比：1:90
- 空载转速：50RPM±10%
- 空载电流：<=100mA
- 额定电流：<=300mA
- 额定力矩：4.5Kg.cm
- 重量：191g


### 功能特性

- 支持正反转
- 有 Arduino 库方便编程
- 支持图形化编程软件mBlock和慧编程
- 支持树莓派 Python 编程
- 孔位支持 Makeblock 平台结构件连接

### 编程指南

● **Arduino编程**

使用Arduino编程，需要调用库 `Makeblock-Library-master` 来控制电机。

**函数功能说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">函数</th>
    <th class="tg-7g6k">功能</th>
  </tr>
  <tr>
    <td class="tg-3xi5">MeDCMotor （uint8_ port）</td>
    <td class="tg-c6of">选择接口</td>
  </tr>
  <tr>
    <td class="tg-3xi5">run(int16_t speed)</td>
    <td class="tg-c6of">设定转速（–255~+255）</td>
  </tr>
  <tr>
    <td class="tg-3xi5">stop()</td>
    <td class="tg-c6of">停止电机</td>
  </tr>
</table>

以下程序运行后四路电路模块接口 1 的直流电机以全速旋转 1 秒，停止一秒，循环运动。

```
#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>

#include <MeMegaPiPro.h>
MeDCMotor dcfourmotor_1(1);

void loop(){
    dcfourmotor_1.run(255);
    delay(1);
    dcfourmotor_1.run(0);
    delay(1);
    loop();
}
```
<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/dc-motor-37-12v-50/DC_Motor-37_12V50RPM.ino" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **mBlock 编程**

**积木块说明**
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/dc-motor-37-12v-50/dc-motor-37-12v-50-1.png" width="200"></td>
    <td class="tg-c6of">选定接口（M9~M10）;设置速度（–255~+255）</td>
  </tr>
</table>

以下程序运行后四路直流电机驱动接口 1 电机以全速转动 1 秒，停止一秒，循环往复。

<img src="images/dc-motor-37-12v-50/dc-motor-37-12v-50-2.png" width="300">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/dc-motor-37-12v-50/DC Motor-37 12V50RPM.sb2" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **慧编程编程**

**积木块说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/dc-motor-37-12v-50/dc-motor-37-12v-50-3.png" width="200"></td>
    <td class="tg-c6of">选定接口（M9~M12）；设定参数（–100~+100）</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/dc-motor-37-12v-50/dc-motor-37-12v-50-4.png" width="200"></td>
    <td class="tg-c6of">选定接口（M9~M12）；设定参数（通电/断电）</td>
  </tr>
</table>

以下程序运行后直流电机 M9 以 50% 动力运行，电磁阀 M10 为通电，一秒后直流电机 M9 停止转动，电磁阀 M10 断电，重复此过程。

<img src="images/dc-motor-37-12v-50/dc-motor-37-12v-50-5.png" width="300">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/dc-motor-37-12v-50/DC Motor-37 12V50RPM.mblock" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **Python3 编程**

1、主控板 MegaPi Pro 和树莓派连接。<br>
2、树莓派安装最新的 Makeblock库 `pip3 install makeblock --upgrade`。<br>
3、新建 python 文件，后缀为 .py。<br>
4、在 python 文件里写入程序。<br>
5、运行 python 文件如 “python123.py”。

**函数功能说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">函数</th>
    <th class="tg-7g6k">功能</th>
  </tr>
  <tr>
    <td class="tg-3xi5">DCMotor(port)</td>
    <td class="tg-c6of">创建直流电机对象。<br>port: MegaPiPro.M9~MegaPiPro.M12
</td>
  </tr>
  <tr>
    <td class="tg-3xi5">run(speed)</td>
    <td class="tg-c6of">以百分比速度旋转。<br>speed：速度百分比，范围：–100~+100</td>
  </tr>
</table>

以下程序运行后 M9 直流电机将以 50% 的动力运行 2 秒，停止运动 1 秒，以 -50% 的动力运行 2 秒，停止一秒，循环运动。

```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
motor = board.DCMotor(MegaPiPro.M9)
while True:
    motor.run(50)
    sleep(2)
    motor.run(0)
    sleep(1)
    motor.run(-50)
    sleep(2)
    motor.run(0)
    sleep(1)
```

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/dc-motor-37-12v/DCMotor.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

### 连接方式

● **电子接线**

<img src="images/dc-motor-37-12v/dc-motor-37-12v-6.png" width="300">

● **结构搭建**

<img src="images/dc-motor-37-12v/dc-motor-37-12v-7.png" width="500">

### 尺寸图 (mm)

<img src="images/dc-motor-37-12v/dc-motor-37-12v-8.png" width="500">