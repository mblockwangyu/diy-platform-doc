# 智能舵机MS-12A

<img src="../../../zh/electronic-modules/motors/images/smart-servo-ms-12a/smart-servo-ms-12a.jpg" width="300" style="padding:5px 5px 12px 0px;">

### 概述

智能舵机 MS-12A 是 Makeblock 自主研发的新一代智能舵机，重点解决了传统舵机存在的控制复杂，角度范围小，不可连续旋转，配件单一等问题。
智能舵机 MS-12A 可以快速组装成多关节机器人：如人形机器人、多足蜘蛛机器人、机械臂等，也可以利用其可连续旋转的特性，组装成可控旋转平台，车轮驱动等机构。

配合软件，智能舵机 MS-12A 具有动作录制功能，无需编程，就能够实现复杂的动作。智能舵机 MS-12A 还支持Makeblock 丰富的编程软件平台：mBlock、Makeblock APP 图形化编程、神经元流式编程，同时支持 Arduino。

### 技术规格

- 型号：MS-12A 
- 电机：铁芯电机
- 齿轮：全金属齿轮（减速比1:305）
- 轴承：球轴承
- 外壳材料：PA
- 安装孔：M4盲孔铜螺母，有效深度6mm，间距16mm
- 舵盘：带4个M4螺纹孔可与舵机支架或者其他零件固定
- 安装支架：铝合金材质，带有直径4mm、间距16mm得到安装孔，通过螺丝固定，可兼容makeblock平台零件和乐高零件
- 尺寸：48x24x41mm
- 重量：72g
- 通讯：全双工串口 UART
- 速度：0.18s/60°，7.4v
- 角度分辨率：4096
- 扭矩：12kgf.cm
- 转角范围：0~360°,可连续旋转
- 指示灯：RGB LED可编程控制灯光颜色、亮度
- 接线端子：左IN（红）右OUT（白）
- 工作电压：DC6V~12.6V
- 位置反馈：磁编码
- 堵转电流：2A 
- 使用温度范围：0℃~80℃
- 保护：过流、过压、过温、欠压
- 死区带宽：无死区
- 反馈：位置、速度、温度、电流、电压
- 寿命：9万次
- 控制板支持：Me Auriga、MegaPi Pro、NovaPi

### 功能特性

- 高精度，大扭矩
- 丰富的硬件支持
- 可连续旋转
- 支持动作录制
- 智能保护，电机过热或电流过大是舵机启动智能保护
- 有Arduino库，方便编程
- 支持图形化编程软件 mBlock 和慧编程编程，适合全年龄用户
- 支持树莓派 Python 编程

### 编程指南

● **Arduino编程**

如果使用Arduino编程，需要调用库`Makeblock-Library-master` 来控制。

**函数功能说明**

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">mysmartservo.begin(115200)<br>mysmartservo.assignDevIdRequest()</td>
<td style="border: 1px solid black;">设置波特率并初始化智能舵机。</td>
</tr>

<tr>
<td style="border: 1px solid black;">mysmartservo.setRGBLed(index,R,G,B)</td>
<td style="border: 1px solid black;">设置智能舵机灯颜色。<br>参数：(舵机序号，R，G，B)<br>R，G，B 取值范围: 0~255
</td>
</tr>

<tr>
<td style="border: 1px solid black;">mysmartservo.setInitAngle(index)</td>
<td style="border: 1px solid black;">复位智能舵机。<br>参数：舵机序号</td>
</tr>

<tr>
<td style="border: 1px solid black;">mysmartservo.moveTo(index,position,speed)</td>
<td style="border: 1px solid black;">设置智能舵机以指定速度旋转到指定角度。<br>参数：舵机序号<br>角度：整数<br>速度：–50~+50</td>
</tr>

<tr>
<td style="border: 1px solid black;">mysmartservo.move(index,position,speed)</td>
<td style="border: 1px solid black;">设置智能舵机以指定速度旋转固定角度。<br>参数：舵机序号<br>角度：整数<br>速度：–50~+50</td>
</tr>

<tr>
<td style="border: 1px solid black;">mysmartservo.setPwmMove(index,speed)</td>
<td style="border: 1px solid black;">设置智能舵机以指定pwm速度旋转。<br>参数：舵机序号<br>速度：–255~+255</td>
</tr>

<tr>
<td style="border: 1px solid black;">mysmartservo.getSpeedRequest(index)<br>mysmartservo.getTempRequest(index)<br>mysmartservo.getCurrentRequest(index)<br>mysmartservo.getCurrentRequest(index)<br>mysmartservo.getAngleRequest(index)
</td>
<td style="border: 1px solid black;">读取智能舵机的速度/温度/电流/电压/角度值。<br>参数：舵机序号</td>
</tr>
</table>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/smart-servo-ms-12a/Smart_Servo_MS-12A.ino" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **mBlock编程** 

在使用mBlock控制智能舵机之前，需要通过扩展管理器添加智能舵机控制模块。

<img src="../../../zh/electronic-modules/motors/images/smart-servo-ms-12a/smart-servo-ms-12a-1.png" width="600" style="padding:5px 5px 12px 0px;">

智能舵机设置说明：
在扩展菜单栏下，智能舵机有两个设置：**设置零位** 和 **解除锁定**
**设置零位：**当智能舵机连接上MegaPi Pro主控板时将当前舵机位置设置为0°；
例如：使用语句块将舵机 [角度] 设置成90°，则以当前位置为 0° 将舵盘旋转至绝对角度90°；
**解除锁定：**在舵机通电使用过程中，如需手动调整角度，点击即可以让舵机恢复自由调整状态。

<img src="../../../zh/electronic-modules/motors/images/smart-servo-ms-12a/smart-servo-ms-12a-2.png" width="600" style="padding:5px 5px 12px 0px;">

**积木块说明**
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/smart-servo-ms-12a/smart-servo-ms-12a-3.png" width="200"></td>
    <td class="tg-c6of">初始化智能舵机，使用智能舵机时程序开头必须先添加此积木。
</td>
  </tr>
   <tr>
    <td class="tg-3xi5"><img src="images/smart-servo-ms-12a/smart-servo-ms-12a-3.png" width="200"></td>
    <td class="tg-c6of">初始化智能舵机，使用智能舵机时程序开头必须先添加此积木。
</td>
  </tr>
   <tr>
    <td class="tg-3xi5"><img src="images/smart-servo-ms-12a/smart-servo-ms-12a-4.png" width="200"></td>
    <td class="tg-c6of">选择智能舵机（1~6）；智能舵机转动到初始位置。
</td>
  </tr>
   <tr>
    <td class="tg-3xi5"><img src="images/smart-servo-ms-12a/smart-servo-ms-12a-5.png" width="200"></td>
    <td class="tg-c6of">选择智能舵机（1~6）；设置智能舵机动力（–255~+255）。
</td>
  </tr>
   <tr>
    <td class="tg-3xi5"><img src="images/smart-servo-ms-12a/smart-servo-ms-12a-6.png" width="200"></td>
    <td class="tg-c6of">选择智能舵机（1~6）；转动到目标角度（整数）；设置速度（0~50）。
</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/smart-servo-ms-12a/smart-servo-ms-12a-7.png" width="200"></td>
    <td class="tg-c6of">选择智能舵机（1~6）；相对转动角度（整数）；设置速度（0~50）。
</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/smart-servo-ms-12a/smart-servo-ms-12a-8.png" width="200"></td>
    <td class="tg-c6of">选择智能舵机（1~6）；设置 RGB 色（0~255）。
</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/smart-servo-ms-12a/smart-servo-ms-12a-9.png" width="200"></td>
    <td class="tg-c6of">选择智能舵机（1~6）；获取当前电流/电压/速度/角度/温度值。
</td>
  </tr>
</table>

以下程序运行后智能舵机 1 一秒内旋转到 30°，然后一秒内旋转到 120°，重复此过程。

<img src="images/smart-servo-ms-12a/smart-servo-ms-12a-10.png" width="300"><br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/smart-servo-ms-12a/Smart Servo MS-12A.sb2" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **慧编程编程** 

在慧编程中使用舵机，如果在对应设备的运动语句块中里找不到此模块，则需要添加创客平台扩展。

<img src="images/9g-micro-servo-metal-gear/9g-micro-servo-metal-gear-4.png" width="800">

**积木块说明**
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/smart-servo-ms-12a/smart-servo-ms-12a-11.png" width="200"></td>
    <td class="tg-c6of">选择智能舵机（1~6）；智能舵机转动到初始位置。
</td>
  </tr>
   <tr>
    <td class="tg-3xi5"><img src="images/smart-servo-ms-12a/smart-servo-ms-12a-12.png" width="200"></td>
    <td class="tg-c6of">选择智能舵机（1~6）；设置智能舵机动力（–100~+100）。
</td>
  </tr>
   <tr>
    <td class="tg-3xi5"><img src="images/smart-servo-ms-12a/smart-servo-ms-12a-13.png" width="200"></td>
    <td class="tg-c6of">选择智能舵机（1~6）；转动到目标角度（整数）；设置速度（0~50）。
</td>
  </tr>
   <tr>
    <td class="tg-3xi5"><img src="images/smart-servo-ms-12a/smart-servo-ms-12a-14.png" width="200"></td>
    <td class="tg-c6of">选择智能舵机（1~6）；相对转动角度（整数）；设置速度（0~50）。

</td>
  </tr>
   <tr>
    <td class="tg-3xi5"><img src="images/smart-servo-ms-12a/smart-servo-ms-12a-15.png" width="200"></td>
    <td class="tg-c6of">选择智能舵机（1~6）；获取当前电流/电压/速度/角度/温度值。
</td>
  </tr>
</table>

以下程序运行后智能舵机 1 一秒内旋转到 30°，然后一秒内旋转到 120°，重复此过程。

<img src="images/smart-servo-ms-12a/smart-servo-ms-12a-16.png" width="300"><br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/smart-servo-ms-12a/Smart Servo MS-12A.mblock" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **Python 编程**

1、主控板 MegaPi Pro 和树莓派连接，RJ25适配器与 MegaPi Pro RJ25 转接板接口相连，智能舵机与 RJ25 适配器相连接。<br>
2、树莓派安装最新的 Makeblock库 `pip3 install makeblock --upgrade`。<br>
3、新建 python 文件，后缀为 .py。<br>
4、在 python 文件里写入程序。<br>
5、运行 python 文件如 “python123.py”。

**函数功能说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">函数</th>
    <th class="tg-7g6k">功能</th>
  </tr>
  <tr>
    <td class="tg-3xi5">SmartServo(port)</td>
    <td class="tg-c6of">创建智能舵机对象。<br>port: 接口，默认MegaPiPro.PORT5</td>
  </tr>
  <tr>
    <td class="tg-3xi5">run(index,pwm)</td>
    <td class="tg-c6of">设置动力大小。<br>index: 舵机序号;pwm: PWM占空比，范围–100～+100</td>
  </tr>
  <tr>
    <td class="tg-3xi5">move_to(index,position,speed)</td>
    <td class="tg-c6of">按指定速度移动到绝对角度。<br>index: 舵机序号；position：目标角度；speed：转速，范围1～50，单位rpm</td>
  </tr>
  <tr>
    <td class="tg-3xi5">move(index,position,speed)</td>
    <td class="tg-c6of">按指定速度移动相对角度。<br>index：舵机序号；position：目标角度；speed：转速，范围1～50，单位rpm</td>
  </tr>
     <tr>
    <td class="tg-3xi5">set_zero(index)</td>
    <td class="tg-c6of">设置当前位置为原点。<br>index：舵机序号</td>
  </tr>
    <tr>
    <td class="tg-3xi5">set_led(index,red,green,blue)</td>
    <td class="tg-c6of">index：舵机序号；red：红；green：绿；blue：蓝</td>
  </tr>
</table>

**程序示例 1：**

以下程序运行后接在 MegaPi Pro 上的智能舵机 1 以速度 10 旋转 2 秒，停止一秒，以速度 -10 旋转 2秒，停止一秒，重复此过程。

```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
smartservo = board.SmartServo()
while True:
    smartservo.run(1,10)
    sleep(2)
    smartservo.run(1,0)
    sleep(1)
    smartservo.run(1,-10)
    sleep(2)
    smartservo.run(1,0)
    sleep(1)
```

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/smart-servo-ms-12a/SmartServoMS-12A_1.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

**程序示例 2：**

以下程序运行后 MegaPi Pro 连接的智能舵机 1 以 50 的速度旋转 720 度后停止，重复此过程运行。

```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
smartservo = board.SmartServo()
while True:
    smartservo.move_to(1,720,50)
    sleep(4)
    smartservo.move_to(1,0,50)
    sleep(4)
```

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/smart-servo-ms-12a/SmartServoMS-12A_2.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

**程序示例 3：**

程序运行后接在 MegaPi Pro6 口 RJ25 转接板 1 口的智能舵机旋转到30°，再旋转到 120°，重复运行。

```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
smartservo = board.SmartServo()
while True:
    smartservo.move(1,720,50)
    sleep(4)
    smartservo.move(1,-720,50)
    sleep(4)
```

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/smart-servo-ms-12a/SmartServoMS-12A_3.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

**程序示例 4：**

程序运行后接在 MegaPi Pro6 口 RJ25 转接板 1 口的智能舵机旋转到30°，再旋转到 120°，重复运行。

```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
smartservo = board.SmartServo()
while True:
    smartservo.set_led(1,50,0,0)
    sleep(0.5)
    smartservo.set_led(1,0,50,0)
    sleep(0.5)
    smartservo.set_led(1,0,0,50)
    sleep(0.5)
```

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/smart-servo-ms-12a/SmartServoMS-12A_4.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

### 连接方式

● **电子接线**

<img src="images/smart-servo-ms-12a/smart-servo-ms-12a-17.png" width="600">

● **结构连接**

<img src="images/smart-servo-ms-12a/smart-servo-ms-12a-18.png" width="600"><br>

<img src="images/smart-servo-ms-12a/smart-servo-ms-12a-19.png" width="600">

### 尺寸图

<img src="images/smart-servo-ms-12a/smart-servo-ms-12a-20.png" width="600">