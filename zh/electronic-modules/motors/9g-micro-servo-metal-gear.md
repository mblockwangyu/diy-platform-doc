# 9g 小舵机

<img src="../../../zh/electronic-modules/motors/images/9g-micro-servo-metal-gear/9g-micro-servo-metal-gear.png" width="300" style="padding:5px 5px 12px 0px;">

### 概述

9克小舵机是一种位置(角度)伺服的驱动器，适用于那些需要角度不断变化并可以保持的控制系统。常见于航模，飞机模型，遥控机器人及机械部件当中。在使用中，舵机的配件通常包含一个能把舵机固定到基座上的支架以及可以套在驱动轴上的舵盘，通过舵盘上的孔可以连接其它物体构成传动模型。小舵机自带的3线接口可以通过RJ25适配器与主板相连。

9克小舵机由直流电机、减速齿轮组、传感器和控制电路组成。其外部红黑白线分别代表电源正极、地线与控制信号线。Arduino开发环境下的微控制器所产生的控制信号是一种脉宽调制（PWM）信号，9克小舵机接收一束每20ms触发1-2ms的脉冲，例如：在1ms脉冲下舵机在0度位置；在1.5ms脉冲下，舵机会保持在90度位置；在2ms脉冲下，舵机在180度位置；通过调整脉宽可以实现舵机在不同范围内的运动。

### 技术规格

* 工作电压：4.8V~6V DC
* 工作电流：80~100mA
* 待机电流：5mA
* 极限角度： 210°±5%
* 扭力： 1.3到1.7kg/cm
* 工作温度：-10℃到60℃
* 湿度范围：60%±10%
* 转速： 0.09到0.10 sec/60°(4.8V)
* 信号周期：20 ms
* 信号高电平时间范围：1000到2000 us/周期
* 尺寸：32.3 x 12.3 x 30.6 mm (长x宽x高)

### 功能特性

- 体积小，重量轻
- 采用防反插接口
- 金属齿，更耐用
- 具有反接保护，电源反接不会损坏IC
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程
- 支持 mBlock 图形化编程，适合全年龄用户
- 支持树莓派 Python 编程

### 引脚定义

9克小舵机模块有三个针脚的接头,每个针脚的功能如下表：

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

</td>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;"> GND</td>
<td style="border: 1px solid black;">地线(黑色)</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">电源线（红色）</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">SIG</td>
<td style="border: 1px solid black;">控制信号（白色）</td>
</tr>
</table>


### 编程指南

● **Arduino编程**

如果使用Arduino编程，需要调用库 `Makeblock-Library-master` 来控制 9克小舵机模块。

<img src="images/9g-micro-servo-metal-gear/9g-micro-servo-metal-gear-1.png" width="421" style="padding:5px 5px 12px 0px;">

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">MePort(uint8_t port)</td>
<td style="border: 1px solid black;">定义连接端口</td>
</tr>

<tr>
<td style="border: 1px solid black;">void attach()</td>
<td style="border: 1px solid black;">选定引脚</td>
</tr>

<tr>
<td style="border: 1px solid black;">void write(int angle)</td>
<td style="border: 1px solid black;">控制舵机旋转到指定角 </td>
</tr>
</table>

**编程示例**

以下程序运行后舵机一秒内旋转到30°，然后一秒内旋转到120°，重复此过程。
```
#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>
#include "analog_servo.h"
#include <MeMegaPiPro.h>

Servo servo_6_1;

void setup(){
    servo_6_1.attach(port_pro(6,1)+A0);}

void loop(){
    servo_6_1.write(30);    _delay(1);
    servo_6_1.write(120);    _delay(1);
    _loop();
}
```
<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/9g-micro-servo-metal-gear/9g_Micro_Servo.ino" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **mBlock编程** 

9克小舵机支持mBlock编程环境，如下是该模块指令简介（以MegaPi Pro为例）：

**积木块说明**
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/9g-micro-servo-metal-gear/9g-micro-servo-metal-gear-2.png" width="200"></td>
    <td class="tg-c6of">选择接口<br>选择RJ25适配器插口<br>设置转动目标角度（0~180）
</td>
  </tr>
</table>

以下程序运行后舵机一秒内旋转到30°，然后一秒内旋转到120°，重复此过程。  

<img src="images/9g-micro-servo-metal-gear/9g-micro-servo-metal-gear-3.png" width="300"><br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/9g-micro-servo-metal-gear/9g Micro Servo Pack.sb2" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **慧编程编程** 

以 mCore 为例。

在慧编程中使用舵机，如果在对应设备的运动语句块中里找不到此模块，则需要添加扩展。

<img src="images/9g-micro-servo-metal-gear/9g-micro-servo-metal-gear-4.png" width="800">

**积木块说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/9g-micro-servo-metal-gear/9g-micro-servo-metal-gear-5.png" width="200"></td>
    <td class="tg-c6of">选择接口<br>选择RJ25适配器插口<br>设置转动目标角度（0~180）
</td>
  </tr>
</table>

以下程序运行后舵机一秒内旋转到30°，然后一秒内旋转到120°，重复此过程。

<img src="images/9g-micro-servo-metal-gear/9g-micro-servo-metal-gear-6.png" width="300"><br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/9g-micro-servo-metal-gear/9g Micro Servo Pack.mblock" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **Python3 编程** 

1、主控板MegaPi Pro和树莓派连接，RJ25 适配器与 MegaPi Pro RJ25 转接板接口相连，9g 小舵机与RJ25 适配器相连接。<br>
2、树莓派安装最新的 makeblock 库 `pip3 install makeblock --upgrade`。<br>
3、新建 Python 文件，后缀为 .py。<br>
4、在 Python 文件里写入程序。<br>
5、运行 Python文件，如 “python123.py ”。

**语句说明：以MegaPi Pro为例**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">函数</th>
    <th class="tg-7g6k">功能</th>
  </tr>
  <tr>
    <td class="tg-3xi5">Servo(port,slot)</td>
    <td class="tg-c6of">创建舵机对象。<br>port: MegaPiPro.PORT6~MegaPiPro.PORT12<br>slot: MegaPiPro.SLOT1~MegaPiPro.SLOT2</td>
  </tr>
  <tr>
    <td class="tg-3xi5">set_angle(angle)</td>
    <td class="tg-c6of">设置舵机旋转角度<br>angle: 旋转角度，范围：0 ~ 180°</td>
  </tr>
</table>

以下程序运行后接在MegaPi Pro6口RJ25转接板1口的9g舵机旋转到30°，再旋转到120°，重复运行。

```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
servo = board.Servo(MegaPiPro.PORT6,MegaPiPro.SLOT1)
while True:
    servo.set_angle(30)
    sleep(1)
    servo.set_angle(120)
    sleep(1)
```
<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/9g-micro-servo-metal-gear/Servo.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

### 连接方式

#### 电子连接
##### RJ25 连接

9克小舵机可以通过 RJ25 适配器与主板相连。
以 Makeblock Orion 为例，可以连接到3，4，5，6，7,8号接口，当接到7，8号接口时，舵机只能在 RJ25 适配器的 SLOT2 端口上，如下图所示：

<img src="images/9g-micro-servo-metal-gear/9g-micro-servo-metal-gear-7.png" width="300">

##### 杜邦线连接

当使用杜邦线连接到 Arduino Uno 主板时，舵机 SIG 引脚需要连接到 DIGITAL（数字）口，如下图所示：

<img src="images/9g-micro-servo-metal-gear/9g-micro-servo-metal-gear-8.png" width="300">

#### 结构搭建

<img src="images/9g-micro-servo-metal-gear/9g-micro-servo-metal-gear-9.png" width="200"><br>

<img src="images/9g-micro-servo-metal-gear/9g-micro-servo-metal-gear-10.png" width="200"><br>

<img src="images/9g-micro-servo-metal-gear/9g-micro-servo-metal-gear-11.png" width="200"><br>

<img src="images/9g-micro-servo-metal-gear/9g-micro-servo-metal-gear-12.png" width="200"><br>

<img src="images/9g-micro-servo-metal-gear/9g-micro-servo-metal-gear-13.png" width="200">
