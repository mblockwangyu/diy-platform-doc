# RGB灯模块


![](../../../en/electronic-modules/displays/images/me-rgb-led_Me-RGB-LED.jpg)


### 概述

彩色LED模块包含四个可调全色域RGB LED。每个LED的颜色可以红(R)、绿(G)、蓝(B)三个颜色的数值大小来决定。每个RGB LED内部集成了控制芯片，只需一根信号线就可以实现独立全彩功能。具备高亮和亮度可调的特点，从而可以实现流水、闪烁、彩虹灯等效果。本模块接口是黄色色标，说明是单数字口控制，需要连接到主板上带有黄色标识接口。

### 技术规格

- 工作电压： 5V DC  
- 灯数量： 4 x RGB LED  
- 最大电流：每个60mA，共240mA  
- 灯型号：WS2812-4  
- 亮度范围：0\~255  
- 控制方式：单数字口控制  
- 工作温度：-25～+80℃  
- 可视角：&gt;140 度  
- 模块尺寸：52 x 24 x 18 mm (长x宽x高)

### 功能特性

- 每个像素点的三基色颜色可实现256级亮度显示，完成16777216种颜色的全真色彩显示，扫描频率不低于400Hz/s。  
- 串行级联接口，能通过一根信号线完成数据的接收与解码；  
- 模块的白色区域是与金属梁接触的参考区域；  
- 具有反接保护，电源反接不会损坏IC；  
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程；  
- 支持 mBlock 图形化编程，适合全年龄用户；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 配有接头支持绝大多数 Arduino 系列主控板。

<img src="../../../en/electronic-modules/displays/images/me-rgb-led_微信截图_20160129105959.png" alt="微信截图_20160129105959" width="580" style="padding:5px 5px 12px 0px;">

### 引脚定义

彩色LED模块有三个针脚的接头,每个针脚的功能如下表

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;"> 地线</td>
</tr>

<tr>
<td style="border: 1px solid black;">2 </td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">电源线</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">SIG</td>
<td style="border: 1px solid black;">信号控制</td>
</tr>

</table>

<br>

### 连线模式

● **RJ25连接**  

由于彩色LED模块接口是黄色色标，当使用RJ25接口时，需要连接到主控板上带有黄色色标的接口。

以 Makeblock Orion 为例，可以连接到3，4，5，6，7，8 号接口，如图 

<img src="../../../en/electronic-modules/displays/images/me-rgb-led_微信截图_20160129110115.png" alt="微信截图_20160129110115" width="259" style="padding:5px 5px 12px 0px;">

● **杜邦线连接** 

当使用杜邦线连接到Arduino Uno主板时，模块SIG引脚需要连接到
DIGITAL（数字）口，如下图所示：

<img src="../../../en/electronic-modules/displays/images/me-rgb-led_微信截图_20160129110158.png" alt="微信截图_20160129110158" width="300" style="padding:5px 5px 12px 0px;">

### 编程指南

● **Arduino 编程**  

<img src="../../../en/electronic-modules/displays/images/me-rgb-led_微信截图_20160129110241.png" alt="微信截图_20160129110241" width="523" style="padding:5px 5px 12px 0px;">

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>

<tr>
<td style="border: 1px solid black;">MeRGBLed(uint8_t port)</td>
<td style="border: 1px solid black;">选定接口</td>
</tr>
<tr>
<td style="border: 1px solid black;">void show()</td>
<td style="border: 1px solid black;"> 开始显示</td>
</tr>|
<tr>
<td style="border: 1px solid black;">void setNumber(uint8_t num_leds)</td>
<td style="border: 1px solid black;">设定LED总数</td>
</tr>
<tr>
<td style="border: 1px solid black;">Bool setColorAt(uint8_t index, uint8_t red, uint8_t green, uint8_t blue)</td>
<td style="border: 1px solid black;">设定LED红绿蓝参数</td>
</tr>
<tr>
<td style="border: 1px solid black;">void reset(uint8_t port)</td>
<td style="border: 1px solid black;">重置端口</td>
</tr>
<tr>
<td style="border: 1px solid black;">void clear()</td>
<td style="border: 1px solid black;">清除显示</td>
</tr>
</table>

<br>

● **mBlock 编程**

LED 彩灯支持 mBlock 编程环境，其指令介绍如下：  

<img src="../../../en/electronic-modules/displays/images/me-rgb-led_微信截图_20160129110342.png" alt="微信截图_20160129110342" width="718" style="padding:5px 5px 12px 0px;">

### 原理解析

RGB灯的数据协议采用单线归零码的通讯方式，像素点在上电复位以后，DIN端接受从控制器传输过来的数据，首先送过来的24bit数据被第一个像素点提取后，送到像素点内部的数据锁存器，剩余的数据经过内部整形处理电路整形放大后通过DO端口开始转发输出给下一个级联的像素点，每经过一个像素点的传输，信号减少24bit。像素点采用自动整形转发技术，使得该像素点的级联个数不受信号传送的限制，仅仅受限信号传输速度要求。

### 原理图

<img src="../../../en/electronic-modules/displays/images/me-rgb-led_LED-RGB.png" alt="LED RGB" width="1269" style="padding:5px 5px 12px 0px;">

### 相关链接

RGB color table: http://tool.oschina.net/commons?type=3
