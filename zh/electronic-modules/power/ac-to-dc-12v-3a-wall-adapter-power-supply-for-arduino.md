# 12V 插墙式电源适配器


![](../../../en/electronic-modules/power/images/ac-to-dc-12v-3a-wall-adapter-power-supply-for-arduino_AC-to-DC-12V-3A-Wall-Adapter-Power-Supply-For-Arduino-Meduino.jpg)

<img src="../../../en/electronic-modules/power/images/ac-to-dc-12v-3a-wall-adapter-power-supply-for-arduino_微信截图_20160126174621-300x270.png" alt="微信截图_20160126174621" width="300" style="padding:5px 5px 12px 0px;">

### 概述

交流到直流12V 3A适配器电源对于 Arduino 是一种小型便携式电子设备和电器的电源设备。它的输入是市电，其输出为直流12V。它自带的5.5×2.1毫米直流插头可插入 Makeblock Orion 开发板，为电机驱动器供电。该适配器可以提供大电流以满足大量电机的大规模建设和应用。

### 技术规格

- 输入电压：100-240V AC
- 输入电压的频率：50/60 HZ
- 输出电压：12V DC
- 输出电流：3A
- 峰值电流：10A
- 输出功率：36W
- 最大纹波噪音：120 MVP-P
- 工作温度：0〜40℃
- 存储温度：-20〜60℃
- 输出线的长度：1.2米
- DC插头的大小：5.5×2.1×10毫米（外径×内径×L）
- 尺寸：84 X47 X37毫米（长x宽x高）

### 功能特性

- 全范围AC输入电压
- 良好的抗干扰性能和高可靠性
- 小DC纹波和高效率
- 紧凑和有效的
- 良好的绝缘性能和高介电强度
