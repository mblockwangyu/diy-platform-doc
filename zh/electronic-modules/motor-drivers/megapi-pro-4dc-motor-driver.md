# MegaPi Pro 四路直流电机驱动

<img src="images/megapi-pro-4dc-motor-driver/megapi-pro-4dc-motor-driver.png" width="300">

### 概 述

本模块用于 MegaPi Pro，可以扩展出 4 个直流电机接口，具有方便的插装结构。可以驱动 4 个 25 直流电机/ 37 直流电机/电磁阀。
直流电机加驱动电路目的是为了提供足够大的电流，H桥驱动电路是为了控制直流电机而设计的一种常见电路，它主要实现直流电机的正反向驱动，其形状类似于字母 H，4 个开关（MOSFET）所在位置称为桥臂，而作为负载的直流电机是像桥一样架在上面，故称之为H桥驱动。借助 4 个开关不同的开合状态，可以产生电机的 4 个工作状态：正转、反转、刹车与惰行。

### 技术规格

* 电机驱动：MP80495
* 电机通道：4
* 最低工作电压：6V
* 最高工作电压：12V
* 逻辑电压：5V
* 各通道额定工作电流：3A
* 峰值工作电流：5.5A
* 模块尺寸：39mmx39mm（长x宽）


### 功能特性

* 支持工作电压为 6~12V 的电机
* 12V 电源供电时，工作电流可达 3A (峰值电流可达5.5A)
* 模块拥有过压保护、过流保护、过温保护，全方位保证使用安全
* 可以驱动 4 个直流电机
* 编码电机驱动端有反响电动势保护（TVS保护）
* 本模块电机转向与编码/直流电机驱动模块相反，便于用户多样使用

### 编程指南

● **Arduino编程**

如果使用Arduino编程，需要调用库 `Makeblock-Library-master` 来控制四路电机驱动模块。

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">MeDCMotor （uint8_ port）</td>
<td style="border: 1px solid black;">选择接口<br>( dcfourmotor_1(1)~dcfourmotor_4(4) )</td>
</tr>

<tr>
<td style="border: 1px solid black;">run(int16_t speed)</td>
<td style="border: 1px solid black;">设定转速（–255~+255）</td>
</tr>

<tr>
<td style="border: 1px solid black;">stop()</td>
<td style="border: 1px solid black;">停止电机</td>
</tr>
</table>

**编程示例**

以下程序运行后四路电路模块接口 1 的直流电机以全速旋转 1 秒，停止 1 秒，循环运动。
```
#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>

#include <MeMegaPiPro.h>
MeDCMotor dcfourmotor_1(1);

void loop(){
    dcfourmotor_1.run(255);
    delay(1);
    dcfourmotor_1.run(0);
    delay(1);
    loop();
}
```
<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-4dc-motor-driver/MegaPi_Pro_4DC_Motor_Driver_V1.0.ino" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **mBlock编程** 

**积木块说明**
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-4dc-motor-driver/megapi-pro-4dc-motor-driver-1.png" width="200"></td>
    <td class="tg-c6of">选定接口（M9~M10）;设置速度（–255~+255）。
</td>
  </tr>
</table>

以下程序运行后四路直流电机驱动接口 1 连接的电机以全速转动 1 秒，停止 1 秒，循环往复。

<img src="images/megapi-pro-4dc-motor-driver/megapi-pro-4dc-motor-driver-2.png" width="300"><br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-4dc-motor-driver/MegaPi Pro 4DC Motor Driver V1.0.sb2" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **慧编程编程** 

**积木块说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-4dc-motor-driver/megapi-pro-4dc-motor-driver-3.png" width="200"></td>
    <td class="tg-c6of">选定接口（M9~M12）；设定参数（–100~+100）。</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-4dc-motor-driver/megapi-pro-4dc-motor-driver-4.png" width="200"></td>
    <td class="tg-c6of">选定接口（M9~M12）；设定参数（通电/断电）。</td>
  </tr>
</table>

以下程序运行后直流电机 M9 以 50% 动力运行，电磁阀 M10 为通电，1 秒后直流电机 M9 停止转动，电磁阀 M10 断电，重复此过程。

<img src="images/megapi-pro-4dc-motor-driver/megapi-pro-4dc-motor-driver-5.png" width="300"><br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-4dc-motor-driver/MegaPi Pro 4DC Motor Driver V1.0.mblock" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **Python3 编程** 

1、主控板 MegaPi Pro 和树莓派连接。<br>
2、树莓派安装最新的 makeblock 库 `pip3 install makeblock --upgrade`。<br>
3、新建 Python 文件，后缀为 .py。<br>
4、在 Python 文件里写入程序。<br>
5、运行 Python文件，如 “python123.py ”。

**语句说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">函数</th>
    <th class="tg-7g6k">功能</th>
  </tr>
  <tr>
    <td class="tg-3xi5">DCMotor(port)</td>
    <td class="tg-c6of">创建直流电机对象。<br>port: MegaPiPro.M9~MegaPiPro.M12</td>
  </tr>
  <tr>
    <td class="tg-3xi5">run(speed)</td>
    <td class="tg-c6of">以百分比速度旋转。<br>speed：速度百分比，范围：–100~+100</td>
  </tr>
</table>

以下程序运行后 M9 直流电机将以 50% 的动力运行 2 秒，停止运动 1秒，以 –50% 的动力运行 2 秒，停止 1 秒，循环运动。

```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
motor = board.DCMotor(MegaPiPro.M9)
while True:
    motor.run(50)
    sleep(2)
    motor.run(0)
    sleep(1)
    motor.run(-50)
    sleep(2)
    motor.run(0)
    sleep(1)
```
<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-4dc-motor-driver/DCMotor.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

### 连接方式

<img src="images/megapi-pro-4dc-motor-driver/megapi-pro-4dc-motor-driver-6.png" width="300"><br>

<img src="images/megapi-pro-4dc-motor-driver/megapi-pro-4dc-motor-driver-7.png" width="300">

### 原理图

<img src="images/megapi-pro-4dc-motor-driver/megapi-pro-4dc-motor-driver-8.png" width="800">