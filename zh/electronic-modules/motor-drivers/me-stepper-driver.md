# 步进电机驱动模块

<img src="../../../en/electronic-modules/motor-drivers/images/me-stepper-driver_微信截图_20160203152127.png" alt="微信截图_20160203152127" width="347" style="padding:5px 5px 12px 0px;">

### 概 述

步进电机驱动模块是用来精确驱动双极步进电机的。当有脉冲输入，步进电动机一步一步地转动，每给它一个脉冲信号，它就转过一定的角度。它可以用在3D打印、数控、Makeblock音乐机器人以及精确动作控制等方面。本模块贴有红色色标，我们需要使用RJ25连接线连接到主控板上带有红色标识的接口。

### 技术规格\`

- 驱动电压: 6V-12V DC  
- 最大电流: 1.35A  
- 尺寸: 51 x 24 x 18mm (L x W x H)

### 功能特性

- 兼容4线双极步进电机；  
- 只需要两个端口就可以控制步进和方向；  
- 可调电位器可以调节最大电流输出，改变步进电机扭矩；  
- 具有板上拨码开关支持 全, 半, 1/4, 1/8, 1/16步进模式；  
- 具有接地短路保护和加载短路保护；  
- 具有反接保护，电源反接不会损坏IC；  
- 模块的白色区域是与金属梁接触的参考区域；  
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程；  
- 支持 mBlock 图形化编程，适合全年龄用户；  
- 使用 RJ25 接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 配有接头支持绝大多数 Arduino 系列主控板。

### 引脚定义

步进电机驱动模块有7个针脚的接头,每个针脚的功能如下表:

<img src="../../../en/electronic-modules/motor-drivers/images/me-stepper-driver_微信截图_20160203152355.png" alt="微信截图_20160203152355" width="720" style="padding:5px 5px 12px 0px;">

### 连线方式

● **RJ25连接**  

由于步进电机驱动模块接口是红色色标，属于电机驱动。当使用RJ25接口时，需要连接到主控板上带有红色色标的接口。以 Makeblock
Orion 为例，可以连接到1,2号接口，如图:

<img src="../../../en/electronic-modules/motor-drivers/images/me-stepper-driver_微信截图_20160203152448.png" alt="微信截图_20160203152448" width="388" style="padding:5px 5px 12px 0px;">  

> <small>注：驱动板长时间工作，芯片会发热，使用的时候请注意。有需要的话可以在上面加个散热片，帮助芯片散热。</small>  

● **杜邦线连接** 

当使用杜邦线连接到 Arduino Uno 主板时，EN 接低电平,RST 和 SLP 接高电平，STP 和 DIR 引脚需要连接到 ANALOG（模拟）口（也可以只连接STP、DIR管脚），如下图所示：

<img src="../../../en/electronic-modules/motor-drivers/images/me-stepper-driver_微信截图_20160203152526.png" alt="微信截图_20160203152526" width="389" style="padding:5px 5px 12px 0px;">

### 编程指导

● **Arduino编程**  

如果使用 Arduino 编程，需要调用库 ``Makeblock-Library-master``
 来控制步进电机驱动模块本程序通过 Arduino 编程让电机按需求转动。

<img src="../../../en/electronic-modules/motor-drivers/images/me-stepper-driver_微信截图_20160203152612.png" alt="微信截图_20160203152612" width="487" style="padding:5px 5px 12px 0px;">

<img src="../../../en/electronic-modules/motor-drivers/images/me-stepper-driver_微信截图_20160203152751.png" alt="微信截图_20160203152751" width="583" style="padding:5px 5px 12px 0px;">

● **mBlock编程**  

步进电机驱动模块支持 mBlock 编程环境，如下是该模块指令简介：

<img src="../../../en/electronic-modules/motor-drivers/images/me-stepper-driver_微信截图_20160203152822.png" alt="微信截图_20160203152822" width="581" style="padding:5px 5px 12px 0px;">

以下是如何使用mBlock控制步进电机驱动模块的例子： 

mBlock 可以使步进电机在不同时间段速度由小到大转动，循环往复。

<img src="../../../en/electronic-modules/motor-drivers/images/me-stepper-driver_微信截图_20160203152902.png" alt="微信截图_20160203152902" width="791" style="padding:5px 5px 12px 0px;">

### 原理解析

步进电动机是一种将脉冲信号变换成相应的角位移(或线位移)的电磁装置，是一种特殊的电动机。一般电动机都是连续转动的，而步进电动机则有定位和运转两种基本状态，当有脉冲输入时，步进电动机一步一步地转动，每给它一个脉冲信号，它就转过一定的角度。

本模块主要元件为A4988微步驱动器，可在全、半、1/4、 1/8 及 1/16
步进模式时操作双极步进电动机，在具体的使用中我们只要控制 STEP 和 DIR 就可以了。例如：当为全步进模式时，转一圈要 200步（即每一步1.8°）。如果要求更高的精度，我们可以通过选择其他的模式，比如我们如果选择1/4 步进模式，那么电机转一圈就要800 个微步才能完成。  

模块步进模式选择表：

<img src="../../../en/electronic-modules/motor-drivers/images/me-stepper-driver_微信截图_20160203153000.png" alt="微信截图_20160203153000" width="635" style="padding:5px 5px 12px 0px;">

模块上有电位器，可以通过其来调节电机的扭矩，但使用时不宜将其调节过大，否则容易因为发热而将芯片烧毁。

### 原理图

<img src="../../../en/electronic-modules/motor-drivers/images/me-stepper-driver_Me-Stepper-Module.png" alt="Me Stepper Module" width="1220" style="padding:5px 5px 12px 0px;">
