# MegaPi Pro 步进电机驱动模块

<img src="images/megapi-pro-stepper-motor-driver/megapi-pro-stepper-motor-driver.png" width="300" style="padding:5px 5px 12px 0px;">

### 概述

MegaPi Pro 步进电机驱动模块用于驱动步进电机适用于 MegaPi 和MegaPi Pro 主控板。采用 DRV8825 芯片，最大驱动电流为 2.5A。DRV8825是一款完整的微步进电机驱动器，内置转换器，操作方便。它可用于驱动以 1/2，1/4，1/8 / 1/16 或 1/32 步进模式操作的双极步进电机。它还配备有一个板载电位器，可轻松地调整电机的电流大小。配合丝杆机构可以做运动精确控制，可以用于工业级的直线运动控制，如3D打印机，激光切割机等XYZ结构的机器。

### 技术规格

- 电机驱动：DRV8825
- 输出电流：1.75A（24V和25℃散热适当条件下）
- 最大电流：2.5A（注意散热）
- 驱动电压：8.2V-45V
  **注：** MegaPi 最大电源电压为 12V。
•	逻辑电压：5V
•	尺寸：1.5cm x 2.0cm


### 功能特性

- 兼容四线双极步进电机
- 板载电位器，用于控制步进电机的电流
- 板载散热器，用于散热
- 可驱动一步步进电机（绿色接口）
- 公母针脚采用颜色进行区分，确保连接正确
- 小尺寸，便于连接
- 有 Arduino 库，方便编程
- 支持图形化编程软件 mBlock3 及 慧编程
- 支持树莓派 Python 编程

### 编程指南

● **Arduino编程（以 MegaPi Pro 为例）**

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">MeStepperOnBoard （uint8_ port）</td>
<td style="border: 1px solid black;">选择接口<br>（stepper_1(1) ~ stepper_4(4)）</td>
</tr>

<tr>
<td style="border: 1px solid black;">move(position)</td>
<td style="border: 1px solid black;">移动到指定位置（非负整数）</td>
</tr>

<tr>
<td style="border: 1px solid black;">setSpeed(speed)</td>
<td style="border: 1px solid black;">设置速度（最大速度由电机型号决定）</td>
</tr>
</table>

**编程示例**

以下程序运行后步进电机 1 以 3000/RPM 的速度旋转到指定位置，停止 1 秒，重复此过程。
```
#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>

#include <MeMegaPiPro.h>
MeStepperOnBoard stepper_1(1);

void setup(){
    TCCR1A = _BV(WGM10);//PIN12
    TCCR1B = _BV(CS11) | _BV(CS10) | _BV(WGM12);
    stepper_1.setMicroStep(16);
    stepper_1.enableOutputs();
}

void loop(){
    stepper_1.move(1000);
    stepper_1.setMaxSpeed(3000);
    stepper_1.setSpeed(3000);
    delay(1);
    stepper_1.move(0);
    stepper_1.setMaxSpeed(0);
    stepper_1.setSpeed(0);
    delay(1);
    loop();
}
```
<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-stepper-motor-driver/42BYG_Stepper_Motor.ino" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **mBlock编程** 

**积木块说明（以MegaPi Pro为例）**
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-stepper-motor-driver/megapi-pro-stepper-motor-driver-1.png" width="200"></td>
    <td class="tg-c6of">设置接口（接口1~接口4）；设置速度（–3000~+3000）；设置距离（非负整数）</td>
  </tr>
</table>

以下程序运行后 MegaPi Pro 接口 1 的步进电机以 3000 的速度运动 1000 距离，停止运动 1 秒，以 –3000 的速度运动 1000 距离，停止运动 1 秒，重复此过程。

<img src="images/megapi-pro-stepper-motor-driver/megapi-pro-stepper-motor-driver-2.png" width="300"><br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-stepper-motor-driver/42BYG Stepper Motor.sb2" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **慧编程编程** 

**积木块说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-stepper-motor-driver/megapi-pro-stepper-motor-driver-3.png" width="200"></td>
    <td class="tg-c6of">设置接口（接口1~接口4）；设置距离（非负整数）；设置速度（–3000~+3000）</td>
  </tr>
</table>

以下程序运行后 MegaPi Pro 接口 1 的步进电机以 1000 的速度运动 1000 距离，停止运动1 秒，以 –1000 的速度运动 1000 距离，停止运动 1 秒，重复此过程。

<img src="images/megapi-pro-stepper-motor-driver/megapi-pro-stepper-motor-driver-4.png" width="300"><br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-stepper-motor-driver/42BYG Stepper Motor.mblock" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **Python3 编程** 

1、主控板 MegaPi Pro 和树莓派连接。<br>
2、树莓派安装最新的 makeblock 库 `pip3 install makeblock --upgrade`。<br>
3、新建 Python 文件，后缀为 .py。<br>
4、在 Python 文件里写入程序。<br>
5、运行 Python文件，如 “python123.py ”。

**函数功能说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">函数</th>
    <th class="tg-7g6k">功能</th>
  </tr>
  <tr>
    <td class="tg-3xi5">StepperMotor(port)</td>
    <td class="tg-c6of">创建步进电机对象。<br>port：MegaPiPro.PORT1~MegaPiPro.PORT4</td>
  </tr>
  <tr>
    <td class="tg-3xi5">run(speed)</td>
    <td class="tg-c6of">以指定速度旋转。<br>speed：转速</td>
  </tr>
  <tr>
    <td class="tg-3xi5">move_to(position,speed,callback)</td>
    <td class="tg-c6of">以指定速度旋转到指定位置。<br>position:目标位置；speed：转速；callback：达到目标位置时触发回调</td>
  </tr>
  <tr>
    <td class="tg-3xi5">set_home()</td>
    <td class="tg-c6of">设置当前位置为原点</td>
  </tr>
</table>

**示例程序 1**

以下程序运行后 MegaPi Pro 接口1的步进电机以 50 的速度旋转 2
秒，停止 1 秒，以 –50 的速度旋转 2 秒，停止 1 秒，按照此过程循环。

```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
stepper = board.StepperMotor(MegaPiPro.PORT1)
while True:
    stepper.run(50)
    sleep(2)
    stepper.run(0)
    sleep(1)
    stepper.run(-50)
    sleep(2)
    stepper.run(0)
    sleep(1)
```
<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-stepper-motor-driver/StepperMotor_1.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

**示例程序 2**

以下程序运行后 MegaPi Pro 接口 1 的步进电机旋转到指定位置。

```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
stepper = board.StepperMotor(MegaPiPro.PORT1)
position = 0
def on_finished(value):
    position = 5000 - position
    stepper.move_to(position,100,on_finished)

on_finished(position)
```
<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-stepper-motor-driver/StepperMotor_2.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>