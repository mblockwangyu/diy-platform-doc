# 大功率直流编码电机驱动模块

<img src="images/me-hp-encoder-dc-motor-driver.png" width="352" style="padding:5px 5px 12px 0px;">

### 概述

大功率直流编码电机驱动模块（Me High Power Encoder/DC Motor Driver）支持两路 36 直流编码电机或直流电机驱动，并且可以对编码电机进行速度与方位的精确控制。通过 makeblock 官网提供的软件可以调控电机的 PID 参数使它能在不同环境下达到最佳工作状态。本模块接口是红白色色标，代表输入电压值可以是 6 到12V，也可以是 5V，实际应用需要连接到主板带有红色或白色标识的接口。

### 功能特性

-   两档拨码开关设置方式更便于使用及控制
- 可手动复位主控芯片
- 自带 8A 自恢复保险丝提供短路保护
- 使用 XT30PW-M 端子提供大电流通过能力以及输入防反接功能
- 使用大功率 MOS 管提供峰值 20A 驱动能力
- 提供 6 个 M4 同款安装孔，便于安装与固定
- 提供 RJ25 接口与排针，实现安全而灵活、多变的连接方式
- 连接主控时可选择红色或白色接口
- 可实现位置、速度、方向的精确控制
- 可同时接入两路 36 编码电机或大功率直流电机

### 技术规格

- 电机通道：2
- 工作电压：6V-12V DC
- 持续输出电流：8A
- 控制器电压：5V
- 主控芯片：ATmega328P
- 工作温度：-40-85℃
- 控制方式：I²C、数字I/O、模拟输入
- 器件地址设定方式：拨码开关手动设定
- 尺寸：100x47x18mm（长x宽x高）

### 使用指导

#### 1、拨码开关状态
使用时需要将拨码开关状态拨到相应位置。
 
<img src="images/me-hp-encoder-dc-motor-driver-1.png" width="352" style="padding:5px 5px 12px 0px;">

#### 2、mBlock 相关编程积木

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k" width="50%;">名称</th>
    <th class="tg-7g6k" width="50%;">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5" width="50%;"><img src="images/me-hp-encoder-dc-motor-driver-2.png" width="300"></td>
    <td class="tg-c6of" width="50%;">用于控制编码或直流电机，模块的拨码开关状态需要与程序对应，动力范围为0~100</td>
  </tr>
  <tr>
    <td class="tg-3xi5" width="50%;"><img src="images/me-hp-encoder-dc-motor-driver-3.png" width="300"></td>
    <td class="tg-c6of" width="50%;">用于控制编码或直流电机，模块的拨码开关状态需要与程序对应，速度范围为0~240</td>
  </tr>
  <tr>
    <td class="tg-3xi5" width="50%;"><img src="images/me-hp-encoder-dc-motor-driver-4.png" width="300"></td>
    <td class="tg-c6of" width="50%;">用于控制编码电机，模块的拨码开关状态需要与程序对应，速度范围为0~240</td>
  </tr>
</table>
<br>

##### 编程示例

<img src="images/me-hp-encoder-dc-motor-driver-5.png" width="500">

#### 3、慧编程相关积木

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k" width="50%;">名称</th>
    <th class="tg-7g6k" width="50%;">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5" width="50%;"><img src="images/me-hp-encoder-dc-motor-driver-6.png" width="300"></td>
    <td class="tg-c6of" width="50%;">用于控制编码或直流电机，模块的拨码开关状态需要与程序对应，动力范围为0~100</td>
  </tr>
  <tr>
    <td class="tg-3xi5" width="50%;"><img src="images/me-hp-encoder-dc-motor-driver-7.png" width="300"></td>
    <td class="tg-c6of" width="50%;">用于控制编码或直流电机，模块的拨码开关状态需要与程序对应，速度范围为0~240</td>
  </tr>
  <tr>
    <td class="tg-3xi5" width="50%;"><img src="images/me-hp-encoder-dc-motor-driver-8.png" width="300"></td>
    <td class="tg-c6of" width="50%;">用于控制编码电机，模块的拨码开关状态需要与程序对应，速度范围为0~240</td>
  </tr>
  <tr>
    <td class="tg-3xi5" width="50%;"><img src="images/me-hp-encoder-dc-motor-driver-9.png" width="300"></td>
    <td class="tg-c6of" width="50%;">反馈电机实时转速</td>
  </tr>
  <tr>
    <td class="tg-3xi5" width="50%;"><img src="images/me-hp-encoder-dc-motor-driver-10.png" width="300"></td>
    <td class="tg-c6of" width="50%;">反馈电机实时角度</td>
  </tr>
</table>
<br>

##### 编程示例

参见上文中的 mBlock 编程示例。