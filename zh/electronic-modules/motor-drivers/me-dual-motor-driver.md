# 双电机驱动模块

<img src="../../../en/electronic-modules/motor-drivers/images/me-dual-motor-driver_微信截图_20160203153331.png" alt="微信截图_20160203153331" width="352" style="padding:5px 5px 12px 0px;">

### 概述

双直流电机驱动模块通过板载RJ25端口，可以在恒定电流下驱动两个直流电机。该电机驱动模块所用 IC 是一种高效的、低散热的 MOSFET，并且含有过流保护功能。本模块接口是红色色标，代表输入电压值是6到12V，需要连接到主板上带有红色标识接口。

### 技术规格

- 工作电压：6-12V DC  
- 单通道持续输出电流：1 A  
- 单通道峰值输出电流：2 A  
- 电机通道：2  
- 电机类型: 直流电机  
- 模块尺寸：56 x 32 x 18 mm (长x宽x高)

### 功能特性

- 模块的白色区域是与金属梁接触的参考区域；  
- 带有高效 MOSFET 基于 H 桥的电机驱动模块 IC；  
- 每个电机最大 1A 持续电流（峰值2A）；  
- 过电流保护；  
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程；  
- 支持 mBlock 图形化编程，适合全年龄用户；  
- 使用RJ25接口连线方便；  
- 配有V-M、PWM、DIR、GND接头支持绝大多数 Arduino 系列主控板。

### 引脚定义

双电机驱动模块有四个针脚的接头,每个针脚的功能如下表：

<table cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 25px 0px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">PWM</td>
<td style="border: 1px solid black;">脉冲宽度调制</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">DIR</td>
<td style="border: 1px solid black;">方向控制</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">地线</td>
</tr>

<tr>
<td style="border: 1px solid black;">4</td>
<td style="border: 1px solid black;">V-M</td>
<td style="border: 1px solid black;">电机电源6~12V</td>
</tr>

</table>

<br>

### 接线方式

● **RJ25连接**  

由于双电机驱动模块接口是红色色标，当使用RJ25接口时，需要连接到主控板上带有红色色标的接口。以 Makeblock Orion 为例，可以连接到1，2号接口，如图

<img src="../../../en/electronic-modules/motor-drivers/images/me-dual-motor-driver_微信截图_20160203153627.png" alt="微信截图_20160203153627" width="357" style="padding:5px 5px 12px 0px;">

> 图 1 双电机驱动 模块与 Makeblock Orion连接

● **杜邦线连接**

当使用杜邦线连接到 Arduino Uno 主板时，模块 PWM 与 DIR 引脚需要连接到DIGITAL（数字）口，如下图所示：

<img src="../../../en/electronic-modules/motor-drivers/images/me-dual-motor-driver_微信截图_20160203153657.png" alt="微信截图_20160203153657" width="352" style="padding:5px 5px 12px 0px;">

> 图 2 双电机驱动模块 与 Arduino UNO 连接图

> 注：接杜邦线时，模块上需要焊接排针。

### 编程指南

● **Arduino编程** 

如果使用Arduino编程，需要调用库 ``Makeblock-Library-master``
来控制双电机驱动模块。  

本程序通过Arduino编程让两直流电机同时做同向与反向转动。

<img src="images/me-dual-motor-driver_1241445345.jpg" alt="微信截图_20160203153738" width="742" style="padding:5px 5px 12px 0px;">

● **mBlock编程**  

双电机驱动模块支持mBlock编程环境，如下是该模块指令简介：

<img src="images/me-dual-motor-driver_1231231243.jpg" alt="微信截图_20160203153827" width="721" style="padding:5px 5px 12px 0px;">

下面是使两直流电机向不同方向转动，循环往复效果：

<img src="images/me-dual-motor-driver_101126tlvjik4hlylquvkj.png" alt="微信截图_20160203153849" width="723" style="padding:5px 5px 12px 0px;">

### 原理解析

直流电机驱动原理：

直流电机加驱动电路目的是为了提供足够大的电流，H桥驱动电路是为了控制直流电机而设计的一种常见电路，它主要实现直流电机的正反向驱动，其形状类似于字母H，4个开关（MOSFET）所在位置称为桥臂，而作为负载的直流电机是像桥一样架在上面，故称之为H桥驱动。借助4个开关不同的开合状态，可以产生电机的4个工作状态：正转、反转、刹车与惰行。

### 原理图

<img src="../../../en/electronic-modules/motor-drivers/images/me-dual-motor-driver_Me-Dual-Motor-Driver-1.png" alt="Me Dual Motor Driver" width="1499" style="padding:5px 5px 12px 0px;">
