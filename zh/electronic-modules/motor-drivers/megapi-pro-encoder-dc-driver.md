# MegaPi Pro 编码/直流电机驱动模块

<img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver.jpg" width="300">

### 概 述

本模块为 MegaPi Pro 专用模块，可以驱动 2 个直流电机或 1 个编码电机，采用 2x8Pin 接插方式，可以方便安装在 MegaPi Pro 上。MegaPi Pro 主控板驱动电机必须安装此模块，MegaPi Pro 最多可以安装四个此模块用来驱动 4 个编码电机（或 8 个直流电机或 8 个电磁阀）其编码口可插 25 光电编码电机、180 光电编码电机，其驱动的 MegaPi Pro 电机口可插 25 直流电机、37 直流电机、电磁阀。

### 技术规格

* 电机驱动：MP80495
* 电机通道：2
* 最低工作电压：6V
* 最高工作电压：12V
* 逻辑电压：5V
* 各通道额定工作电流：3A
* 峰值工作电流：5.5A
* 模块尺寸：30mmx15mm（长x宽）


### 功能特性

* 支持工作电压为6~12V的电机
* 12V电源供电时，工作电流可达3A 
* 模块拥有过压保护、过流保护、过温保护，全方位保证使用安全
* 可以驱动 1 个编码电机（白色接口）或者 2 个直流电机（绿色接口）
* 彩色公母插座，防止插错
* 编码电机驱动端有反响电动势保护（TVS保护）
* 模块体积小巧，便于换取

### 编程指南

#### 直流电机编程

● **Arduino编程**

如果使用Arduino编程，需要调用库 `Makeblock-Library-master` 来控制电机。

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">MeDCMotor （uint8_ port）</td>
<td style="border: 1px solid black;">选择接口</td>
</tr>

<tr>
<td style="border: 1px solid black;">run(int16_t speed)</td>
<td style="border: 1px solid black;">设定转速（–255~+255）</td>
</tr>

<tr>
<td style="border: 1px solid black;">stop()</td>
<td style="border: 1px solid black;">停止电机</td>
</tr>
</table>

**编程示例**

以下程序运行后四路电路模块接口 1 的直流电机以全速旋转 1 秒，停止 1 秒，循环运动。
```
#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>

#include <MeMegaPiPro.h>
MeDCMotor dcfourmotor_1(1);

void loop(){
    dcfourmotor_1.run(255);
    delay(1);
    dcfourmotor_1.run(0);
    delay(1);
    loop();
}
```
<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-encoder-dc-driver/DC_Motor-37_12V50RPM.ino" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **mBlock编程** 

**积木块说明**
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-1.png" width="200"></td>
    <td class="tg-c6of">选定接口（M9~M10）;设置速度（–255~+255）。
</td>
  </tr>
</table>

以下程序运行后四路直流电机驱动接口 1 电机以全速转动1秒，停止 1 秒，循环往复。

<img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-2.png" width="300">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-encoder-dc-driver/DC Motor-37 12V50RPM.sb2" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **慧编程编程** 

**积木块说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-3.png" width="200"></td>
    <td class="tg-c6of">选定接口（M9~M12）；设定参数（–100~+100）。</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-4.png" width="200"></td>
    <td class="tg-c6of">选定接口（M9~M12）；设定参数（通电/断电）。</td>
  </tr>
</table>

以下程序运行后直流电机 M9 以 50% 动力运行，电磁阀 M10 为通电，1 秒后直流电机 M9 停止转动，电磁阀 M10 断电，重复此过程。

<img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-5.png" width="300">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-encoder-dc-driver/DC Motor-37 12V50RPM.mblock" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **Python3 编程** 

1、主控板 MegaPi Pro 和树莓派连接。<br>
2、树莓派安装最新的 makeblock 库 `pip3 install makeblock --upgrade`。<br>
3、新建 Python 文件，后缀为 .py。<br>
4、在 Python 文件里写入程序。<br>
5、运行 Python文件，如 “python123.py ”。

**语句说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">函数</th>
    <th class="tg-7g6k">功能</th>
  </tr>
  <tr>
    <td class="tg-3xi5">DCMotor(port)</td>
    <td class="tg-c6of">创建直流电机对象。<br>port: MegaPiPro.M9 ~ MegaPiPro.M12</td>
  </tr>
  <tr>
    <td class="tg-3xi5">run(speed)</td>
    <td class="tg-c6of">以百分比速度旋转。<br>speed：速度百分比，范围：–100~+100</td>
  </tr>
</table>

以下程序运行后 M9 直流电机将以 50% 的动力运行 2 秒，停止运动 1秒，以 –50% 的动力运行 2 秒，停止 1 秒，循环运动。

```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
motor = board.DCMotor(MegaPiPro.M9)
while True:
    motor.run(50)
    sleep(2)
    motor.run(0)
    sleep(1)
    motor.run(-50)
    sleep(2)
    motor.run(0)
    sleep(1)
```
<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-encoder-dc-driver/DCMotor.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

#### 编码电机编程

● **Arduino编程**

如果使用Arduino编程，需要调用库 `Makeblock-Library-master` 来控制电机。

以下程序运行后编码电机 1 和 2 将以 100 速度运行 1 秒，停止 1 秒，重复此过程。
```
#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>

#include <MeMegaPiPro.h>

double angle_rad = PI/180.0;
double angle_deg = 180.0/PI;
MeEncoderOnBoard Encoder_1(SLOT1);
MeEncoderOnBoard Encoder_2(SLOT2);
void isr_process_encoder1(void)
{
    if(digitalRead(Encoder_1.getPortB()) == 0){
        Encoder_1.pulsePosMinus();
    }else{
        Encoder_1.pulsePosPlus();
    }
}
void isr_process_encoder2(void)
{
    if(digitalRead(Encoder_2.getPortB()) == 0){
        Encoder_2.pulsePosMinus();
    }else{
        Encoder_2.pulsePosPlus();
    }
}

void setup(){
    TCCR1A = _BV(WGM10);//PIN12
    TCCR1B = _BV(CS11) | _BV(CS10) | _BV(WGM12);
    TCCR2A = _BV(WGM21) | _BV(WGM20);//PIN8
    TCCR2B = _BV(CS22);
    attachInterrupt(Encoder_1.getIntNum(), isr_process_encoder1, RISING);
    attachInterrupt(Encoder_2.getIntNum(), isr_process_encoder2, RISING);
}

void loop(){
    Encoder_1.setTarPWM(100);
    Encoder_2.setTarPWM(100);
    _delay(1);
    Encoder_1.setTarPWM(0);
    Encoder_2.setTarPWM(0);
    _delay(1);
    _loop();
}
```
<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-encoder-dc-driver/180_Encoder_Motor_V1.1.ino" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **mBlock编程** 

**积木块说明**
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-6.png" width="200"></td>
    <td class="tg-c6of">选择接口；设置动力（–255~+255）。</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-7.png" width="200"></td>
    <td class="tg-c6of">选择接口；设置速度（最大速度由电机型号决定）。</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-8.png" width="200"></td>
    <td class="tg-c6of">选择接口；设置旋转角度（非负整数）；设置速度（最大速度由电机型号决定）。</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-9.png" width="200"></td>
    <td class="tg-c6of">获取当前电机速度。</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-10.png" width="200"></td>
    <td class="tg-c6of">获取电机当前相对于零点的角度位置。</td>
  </tr>
</table>

以下程序运行后编码电机 1 和 2 将以 100 速度运行 1 秒，停止 1 秒，重复此过程。

<img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-11.png" width="300">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-encoder-dc-driver/180 Encoder Motor V1.1.sb2" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **慧编程编程** 

**积木块说明（以MegaPi Pro主控为例）**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-12.png" width="200"></td>
    <td class="tg-c6of">选择接口；设置动力（–255~+255）。</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-13.png" width="200"></td>
    <td class="tg-c6of">选择接口；设置速度（最大速度由电机型号决定）。</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-14.png" width="200"></td>
    <td class="tg-c6of">选择接口；设置旋转角度（非负整数）；设置速度（最大速度由电机型号决定）。</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-15.png" width="200"></td>
    <td class="tg-c6of">获取当前电机速度。</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-16.png" width="200"></td>
    <td class="tg-c6of">获取电机当前相对于零点的角度位置。</td>
  </tr>
</table>

以下程序运行后编码电机 1 和 2 将以 100 速度运行 1 秒，停止 1 秒，重复此过程。

<img src="images/megapi-pro-encoder-dc-driver/megapi-pro-encoder-dc-driver-17.png" width="300">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-encoder-dc-driver/180 Encoder Motor V1.1.mblock" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **Python3 编程** 

1、主控板 MegaPi Pro 和树莓派连接，180 光电编码电机和直流/编码驱动模块相连接。<br>
2、树莓派安装最新的 makeblock 库 `pip3 install makeblock --upgrade`。<br>
3、新建 Python 文件，后缀为 .py。<br>
4、在 Python 文件里写入程序。<br>
5、运行 Python文件，如 “python123.py ”。

**语句说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">函数</th>
    <th class="tg-7g6k">功能</th>
  </tr>
  <tr>
    <td class="tg-3xi5">EncoderMotor(port)</td>
    <td class="tg-c6of">创建直流编码电机对象。<br>port: MegaPiPro.PORT1~MegaPiPro.PORT4</td>
  </tr>
  <tr>
    <td class="tg-3xi5">run(speed)</td>
    <td class="tg-c6of">以指定速度旋转。<br>speed：转速（范围为–180~+180）</td>
  </tr>
  <tr>
    <td class="tg-3xi5">move_to(position,speed,callback)</td>
    <td class="tg-c6of">以指定速度旋转到指定位置。<br>position：目标位置；speed：转速（范围为–180~+180）；callback：达到目标位置时触发回调</td>
  </tr>
  <tr>
    <td class="tg-3xi5">set_home()</td>
    <td class="tg-c6of">设置当前位置为原点。</td>
  </tr>
</table>

**程序示例 1**

以下程序运行后接在 MegaPi Pro 直流/编码电机驱动接口 1 的编码电机以 50 速度旋转 2 秒，停止 1 秒，以 –50 速度旋转 2 秒，停止 1 秒，重复此过程。

```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
encoder = board.EncoderMotor(MegaPiPro.PORT1)
while True:
    encoder.run(50)
    sleep(2)
    encoder.run(0)
    sleep(1)
    encoder.run(-50)
    sleep(2)
    encoder.run(0)
    sleep(1)
```
<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-encoder-dc-driver/EncoderMotor_1.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

**程序示例 2**

以下程序运行后 MegaPi Pro 直流/编码电机驱动接口 1 连接的编码电机旋转到目标位置。
```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
encoder = board.EncoderMotor(MegaPiPro.PORT1)
position = 0
def on_finished(value):
    position = 5000 - position
    encoder.move_to(position,100,on_finished)

on_finished(position)
```
<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-encoder-dc-driver/EncoderMotor_2.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>


### 连接方式

<img src="images/megapi-pro-encoder-dc-driver-2.png" width="400">