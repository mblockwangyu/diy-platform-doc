# 气体传感器

<img src="../../../en/electronic-modules/sensors/images/me-gas-sensormq2_微信截图_20160129122027.png" alt="微信截图_20160129122027" width="222" style="padding:5px 5px 12px 0px;">

### 概述

气体传感器包括具有重复性好，长期稳定性，响应时间短，和耐用性能的工作类型MQ2的烟雾传感器。它通常被用作在家庭和工厂的气体泄漏监测装置，并适用于检测液化天然气（LNG），丁烷，丙烷，甲烷，乙醇，氢气，烟雾等其黑色ID意味着它有一个模拟端口并需要被连接到与上Makeblock Orion黑色ID的端口。

### 技术规格

- 工作电压：5.0V±0.1V的  
- 加热电压：5.0V±0.1V的  
- 加热电阻：33Ω±5％（在室温下）  
- 加热功率：&lt;800MW - 预热时间：&gt; 24小时  
- 检测范围：100-为10000ppm  
- 工作温度：20±2℃（在标准条件）  
- 工作温度：-20℃-50℃  
- 相对湿度：&lt;95％RH - 氧浓度：21％（在标准状态） - 模块尺寸：51 X
24×18毫米（长x宽x高）

### 功能特性

- 该模块的白色区域是参考区以接触金属梁  
- 提供可调电阻来调整灵敏度  
- 预热一段时间在使用之前保持温暖预热后  
- 蓝色指示灯亮时的可燃性气体的一定量的积聚在和检测  
- 提供用于数字和模拟信号输出端口  
- 强稳定性和快速的检测  
- 支持 MBLOCK GUI 编程，并适用于所有年龄段的用户  
- 采用RJ25端口，便于连接  
- 提供销型端口，以支持大多数开发板包括 Arduino 的系列

### 连线方式

● **RJ25连接**  

因为气体传感器的端口有黑色的ID，您需要使用RJ25端口时，用黑色标识的端口连接Makeblock Orion。以Makeblock Orion作为例子，可以连接到端口号6，图7，和图8，如下所示:

<img src="../../../en/electronic-modules/sensors/images/me-gas-sensormq2_微信截图_20160129122204.png" alt="微信截图_20160129122204" width="297" style="padding:5px 5px 12px 0px;">

● **杜邦线连接** 

当杜邦导线被用于将模块到 Arduino UNO 底板连接，其DO引脚应连接到数字端口如下:

<img src="../../../en/electronic-modules/sensors/images/me-gas-sensormq2_微信截图_20160129122244.png" alt="微信截图_20160129122244" width="331" style="padding:5px 5px 12px 0px;">

### 引脚定义

气体传感器的端口有三个引脚，它们的功能如下：

<img src="../../../en/electronic-modules/sensors/images/me-gas-sensormq2_微信截图_20160129122322.png" alt="微信截图_20160129122322" width="328" style="padding:5px 5px 12px 0px;">

### 编程指导

● **Arduino编程** 

如果使用的Arduino编写一个程序，库Makeblock库主应调用来控制气体传感器。这个程序读取通过Arduino的编程传感器的数字和模拟输出值，并将其输出到串行端口以用于显示。

<img src="../../../en/electronic-modules/sensors/images/me-gas-sensormq2_微信截图_20160129122416.png" alt="微信截图_20160129122416" width="456" style="padding:5px 5px 12px 0px;">

<img src="../../../en/electronic-modules/sensors/images/me-gas-sensormq2_微信截图_20160129122530.png" alt="微信截图_20160129122530" width="723" style="padding:5px 5px 12px 0px;">

代码段的作用是：  
如果烟雾被气体传感器检测到时，输出为“燃气”;
如果没有检测到烟雾，输出为“无气”。然后，检测的烟的模拟值被输出，并且结果显示在Arduino的IDE串行监视器上。

上传代码段的Makeblock主板，然后单击Arduino的串口监视器，你会看到的结果如下：

<img src="../../../en/electronic-modules/sensors/images/me-gas-sensormq2_微信截图_20160129122614.png" alt="微信截图_20160129122614" width="620" style="padding:5px 5px 12px 0px;">

### 原理分析

气体传感器具有可调节的电阻器来调节传感器的用于烟雾灵敏度。烟雾浓度从传感器和烟雾源之间的距离而变化。在相同环境中，越接近的距离，烟浓度越大;
距离越远，烟浓度越低。因此，有必要设置对应于相应的烟雾浓度阈值的电压值。

检测原理：MQ-2型烟雾传感器的采用氧化锡作为其表面的离子的N型半导体的半导体气体敏感材料。当温度在200〜300℃的范围内，氧化锡吸附在空气中的氧气，以形成到负氧离子，使半导体的电子密度降低，其电阻增大。当与烟接触时，如果在晶界中的势能垒由烟雾调制，其变化将导致电导的变化。这可以用来烟雾是否存在获得的信息。烟雾浓度越大，越大电导，并且下输出电阻。传感器的体积电阻的降低将导致其输出电压到地增加。输出电压，并通过使用所述模块上的比较器的阈值电压进行比较，检测的烟的浓度可以被识别。

### 原理图

<img src="../../../en/electronic-modules/sensors/images/me-gas-sensormq2_ME-GAS-SENORMQ2.png" alt="ME GAS SENOR(MQ2)" width="1179" style="padding:5px 5px 12px 0px;">
