# 指南针模块


![](../../../en/electronic-modules/sensors/images/me-compass_Me-Compass.jpg)

<img src="../../../en/electronic-modules/sensors/images/me-compass_微信截图_20160129141724.png" alt="微信截图_20160129141724" width="233" style="padding:5px 5px 12px 0px;">

### 概述
  
- 工作电压：5V；  
- 通信接口：I2C通信（I2C地址固定为0x1E）；  
- 罗盘精度：理想条件下精确到1°\~2°但实际使用环境部分角度在10°~15°； 
- 磁场测量范围：-8高斯\~ +8高斯（精度为：2毫高斯）；  
- 极限性能：反接到主控板的电机端口时，不会导致模块和主控板损坏  
- 校准功能：模块上具有用于校准的按键和指示灯，当模块周围的机械结构或模块的安装位置（方向）发生改变时，用户可以通过下载我们的程序库并简单操作按键就可校准，以使得模块在新的环境下能准确测量出角度值，使本模块简单易用。

### 硬件介绍

- 将指南针模块接入控制板任意的I2C接口（电机口除外），红色指示灯亮起说明电源供电正常。  
- DIY孔用户可以自己焊接排针用于扩展。  
- 校正按键和校正指示灯用来指示校准。  
- 4个4MM安装孔可以固定在 Makeblock 机械套件上。

### 引脚定义

指南针模块有六根引脚，它们的功能如下:

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">地线</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">电源线</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">SDA</td>
<td style="border: 1px solid black;">I2C数据口</td>
</tr>

<tr>
<td style="border: 1px solid black;">4</td>
<td style="border: 1px solid black;">SCL</td>
<td style="border: 1px solid black;">I2C时钟口</td>
</tr>

<tr>
<td style="border: 1px solid black;">5</td>
<td style="border: 1px solid black;">RDY</td>
<td style="border: 1px solid black;">决定数据收集与否 </td>
</tr>

<tr>
<td style="border: 1px solid black;">6</td>
<td style="border: 1px solid black;">KEY</td>
<td style="border: 1px solid black;">决定是否按了校准键</td>
</tr>

</table>

<br>


### 连线方式

● **RJ25连接** 

模块需要连接到带有白色标签的端口上，模块可以用RJ25线连接到Orion板，比如连到3, 4, 6, 7,和 8口：

<img src="../../../en/electronic-modules/sensors/images/me-compass_微信截图_20160129141947.png" alt="微信截图_20160129141947" width="336" style="padding:5px 5px 12px 0px;">

● **杜邦线连接** 

当用杜邦线连到 Arduino UNO 板时 , 它的 SCL 和 SDA 引脚应该连到I2C端口，即A5和A4口如下:  

<img src="../../../en/electronic-modules/sensors/images/me-compass_微信截图_20160129142017.png" alt="微信截图_20160129142017" width="362" style="padding:5px 5px 12px 0px;">

### 编程指导

● **Arduino 编程**  

如果使用 Arduino 写一个程序，``Makeblock-Library-master`` 库可调用来控制指南针模块。

这个例程读取指南针检测到的角度值，并通过 Arduino 编程将结果输出到串行监视器。

将代码段上传到 Makeblock Orion，点击 Arduino 串口监视器，就可以看到运行结果。  

<img src="../../../en/electronic-modules/sensors/images/me-compass_微信截图_20160129142055.png" alt="微信截图_20160129142055" width="457" style="padding:5px 5px 12px 0px;">

<img src="../../../en/electronic-modules/sensors/images/me-compass_微信截图_20160129142120.png" alt="微信截图_20160129142120" width="745" style="padding:5px 5px 12px 0px;"> 

**库函数** 

```
void init(void); //指南针初始化函数  
bool testConnection(void); //模块连接检测函数  
double getAngle(void); //获取角度测量值  
int16\_t getHeadingX(void); //获取X轴磁场测量值  
int16\_t getHeadingY(void); //获取Y轴磁场测量值  
int16\_t getHeadingZ(void); //获取Z轴磁场测量值  
void getHeading(int16\_t \*x, int16\_t \*y, int16\_t \*z);
//同时获取X、Y、Z轴磁场测量值
```

### 注意事项

指南针模块对周围磁场的变化比较敏感。因此，周围的机械结构会发生变化，或者组装方式可能会导致环境的变化磁场，从而指南针模块测量结果的偏差。在这种情况下，用户需要在当前环境下校准模块以获得正确的角度值。

**模块校正流程:**  

1）将指南针模块正确连接到Orion板上，打开电源，然后下载我们提供的任何一个指南针库。  
2）按下模块上的按键，直到模块的蓝灯开始闪烁，然后松开按键。  
3）在蓝光闪烁过程中，将模块（包括与其连接的机械结构）沿水平面平稳旋转360°以上。  
4）旋转后，按模块上的键退出校准。 模块上的蓝灯将亮起。 

**备注:**  

1）指南针的两种模式：测量模式→蓝灯亮起; 校准模式→蓝灯闪烁;  
2）如果在当前情况下已经校准过模块，则在断电再次上电后不需要校准模块  
3）如果您不打算校准模块，请不要按下模块上的按钮，否则会使之前的校准失效  
4）在校准过程中，确保模块（包括与之连接的机械结构）沿水平面旋转超过360°。否则，校准将无法工作。

### 原理分析

传统的指南针使用一个磁化针来感应地球的磁场。磁场和针之间的磁力旋转针，直到针与地球磁场的水平分量（北和南）对齐。这也适用于电子罗盘，除了用磁阻传感器代替针。磁阻传感器将它接收的地磁信号转换成数字信号并输出​​使用。

3轴数字罗盘由3轴磁阻传感器，2轴倾斜传感器和MCU组成。三轴磁阻传感器用于测量磁场;倾斜传感器用于在磁力计不处于水平状态时进行补偿;
MCU负责分析磁力仪和倾斜传感器输出的信号，输出数据，补偿软铁和硬铁。三个互相垂直的磁阻传感器检测地磁场各自轴向的强度。模拟输出将被放大并传送到MCU。

### 原理图

<img src="../../../en/electronic-modules/sensors/images/me-compass_Me-compass.png" alt="Me compass" width="1440" style="padding:5px 5px 12px 0px;">
