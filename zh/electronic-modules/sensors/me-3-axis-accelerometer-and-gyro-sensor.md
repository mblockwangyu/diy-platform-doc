# 陀螺仪

<img src="../../../en/electronic-modules/sensors/images/me-3-axis-accelerometer-and-gyro-sensor_微信截图_20160203143634.png" alt="微信截图_20160203143634" width="262" style="padding:5px 5px 12px 0px;">

### 概述

陀螺仪是一款理想的机器人运动检测、姿态检测模块。包含3轴加速度计、3轴角速度传感器与运动处理器，并提供I2C接口通信。可以应用在自平衡小车、四轴飞行器、机器人及移动设备上，具有高动态测量范围与低电流消耗等优点。本模块接口是白色色标，说明是I2C通信模式，需要连接到主板上带有白色标识接口。

### 技术规格

-   工作电压：5V DC
-   工作温度：0到70℃
-   信号模式：I2C通信
-   模块尺寸：51 x 24 x 18 mm (长x宽x高)

### 功能特性

-   模块的白色区域是与金属梁接触的参考区域；
-    数字输出6轴或9轴的旋转矩阵、四元数、欧拉角格式的融合演算数据；
-   3轴角速度可控测量范围为±250，±500，±1000，±2000°/秒（dps）；
-   3轴加速计可控测量范围为±2g，±4g，±8g与±16g；
-   数字运动处理(DMP)引擎可减少复杂的运动融合、传感器同步与姿态检测的负荷；
-   移除加速计与陀螺仪轴间敏感度，降低设定给予的影响与传感器的漂移；
-   内嵌运作时间偏差与磁力传感器校正演算技术；
-   具有反接保护，电源反接不会损坏IC；
-   支持Arduino IDE编程, 并且提供运行库来简化编程；
-   支持mBlock图形化编程，适合全年龄用户；
-    使用RJ25接口连线方便；
-   模块化安装，兼容乐高系列。

### 引脚定义

陀螺仪模块有四个针脚的接头，每个针脚的功能如下表：

<img src="images/me-3-axis-accelerometer-and-gyro-sensor_TB2dB8tlFXXXXb1XpXXXXXXXXXX_2586845279-1.png" alt="tb2db8tlfxxxxb1xpxxxxxxxxxx_2586845279" width="400" style="padding:5px 5px 12px 0px;">

### 接线方式

● **RJ25连接**

由于陀螺仪模块接口是白色色标，当使用RJ25接口时，需要连接到主控板上带有白色色标的接口。以 Makeblock Orion 为例，可以连接到3，4，6，7，8号接口，如图

<img src="../../../en/electronic-modules/sensors/images/me-3-axis-accelerometer-and-gyro-sensor_微信截图_20160203144150.png" alt="微信截图_20160203144150" width="332" style="padding:5px 5px 12px 0px;">

● **杜邦线连接** 

当使用杜邦线连接到 Arduino Uno 主板的时候，模块SCL、SDA引脚需要连接到I2C接口，即连接到A4、A5接口如下图所示：

<img src="../../../en/electronic-modules/sensors/images/me-3-axis-accelerometer-and-gyro-sensor_微信截图_20160203144159.png" alt="微信截图_20160203144159" width="340" style="padding:5px 5px 12px 0px;">

### **编程指南**

● **Arduino编程**

如果使用Arduino编程，需要调用库[`Makeblock-Library-master`](https://github.com/Makeblock-official/Makeblock-Libraries/tree/master/examples/Me_Gyro/MeGyroTest)
来控制陀螺仪模块

本程序通过Arduino编程，当我们通过串口监视器输入任意值时，模块接收并返回当前的X，Y，Z的值。从而能够判断当前模块的姿态，从串口监视器可观察到X,，Y，Z的值。

<img src="../../../en/electronic-modules/sensors/images/me-3-axis-accelerometer-and-gyro-sensor_微信截图_20160203144211.png" alt="微信截图_20160203144211" width="433" style="padding:5px 5px 12px 0px;">

**陀螺仪函数功能列表**

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">double angleX()</td>
<td style="border: 1px solid black;">读取X轴的值</td>
</tr>

<tr>
<td style="border: 1px solid black;">double angleY()</td>
<td style="border: 1px solid black;">读取Y轴的值</td>
</tr>

<tr>
<td style="border: 1px solid black;">double angleZ()</td>
<td style="border: 1px solid black;">读取Z轴的值</td>
</tr>

<tr>
<td style="border: 1px solid black;">void update()</td>
<td style="border: 1px solid black;">刷新数值  </td>
</tr>

<tr>
<td style="border: 1px solid black;">void begin()</td>
<td style="border: 1px solid black;">模块初始化 </td>
</tr>

</table>

<br>

<img src="../../../en/electronic-modules/sensors/images/me-3-axis-accelerometer-and-gyro-sensor_微信截图_20160203144541.png" alt="微信截图_20160203144541" width="572" style="padding:5px 5px 10px 0px;">

● **mBlock编程**

陀螺仪模块支持mBlock编程环境，如下是该模块指令简介：

<table cellpadding="10px;" cellspacing="14px;">
<colgroup>
<col style="width: 50%" style="padding:5px 5px 12px 0px;">
<col style="width: 50%" style="padding:5px 5px 12px 0px;">
</colgroup>
<tbody>
<tr >
<td style="border: 1px solid black;">编程说明</td>
<td style="border: 1px solid black;">功能</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/me-3-axis-accelerometer-and-gyro-sensor_170528awpw3yzh3obzpvwl.png" id="aimg_2950"  style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">参数一：选择坐标轴</td>
</tr>
</tbody>
</table>

<br>
