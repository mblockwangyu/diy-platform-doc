# 温度传感器

<img src="../../../en/electronic-modules/sensors/images/temperature-sensor-waterproofds18b20_微信截图_20160129140754.png" alt="微信截图_20160129140754" width="412" style="padding:5px 5px 12px 0px;">

### 概述

温度传感器是含有 DS18B20 探测器的金属管温度计，抗干扰能力强，精度高且外部有橡胶管能防水。测量温度范围:-55°C + 125°C，可以将温度计连接到RJ25适配器模块，后将R25适配器连接到 Makeblock Orion 主控板进行温度的测量。

### 技术规格

- 工作电压：5V DC  
- 传感器型号：DS18B20  
- 温度范围:-55°C 到125°C  
- 控制方式：单总线接口

### 功能特性

- 9位～12位A/D转换精度；  
- 高精度:±0.5°C(在-10°C + 85°C范围内)；  
- 探头直径6毫米, 长大约50毫米。 总长度(包括线)是1米；  
- 温度转换延时时间小，最大750ms；  
- 支持多点组网功能；  
- 支持Arduino IDE编程, 并且提供运行库来简化编程；  
- 支持mBlock图形化编程，适合全年龄用户。

### 引脚定义

温度传感器模块有三个针脚的接头,每个针脚的功能如下表:

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">颜色</th>
<th style="border: 1px solid black;">功能</th>
</tr>

</td>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">黑</td>
<td style="border: 1px solid black;">地线</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">红</td>
<td style="border: 1px solid black;">接电源</td>
</tr>

<tr>
<td style="border: 1px solid black;">3 </td>
<td style="border: 1px solid black;">SIG</td>
<td style="border: 1px solid black;">黄</td>
<td style="border: 1px solid black;">温度信号输出</td>
</tr>
</table>


### 接线方式

● **RJ25连接** 

如果想通过RJ25接口连接温度计，请将温度计链接到RJ25适配器模块，然后将此模块连接到 Makeblock Orion 主控板，如图：

<img src="../../../en/electronic-modules/sensors/images/temperature-sensor-waterproofds18b20_微信截图_20160129141125.png" alt="微信截图_20160129141125" width="387" style="padding:5px 5px 12px 0px;">

● **杜邦线连接** 

当使用杜邦线连接到 Arduino Uno 主板时，模块SIG引脚需要连接到DIGITAL（数字）引脚，如下图所示：

<img src="../../../en/electronic-modules/sensors/images/temperature-sensor-waterproofds18b20_微信截图_20160129141205.png" alt="微信截图_20160129141205" width="391" style="padding:5px 5px 12px 0px;">

### 编程指导

● **Arduino编程**  

如果使用 Arduino 编程，需要调用库 ``Makeblock-Library-master`` 来控制温度传感器。 

本程序通过Arduino编程读取当前温度值。

<img src="../../../en/electronic-modules/sensors/images/temperature-sensor-waterproofds18b20_微信截图_20160129141241.png" alt="微信截图_20160129141241" width="625" style="padding:5px 5px 12px 0px;">

本代码将读取温度传感器的读数，输出结果到 Arduino IDE 串口监视器, 周期为
1s。

上传代码到 Makeblock 主板点击 Arduino 串口监视器。 您将看到运行结果如下：

<img src="../../../en/electronic-modules/sensors/images/temperature-sensor-waterproofds18b20_微信截图_20160129141323.png" alt="微信截图_20160129141323" width="470" style="padding:5px 5px 12px 0px;">

● **mBlock 编程**  

温度传感器模块支持 mBlock 编程环境，如下是该模块指令简介:

<img src="../../../en/electronic-modules/sensors/images/temperature-sensor-waterproofds18b20_微信截图_20160129141356.png" alt="微信截图_20160129141356" width="787" style="padding:5px 5px 12px 0px;"> 

以下是如何使用 mBlock 控制温度传感器模块的例子：

这个方程将会使小熊猫说出来自温度传感器的温度数据，运行结果如下：

<img src="../../../en/electronic-modules/sensors/images/temperature-sensor-waterproofds18b20_微信截图_20160129141428.png" alt="微信截图_20160129141428" width="788" style="padding:5px 5px 12px 0px;">

### 原理分析

本模块主要元件为DS18B20温度传感器，具有微型化，低功耗，高性能，抗干扰能力强，易配微处理器等优点，独特的单线接口方式，DS18B20在与微处理器连接时仅需要一条总线即可实现微处理器与DS18B20的双向通讯。支持多点组网功能，多个DS18B20可以并联在唯一的三线上，实现多点测温，测量结果以9\~12位数字量方式串行传送。和单片机通信时，可以通过配置寄存器来设置分辨率。

DS18B20中的温度传感器完成对温度的测量，用16位二进制形式提供，其中S为符号位。  

<img src="../../../en/electronic-modules/sensors/images/temperature-sensor-waterproofds18b20_微信截图_20160129141520.png" alt="微信截图_20160129141520" width="610" style="padding:5px 5px 12px 0px;">  

例如：  
- +125℃的数字输出07D0H  
(正温度直接把16进制数转成10进制即得到温度值 )  
- -55℃的数字输出 FC90H  
(负温度把得到的16进制数取反后加1 再转成10进制数)

<img src="../../../en/electronic-modules/sensors/images/temperature-sensor-waterproofds18b20_微信截图_20160129141525.png" alt="微信截图_20160129141525" width="304" style="padding:5px 5px 12px 0px;">
