# RGB巡线模块

<img src="images/rgb-line-follower_20183271632.png" alt="微信截图_20160129150749" width="500" style="padding:5px 5px 12px 0px;">

### 概述

RGB巡线传感器模块专为小车巡线比赛设计。它包含4个RGB补光灯和4个光敏接收管，该模块既可以用于深色背景浅色赛道做巡线，也可以用于浅色背景深色赛道做巡线，只要背景与赛道色差灰度值大于阈值（背景与赛道在RGB域的色差越大，巡线效果越好），RGB巡线传感器模块均能用于巡线。该模块具有检测速度快，通过按键学习场地功能，适应性良好等优点。本模块接口是蓝白色色标，说明是双数字、I2C接口，需要连接到主板上带有蓝白色标示接口。

### 技术规格

- 工作电压：5V DC  
- 工作温度：0℃\~70℃  
- 检测高度：依跑道材质和光照情况不等，建议模块探头距跑道5mm\~15mm间  
- 信号模式：I2C通信（对应蓝色白色色标）  
- 模块尺寸：48 x 72 x 26.5 mm （长x宽x高）

### 功能特性

- 支持Arduino IDE编程, 并且提供运行库来简化编程；  
- 具有四只LED指示灯用于巡线反馈以及新环境学习情况；  
- 使用RJ25接口连线方便；  
- 适配 Makeblock DIY 平台金属件：模块的白色区域是与金属梁接触的参考区域；  
- 模块化安装，兼容乐高系列；  
- 配有接头支持绝大多数 Arduino 系列主控板；  
- 支持场地学习功能：识别并记录所用场地的背景及路径的颜色；  
- 支持eeprom存储：学习后的数据会保留到eeprom，防断电丢失数据；  
- 支持切换RGB补光颜色：目前暂时支持三种颜色切换（红、绿、蓝），长按2秒按键切换RGB颜色；  
- 巡线灵敏度可调；  
- 具有反接保护，电源反接不会损坏IC；  
- 一个主板最多同时支持4路巡线传感器模块。

### 原理分析

RGB巡线传感器主要有4对RGB发射管和光敏接收管，如下图：

<img src="images/rgb-line-follower_11.png" alt="微信截图_20160129150749" width="500" style="padding:5px 5px 12px 0px;">

当RGB经过不同颜色背景时，光敏接收管将接收到不同的光信息转化为电信号，并经过放大器后由模拟口输出具体数值。软件再把4个光敏接收管的模拟值做融合算法，最终计算输出传感器模块偏离赛道的位置偏移量，用户可以直接把该偏移量用于控制左右两个电机转速实现巡线。


### 引脚定义

RGB巡线模块有六个针脚的接头，每个针脚的功能如下表

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">SDA</td>
<td style="border: 1px solid black;">I2C数据接口</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">SCL</td>
<td style="border: 1px solid black;">I2C时钟接口</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">电源线</td>
</tr>

<tr>
<td style="border: 1px solid black;">4</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">接地</td>
</tr>

<tr>
<td style="border: 1px solid black;">5</td>
<td style="border: 1px solid black;">S2</td>
<td style="border: 1px solid black;">I2C地址分配口</td>
</tr>

<tr>
<td style="border: 1px solid black;">6</td>
<td style="border: 1px solid black;">S1</td>
<td style="border: 1px solid black;">I2C地址分配口</td>
</tr>

</table>

<br>

### 连线模式

● **RJ25连接**

由于RGB巡线传感器模块接口是蓝白色标，当使用RJ25接口时，需要连接到主控板上带有蓝白色标的接口。

以 Makeblock Megapri为例，可以连接到5，6，7，8号接口，如下图所示:

<img src="images/rgb-line-follower_22222-400x198.jpg" alt="微信截图_20160129151012" width="650" style="padding:5px 5px 12px 0px;">

● **杜邦线连接** 

当使用杜邦线连接到 Arduino Uno 主板时，模块A0引脚需要连接到ANALOG（模拟）口，如下图所示：

<img src="images/rgb-line-follower_324333333.jpg" alt="微信截图_20160129151050" width="906" style="padding:5px 5px 12px 0px;">

### 学习方法

如果比赛场地、环境、RGB传感器模块安装位置等发生了变化，建议重新学习传感器模块，学习信息会保存在eeprom，防掉电丢失。完整的学习过程包括四个步骤：

1. 调整补光灯颜色：根据场地背景和线的颜色，通过长按按钮选择合适的补光灯颜色。
2. 学习背景颜色：将RGB传感器模块安装在待使用的位置，并使其4个RGB灯都正对场地背景，单击按键，四个led指示灯开始慢闪，2\~3秒后学习完成，led指示灯停止闪烁。
3. 学习赛道颜色：将RGB传感器模块安装在待使用的位置，并使其4个RGB灯都正对赛道，双击按键，四个led指示灯开始慢闪，2\~3秒后学习完成，led指示灯停止闪烁。
4. 测试确认：保持该模块在待使用的位置，调整四个探头的位置以确认其是否能正确检测到背景及线的颜色。检测到背景时探头对应的LED亮起，检测到线时对应的LED熄灭。

### 编程指导

● **Arduino 编程** 

如果使用 Arduino 编程，需要调用库 ``Makeblock-Library-master`` 来控制RGB巡线模块。

本程序通过 Arduino 编程，使用两路增强型180编码电机进行巡线控制，工程文件根目录必须包含 `MeRGBLineFollower` 源文件和 `MeEnhanceEncoderOnBoard` 源文件，如下：

[程序下载](http://download.makeblock.com/MeRGBLineFollower180motorAuriga%2820171212%29.rar) 

<img src="images/rgb-line-follower_44444.png" alt="微信截图_20160129151123" width="419" style="padding:5px 5px 12px 0px;">  
`MeRGBLineFollower180motorAuriga.ino` 是基于 Auriga 主控板、180编码电机做底盘的巡线例子，代码如下：  

<img src="images/rgb-line-follower_5123123.png" alt="微信截图_20160129151123" width="607" style="padding:5px 5px 12px 0px;">

**RGB巡线传感器主要函数功能列表**

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">void setKp(float value)</td>
<td style="border: 1px solid black;">设置巡线灵敏度，灵敏度是用于调节巡线响应速度，值越大巡线时转弯越灵敏</td>
</tr>


<tr>
<td style="border: 1px solid black;">uint8_t getStudyTypes(void)</td>
<td style="border: 1px solid black;">获取RGB传感器学习状态，0-未学习，1-正在学习背景色，2-正在学习赛道色   </td>
</tr>

<tr>
<td style="border: 1px solid black;">int16_t getPositionOffset(void)</td>
<td style="border: 1px solid black;">获取传感器偏离赛道的位置偏移量，输出值为巡线灵敏度Kp与4个传感器融合算法模拟量的乘积，范围-512 \~512，大于0时表示线的位置偏向RGB4一侧，值越大偏离度越高；小于0时表示线的位置偏向RGB1一侧，负值越大偏离越大。<br>
换而言之，当传感器的RGB探头一侧朝向前进方向安装时，偏移量是正数表示传感器偏向线的左侧；偏移量是负数表示传感器偏向线的右侧。

<img src="images/rgb-line-follower_12.png"  width="607" style="padding:5px 5px 12px 0px;">
</td>
</tr>

<tr>
<td style="border: 1px solid black;">uint8_t getPositionState(void)</td>
<td style="border: 1px solid black;">
<img src="images/rgb-line-follower_13.png"  width="607" style="padding:5px 5px 12px 0px;">
如图所示，用bit0~bit3分别代表从左至右四个RGB探头RGB4，RGB3，RGB2，RGB1。 用‘1’代表检测到背景（对应的LED亮起），用‘0’代表检测到线（对应的LED熄灭）。<br><br>
如‘0000’代表四个探头全部检测到线（LED状态为‘灭灭灭灭’），返回值为0；<br><br>
‘0111’代表从左至右四个探头分别检测到‘线’‘背景’‘背景’‘背景’（LED状态为‘灭亮亮亮’），返回值为7；<br><br>
‘0011’代表从左至右四个探头分别检测到‘线’‘线’‘背景’‘背景’（LED状态为‘灭灭亮亮’），返回值为3。
</td>
</tr>

<tr>
<td style="border: 1px solid black;">uint8_t getADCValueRGB1(void)<br><br>
uint8_t getADCValueRGB2(void)<br><br>
uint8_t getADCValueRGB3(void)<br><br>
uint8_t getADCValueRGB4(void)
</td>
<td style="border: 1px solid black;">
获取RGB巡线传感器各通道的ADC值，返回值为0~255
</td>
</tr>

<tr>
<td style="border: 1px solid black;">int8_t setRGBColour(uint8_t colour)
</td>
<td style="border: 1px solid black;">
设置RGB补光灯颜色参数color设置为1--红色、2--绿色、3--蓝色；
</td>
</tr>

</table>

<br>

● **mBlock 3 编程**

RGB巡线传感器模块支持mBlock编程环境，必须安装 `RGBLineFollower` 或者`RGBLineFollowerAdvanced`插件，如果使用180编码电机做底盘巡线则还必须安装 `EnhanceEncoderOnBoard` 增强型编码电机插件。

**RGBLineFollower插件**

`RGBLineFollower`提供基本的功能以方便快速实现巡线应用。

插件安装方法：  
打开mBlock软件 → 扩展 → 扩展管理器→找到 `RGBLineFollower` 和`EnhanceEncoderOnBoard` 插件包 → 下载插件包即可。现在机器人模块会出现插件语句块，如下：  

<img src="images/rgb-line-follower_6.png" alt="微信截图_20160129151218" width="368" style="padding:5px 5px 12px 0px;">  

语句块简介：  
- 接口：Port1 \~Port12  
- 地址：地址1 \~ 地址4（0 \~ 3），任意端口可以和任意地址绑定，但所有语句块里面一个端口只能绑定唯一一个地址

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;width:40%;">语句块</th>
<th style="border: 1px solid black;width:60%;">描述</th>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_7.png" alt="微信截图_20160129151218" width="368" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">设置巡线灵敏度，灵敏度是用于调节巡线响应速度，值越大巡线时转弯越灵敏</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_8.png" alt="微信截图_20160129151218" width="368" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">获取传感器偏离赛道的位置偏移量，输出值为巡线灵敏度Kp与4个传感器融合算法模拟量的乘积，范围-512 \~512，大于0时表示线的位置偏向RGB4一侧，值越大偏离度越高；小于0时表示线的位置偏向RGB1一侧，负值越大偏离越大。<br>
换而言之，当传感器的RGB探头一侧朝向前进方向安装时，偏移量是正数表示传感器偏向线的左侧；偏移量是负数表示传感器偏向线的右侧。

<img src="images/rgb-line-follower_12.png"  width="607" style="padding:5px 5px 12px 0px;">
</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_9.png" alt="微信截图_20160129151218" width="368" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_13.png"  width="607" style="padding:5px 5px 12px 0px;">
如图所示，用bit0\~bit3分别代表从左至右四个RGB探头RGB4，RGB3，RGB2，RGB1。 用‘1’代表检测到背景（对应的LED亮起），用‘0’代表检测到线（对应的LED熄灭）。<br><br>
如‘0000’代表四个探头全部检测到线（LED状态为‘灭灭灭灭’），返回值为0；<br><br>
‘0111’代表从左至右四个探头分别检测到‘线’‘背景’‘背景’‘背景’（LED状态为‘灭亮亮亮’），返回值为7；<br><br>
‘0011’代表从左至右四个探头分别检测到‘线’‘线’‘背景’‘背景’（LED状态为‘灭灭亮亮’），返回值为3。</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_10.png" alt="微信截图_20160129151218" width="368" style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">获取RGB传感器学习状态。输出值：0-未学习，1-正在学习背景色，2-正在学习赛道色 </td>
</tr>
</table>

<br>

基于Auriga主控板、180编码电机做底盘的巡线例子，如下：

<img src="images/rgb-line-follower_201872.png" alt="微信截图_20160129151218" width="588" style="padding:5px 5px 12px 0px;">  

基于mCore主控板、TT马达做底盘的巡线例子，如下：

<img src="images/rgb-line-follower_mbot-rgbline.png" alt="微信截图_20160129151218" width="608" style="padding:5px 5px 12px 0px;">

**RGBLineFollowerAdvanced插件**

RGBLineFollowerAdvanced插件提供了包括传感器探头原始返回值的更加底层的语句块，支持更加灵活的教学、应用场景。
插件安装方法：打开mBlock软件 → 扩展 → 扩展管理器 → 找到`RGBLineFollowerAdvanced`和`EnhanceEncoderOnBoard`插件包 → 下载插件包即可。现在机器人模块会出现插件语句块，如下：

<img src="images/rgb-line-follower_14.png" alt="微信截图_20160129151218" width="608" style="padding:5px 5px 12px 0px;">


语句块简介：

端口：Port1 \~Port12

传感器编号：1\~4，任意端口可以和任意编号传感器绑定，每个编号对应唯一RGB巡线模块。

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;width:40%;">语句块</th>
<th style="border: 1px solid black;width:60%;">描述</th>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_15.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">初始化RGB巡线传感器，绑定RGB巡线模块到要接入的端口（使用RGB巡线传感器必须先调用该初始化语句块）</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_16.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">设置RGB补光灯颜色，目前暂支持设置三种颜色（红、绿、蓝）

</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_17.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;"> 	设置巡线灵敏度（范围0\~1），灵敏度是用于调节巡线响应速度，值越大巡线时转弯越灵敏</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_18.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">获取RGB巡线传感器模块各探头采集的模拟量值（ADC值），范围为0\~255，探头下颜色越趋近黑色值就接近0，颜色越趋近白色值就接近255 </td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_19.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">获取传感器输出的电机差速值，输出值为巡线灵敏度Kp与4个传感器融合算法模拟量的乘积，范围-512 \~512，大于0时表示线的位置偏向RGB4一侧，值越大偏离度越高，电机差速越大；小于0时表示线的位置偏向RGB1一侧，负值越大偏离越大，电机差速越大。<br>
换而言之，当传感器的RGB探头一侧朝向前进方向安装时，偏移量是正数表示传感器偏向线的左侧；偏移量是负数表示传感器偏向线的右侧。
<img src="images/rgb-line-follower_12.png"  width="607" style="padding:5px 5px 12px 0px;">

 </td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_20.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">如图所示，从左至右四个RGB探头RGB4，RGB3，RGB2，RGB1。 用‘1’代表检测到背景（对应的LED亮起），用‘0’代表检测到线（对应的LED熄灭）。<br><br>
如‘0000’代表四个探头全部检测到线（LED状态为‘灭灭灭灭’）；<br><br>
‘0111’代表从左至右四个探头分别检测到‘线’‘背景’‘背景’‘背景’（LED状态为‘灭亮亮亮’）；<br><br>
‘0011’代表从左至右四个探头分别检测到‘线’‘线’‘背景’‘背景’（LED状态为‘灭灭亮亮’）。<br><br>
如探头检测到的状态与设定的状态一致，返回‘真’；反之，返回‘假’。

<img src="images/rgb-line-follower_13.png"  width="607" style="padding:5px 5px 12px 0px;">

 </td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_21.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">获取单个RGB探头当前状态，是检测到背景还是检测到线；
如探头检测到背景，返回‘真’；检测到线，返回‘假’。
<img src="images/rgb-line-follower_13.png"  width="607" style="padding:5px 5px 12px 0px;">

 </td>
</tr>

</table>

基于Auriga主控板、180编码电机做底盘的默认巡线策略例程，例程为巡线遇到“十”字路口停下2秒后直线运行0.5秒继续巡线，需要预先学习，例程如下：

<img src="images/rgb-line-follower_22.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;">

基于Auriga主控板、180编码电机做底盘的用户获取传感器各通道模拟值（ADC值）自行编写巡线算法的巡线例子，例程为白底黑线巡线，例程如下：

<img src="images/rgb-line-follower_23.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;">

● **慧编程（mblock 5）编程**

1.以Auriga为例，先添加扩展

<img src="images/rgb-line-follower_24.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;">

2.添加RGB传感器扩展

<img src="images/rgb-line-follower_25.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;">

3.在上传模式下使用RGB巡线传感器语句块

<img src="images/rgb-line-follower_26.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;">

语句块简介：

端口：Port1 \~Port12

传感器编号：1\~4，任意端口可以和任意编号传感器绑定，每个编号对应唯一RGB巡线模块。

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;width:40%;">语句块</th>
<th style="border: 1px solid black;width:60%;">描述</th>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_27.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">初始化RGB巡线传感器，绑定RGB巡线模块到要接入的端口（使用RGB巡线传感器必须先调用该初始化语句块）</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_28.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">设置RGB补光灯颜色，目前暂支持设置三种颜色（红、绿、蓝）

</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_29.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;"> 	设置巡线灵敏度（范围0\~1），灵敏度是用于调节巡线响应速度，值越大巡线时转弯越灵敏</td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_30.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">获取RGB巡线传感器模块各探头采集的模拟量值（ADC值），范围为0\~255，探头下颜色越趋近黑色值就接近0，颜色越趋近白色值就接近255 </td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_31.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">获取传感器输出的电机差速值，输出值为巡线灵敏度Kp与4个传感器融合算法模拟量的乘积，范围-512 \~512，大于0时表示线的位置偏向RGB4一侧，值越大偏离度越高，电机差速越大；小于0时表示线的位置偏向RGB1一侧，负值越大偏离越大，电机差速越大。<br>
换而言之，当传感器的RGB探头一侧朝向前进方向安装时，偏移量是正数表示传感器偏向线的左侧；偏移量是负数表示传感器偏向线的右侧。
<img src="images/rgb-line-follower_12.png"  width="607" style="padding:5px 5px 12px 0px;">

 </td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_32.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">如图所示，从左至右四个RGB探头RGB4，RGB3，RGB2，RGB1。 用‘1’代表检测到背景（对应的LED亮起），用‘0’代表检测到线（对应的LED熄灭）。<br><br>
如‘0000’代表四个探头全部检测到线（LED状态为‘灭灭灭灭’）；<br><br>
‘0111’代表从左至右四个探头分别检测到‘线’‘背景’‘背景’‘背景’（LED状态为‘灭亮亮亮’）；<br><br>
‘0011’代表从左至右四个探头分别检测到‘线’‘线’‘背景’‘背景’（LED状态为‘灭灭亮亮’）。<br><br>
如探头检测到的状态与设定的状态一致，返回‘真’；反之，返回‘假’。

<img src="images/rgb-line-follower_13.png"  width="607" style="padding:5px 5px 12px 0px;">

 </td>
</tr>

<tr>
<td style="border: 1px solid black;"><img src="images/rgb-line-follower_33.png" alt="微信截图_20160129151218"  style="padding:5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">获取单个RGB探头当前状态，是检测到背景还是检测到线；
如探头检测到背景，返回‘真’；检测到线，返回‘假’。
<img src="images/rgb-line-follower_13.png"  width="607" style="padding:5px 5px 12px 0px;">

 </td>
</tr>

</table>

**慧编程编程实例**

<img src="images/rgb-line-follower_34.png"  width="607" style="padding:5px 5px 12px 0px;">

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="../sensors/4RGBauriga.mblock" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>