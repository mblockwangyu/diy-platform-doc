# 音频播放模块

![](../../../en/electronic-modules/sensors/images/me-audio-player-1.jpg)

### 概述

音频播放模块兼容全系列makeblock主控板，内置语音解码芯片，可以播放音乐及录音。本模块接口是白色色标，说明是I2C信号控制，需要连接到主板上带有白色标识接口。插入TF内存卡即可感受音乐的快乐，使用起来非常方便。

### 技术规格

- 工作电压：5V DC  
- 麦克风灵敏度（1Khz）:50-54dB  
- 麦克风阻抗:2.2 kΩ  
- 麦克风信噪比:58 db  
- 喇叭额定功率：1W  
- 喇叭额定阻抗：8±15%Ω  
- 通讯方式：I2C  
- 最大电流：500mA  
- 模块尺寸：56 x 41 x 28 mm (长x宽x高)

### 功能特性

- 板载蓝色LED常亮表示音乐播放状态，闪烁表示录音状态  
- 对声音灵敏度高；  
- 模块的金属孔区域是与金属梁接触的参考区域；  
- 具有反接保护，电源反接不会损坏IC；  
- 支持 mBlock 图形化编程，适合全年龄用户；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 模块支持 Micro USB 直接拷贝音频文件，不需要读卡器； 
- 模块直接支持 MP3、WMA、WAV 文件

### 连线模式

● **RJ25连接** 

由于音频播放模块接口是白色色标，当使用RJ25接口时，需要连接到主控板上带有白色色标的接口。以 Makeblock Orion 为例，可以连接到3，4，6，7，8
号接口，如图:

<img src="images/me-audio-player_20184171.jpg" alt="微信截图_20160129151012" width="350" style="padding: 5px 5px 12px 0px;">

### 音频导入方法

播放指定 MP3 文件时需要先导入该文件，导入方法包括以下两种：

* 将音频播放模块的内存卡取出，使用读卡器插入电脑，将文件导入到内存卡中。内存卡位置如下图所示，可按压弹出。

  <img src="images/me-audio-player_memory_card.png" width="150" style="padding: 5px 5px 12px 0px;">

* 使用 Micro USB 线将音频播放模块连接至电脑并导入音频文件。

### 编程指导

● **Arduino 编程**

如果使用 Arduino 编程，需要调用库 ``Makeblock-Library-master`` 来控制音频播放模块。

本程序通过 Arduino 编程让音频播放模块通过按键模块控制，实现音频文件的播放、暂停、开始录音和停止录音。

<img src="images/me-audio-player_20184172.png" alt="微信截图_20160129151123" width="569" style="padding: 5px 5px 12px 0px;">

**音频播放模块主要函数功能列表**

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;width:40%">函数</th>
<th style="border: 1px solid black;width:30%">功能</th>
<th style="border: 1px solid black;width:30%"> </th>
</tr>
<tr>
<td style="border: 1px solid black;">MeAudioPlayer(uint8_t port)</td>
<td style="border: 1px solid black;">选定接口</td>
<td style="border: 1px solid black;"> </td>
</tr>
<tr>
<td style="border: 1px solid black;">playMusicFileIndex uint16_t music_index)</td>
<td style="border: 1px solid black;">指定音频文件索引播放</td>
<td style="border: 1px solid black;">数值：1.2.3……</td>
</tr>
<tr>
<td style="border: 1px solid black;">pauseMusic()</td>
<td style="border: 1px solid black;">暂停播放</td>
<td style="border: 1px solid black;"> </td>
</tr>
<tr>
<td style="border: 1px solid black;">stopMusic()</td>
<td style="border: 1px solid black;">停止播放</td>
<td style="border: 1px solid black;"> </td>
</tr>
<tr>
<td style="border: 1px solid black;">playNextMusic()</td>
<td style="border: 1px solid black;">下一曲</td>
<td style="border: 1px solid black;"> </td>
</tr>
<tr>
<td style="border: 1px solid black;">playPrevMusic()</td>
<td style="border: 1px solid black;">上一曲</td>
<td style="border: 1px solid black;"> </td>
</tr>
<tr>
<td style="border: 1px solid black;">setMusicVolume(uint8_t vol)</td>
<td style="border: 1px solid black;">设置音量值</td>
<td style="border: 1px solid black;">范围0~100</td>
</tr>
<tr>
<td style="border: 1px solid black;">setMusicPlayMode(uint8_t mode)</td>
<td style="border: 1px solid black;">设置播放模式</td>
<td style="border: 1px solid black;">0.单曲播放<br>
1.单曲循环<br>
2.列表循环<br>
3.随机播放</td>
</tr>
<tr>
<td style="border: 1px solid black;">startRecordingFileName(char *str)</td>
<td style="border: 1px solid black;">指定文件名开始录音</td>
<td style="border: 1px solid black;"> </td>
</tr>
<tr>
<td style="border: 1px solid black;">stopRecording()</td>
<td style="border: 1px solid black;">停止录音</td>
<td style="border: 1px solid black;"> </td>
</tr>
</table>

<br>

● **mBlock 编程** 

音频播放模块支持 mBlock 编程环境，使用时需先添加扩展，步骤如下：

1、 打开扩展管理器。

<img src="images/me-audio-player_1.png" width="400" style="padding: 5px 5px 12px 0px;">

2、在扩展管理器中下载“MeAudioPlayer”扩展。

<img src="images/me-audio-player_2.png" width="300" style="padding: 5px 5px 12px 0px;">

下载成功后，mBlock 将显示相关的积木。

<img src="images/me-audio-player_3.png" width="400" style="padding: 5px 5px 12px 0px;">


音频播放模块在 mBlock 中的积木简介

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;width:50%">积木块</th>
<th style="border: 1px solid black;width:50%">描述</th>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_zhiling1.png" alt="微信截图_20160129151218" width="268" style="padding: 5px 5px 12px 0px;">   </td>
<td style="border: 1px solid black;">初始化音频模块，定义其接口<br><strong>注: </strong>在使用音频模块的其它积木之前需先添加此积木。</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_zhiling2.png" alt="微信截图_20160129151218" width="128" style="padding: 5px 5px 12px 0px;">   </td>
<td style="border: 1px solid black;"> 指定音频文件索引播放</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_zhiling3.png" alt="微信截图_20160129151218" width="160" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;"> 指定音频文件名播放</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_zhingling4.png" alt="微信截图_20160129151218" width="200" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;"> 设置播放模式 </td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_zhiling5.png" alt="微信截图_20160129151218" width="120" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;"> 播放上一首音频</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_zhiling6.png" alt="微信截图_20160129151218" width="120" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;"> 播放下一首音频</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ziling7.png" alt="微信截图_20160129151218" width="120" style="padding: 5px 5px 12px 0px;">   </td>
<td style="border: 1px solid black;">暂停/恢复播放</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_zhiling8.png" alt="微信截图_20160129151218" width="75" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;">停止播放</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_zhiling9.png" alt="微信截图_20160129151218" width="120" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">音量设置大小</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_hizhiling10.png" alt="微信截图_20160129151218" width="75" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">音量增加</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_zhiling11.png" alt="微信截图_20160129151218" width="75" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;"> 音量减小</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_ziling12.png" alt="微信截图_20160129151218" width="120" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;"> 以“T001”名称开始录音 </td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_zhiling13.png" alt="微信截图_20160129151218" width="73" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;"> 停止录音</td>
</tr>
</table>

<br>

以下是如何使用 mBlock 控制音频播放模块的例子
    
本程序可以通过按键模块控制音频播放模块。实现音频文件的播放、暂停、开始录音和停止录音，以下是运行结果:  

<img src="images/me-audio-player_20184174.png" alt="微信截图_20160129151218" width="1108" style="padding: 5px 5px 12px 0px;">

● **慧编程编程**

音频播放模块支持慧编程编程环境，使用时需先添加扩展。以 mBot 为例，添加扩展步骤如下：

1、点击“添加扩展”。

<img src="images/me-audio-player_4.png" width="400" style="padding: 5px 5px 12px 0px;">

2、在弹出的“扩展中心”页面，点击“音频播放模块”下方的“添加”按钮。

<img src="images/me-audio-player_5.png" width="600" style="padding: 5px 5px 12px 0px;">

添加成功后，慧编程显示相关积木。

<img src="images/me-audio-player_6.png" width="500" style="padding: 5px 5px 12px 0px;">

音频播放模块在慧编程中的积木简介

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;width:50%">积木块</th>
<th style="border: 1px solid black;width:50%">描述</th>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_7.png" width="200" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">初始化音频模块，定义其接口<br><strong>注: </strong>在使用音频模块的其它积木之前需先添加此积木。</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_8.png" width="200" style="padding: 5px 5px 12px 0px;">   </td>
<td style="border: 1px solid black;"> 指定音频文件索引播放</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_8-1.png" width="200" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;"> 指定音频文件名播放</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_9.png" width="200" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;">设置播放模式：单曲播放、单曲循环、列表循环、随机播放</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_10.png" width="200" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;"> 播放上一首音频</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_11.png" width="200" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;"> 播放下一首音频</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_12.png" width="200" style="padding: 5px 5px 12px 0px;">   </td>
<td style="border: 1px solid black;">暂停/恢复播放</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_13.png" width="200" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;">停止播放</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_14.png" width="200" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">音量设置大小</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_15.png" width="200" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;">音量增加</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_16.png" width="200" style="padding: 5px 5px 12px 0px;">  </td>
<td style="border: 1px solid black;"> 音量减小</td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_17.png" width="200" style="padding: 5px 5px 12px 0px;"></td>
<td style="border: 1px solid black;"> 以“T001”名称开始录音 </td>
</tr>
<tr>
<td style="border: 1px solid black;"><img src="images/me-audio-player_18.png" width="200" style="padding: 5px 5px 12px 0px;"> </td>
<td style="border: 1px solid black;"> 停止录音</td>
</tr>
</table>

**编程示例**

<img src="images/me-audio-player_19.png" width="300" style="padding: 5px 5px 12px 0px;">

### 音频文件格式说明

- 音量播放语句块后面请增加适当的延时（必须），等待其生效。  
- 使用外部存储器TF卡存储音频文件，支持播放MP3，WAV，WMA高品质（低品质可能播放不顺畅）音频格式文件  
- 采用FAT和FAT32文件系统  
- 音频文件命名格式支持英文命名（不区分大小写），英文与数字混合命名，命名长度建议小于8个字符，例如：Hello.MP3、T002.MP3、R000001.MP3、（不建议使用纯数字命名）  
- 音频文件在TF卡中的排序：建议按文件名排序  
- 不建议用中文命名的音频文件  
- 禁止使用特殊的字符命名，如：v1.0”、o\_o0、….（都是不支持的）
