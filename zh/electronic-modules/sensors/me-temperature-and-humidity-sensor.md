# 温湿度传感器

<img src="../../../en/electronic-modules/sensors/images/me-temperature-and-humidity-sensor_微信截图_20160129134216.png" alt="微信截图_20160129134216" width="290" style="padding:5px 5px 12px 0px;">

### 概述

温度和湿度传感器是一个包含校准数字信号输出的传感器。
它采用特定的数字模块采集技术和温湿度传感技术，确保高度可靠性与出色的长周期稳定性。
该模块可以测量从0到50摄氏度温度。
用户可以使用它建立一个具有成本效益的温度和湿度监测系统。
它的黄色端口意味着它有一个单数字端口，需要连接到Makeblock
Orion上带有黄色的端口。

### 技术规格

- 工作电压: 5V DC  
- 控制模式: 单总线数字信号  
- 输出电流: max 2.5mA  
- 温度范围: 0-50℃ ± 2℃  
- 湿度范围: 20-90% RH ± 5% RH  
- 精度: 1% RH, 1℃  
- 模块尺寸: 51 x 24 x 18mm(L x W x H)

### 功能特性

- 尺寸小与功耗小  
- 较强抗干扰能力  
- 总校准的数字输出  
- 模块的白色区域是接触金属脚的反应区  
- 支持Arduino IDE编程, 并且提供运行库来简化编程  
- 采用RJ25口方便连接  
- 支持mBlock图形化编程，适合全年龄用户。

### 引脚定义

温湿度传感器模块有三个针脚的接头,每个针脚的功能如下表:

| 序号 | 引脚 | 功能     |
|------|------|----------|
| 1    | 5V   | 接电源   |
| 2    | DATA | 数据输出 |
| 3    | GND  | 接地     |

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能 </th>
</tr>

<tr>
<td style="border: 1px solid black;"> 1 </td>
<td style="border: 1px solid black;">5V</td>
<td style="border: 1px solid black;">接电源</td>
</tr>

<tr>
<td style="border: 1px solid black;">2 </td>
<td style="border: 1px solid black;">DATA</td>
<td style="border: 1px solid black;">数据输出</td>
</tr>

<tr>
<td style="border: 1px solid black;"> 3 </td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">接地</td>
</tr>

</table>

<br>

### 连线模式

● **RJ25连接** 

因为温湿度传感器有一个黄色端口，需要用RJ25线连接到 Makeblock
Orion 上带有黄色的端口如下:

<img src="../../../en/electronic-modules/sensors/images/me-temperature-and-humidity-sensor_微信截图_20160129134631.png" alt="微信截图_20160129134631" width="327" style="padding:5px 5px 12px 0px;">

● **杜邦线连接**  

当使用杜邦线连接温湿度传感器与 Arduino UNO 板，它的数据引脚必须连接到数字端口如下图:

<img src="../../../en/electronic-modules/sensors/images/me-temperature-and-humidity-sensor_微信截图_20160129134707.png" alt="微信截图_20160129134707" width="322" style="padding:5px 5px 12px 0px;">

### 编程指导

● **Arduino编程**  

如果使用Arduino编程，需要调用库`Makeblock-Library-master`来控制温度传感器。

本程序通过Arduino编程读取当前温度值与湿度值。

<img src="../../../en/electronic-modules/sensors/images/me-temperature-and-humidity-sensor_微信截图_20160129134748.png" alt="微信截图_20160129134748" width="467" style="padding:5px 5px 12px 0px;">

代码段的功能是：  
读取温湿度传感器测量的结果，并将结果输出到Arduino IDE中的串行监视器。 

将代码段上传到 Makeblock Orion 并点击 Arduino 串口监视器，然后你会看到运行结果如下:

<img src="../../../en/electronic-modules/sensors/images/me-temperature-and-humidity-sensor_微信截图_20160129134851.png" alt="微信截图_20160129134851" width="738" style="padding:5px 5px 12px 0px;">

### 原理分析

本模块主要元件为DHT11温湿度传感器,包括了一个电阻式湿度传感组件和NTC温度测量组件。校准系数以程序形式存储在OTP存储器中，并在处理检测到的信号时由传感器调用。采用单总线串口，传感器只能通过总线与微处理器进行双向通信，一次通信的时间约为4ms。系统集成变得快速和容易。

### 原理图

<img src="../../../en/electronic-modules/sensors/images/me-temperature-and-humidity-sensor_Huminity-and-temp.png" alt="Huminity and temp" width="1264" style="padding:5px 5px 12px 0px;">
