# RJ25 转杜邦线

<img src="../../../en/electronic-modules/cables/images/rj25-to-dupont-wire_微信截图_20160126173803-300x253.png" alt="微信截图_20160126173803" width="300" style="padding:5px 5px 12px 0px;">

### 描述

RJ25转杜邦线连接Makeblock电子模块和其他带杜邦接口的模块。有了这根电线，就可以使用 Makeblock Orion 来控制更多的传感器和电子模块，并且还可以使用其他制造商的开发板来控制 Makeblock 的电子模块。RJ25转杜邦线提取所有6pin，因此用户需要在通电之前确认正确的引脚连接。

### 技术规格

- 端口：标准RJ25接口/杜邦接口×6

### 功能特性

<img src="../../../en/electronic-modules/cables/images/rj25-to-dupont-wire_微信截图_20160126151842-1-300x97.png" alt="微信截图_20160126151842" width="300" style="padding:5px 5px 12px 0px;">
