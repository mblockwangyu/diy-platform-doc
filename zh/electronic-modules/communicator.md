# 通信类

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="25%;"><a href="communicators/2-4g-wireless-serial.html" target="_blank"><img src="../../en/electronic-modules/communicators/images/2-4g-wireless-serial_2.4G-Wireless-Serial-for-mBot.jpg" width="150px;"></a><br>
<p>2.4G 模块</p></td>

<td width="25%;"><a href="communicators/bluetooth-moduledual-mode.html" target="_blank"><img src="../../en/electronic-modules/communicators/images/bluetooth-moduledual-mode_Bluetooth-Module-for-mBot.jpg" width="150px;"></a><br>
<p>mBot 蓝牙模块</p></td>

<td width="25%;"><a href="communicators/me-bluetooth-moduledual-mode.html" target="_blank"><img src="../../en/electronic-modules/communicators/images/me-bluetooth-moduledual-mode_Me-Bluetooth-Module-(Dual-Mode).jpg" width="150px;"></a><br>
<p>蓝牙模块</p></td>

<td width="25%;"><a href="communicators/me-infrared-reciver-decode.html" target="_blank"><img src="../../en/electronic-modules/communicators/images/me-infrared-reciver-decode_Me-Infrared-Receiver-Decode.jpg" width="150px;"></a><br>
<p>红外接收模块</p></td>
</tr>

<tr>
<td><a href="communicators/me-usb-host.html" target="_blank"><img src="../../en/electronic-modules/communicators/images/me-usb-host_Me-USB-Host.jpg" width="150px;"></a><br>
<p>USB-Host模块</p></td>

<td><a href="communicators/me-wifi.html" target="_blank"><img src="../../en/electronic-modules/communicators/images/me-wifi_Me-WiFi-Module.jpg" width="150px;"></a><br>
<p>WIFI模块</p></td>

</tr>

</table>