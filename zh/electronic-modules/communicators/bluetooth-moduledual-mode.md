# mBot 蓝牙模块

<img src="../../../en/electronic-modules/communicators/images/bluetooth-moduledual-mode_微信截图_20160203161005.png" alt="微信截图_20160203161005" width="301" style="padding:5px 5px 12px 0px;">

### 描述

这个蓝牙模块是专门为个人用户或家庭玩乐。您可以使用智能手机或电脑（支持蓝牙）通过此模块无线控制 mBot。

### 特点

- Android 与 IOS App 提供给客户使用于不同场景中
- 适配 mBlock 图形化编程软件

### 规格

- 工作电压: 5V DC power;
- 版本: Bluetooth 2.0 and 4.0 compatible;
- 输出电压: 5V/high, 0V/low;
- 尺寸: 30mm\*20mm\*14mm (长 x 宽 x 高)
