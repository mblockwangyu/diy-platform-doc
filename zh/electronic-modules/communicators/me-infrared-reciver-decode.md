# 红外接收模块


![](../../../en/electronic-modules/communicators/images/me-infrared-reciver-decode_Me-Infrared-Receiver-Decode.jpg)

<img src="../../../en/electronic-modules/communicators/images/me-infrared-reciver-decode_微信截图_20160129115255.png" alt="微信截图_20160129115255" width="240" style="padding:5px 5px 12px 0px;">

### 概述

红外接收模块通过红外信号接收器接收远处发来的红外信号，红外线遥控是目前使用最广泛的一种通信和遥控手段，具备体积小、功耗低、功能强等优点。如各种家用电器、音响设备、空调机、机器人动作控制、小车控制以及其它智能控制。在高压、辐射、有毒气体、粉尘等环境下，采用红外遥控可以有效地隔离电气干扰。本模块接口是蓝色色标，说明是双数字口控制，需要连接到主板上带有蓝色标识接口。

### 技术规格

- 工作电压：4.8V到5.3V DC  
- 工作电流：1.7到2.7 mA  
- 接收频率：38KHz  
- 峰值波长: 980 nm  
- 有效接收距离：10米  
- 工作温度：0到70℃  
- 控制方式：双数字口控制  
- 模块尺寸：51 x 24 x 24.8 mm (长x宽x高)

### 功能特性

- 模块的白色区域是与金属梁接触的参考区域；  
- 使用 Makeblock 配套的遥控器；  
- 在近距离1米范围内使用遥控器需对准模块红外接头；  
- 具有两只LED指示灯用于调试与反馈；  
- 使用NEC IR协议通过IR控制器实现简易控制；  
- 具有反接保护，电源反接不会损坏IC；  
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程；  
- 支持 mBlock 图形化编程，适合全年龄用户；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 配有DAT、RX、VCC、GND接头支持绝大多数 Arduino 系列主控板。

### 引脚定义

红外接收模块有四个针脚的接头,每个针脚的功能如下表：

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">DAT</td>
<td style="border: 1px solid black;">遥控按键输出</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">RX</td>
<td style="border: 1px solid black;">遥控按键值输出，接主板串口接收端</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">电源线</td>
</tr>

<tr>
<td style="border: 1px solid black;">4 </td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">地线</td>
</tr>
</table>

<br>


### 接线方式

● **RJ25连接** 

由于红外接收模块接口是蓝色色标，当使用RJ25接口时，需要连接到主控板上带有蓝色色标的接口。以 Makeblock Orion 为例，可以连接到3，4，5，6号接口，如图： 

<img src="images/me-infrared-reciver-decode_微信截图_20160129115719.png" alt="微信截图_20160129115719" width="384" style="padding:5px 5px 12px 0px;">

● **杜邦线连接**

当使用杜邦线连接到 Arduino Uno 主板时，模块RX与DAT引脚需要连接到
DIGITAL（数字）口，如下图所示:

<img src="images/me-infrared-reciver-decode_微信截图_20160129115754.png" alt="微信截图_20160129115754" width="336" style="padding:5px 5px 12px 0px;">

### 编程指导

● **Arduino 编程**  

如果使用 Arduino 编程，需要调用库 `Makeblock-Library-master` 来控制红外接收模块。 

本程序通过 Arduino 编程，通过串口监视器可观查到被按下的红外遥控器按键。

<img src="../../../en/electronic-modules/communicators/images/me-infrared-reciver-decode_微信截图_20160129115839.png" alt="微信截图_20160129115839" width="468" style="padding:5px 5px 12px 0px;">

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>

<tr>
<td style="border: 1px solid black;"> MeInfraredReceiver(uint8_t port)</td>
<td style="border: 1px solid black;"> 选定接口</td>
</tr>
<tr>
<td style="border: 1px solid black;"> bool avaliable()</td>
<td style="border: 1px solid black;">检测是否接收到按键值            </td>
</tr>
<tr>
<td style="border: 1px solid black;"> byte available()</td>
<td style="border: 1px solid black;">获取接收缓冲区中未读的数据字节数 </td>
</tr>
<tr>
<td style="border: 1px solid black;"> int read()</td>
<td style="border: 1px solid black;">读取红外控制器发送的红外信号     </td>
</tr>
</table>

<br>

<img src="../../../en/electronic-modules/communicators/images/me-infrared-reciver-decode_微信截图_20160129115945.png" alt="微信截图_20160129115945" width="433" style="padding:5px 5px 12px 0px;">

我们可以看到，当接收到红外控制器发出的红外信号时，红外接收器读取红外信号并译码，然后输出到串口显示。

● **mBlock 编程**

红外接收模块支持 mBlock 编程环境，如下是该模块指令简介： 
 
<img src="../../../en/electronic-modules/communicators/images/me-infrared-reciver-decode_微信截图_20160129120037.png" alt="微信截图_20160129120037" width="718" style="padding:5px 5px 12px 0px;">

### 原理解析

红外通信是利用红外技术实现两点间近距离保密通信和信息转发，一般由红外发射和红外接收两部分系统组成。红外接收模块含有红外一体化接收头，其内部含有高频的滤波电路，专门用来滤除红外线合成信号的载波信号，随后信号进入模块内部解码芯片。当红外线合成信号进入红外接收头，在其输出端便可以得到远红外发射器发出的数字编码（当模块接收到有效的红外编码数据时，STA变为低电平；如果红外遥控按键被持续按下，STA会保持低电平，同时重复发送数据码）。

### 原理图

<img src="../../../en/electronic-modules/communicators/images/me-infrared-reciver-decode_Me-infrared-sensor.png" alt="Me infrared sensor" width="1093" style="padding:5px 5px 12px 0px;">
