# Wi-Fi模块

<img src="../../../en/electronic-modules/communicators/images/me-wifi_微信截图_20160129114325.png" alt="微信截图_20160129114325" width="223" style="padding:5px 5px 12px 0px;">

### 概述

Wi-Fi 模块主要部件为 ESP8266 模块，ESP8266 是一款超低功耗的 UART-WiFi 透传模块，支持无线802.11 b/g/n 标准，工作电压3.3V。Wi-Fi 模块内置电平转换，将5V转为3.3V，本模块接口是蓝灰色标，需要通过RJ25连接主板上的标有蓝色或者灰色的接口。

### 技术规格

- 工作电压：5V DC  
- 支持无线：802.11 b/g/n 标准  
- 频率范围：2.412GHz\~2.484GHz，  
- 工作电流：50mA；  
- 峰值电流：200mA；  
- 工作温度：-25℃\~80℃  
- 芯片型号：ESP8266

### 功能特性

- 工作模式：STA（工作站模式）+AP（热点模式）
- 内置 TCP/IP 协议栈
- 支持 WPA WPA2/WPA2–PSK 加密
- 模块的白色区域是与金属梁接触的参考区域
- 具有反接保护，电源反接不会损坏 IC
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程
- 使用 RJ25 接口连线方便
- 模块化安装，兼容乐高系列
- 配有接头支持绝大多数 Arduino 系列主控板


### 引脚定义

Wi-Fi模块有四个针脚的接头,每个针脚的功能如下表：

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能 </th>
</tr>

<tr>
<td style="border: 1px solid black;"> 1</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">地线</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">电源线</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">TX</td>
<td style="border: 1px solid black;">串口信号发送</td>
</tr>

<tr>
<td style="border: 1px solid black;">4</td>
<td style="border: 1px solid black;">RX</td>
<td style="border: 1px solid black;">串口信号接收</td>
</tr>
</table>

<br>

### 连线模式

● **RJ25连接** 

由于 Wi-Fi 模块接口是蓝或灰色色标，当使用RJ25接口时，需要连接到主控板上带有蓝或灰色色标的接口。

以 Makeblock Orion 为例，可以连接到 3，4，5，6 号接口，如图：  

<img src="../../../en/electronic-modules/communicators/images/me-wifi_微信截图_20160129114619.png" alt="微信截图_20160129114619" width="270" style="padding:5px 5px 12px 0px;">

● **杜邦线连接** 

当使用杜邦线连接到 Arduino Uno 主板时，模块TX、RX引脚需要分别连接到 Uno 板上的RX、TX引脚，如下图所示：

<img src="../../../en/electronic-modules/communicators/images/me-wifi_微信截图_20160129114705.png" alt="微信截图_20160129114705" width="299" style="padding:5px 5px 12px 0px;">

### Wi-Fi模块热点设置

1\. 使模块处在 Work 模式，连接到主控板带有灰色或者蓝色的口，打开主控板，模块指示灯亮后查看电脑 Wi-Fi。

<img src="images/wifi.jpg" width="300" style="padding:5px 5px 12px 0px;">

2\. 连接 ESP 开头的 Wi-Fi（个别情况下会显示隐藏的网络，可以继续连接）。

3\. 浏览器输入 192.168.4.1 进入到 Wi-Fi 设置页面，在此页面下可以设置 Wi-Fi 热点的加密方式，Wi-Fi 热点的名称及密码及工作模式。

<img src="images/wifi-2.jpg" width="600" style="padding:5px 5px 12px 0px;">

### 编程指南

● **mblock 3 编程** 

1\. 打开 mblock 3，使用扩展管理器添加扩展。

<img src="images/wifi-3.jpg" width="600" style="padding:5px 5px 12px 0px;">

2\. 搜索并下载 IOT_IFTTT 扩展（物联网应用）。

<img src="images/wifi-4.jpg" width="600" style="padding:5px 5px 12px 0px;">

3\. 使用语句块进行编程。

<img src="images/wifi-5.jpg" width="600" style="padding:5px 5px 12px 0px;">

### 物联网应用案例

注意，刷了此固件后 Wi-Fi 名称会变为 AI-THINKER 开头，且不支持192.168.4.1的配置页面，所以自身 Wi-Fi 设置需要使用 AT 指令去设置，文末只提供常用配置 Wi-Fi 的 AT 指令，其他 AT 指令请用户自行查询 ESP8266 AT 指令集。

**设备准备：** 

电脑，Me Wi-Fi 模块，USB 转 TTL 模块（需自行购买），转接线，mCore

相关资源下载：<a href="images/iot-ifttt.rar" download style="font-weight:bold;">点击下载 iot-ifttt 压缩包</a>


#### 1、更新 Me Wi-Fi 模块固件

1）连接 Me Wi-Fi 模块和 USB 转 TTL 模块。

<img src="images/wifi-6.jpg" width="300" style="padding:5px 5px 12px 0px;">

<img src="images/wifi-7.jpg" width="600" style="padding:5px 5px 12px 0px;">

接线：按照下表一一对应。

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">Me Wi-Fi 模块</th>
<th style="border: 1px solid black;">USB 转 TTL 模块</th>
</tr>

<tr>
<td style="border: 1px solid black;"> 5V </td>
<td style="border: 1px solid black;">5V</td>
</tr>

<tr>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">GND</td>
</tr>

<tr>
<td style="border: 1px solid black;">RX</td>
<td style="border: 1px solid black;">TX</td>
</tr>

<tr>
<td style="border: 1px solid black;">TX</td>
<td style="border: 1px solid black;">RX</td>
</tr>
</table>

2）将 Me Wi-Fi 按钮拨到 PROG 模式。

<img src="images/wifi-8.jpg" width="400" style="padding:5px 5px 12px 0px;">

3）打开 flash_download_tools 工具，选择 ESP8266。

<img src="images/wifi-9.jpg" width="400" style="padding:5px 5px 12px 0px;">

4) 然后选择正确的固件`Ai-Thinker_ESP8266_DOUT_8Mbit_v1.5.4.1-a_20171130.bin`，设置如下：

<img src="images/wifi-10.jpg" width="400" style="padding:5px 5px 12px 0px;">

确保选择了正确的COM端口。如果不确定，请转到计算机设备管理器检查：

<img src="images/wifi-11.jpg" width="600" style="padding:5px 5px 12px 0px;">

#### 2、更改波特率

1）更新固件后，波特率是115200，但我们需要9600。因此，一旦完成，请拔下 ME Wi-Fi 模块，并切换到 work 模式的按钮。然后重新连接并打开“sscom.exe”。

然后设置如下：

选择COM端口，选择波特率115200，并用黄色勾选两行。
在命令区域中，输入AT+CIOBAUD=9600

<img src="images/wifi-12.jpg" width="600" style="padding:5px 5px 12px 0px;">

单击“发送”，如果在结果区域中看到“ok”，则表示更改成功。

2）测试，现在将波特率更改为9600，在命令区域中输入AT，单击发送，如果显示ok，则表示它以波特率9600工作。步骤2完成。

<img src="images/wifi-13.jpg" width="600" style="padding:5px 5px 12px 0px;">

#### 3、用 IFTTT 测试

1）注册 IFTTT 账号 https://ifttt.com/discover （建议科学上网注册）。

2）注册完毕点击“Create”。

<img src="images/image028.png" width="300" style="padding:5px 5px 12px 0px;">

3）点击“+this”。

<img src="images/image029.png" width="300" style="padding:5px 5px 12px 0px;">

4）搜索并添加“webhooks”，然后创建服务。

<img src="images/image030.png" width="400" style="padding:5px 5px 12px 0px;">

<img src="images/image032.png" width="400" style="padding:5px 5px 12px 0px;">

这里“light”是指程序中的“Event name”。

5）点击“+that”。

<img src="images/image034.png" width="300" style="padding:5px 5px 12px 0px;">

6）选择并添加“Email”。

<img src="images/image036.png" width="400" style="padding:5px 5px 12px 0px;">

输入邮箱地址，点击发送，收到邮件后填入邮件中接受的PIN码。

<img src="images/image037.png" width="400" style="padding:5px 5px 12px 0px;">

<img src="images/image039.png" width="400" style="padding:5px 5px 12px 0px;">

7）按照以下步骤完成创建，无需在此处更改任何参数。

<img src="images/image041.png" width="400" style="padding:5px 5px 12px 0px;">

<img src="images/image043.png" width="400" style="padding:5px 5px 12px 0px;">

8）点击 “webhooks”- “documentation”,复制key值。

<img src="images/image045.png" width="400" style="padding:5px 5px 12px 0px;">

<img src="images/image047.png" width="400" style="padding:5px 5px 12px 0px;">

<img src="images/image049.png" width="400" style="padding:5px 5px 12px 0px;">

9）打开mBlock3，选择扩展，添加IoT_IFTTT扩展。

<img src="images/image051.png" width="600" style="padding:5px 5px 12px 0px;">

10）用电脑连接 mCore，选择正确的 COM 口并连接，选择 mCore 主控板，按下图写好程序 key 值选择 3.8 步骤里的 key 值，Eventname 填入步骤 3.4 创建的名称，SSID 填需要连接的 Wi-Fi 名称，password 填入需连接的 Wi-Fi 密码（建议连接手机热点可以查看是否连接成功）。

<img src="images/image053.png" width="600" style="padding:5px 5px 12px 0px;">

上传本程序，然后程序烧录后，当光效的强度小于300时，我们的注册邮箱中就会收到对应的邮件提示。

<img src="images/image057.png" width="600" style="padding:5px 5px 12px 0px;">

Wi-Fi如果连接不成功请使用压缩包中的<IoT-IFTTT配置文件替换>解压缩后使用<IoT-IFTTT>文件夹将电脑中mblock安装文件下下面的libraries文件夹下的<IoT-IFTTT>文件夹替换。

<img src="images/image055.png" width="600" style="padding:5px 5px 12px 0px;">

### Arduino 编程

如果使用 Arduino 编程，需要调用库 Makeblock-Library-master 来控制 Wi-Fi 模块。

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;"> MeWi-Fi (uint8_tport) </td>
<td style="border: 1px solid black;">选定接口</td>
</tr>

<tr>
<td style="border: 1px solid black;">void.begin(9600)</td>
<td style="border: 1px solid black;">设定带宽并启动</td>
</tr>

<tr>
<td style="border: 1px solid black;">int available()</td>
<td style="border: 1px solid black;">判断是否接收到数据</td>
</tr>

<tr>
<td style="border: 1px solid black;">char read()</td>
<td style="border: 1px solid black;">读取接受的数据</td>
</tr>

<tr>
<td style="border: 1px solid black;">char write(outDat)</td>
<td style="border: 1px solid black;">输出数据</td>
</tr>
</table>


### 原理分析

本模块支持STA/AP/STA+AP 三种工作模式。  
- STA 模式：模块通过路由器连接互联网，手机或电脑通过互联网实现对设备的远程控制。  
- AP 模式：模块作为热点，实现手机或电脑直接与模块通信，实现局域网无线控制。  
- STA+AP 模式：两种模式的共存模式，即可以通过互联网控制可实现无缝切换，方便操作。

连接好模块后，红色电源灯亮，约1秒后，蓝色Link指示灯闪烁（闪烁代表正常启动，但未连接）。当连接设备成功，并进行一次数据发送后，指示灯常亮，模块接收数据时蓝色接收指示灯闪烁。拨动开关用于选择模式，Work和PROG工作模式，Work是正常工作状态（平时应在这个状态），PROG是编程模式，切换模式时需要重启。  
模块上电时，在配置模式下模块WiFi信号为：“ESP（+芯片ID号）”，无密码。在浏览器输入WiFi扩展板地址：192.168.4.1打开配置页面即可进行配置。

### 原理图

<img src="../../../en/electronic-modules/communicators/images/me-wifi_Me-wifi.png" alt="Me wifi" width="1082" style="padding:5px 5px 12px 0px;">

### 常用测试AT指令

<img src="images/image059.png" width="600" style="padding:5px 5px 12px 0px;">

<img src="images/image060.png" width="600" style="padding:5px 5px 12px 0px;">

<img src="images/image061.png" width="600" style="padding:5px 5px 12px 0px;">

<img src="images/image062.png" width="600" style="padding:5px 5px 12px 0px;">
