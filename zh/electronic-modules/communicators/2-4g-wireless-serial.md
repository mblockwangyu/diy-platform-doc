# 2.4G 模块

<img src="../../../en/electronic-modules/communicators/images/2-4g-wireless-serial_微信截图_20160129112845.png" alt="微信截图_20160129112845" width="347" style="padding:5px 5px 12px 0px;">

### 描述

2.4G无线模块是专为 mBot 用于教室或车间，当很多人同时使用无线通信。
它使用与无线鼠标相同的技术。具有不需要配对的功能，无需驱动程序，多个模块同时工作时不会产生信号干扰。

包括两部分：一个加密狗插入您的计算机;一个插在 mCore 上的板载模块，可以方便地建立软件 mBlock 和 mBot 之间的无线连接。

### 特点

- 允许自动配对，与无线鼠标相同的技术；
- USB Dongle支持32位、64位Windows与 Mac OS 驱动程序自动安装
- 许多mBots同时工作时没有信号干扰

### 规格

- 固定波特率:115200
- 通讯距离: 大约10米
- 电源供电: 5V DC
- 尺寸: 30mm\*20mm\*14mm

### 包装清单

- 2.4G 模块
- 2.4G 无线电子狗

**用户指导:** <http://v.youku.com/v_show/id_XOTQ1OTc3MDUy.html?beta&from=y1.7-2>
