# MegaPi RJ25 转接板

<img src="images/megapi-shield-for-rj25.png" width="200">

### 概述

本模块用于 MegaPi，将 MegaPi 的针脚转换为 Makeblock 的 RJ25 接口，可以用来连接 Makeblock 的电子模块。

### 技术规格

* 工作电压：5V
* 模块尺寸：25mm x 63mm（长x宽）

### 功能特性

* 引出 4 个 Port 口
* 1 个电源指示灯
* 使用 6P6C RJ25 接口方便接线
* 有 Arduino 库方便编程

### 连接方式

<img src="images/megapi-shield-for-rj25-1.png" width="300">