# MegaPi Pro 电调转接板

<img src="images/megapi-pro-esc-driver/megapi-pro-esc-driver.png" width="300">

### 概述

本模块为 MegaPi Pro 专用模块，本模块可以驱动 1 个 2823 无刷电机，采用 2x8Pin 接插方式，可以方便安装在 MegaPi Pro 上。MegaPi Pro 最多可以安装四个此模块用来驱动 4 个无刷电机。

### 技术规格

- 电机通道：1
- 最低工作电压：9V
- 最高工作电压：12V
- 典型值电压：11.1V
- 额定电流：3A
- 峰值电流：5A
- 模块尺寸：30mmx15mm（长x宽）

### 功能特性

- 支持工作电压为9~12V的电机
- 12V电源供电时，工作电流可达3A（峰值可达5A）
- 模块拥有过压保护、过流保护、过温保护，全方位保证使用安全
- 彩色公母插座，防止插错
- 模块体积小巧，便于换取

### 编程指南

● **Arduino编程**

**函数功能说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">函数</th>
    <th class="tg-7g6k">功能</th>
  </tr>
  <tr>
    <td class="tg-3xi5">MeMegaPiProESCMotor （port）</td>
    <td class="tg-c6of">设置无刷电机接口。<br>bldcmotor_1(1)~ bldcmotor_4(4)</td>
  </tr>
  <tr>
    <td class="tg-3xi5">run(abs(speed))</td>
    <td class="tg-c6of">设置速度 (speed：0~100)。</td>
  </tr>
</table>

以下程序运行后，无刷电机 1 解锁。出现解锁声后，开始以 50% 的动力旋转 2 秒，停止 2 秒，重复此过程。

```
#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>

#include <MeMegaPiPro.h>
MeMegaPiProESCMotor bldcmotor_1(1);

void setup(){
    TCCR1A = _BV(WGM10);//PIN12
    TCCR1B = _BV(CS11) | _BV(CS10) | _BV(WGM12);
    bldcmotor_1.init();
}

void loop(){
    bldcmotor_1.run(abs(50));
    delay(2);
    bldcmotor_1.run(abs(0));
    delay(2);
    loop();
}
```
<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-esc-driver/MegaPi_Pro_ESC_Driver_V1.0.ino" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **mBlock 编程**

**积木块说明**
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-esc-driver/megapi-pro-esc-driver-1.png" width="200"></td>
    <td class="tg-c6of">选择接口，将电机解锁，解锁后会出现解锁声音（程序头必须添加此积木）。
</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-esc-driver/megapi-pro-esc-driver-2.png" width="200"></td>
    <td class="tg-c6of">选择接口；设置无刷电机动力（0~100）。
</td>
  </tr>
</table>

以下程序运行后，无刷电机 1 解锁。出现解锁声后，开始以 50% 的动力旋转 2 秒，停止 2 秒，重复此过程。

<img src="images/megapi-pro-esc-driver/megapi-pro-esc-driver-3.png" width="300"><br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-esc-driver/MegaPi Pro ESC Driver V1.0.sb2" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **慧编程编程**

**积木块说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">积木块</th>
    <th class="tg-7g6k">说明</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-esc-driver/megapi-pro-esc-driver-4.png" width="200"></td>
    <td class="tg-c6of">选择接口，将电机解锁，解锁后会出现解锁声音（程序头必须添加此积木）。
</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="images/megapi-pro-esc-driver/megapi-pro-esc-driver-5.png" width="200"></td>
    <td class="tg-c6of">选择接口；设置无刷电机动力（0~100）。
</td>
  </tr>
</table>

以下程序运行后，无刷电机 1 解锁出现解锁声后，开始以 50% 的动力旋转 2 秒，停止 2 秒，重复此过程。

<img src="images/megapi-pro-esc-driver/megapi-pro-esc-driver-6.png" width="300"><br>

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-esc-driver/MegaPi Pro ESC Driver V1.0.mblock" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

● **Python3 编程**

1、主控板 MegaPi Pro 和树莓派连接，RJ25 适配器与 MegaPi Pro RJ25 转接板接口相连，MegaPi Pro 电调转接板与 RJ25 适配器相连接。<br>
2、树莓派安装最新的 Makeblock库 `pip3 install makeblock --upgrade`。<br>
3、新建 python 文件，后缀为 .py。<br>
4、在 python 文件里写入程序。<br>
5、运行 python 文件如 “python123.py”。

**函数功能说明**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">函数</th>
    <th class="tg-7g6k">功能</th>
  </tr>
  <tr>
    <td class="tg-3xi5">BLDCMotor(port)</td>
    <td class="tg-c6of">创建无刷电机对象。<br>port：MegaPiPro.PORT1~MegaPiPro.PORT4
</td>
  </tr>
  <tr>
    <td class="tg-3xi5">run(speed)</td>
    <td class="tg-c6of">speed：转速（0~100）</td>
  </tr>
</table>

以下程序运行后无刷电机 1 以 50 的动力旋转 2 秒，停止一秒，以 -50 的动力旋转 2 秒，停止一秒。

```
from time import sleep
from makeblock import MegaPiPro 
board = MegaPiPro.create()
bldc = board.BLDCMotor(MegaPiPro.PORT1)
while True:
    bldc.run(50)
    sleep(2)
    bldc.run(0)
    sleep(1)
    bldc.run(-50)
    sleep(2)
    bldc.run(0)
    sleep(1)
```

<p><span style="background-color:#66CDAA;line-height:30px;padding:12px;font-size:16px;border-radius:3px;"><a href="images/megapi-pro-esc-driver/BLDCMotor.py" download style="color:white;font-weight:bold;">下载示例程序</a></span></p>

### 连接方式

● **电子接线**

<img src="images/megapi-pro-esc-driver/megapi-pro-esc-driver-7.png" width="500">