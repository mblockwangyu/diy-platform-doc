# MegaPi Pro RJ25 转接板

<img src="images/megapi-pro-shield-for-rj25/megapi-pro-shield-for-rj25.png" width="300">

### 概述

本模块用于 MegaPi Pro，将 MegaPi Pro 的针脚转换为 Makeblock 的 RJ25 接口，可以用来连接 Makeblock 的电子模块。

### 技术规格

* 工作电压：5V
* 模块尺寸：66mmx42mm（长x宽）

### 功能特性

* 引出8个Port口
* 一个电源指示灯
* 使用 6P6C RJ25 接口方便接线
* 有 Arduino 库方便编程
* 支持图形化编程软件 mBlock3 和慧编程
* 支持树莓派Python编程

**注：** 6/11/12接口不兼容红外接收模块（Me Infrared Reciver Decode）。


### 连接方式

RJ25接口色标介绍

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">颜色</th>
    <th class="tg-7g6k">功能</th>
    <th class="tg-7g6k">使用此接口的模块</th>
  </tr>
  <tr>
    <td width="25%;" class="tg-3xi5">黄、蓝、黑、白（6,7,8,9,10,11,12号接口）</td>
    <td width="25%;" class="tg-c6of">单数字接口<br>双数字接口<br>模拟信号接口<br>I²C接口</td>
    <td width="50%;" class="tg-c6of">超声波模块、彩色LED灯模块、数码管模块、人体红外传感器模块、巡线模块、 光线传感器、电位器、摇杆、四按键模块、声音传感器、陀螺仪、电子罗盘模块、RJ25适配器、大功率电机驱动板、温湿度传感器、温度传感器、限位开关、LED点阵屏
</td>
  </tr>
  <tr>
    <td class="tg-3xi5">（灰色（5号接口）</td>
    <td class="tg-c6of">硬件串口</td>
    <td class="tg-c6of">蓝牙双模模块、WiFi模块</td>
  </tr>
</table>

<img src="images/megapi-pro-shield-for-rj25-2.png" width="300">