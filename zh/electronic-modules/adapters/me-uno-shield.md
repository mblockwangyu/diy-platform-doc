# UNO转接板

<img src="images/me-uno-shield_微信截图_20160128161320.png" alt="微信截图_20160128161320" width="342" style="padding:5px 5px 12px 0px;">

### 描述

这个模块是 Arduino UNO 的转接板。它可以将 Arduino 引脚转换为 Makeblock
RJ25端口。你可以用 Makeblock 电子模块连接 Arduino UNO。这个转接板提供了一个稳定的功率，所以它可以驱动许多伺服和电机。

### 特征

- 2.54mm针孔连接杜邦线
- 易于连接水晶RJ25接口
- 支持Arduino库编程

### 规格

- 额定功率: 6V – 12V
- 额定电流: 1.5A

### 原理图

<img src="images/me-uno-shield_shield-uno.png" alt="shield uno" width="1533" style="padding:5px 5px 12px 0px;">
