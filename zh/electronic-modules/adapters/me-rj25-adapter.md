# RJ25转接板


<img src="images/me-rj25-adapter.png" width="300" style="padding:5px 5px 12px 0px;">

### 概述

RJ25适配器将标准的RJ25接口转换为六个引脚（分别为 VCC、GND、S1、S2、SDA、SCL），方便从 MakeBlock 接口引出来以兼容其他厂商的电子模块，例如温度传感器，舵机模块等。本模块需要连接到主板上带有黄或蓝或黑色标识接口。

本模块将主控板的IO口引出六个，分别为 SDA、SCL、GND、VCC、S1、S2。S1、S2 口可以用来作数字/模拟的输入输出，SDA 为 I<sup>2</sup>C 时钟线,SCL为I2C数据线,可以连接支持 I<sup>2</sup>C 总线的传感器。例如：将支持 I<sup>2</sup>C 协议的温度传感器串联连接在 I<sup>2</sup>C 总线上，可以进行组网测温。

### 技术规格

- 工作电压：5V DC  
- 最大电流：3A  
- 模块尺寸：51 x 24 x 18 mm (长x宽x高)

### 功能特性

- 红色LED为电源指示灯 
- 可连接2个9g小舵机/2个灯带/2个限位开关
- 含有 I<sup>2</sup>C 接口和两个数字/模拟接口  
- 可以连接其他厂商的电子模块  
- 模块的白色区域是与金属梁接触的参考区域  
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程
- 支持 mBlock 和慧编程图形化编程，适合全年龄用户  
- 使用 RJ25 接口连线方便；  
- 模块化安装，兼容乐高系列；

### 引脚定义

RJ25 适配器模块有六个针脚的接头,每个针脚的功能如下表：

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:center;padding:5px 5px 12px 0px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

</td>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">SCL </td>
<td style="border: 1px solid black;">I<sup>2</sup>C 数据总线</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">SDA</td>
<td style="border: 1px solid black;">I<sup>2</sup>C 时钟总线</td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">GND</td>
<td style="border: 1px solid black;">接地</td>
</tr>

<tr>
<td style="border: 1px solid black;">4  </td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">接电源</td>
</tr>

<tr>
<td style="border: 1px solid black;">5 </td>
<td style="border: 1px solid black;">S1</td>
<td style="border: 1px solid black;">数字、模拟口</td>
</tr>

<tr>
<td style="border: 1px solid black;">6 </td>
<td style="border: 1px solid black;">S2 </td>
<td style="border: 1px solid black;">数字、模拟口</td>
</tr>
</table>

### 编程指南

本模块单独不可编程使用，当请查看接口连接的模块的相关编程指南。

### 接线方式

● **电子接线**

<img src="../../../en/electronic-modules/adapters/images/me-rj25-adapter_微信截图_20160128163016.png" alt="微信截图_20160128163016" width="469" style="padding:5px 5px 12px 0px;">

● **结构连接**

<img src="images/me-rj25-adapter-1.png" width="300" style="padding:5px 5px 12px 0px;">

