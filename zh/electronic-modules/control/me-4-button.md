# 按键模块

<img src="../../../en/electronic-modules/control/images/me-4-button_微信截图_20160128173416.png" alt="微信截图_20160128173416" width="234" style="padding:5px 5px 12px 0px;">

### 概述

四按键模块包含4个瞬时按压按钮，按压按钮在家用电器方面的电视机、电脑中的录音笔、医疗器材中的呼叫系统等领域都有涉及应用，具有结构简单，反馈良好等特点。该模块可应用在控制小车的移动方向与视频互动游戏等方面。本模块接口是黑色色标，是模拟量信号，需要连接到主板上带有黑色标识接口。

### 技术规格

- 工作电压： 5V DC  
- 按键数量： 4  
- 控制方式： 单向模拟口控制  
- 模块尺寸： 51.2 x 24 x 18 mm (长x宽x高)

### 功能特性

- 模块的白色区域是与金属梁接触的参考区域；  
- 四按键模块包含状态提示灯与电源提示灯；  
- 具有反接保护，电源反接不会损坏IC；  
- 支持 Arduino IDE 编程, 并且提供运行库来简化编程；  
- 支持 mBlock 图形化编程，适合全年龄用户；  
- 使用RJ25接口连线方便；  
- 模块化安装，兼容乐高系列；  
- 配有OUT、VCC、GND接头支持绝大多数 Arduino 系列主控板。

### 引脚定义

四按键模块有三个针脚的接头,每个针脚的功能如下表：

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;">
<tr>
<th style="border: 1px solid black;">序号</th>
<th style="border: 1px solid black;">引脚</th>
<th style="border: 1px solid black;">功能</th>
</tr>

</td>
<td style="border: 1px solid black;">1</td>
<td style="border: 1px solid black;">GND </td>
<td style="border: 1px solid black;">地线</td>
</tr>

<tr>
<td style="border: 1px solid black;">2</td>
<td style="border: 1px solid black;">VCC</td>
<td style="border: 1px solid black;">电源线 </td>
</tr>

<tr>
<td style="border: 1px solid black;">3</td>
<td style="border: 1px solid black;">OUT</td>
<td style="border: 1px solid black;">模拟量输出</td>
</tr>
</table>

<br>

### 接线方式

● **RJ25连接**  

由于四按键模块接口是黑色色标，当使用RJ25接口时，需要连接到主控板上带有黑色色标的接口。

以 Makeblock Orion 为例，可以连接到6，7，8号接口，如图：
  
<img src="../../../en/electronic-modules/control/images/me-4-button_微信截图_20160129101531.png" alt="微信截图_20160129101531" width="275" style="padding:5px 5px 12px 0px;">

● **杜邦线连接**  

当使用杜邦线连接到 Arduino Uno 主板时，模块OUT引脚需要连接到
ANALOG（模拟）口，如下图所示：  

<img src="../../../en/electronic-modules/control/images/me-4-button_微信截图_20160129101626.png" alt="微信截图_20160129101626" width="267" style="padding:5px 5px 12px 0px;">

### 编程指南

● **Arduino编程**  

如果使用Arduino编程，需要调用库`Makeblock-Library-master` 来控制四按键模块。

本程序通过Arduino编程读取被按下的键值，并输出到串口显示。  

<img src="../../../en/electronic-modules/control/images/me-4-button_微信截图_20160129101733.png" alt="微信截图_20160129101733" width="492" style="padding:5px 5px 12px 0px;">

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">函数</th>
<th style="border: 1px solid black;">功能</th>
</tr>

<tr>
<td style="border: 1px solid black;">Me4Button(uint8_t port)</td>
<td style="border: 1px solid black;">选定接口</td>
</tr>

<tr>
<td style="border: 1px solid black;">uint8_t pressed()</td>
<td style="border: 1px solid black;">输出被按下的按键号</td>
</tr>
</table>

<br>

<img src="../../../en/electronic-modules/control/images/me-4-button_微信截图_20160129101851.png" alt="微信截图_20160129101851" width="520" style="padding:5px 5px 12px 0px;">

### 原理解析

四按键模块包含4个四脚按键，依靠金属弹片来保护受力情况；在四脚按键开关中，当某个按钮被按压时，电路导通；当撤销这种压力的时候，电路断开。这个施压的力，就是用我们的手去开按钮、关按钮的动作。四个按键开关共用一个模拟输出，当不同按键被按下时所输出的模拟值不同，由此可以判断是哪个按钮被按下。

### 原理图

<img src="../../../en/electronic-modules/control/images/me-4-button_Me-4-Button-V1.0_schematic.png" alt="Me-4 Button V1.0_schematic" width="2259" style="padding:5px 5px 12px 0px;">
