# NovaPi 主控板

<img src="../../../zh/electronic-modules/main-control-boards/images/novapi-1.png" width="300">

### 概述

NovaPi主控是新一代赛事平台主控，具有一个高性能M7处理器ATSAMS70N20A-AN以及一个STM32F030CCT6协处理器，具有5个传感器接口，6个智能电机接口。传感器接口使用串口通讯，对外输出5V电压。智能电机接口使用串口通讯，对外提供12V电压。NovaPi主控板载6轴传感器，可测量X,Y,Z三个方向上的加速度和X,Y,Z三个方向上的旋转角速度。NovaPi可搭配动力扩展板使用，提供更加丰富的接口与资源。

### 规格

* 模块尺寸：85 mm × 56 mm × 13  mm
* 工作电压： 6V ~ 13V（若使用电机时，输入最低电压必须满足电机工作电压要求）
* 工作电流：60 mA
* 通讯端口及协议：串口/mbuild协议

### 功能特性

* 支持makeblock金属件、lego销连接
* mbuild体系，适配光环板、新星板等mbuild体系主控
* 5路mBuild接口
* 6路智能电机接口
* 板载6轴运动传感器
* 支持动力扩展板
* 体积小，安装位置灵活，方便嵌入各种项目中
* 主控MCU为高性能M7处理器

### 产品外观图

<img src="../../../zh/electronic-modules/main-control-boards/images/novapi-2.png" width="300">

<img src="../../../zh/electronic-modules/main-control-boards/images/novapi-3.png" width="500">

<img src="../../../zh/electronic-modules/main-control-boards/images/novapi-4.png" width="300">

* MicroUSB程序下载接口 1个
* 电源开关 1个
* 12V电源输入口 1个
* 传感器接口 5个
* 智能电机接口 6个 
* 12V电源输出口 2个
* 状态指示灯 3个
* 动力扩展板接口 1个
* 6轴运动传感器 1个
* M7处理器 ATSAMS70N20A-AN 1个
* STM32F030CCT6 处理器 1个

### 安装使用建议

主控安装时一定要注意防止主板短路，装配时主板螺丝孔下面可以加塑胶垫片抬高以保证主板与支架有足够的安全间距，防止主板压坏，短路。主控板链接电机，传感器等外设时，注意整理好走线。

NovaPi主板孔的间距不一定能匹配结构件安装孔的间距，打螺丝时注意，不要因为间距不一致强行打螺丝，把主板打变形，这样会损坏主板。

不要在通电情况下安装主控板，通电前请先检查一遍，不要有露铜线，锡渣，螺丝等任何导电的东西留在主板上。

### 内置功能

模块内置6轴运动传感器，可以用来检测产品当前的姿态，如当前速度，加速度，选择角速度，以及倾斜角等姿态。

### 软件

NovaPi 主控板的相关积木块如下表所述：

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-ycr8{background-color:#ffffff;text-align:left;vertical-align:center}
.tg .tg-7g6k{font-weight:bold;background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-3xi5{background-color:#ffffff;border-color:inherit;text-align:center;vertical-align:center}
.tg .tg-c6of{background-color:#ffffff;border-color:inherit;text-align:left;vertical-align:center}
.tg .tg-i81m{background-color:#ffffff;text-align:center;vertical-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-7g6k">名称</th>
    <th class="tg-7g6k">说明</th>
    <th class="tg-7g6k">输入/输出</th>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../zh/electronic-modules/main-control-boards/images/novapi-5.png" width="60"></td>
    <td class="tg-c6of">功能：获取系统计时器时间<br>单位：毫秒<br>返回类型：float</td>
    <td class="tg-i81m">输入</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../zh/electronic-modules/main-control-boards/images/novapi-6.png" width="60"></td>
    <td class="tg-c6of">功能：复位系统计时器，归零<br>单位：无<br>返回类型：无</td>
    <td class="tg-i81m">输出</td>
  </tr>
  <tr>
    <td class="tg-3xi5"><img src="../../../zh/electronic-modules/main-control-boards/images/novapi-7.png" width="200"></td>
    <td class="tg-c6of">功能：获取板载陀螺仪"X" / "Y" / "Z"轴加速度值<br>单位：m/s²<br>返回类型：float</td>
    <td class="tg-i81m">输入</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../zh/electronic-modules/main-control-boards/images/novapi-8.png" width="200"></td>
    <td class="tg-ycr8">功能：获取板载陀螺仪"X" / "Y" / "Z"轴角速度值<br>单位：(度/秒)<br> 返回类型：float</td>
    <td class="tg-i81m">输入</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../zh/electronic-modules/main-control-boards/images/novapi-9.png" width="200"></td>
    <td class="tg-ycr8">功能：获取板载陀螺仪"X" / "Y" / "Z"轴角度值<br>单位：度<br>返回类型：float</td>
    <td class="tg-i81m">输入</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../zh/electronic-modules/main-control-boards/images/novapi-10.png" width="200"></td>
    <td class="tg-ycr8">功能：设置震动阈值<br>单位：无<br>返回类型：无</td>
    <td class="tg-i81m">输出</td>
  </tr>
  <tr>
    <td class="tg-i81m"><img src="../../../zh/electronic-modules/main-control-boards/images/novapi-11.png" width="150"></td>
    <td class="tg-ycr8">功能：检测主板是否发送震动<br> 单位：无<br> 返回类型：BOOL， True-震动， False-未震动</td>
    <td class="tg-i81m">输入</td>
  </tr>
</table>


