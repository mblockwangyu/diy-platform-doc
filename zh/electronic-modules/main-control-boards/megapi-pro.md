# MegaPi Pro 主控板

<img src="../../../zh/electronic-modules/main-control-boards/images/megapi-pro-1.png" width="300">

### 概 述

MegaPi Pro 是一个基于 ATmega2560 的微主控板，完全兼容 Arduino 编程。MegaPi Pro 拥有强大的驱动能力，输出功率最高可达 120W，有四个 PORT 插口，一个四路电机扩展接口，一个 RJ25 转接板接口，一个智能舵机接口。强大的扩展能力使其可以轻松应对教育、比赛、娱乐等各方面的要求。MegaPi Pro 可以方便地安装在树莓派上，并通过串口连接，，配合 Python 编程能够快速实现树莓派控制电机、传感器等电子模块。

#### 驱动能力
可同时驱动 4 个 25 直流电机（或 4 个 37 直流电机或 4 个电磁阀）和 4 个 42 步进电机（或 4 个 25 编码电机或 4 个无刷电机或 8 个直流电机或 8 个电磁阀）和 6 个智能舵机和 8 个电子模块（或 14 个普通舵机或 14 个大功率 36 编码电机或 14 个大功率直流电机），曾经作为 MakeX 的主要主控板，经过各种极端的考验和反复验证，最终证明它是一款性能强大并且技术成熟的主控板。

#### 机器人赛事应用
曾经作为 MakeX 中 Challenge 赛项的主控板，足迹遍布中国 30 多个城市，在超过 20 多个国家和地区进行机器人比赛使用。通过连接大功率驱动板，可以驱动 36 编码电机控制麦轮底盘进行赛车底盘的矢量操控，通过直流电机的控制和无刷电机的控制，可以制作出强大的发射球机构，为赛车提供强大的运动性能，目前也可以使用此主控参加 MakeX 赛事中的 Spark 比赛。配合 Makeblock 平台丰富的电子件和结构件，可以满足各种科技竞赛及创新大赛。

#### 赋能科学教育
MegaPi Pro 作为一款强大的主控，支持多个编程环境，包括基于 Scratch 的图形化编程软件 mBlock 和慧编程，还有Arduino c，以及 Python3 环境的编程，真正实现从新手入门到高级进阶，覆盖全年龄段。RJ25 平台电子件即插即用，免焊接，慧编程社区有丰富的学生老及师的创客项目分享。开发课程及项目简单便捷，可以实现学生及老师进行STEAM教育活动、编程学习、人工智能和物联网教学、创意制作等需求。

#### 和树莓派协同使用
树莓派是一款只有信用卡大小的微型电脑，其系统基于 Linux。MegaPi Pro 跟树莓派有两种连接方式，USB 线直连或者利用引脚直插。MegaPi Pro 和树莓派结合使用 Python 语言编程更能发挥 MegaPi Pro 的强大性能开发更复杂更具技术含量的项目，例如带摄像头自动规划路径的智能小车、声控机械臂、监控家中环境如温湿度监控及侵入报警、智能鱼缸系统实现远程喂鱼等。

### 技术规格

* 主控芯片：ATmega2560-16AU
* 输入电压：DC6V-12V
* 工作电压：DC5V
* 串口：3个
* I²C口：1个
* SPI口：1个
* 模拟输入口：16个
* DC Current per I/O Pin：20mA
* Flash：256KB
* SRAM：8KB
* EEPROM：4KB
* Clock speed：16MHz
* 模块尺寸：87mmx63mm（长x宽）

### 功能特性

- 4个电机驱动接口，可以方便插取编码电机驱动模块、步进电机驱动模块、电调转接板模块
- 可同时控制 4 个 25 直流电机（或 4 个 37 直流电机或 4 个电磁阀）和 4 个 42 步进电机（或 4 个 25 编码电机或 4 个无刷电机或 8 个直流电机或 8 个电磁阀）和 6 个智能舵机和 8 个电子模块（或 14 个普通舵机或 14 个大功率 36 编码电机或 14 个大功率直流电机）
- 1 个无线通讯模块接口，可以方便插取蓝牙模块、2.4G 模块等实现无线控制
- 1 个智能舵机接口，可扩展4个智能舵机
- 1 个 RJ25 转接板接口，可扩展 8 个 RJ25 接口
- 3 个 M4 安装孔，孔位置和树莓派一致可通过排针直插铜螺柱固定。
- 过流保护
- 完全兼容 Arduino
- 使用 RJ25 接口体系
- 支持 Arduino 编程并配备专业的 Makeblock 库函数，简化编程难度
- 兼容树莓派 Python3 编程
- 支持 mBlock 及慧编程图形化编程，零基础编程，适合全年龄段用户

### 接口功能

* 红色插针：固件烧录口
* 红色插座：电源和电机输出
* 黄色插针、黑色插针、黑色插座：I/O口
* 白色插针：智能管理系统接口
* 绿色接头：电机接口
* 黄色接头：四路电机驱动电源口
* 白色接头：智能舵机接口

<img src="../../../zh/electronic-modules/main-control-boards/images/megapi-pro-2.png" width="500">

### 编程指南

● **Arduino 编程**

如果使用 Arduino 编程，需要调用库 [`Makeblock-Library-master`](https://github.com/Makeblock-official/Makeblock-Libraries)。

**编程示例**

```
#include <Arduino.h>
#include <Wire.h>
#include <SoftwareSerial.h>
#include "analog_servo.h"
#include <MeMegaPiPro.h>
void setup(){
    servo_6_1.attach(port_pro(6,1)+A0); 
}
void loop(){
    servo_6_1.write(30); 
    _delay(1);
    servo_6_1.write(120); 
    _delay(1);
    _loop();
}
```

● **mBlock 编程** 

[mBlock 快速入门](https://pan.baidu.com/s/1hsEDyao)

mBlock 为 MegaPi Pro 提供了简单易用的积木块。

<img src="images/megapi-pro/megapi-pro-1.gif" width="500">

**编程示例**

<img src="images/megapi-pro/megapi-pro-2.png" width="300">

● **慧编程编程** 

可访问[慧编程网页](https://www.mblock.cc/zh-cn/)了解和下载慧编程。

慧编程为 MegaPi Pro 提供了简单易用的积木块。

<img src="images/megapi-pro/megapi-pro-3.gif" width="500">

**编程示例**

<img src="images/megapi-pro/megapi-pro-4.png" width="300">

● **Python3 编程** 

和树莓派配合使用。

**示例程序**

```
from time import sleep
from makeblock import MegaPiPro 
megapipro = MegaPiPro.create()
servo = megapipro.Servo(MegaPiPro.PORT6,MegaPiPro.SLOT1)
while True:
    servo.set_angle(30)
    sleep(1)
    servo.set_angle(120)
    sleep(1)
```

### 丰富的扩展玩法及配件

配件资料详细信息，可访问 http://docs.makeblock.com/diy-platform/zh/electronic-modules.html 查看。

### 与树莓派连接使用

通过与树莓派连接，利用树莓派强大的处理性能和 Makeblock 电子平台丰富的配件可以进行数据采集、数据分析、人工智能、机器学习、图像识别等方面的应用。

#### 方口连接
使用方口 USB 连接树莓派和 MegaPi Pro 时，MegaPi Pro 驱动初始化命令需要使用下列命令：
```
from makeblock import MegaPiPro
board = MegaPiPro.create('/dev/ttyUSB0')
```
可以使用下列命令查询 ttyUSB0 串口是否启用：
```
ls /devtty*
```
<img src="images/megapi-pro/megapi-pro-5.png" width="500">

查询结果如上图说明 ttyUSB0 串口正常工作。

#### MegaPi Pro排针直插

MegaPi Pro 的 USB 通信端口旁边预留了 10 针的树莓派连接端口，可以通过 MegaPi Pro 套装附件中的 10 针排线连接树莓派，如下图所示。

<img src="images/megapi-pro/megapi-pro-6.png" width="300">

使用此方法连接可以大大减少线材使用。MegaPi Pro 直插电源即可给树莓派供电，整机只需要一个电源即可驱动大功率电机等设备。

**树莓派和 MegaPi Pro 连接引脚定义**

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-lboi{border-color:inherit;text-align:center;vertical-align:middle}
.tg .tg-0lax{text-align:center;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-lboi">功能名<br>树莓派</th>
    <th class="tg-0lax" colspan="2">物理引脚<br>BOARD编码</th>
    <th class="tg-lboi">功能名<br>MegaPi Pro</th>
  </tr>
  <tr>
    <td class="tg-lboi">3.3V</td>
    <td class="tg-0lax">1</td>
    <td class="tg-lboi">2</td>
    <td class="tg-lboi">5V</td>
  </tr>
  <tr>
    <td class="tg-lboi">SDA.1</td>
    <td class="tg-0lax">3</td>
    <td class="tg-lboi">4</td>
    <td class="tg-lboi">5V</td>
  </tr>
  <tr>
    <td class="tg-0lax">SCL.1</td>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">6</td>
    <td class="tg-0lax">GND</td>
  </tr>
  <tr>
    <td class="tg-0lax">GPIO.7</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">8</td>
    <td class="tg-0lax">TXD</td>
  </tr>
  <tr>
    <td class="tg-0lax">GND</td>
    <td class="tg-0lax">9</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">RXD</td>
  </tr>
</table>

**注：** 使用此方法连接MegaPi Pro需要关闭树莓派蓝牙，并且需要恢复硬件串口。

完成后需要使用下列语句初始化驱动：
```
from makeblock import MegaPiPro
board = MegaPiPro.create('/dev/ttyAMA0')
```

### 丰富的电机驱动

<img src="images/megapi-pro/megapi-pro-7.png" width="300">

通过各种电机驱动，可以进行普通编码电机、直流电机、大功率编码电机、步进电机以及无刷电机的控制操作。

<img src="images/megapi-pro/megapi-pro-8.png" width="300">

结合 MagePi Pro、电机驱动、电机及 Makeblock 机械和电子平台的各种零配件，可完成如智能小车、智能机器人、绘图工具、3D打印机等项目。

<img src="images/megapi-pro/megapi-pro-9.png" width="300"><br>

<img src="images/megapi-pro/megapi-pro-10.png" width="400">

### 舵机

支持 9g 小舵机、智能舵机等舵机，可以实现固定位置角度的精准控制。

其中智能舵机 MS-12A 是 Makeblock 自主研发的新一代智能舵机，重点解决了传统舵机存在的控制复杂，角度范围小，不可连续旋转，配件单一等问题。MegaPi Pro 配合智能舵机可以快速组装成多关节机器人：如人形机器人、多足蜘蛛机器人、机械臂等，也可以利用其可连续旋转的特性，组装成可控旋转平台，车轮驱动等机构。

<img src="images/megapi-pro/megapi-pro-11.png" width="300">

### 多种传感器及配件

<img src="images/megapi-pro/megapi-pro-12.png" width="800">

通过 RJ25 转接板可以方便连接控制 Makeblock RJ25 平台各种电子件，可以通过丰富的电子件制作出自己想要的物联网应用及智能机器人。

<img src="images/megapi-pro/megapi-pro-13.png" width="200">
