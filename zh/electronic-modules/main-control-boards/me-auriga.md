# Me Auriga主控板

![](images/me-auriga-1.jpg)

Me Auriga主板为Orion 的升级版本，板载多个传感器，包括温度，陀螺仪，声强检测，Buzzer驱动；支持按键形式的开关机，支持无线蓝牙控制及蓝牙升级固件功能，原本两路红色PORT 口升级为四路，同时保原红色PORT口功能；独立了PORT5口，单独只是具备串口通讯功能，该口不可以更新程序只用于通讯，且与USB串口同时使用不冲突；PORT6到PORT10可以同时兼容双数字，模拟，I²C总线，单总线，模拟串口功能；同时支持板载的编码电机接口，智能舵机接口，LED Ring 灯板（板载开关机按钮）接口；PCB尺寸也有增大。

### 技术参数
输出电压：5V直流&6-12V直流

额定电压：6~12V直流

单片机：ATmega2560

尺寸规格（长*宽\*高）：100×65×20mm

 

### 特点
+ 支持直流电机，步进电机，伺服舵机，智能舵机，编码电机等。
+ 板载可以直接驱动两路编码电机,且支持过流保护4A（瞬间）。
+ 支持蓝牙控制主板及蓝牙无线升级固件。
+ 一键开关机电路设计。
+ PORT5-PORT10支持5V 直流输出持续4A，最大3A。
+ PORT1-PORT4支持持续输出5A，最大电流5A。
+ 板载陀螺仪、声音传感器、无源蜂鸣器、温度传感器。
+ PORT5到PORT10支持短接保护及过流保护，保护电流为3A。
+ PORT1 到PORT4 支持短接保护及过流保护，保护电流为5A。
+ USB口支持人体防静电保护。
+ 兼容ARDUINO IDE。
+ 兼容RJ25网络接口。
+ 提供兼容Arduino 的专用库，接口人性化，易于理解功能强大。
+ 支持mBlock。
 

### Me Auriga 电子器件分布图

![](images/电子器件分布图.png)
 

### 接口简介
<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:60px;">

<tr>
<td>颜色</td>
<td>功能</td>
<tr>

<tr>
<td>
<img src="images/红.png" width="50px" style="padding:0px 0px 0px 0px;">
</td>
<td>
输出电压为6-12V直流
</td>
<tr>

<tr>
<td>
<img src="images/黄.png" width="50px" style="padding:0px 0px 0px 0px;">
</td>
</td>
<td>
单数字口
</td>
<tr>

<tr>
<td>
<img src="images/蓝.png" width="50px" style="padding:0px 0px 0px 0px;">
</td>
</td>
<td>
双数字口
</td>
<tr>

<tr>
<td>
<img src="images/灰.png" width="50px" style="padding:0px 0px 0px 0px;">
</td>
</td>
<td>
硬串口
</td>
<tr>

<tr>
<td>
<img src="images/黑.png" width="50px" style="padding:0px 0px 0px 0px;">
</td>
</td>
<td>
单模拟&双模拟口
</td>
<tr>

<tr>
<td>
<img src="images/白.png" width="50px" style="padding:0px 0px 0px 0px;">
</td>
</td>
<td>
I²C接口
</td>
<tr>

</table>

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">

<tr>
<td>接口编号</td>
<td>接口色标</td>
<td>兼容模块类型</td>
<td>兼容模块</td>
<tr>

<tr>
<td>
1&2&3&4
</td>
<td>
<img src="images/1234-2.png" width="200px" style="padding:0px 0px 0px 0px;">
</td>
</td>
<td>
电机驱动模块（6-12V直流）
</td>
<td>
双电机驱动模块<br>
步进电机驱动模块
</td>
<tr>

<tr>
<td>
5
</td>
<td>
<img src="images/5-2.png" width="50px" style="padding:0px 0px 0px 0px;">
</td>
</td>
<td>
硬串口
</td>
<td>
蓝牙模块<br>
树莓派转接板
</td>
<tr>

<tr>
<td>
6&7&8&9&10
</td>
<td>
<img src="images/678910-2-400x74.png" width="250px" style="padding:0px 0px 0px 0px;">
</td>
</td>
<td>
单数字口<br>
双数字口<br>
I²C接口<br>
单模拟&双模拟口
</td>
<td>
超声波传感器<br>
RGB模块<br>
限位开关模块<br>
数码管<br>
人体红外传感器<br>
快门线模块<br>
巡线传感器<br>
陀螺仪模块<br>
电位器模块<br>
声音传感器<br>
遥感遥控器模块<br>
温湿度传感器<br>
火焰传感器
</td>
<tr>

</table>
 
### 引脚图

![](images/Me-Auriga-引脚图－中文-01.jpg)

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;padding:5px 5px 12px 0px;margin-bottom:20px;">

<th style="width: 300px;">元件名称<th style="width: 300px;" >相应编码
<tr>
<tr >
<td>声音传感器<td>A1</td>
<tr>
<tr>
<td>蜂鸣器<td>D45</td>
<tr>
<tr>
<td rowspan="6">编码电机1</td>

<td>D11(PWM)</td>
<tr>
<tr>
<td>D48<tr>
<tr>
<td>D49</tr>
<tr>

<tr>
<td rowspan="6">编码电机2</td>
<td>D10(PWM)</td>
<tr>
<tr>
<td>D46<tr>
<tr>
<td>D47</tr>

</table>

 

### 板载传感器简介
声音传感器


![](images/声音传感器-2.png)

Me Auriga上拥有一个板载的声音传感器用来检测周围环境的声音强度。该声音传感器基于麦克风，主要部件为LM2904低功率放大器。你可以用它来做一些交互性项目，例如声控开关，跟随舞蹈变动的机器人。

陀螺仪模块

![](images/陀螺仪.png)

Me Auriga拥有一个板载的陀螺仪传感器。它是一个理想的机器人运动检测和姿态检测模块，包含3轴加速度计、3轴角速度传感器与运动处理器，可以应用在自平衡机器人或者其他移动设备上。

 

温度传感器

![](images/温度传感器-2.png)

Me Auriga拥有一个板载的热敏电阻，可以用来检测周围环境的温度变化。

 

### 编程教学
1.Arduino IDE 编程

Arduino是一个开源电子原型平台，它拥有灵活易用的硬件和软件。 软件部分包含Arduino开发环境(IDE)和核心库。 IDE使用Java编写的,基于Processing开发环境。

Me Auriga与Arduino Mega 2560兼容，因此您可以使用Arduino IDE来开发程序。 如果您使用了Makeblock的电子模块，我们建议您安装Makeblock程序库。

了解更多细节请点击:http://learn.makeblock.com/cn/learning-arduino-programming-ranger/

 2.mBlock编程 
  
Me Auriga支持mBlock编程， mBlock 是一款最适合初学者的编程软件，继承了Scratch简单、易用等特点，并融合了Arduino强大的可拓展性。支持在线控制与程序上传，只需轻轻拖拽mBlock语句，就像堆积木一样简单。同时能够帮助初学者顺利过渡到真正的编程语言。

了解更多细节，请点击: http://learn.makeblock.com/cn/getting-started-programming-with-mblock-2/

### 注意事项
+ Me Auriga适用电压 6-12V 直流
+ 谨防短路
+ 防止水、酸/碱度液体或固体杂物等的污染
+ 不要随意丢弃
 

### 原理图

下载电路原理图，请访问：

[原理图下载地址](http://download.makeblock.com/Me%20Auriga%20电路图V1.1%20-%2020160221.pdf)

### 外壳尺寸图

[上壳下载地址](http://download.makeblock.com/ranger/Me%20Auriga%20Case%20-%201.PDF)

[下壳下载地址](http://download.makeblock.com/ranger/Me%20Auriga%20Case%20-2%20.PDF)