# 摇杆

摇杆可以用来控制物体运动的方向，mBuild 的摇杆可以输出摇杆在X轴和Y轴上的坐标，范围及其上下左右的判定区域如下：

<img src="images/joystick.png" style="padding:3px 0px 12px 3px;width:200px;">

<br>

<img src="images/joystick-0.png" style="padding:13px 0px 12px 3px;width:400px;">

### 生活实例

- 游戏手柄利用摇杆控制人物的移动方向或视野方向<br>
<img src="images/joystick-1.jpg" style="padding:8px 0px 12px 3px;width:400px;">
- 无人机遥控器上使用摇杆控制无人机的转向及转速<br>
<img src="images/joystick-2.jpg" style="padding:8px 0px 12px 3px;width:400px;">

### 参数

- 尺寸：24×36mm
- 使用寿命：500,000次
- x轴读值范围：-100~100
- y轴读值范围：-100~100
- 工作电流：15mA
- 摇杆复归精度：±0.2mm