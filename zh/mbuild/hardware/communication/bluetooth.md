# 蓝牙

蓝牙模块能够使你的作品与 Makeblock 蓝牙适配器 或支持蓝牙4.0的设备建立无线连接，使你能够无线地控制你的作品。

<img src="images/bluetooth.png" style="padding:3px 0px 12px 3px;width:300px;">

蓝牙的连接设置参看：[通过蓝牙连接设备](http://www.mblock.cc/doc/zh/part-one-basics/connect-devices.html)

### 蓝牙的灯效及含义

- 闪烁：蓝牙未被连接
- 常亮：蓝牙已连接，工作正常
- 灭：断电或故障

### 参数

- 尺寸：24×24mm
- 推荐使用距离：10m以内
- 蓝牙版本：BT4.0
- 频带范围：2402~2480MHz
- 天线增益：1.5dBi
- 能耗等级：≤4dBm
- 工作电流：15mA