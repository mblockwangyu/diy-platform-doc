# 快速转接件

快速转接件可以用于将 mBuild 模块快速固定到 M4 孔位上，或是利用快速转接件实现 mBuild 模块的堆叠。

<img src="images/connect-1.png" style="padding:3px 0px 12px 3px;width:300px;">

### 快速固定示意

<img src="images/connect-2.png" style="padding:3px 0px 12px 3px;width:300px;">

### 堆叠示意

<img src="images/connect-3.png" style="padding:3px 0px 12px 3px;width:300px;">
