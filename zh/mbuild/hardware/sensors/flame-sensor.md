# 火焰传感器

火焰传感器通过检测红外光来检测火焰及其大小。

<img src="images/flame-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

由于是利用检测红外光进行判断，因此该模块在阳光直射下使用时，会出现严重干扰，无法正常工作，这是因为太阳光的强度很大，其中也包含大量红外波段的光线，这会被元器件误判为火焰。

### 生活实例

- 在一些严格禁火的场所，火焰检测器是必不可少的好帮手<br>
<img src="images/flame-sensor-1.png" style="padding:12px 0px 12px 3px;width:250px;">

### 参数

- 尺寸：24×20mm
- 火焰大小读值范围：0~100
- 一致性误差：【研发补充】
- 工作电流：20mA