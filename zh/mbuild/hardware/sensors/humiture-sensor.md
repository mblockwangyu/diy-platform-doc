# 温湿度传感器

温湿度传感器可以检测的空气的湿度和温度，适合用于环境检测。可以利用其制作诸如智能风扇，智能加湿器之类的案例作品。

<img src="images/humiture-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### 生活实例

- 智能家居设备通过检测温湿度来调节室内环境<br>
<img src="images/humiture-sensor-1.jpg" style="padding:10px 0px 12px 3px;width:300px;">

### 参数

- 尺寸：24×20mm
- 温度读值范围：-40~125℃
- 温度读值误差：±1℃
- 湿度读值范围：0~100%
- 湿度读值误差：±3%
- 工作电流：15mA