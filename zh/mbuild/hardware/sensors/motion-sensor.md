# 运动传感器

运动传感器包含一个 3轴陀螺仪 以及一个 3轴加速度计 ，具备能够检测物体的运动姿态，加速度，以及震动强度。

<img src="images/motion-sensor.png" style="padding:3px 0px 12px 3px;width:200px;">

### 坐标轴及角度方向定义

<table  cellpadding="10px;" cellspacing="15px;" style="text-align:left;;margin-bottom:20px;">
<tr>
<th style="border: 1px solid black;">坐标轴</th>
<th style="border: 1px solid black;">角度英文名</th>
<th style="border: 1px solid black;">角度中文名</th>
<th style="border: 1px solid black;">角度范围</th>
</tr>

</td>
<td style="border: 1px solid black;">X</td>
<td style="border: 1px solid black;">Pitch</td>
<td style="border: 1px solid black;">俯仰角</td>
<td style="border: 1px solid black;">-180~180°</td>
</tr>

<tr>
<td style="border: 1px solid black;">Y</td>
<td style="border: 1px solid black;">Roll</td>
<td style="border: 1px solid black;">翻滚角</td>
<td style="border: 1px solid black;">-90~90°</td>
</tr>

<tr>
<td style="border: 1px solid black;">Z</td>
<td style="border: 1px solid black;">Yaw</td>
<td style="border: 1px solid black;">偏航角</td>
<td style="border: 1px solid black;">-180~180°</td>
</tr>
</table>

### 原理介绍

如果你在互联网上直接搜索陀螺仪，你将很可能得到如下图片：

<img src="images/gyro.gif" style="padding:3px 0px 12px 3px;width:200px;">

这是一种基于角动量守恒的理论，用来感测与维持方向的机械装置。陀螺仪主要是由一个位于轴心且可旋转的转子构成。由于转子的角动量，陀螺仪一旦开始旋转，即有抗拒方向改变的趋向。这类陀螺仪被应用于早期的导航系统中。

现代电子产品中，考虑到体积的限制，并得益于微机电技术的蓬勃发展，主要使用是基于MEMS（Micro Electro Mechanical systems）陀螺仪。它的体积极小，可以直接被焊接在PCB板上，这也是我们得以将运动传感器大小控制在24×20mm的主要原因。

两者在使用的原理上会有较大的不同，但你只需要知道，他们都能较为真实地反映物体在空间中的姿态及运动状态即可。

### 生活实例

- Switch手柄中的陀螺仪可以被用来进行体感游戏<br>
<img src="images/motion-1.jpg" style="padding:10px 0px 12px 3px;width:400px;">
- iPhone中的水平仪也使用到了陀螺仪<br>
<img src="images/motion-2.png" style="padding:10px 0px 12px 3px;width:300px;">

### 参数

- 尺寸：24×20mm
- 度数精度：±1°
- 加速度检测范围：±8g
- 工作电流：18mA