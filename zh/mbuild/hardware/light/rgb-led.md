# RGB 灯

可编程RGB灯，可以在编程指令下放出各种颜色的光芒。

<img src="images/rgb-led.png" style="padding:3px 0px 12px 3px;width:200px;">

### 生活实例

- 手电筒上使用了单色LED灯<br>
<img src="images/rgb-led-1.jpg" style="padding:8px 0px 12px 3px;width:300px;">
- 游戏笔记本使用RGB灯营造出酷炫的效果<br>
<img src="images/rgb-led-2.jpg" style="padding:8px 0px 12px 3px;width:300px;">

### 参数

- 尺寸：24×20mm
- 工作电流：15~73mA