# 彩灯驱动

彩灯驱动能够驱动灯珠、灯带、灯环等多种灯类配件。

<img src="images/led-driver.png" style="padding:3px 0px 12px 3px;width:200px;">

---

### 灯带

<img src="images/led-strip.png" style="padding:3px 0px 12px 3px;width:600px;">

灯带可以被用来制作光剑、灯效文字或是用做氛围渲染。

<img src="images/led-strip-1.jpg" style="padding:3px 0px 12px 3px;width:200px;"><img src="images/led-strip-2.jpg" style="padding:3px 0px 12px 3px;width:200px;">

灯带的 IN 口需要和灯带驱动上的接口或上一级灯珠、灯带、灯环的 OUT 口连接。灯带不防水，在水中使用可能造成模块损坏并将失去售后保修。

**参数**

- 尺寸：8×112mm

---

### 12灯灯环

<img src="images/led-ring-1.png" style="padding:3px 0px 12px 3px;width:200px;">
<img src="images/led-ring-2.png" style="padding:3px 0px 12px 3px;width:200px;">

灯环的 IN 口需要和灯带驱动上的接口或上一级灯珠、灯带、灯环的 OUT 口连接。灯环不防水，在水中使用可能造成模块损坏并将失去售后保修。灯环上的第1颗到第12颗灯的位置与钟表一致，如下图：

<img src="images/led-ring-3.png" style="padding:3px 0px 12px 3px;width:300px;">

**参数**

- 尺寸：45mm直径

---


